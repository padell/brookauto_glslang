/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_GRAM_TAB_H_INCLUDED
# define YY_YY_GRAM_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    IDENT = 258,                   /* IDENT  */
    TAG_NAME = 259,                /* TAG_NAME  */
    LABEL_NAME = 260,              /* LABEL_NAME  */
    TYPEDEF_NAME = 261,            /* TYPEDEF_NAME  */
    STRING = 262,                  /* STRING  */
    LSTRING = 263,                 /* LSTRING  */
    CHAR_CONST = 264,              /* CHAR_CONST  */
    LCHAR_CONST = 265,             /* LCHAR_CONST  */
    INUM = 266,                    /* INUM  */
    RNUM = 267,                    /* RNUM  */
    PP_DIR = 268,                  /* PP_DIR  */
    PP_LINE = 269,                 /* PP_LINE  */
    INVALID = 270,                 /* INVALID  */
    CONST = 271,                   /* CONST  */
    VOLATILE = 272,                /* VOLATILE  */
    OUT = 273,                     /* OUT  */
    REDUCE = 274,                  /* REDUCE  */
    VOUT = 275,                    /* VOUT  */
    ITER = 276,                    /* ITER  */
    KERNEL = 277,                  /* KERNEL  */
    AUTO = 278,                    /* AUTO  */
    EXTRN = 279,                   /* EXTRN  */
    REGISTR = 280,                 /* REGISTR  */
    STATIC = 281,                  /* STATIC  */
    TYPEDEF = 282,                 /* TYPEDEF  */
    VOID = 283,                    /* VOID  */
    SHORT = 284,                   /* SHORT  */
    INT = 285,                     /* INT  */
    LONG = 286,                    /* LONG  */
    SGNED = 287,                   /* SGNED  */
    UNSGNED = 288,                 /* UNSGNED  */
    FLOAT = 289,                   /* FLOAT  */
    FLOAT2 = 290,                  /* FLOAT2  */
    FLOAT3 = 291,                  /* FLOAT3  */
    FLOAT4 = 292,                  /* FLOAT4  */
    FIXED = 293,                   /* FIXED  */
    FIXED2 = 294,                  /* FIXED2  */
    FIXED3 = 295,                  /* FIXED3  */
    FIXED4 = 296,                  /* FIXED4  */
    SHORTFIXED = 297,              /* SHORTFIXED  */
    SHORTFIXED2 = 298,             /* SHORTFIXED2  */
    SHORTFIXED3 = 299,             /* SHORTFIXED3  */
    SHORTFIXED4 = 300,             /* SHORTFIXED4  */
    DOUBLE = 301,                  /* DOUBLE  */
    DOUBLE2 = 302,                 /* DOUBLE2  */
    CHAR = 303,                    /* CHAR  */
    CHAR2 = 304,                   /* CHAR2  */
    CHAR3 = 305,                   /* CHAR3  */
    CHAR4 = 306,                   /* CHAR4  */
    UCHAR = 307,                   /* UCHAR  */
    UCHAR2 = 308,                  /* UCHAR2  */
    UCHAR3 = 309,                  /* UCHAR3  */
    UCHAR4 = 310,                  /* UCHAR4  */
    ENUM = 311,                    /* ENUM  */
    STRUCT = 312,                  /* STRUCT  */
    UNION = 313,                   /* UNION  */
    BREAK = 314,                   /* BREAK  */
    CASE = 315,                    /* CASE  */
    CONT = 316,                    /* CONT  */
    DEFLT = 317,                   /* DEFLT  */
    DO = 318,                      /* DO  */
    ELSE = 319,                    /* ELSE  */
    IF = 320,                      /* IF  */
    FOR = 321,                     /* FOR  */
    GOTO = 322,                    /* GOTO  */
    RETURN = 323,                  /* RETURN  */
    SWITCH = 324,                  /* SWITCH  */
    WHILE = 325,                   /* WHILE  */
    PLUS_EQ = 326,                 /* PLUS_EQ  */
    MINUS_EQ = 327,                /* MINUS_EQ  */
    STAR_EQ = 328,                 /* STAR_EQ  */
    DIV_EQ = 329,                  /* DIV_EQ  */
    MOD_EQ = 330,                  /* MOD_EQ  */
    B_AND_EQ = 331,                /* B_AND_EQ  */
    B_OR_EQ = 332,                 /* B_OR_EQ  */
    B_XOR_EQ = 333,                /* B_XOR_EQ  */
    L_SHIFT_EQ = 334,              /* L_SHIFT_EQ  */
    R_SHIFT_EQ = 335,              /* R_SHIFT_EQ  */
    EQUAL = 336,                   /* EQUAL  */
    LESS_EQ = 337,                 /* LESS_EQ  */
    GRTR_EQ = 338,                 /* GRTR_EQ  */
    NOT_EQ = 339,                  /* NOT_EQ  */
    RPAREN = 340,                  /* RPAREN  */
    RBRCKT = 341,                  /* RBRCKT  */
    LBRACE = 342,                  /* LBRACE  */
    RBRACE = 343,                  /* RBRACE  */
    SEMICOLON = 344,               /* SEMICOLON  */
    COMMA = 345,                   /* COMMA  */
    ELLIPSIS = 346,                /* ELLIPSIS  */
    LB_SIGN = 347,                 /* LB_SIGN  */
    DOUB_LB_SIGN = 348,            /* DOUB_LB_SIGN  */
    BACKQUOTE = 349,               /* BACKQUOTE  */
    AT = 350,                      /* AT  */
    ATTRIBUTE = 351,               /* ATTRIBUTE  */
    ALIGNED = 352,                 /* ALIGNED  */
    PACKED = 353,                  /* PACKED  */
    CDECL = 354,                   /* CDECL  */
    MODE = 355,                    /* MODE  */
    FORMAT = 356,                  /* FORMAT  */
    NORETURN = 357,                /* NORETURN  */
    COMMA_OP = 358,                /* COMMA_OP  */
    EQ = 359,                      /* EQ  */
    ASSIGN = 360,                  /* ASSIGN  */
    QUESTMARK = 361,               /* QUESTMARK  */
    COLON = 362,                   /* COLON  */
    COMMA_SEP = 363,               /* COMMA_SEP  */
    OR = 364,                      /* OR  */
    AND = 365,                     /* AND  */
    B_OR = 366,                    /* B_OR  */
    B_XOR = 367,                   /* B_XOR  */
    B_AND = 368,                   /* B_AND  */
    COMP_EQ = 369,                 /* COMP_EQ  */
    COMP_ARITH = 370,              /* COMP_ARITH  */
    COMP_LESS = 371,               /* COMP_LESS  */
    COMP_GRTR = 372,               /* COMP_GRTR  */
    LESS = 373,                    /* LESS  */
    GRTR = 374,                    /* GRTR  */
    L_SHIFT = 375,                 /* L_SHIFT  */
    R_SHIFT = 376,                 /* R_SHIFT  */
    PLUS = 377,                    /* PLUS  */
    MINUS = 378,                   /* MINUS  */
    STAR = 379,                    /* STAR  */
    DIV = 380,                     /* DIV  */
    MOD = 381,                     /* MOD  */
    CAST = 382,                    /* CAST  */
    UNARY = 383,                   /* UNARY  */
    NOT = 384,                     /* NOT  */
    B_NOT = 385,                   /* B_NOT  */
    SIZEOF = 386,                  /* SIZEOF  */
    INDEXOF = 387,                 /* INDEXOF  */
    INCR = 388,                    /* INCR  */
    DECR = 389,                    /* DECR  */
    HYPERUNARY = 390,              /* HYPERUNARY  */
    ARROW = 391,                   /* ARROW  */
    DOT = 392,                     /* DOT  */
    LPAREN = 393,                  /* LPAREN  */
    LBRCKT = 394                   /* LBRCKT  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */




int yyparse (void);


#endif /* !YY_YY_GRAM_TAB_H_INCLUDED  */
