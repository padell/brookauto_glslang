/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         ps2arb_parse
#define yylex           ps2arb_lex
#define yyerror         ps2arb_error
#define yydebug         ps2arb_debug
#define yynerrs         ps2arb_nerrs
#define yylval          ps2arb_lval
#define yychar          ps2arb_char

/* First part of user prologue.  */
#line 1 "ps2arb_gram.y"


//#define YYDEBUG 1
#ifdef WIN32
#pragma warning(disable:4786)
#pragma warning(disable:4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <string.h>

#include <sstream>
#include "ps2arb.h"
#include "ps2arb_intermediate.h"

using std::map;
using std::string;
using namespace std;//otherwise VC6 dies
extern int ps_lineno;

using namespace ps2arb;

BinOp * MXxYFactory (BinaryFactory * ThreeOrFour, 
                     const InstructionFlags & iflag,
                     const Register & dst, 
                     const Register & src0, 
                     const Register & src1,
                     int y);

static BinOp * M4x4Factory  (const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1) {
	return MXxYFactory(&Dp4Op::factory,iflag,dst,src0,src1,4);
}

static BinOp * M3x4Factory  (const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1) {
	return MXxYFactory(&Dp3Op::factory,iflag,dst,src0,src1,4);
}

static BinOp * M4x3Factory  (const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1) {
	return MXxYFactory(&Dp4Op::factory,iflag,dst,src0,src1,3);
}

static BinOp * M3x3Factory  (const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1) {
	return MXxYFactory(&Dp3Op::factory,iflag,dst,src0,src1,3);
}

static BinOp * M3x2Factory  (const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1) {
	return MXxYFactory(&Dp3Op::factory,iflag,dst,src0,src1,2);
}

static TriOp * ReverseCmpFactory  (const InstructionFlags & iflag, const Register & d, const Register & a, const Register & b, const Register & c) {
	return CmpOp::factory(iflag,d,a,c,b);
}


static map<string,VoidFactory*> createVoidFactory() {
	map<string,VoidFactory*> ret;
	ret.insert(map<string,VoidFactory*>::value_type("texkill",&KilOp::factory));
	return ret;
}
static map<string,UnaryFactory*> createUnaryFactory() {
	map<string,UnaryFactory*> ret;
	ret.insert(map<string,UnaryFactory*>::value_type("abs",&AbsOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("exp",&ExpOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("log",&LogOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("frc",&FrcOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("rcp",&RcpOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("rsq",&RsqOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("nrm",&NrmOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("mov",&MovOp::factory));
	ret.insert(map<string,UnaryFactory*>::value_type("sincos",&ScsOp::factory));
	return ret;
}
static map<string,BinaryFactory*> createBinaryFactory() {
	map<string,BinaryFactory*> ret;
	ret.insert(map<string,BinaryFactory*>::value_type("add",&AddOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("sub",&SubOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("mul",&MulOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("crs",&XpdOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("dp3",&Dp3Op::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("dp4",&Dp4Op::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("pow",&PowOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("min",&MinOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("max",&MaxOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("m3x2",&M3x2Factory));
	ret.insert(map<string,BinaryFactory*>::value_type("m3x3",&M3x3Factory));
	ret.insert(map<string,BinaryFactory*>::value_type("m3x4",&M3x4Factory));
	ret.insert(map<string,BinaryFactory*>::value_type("m4x3",&M4x3Factory));
	ret.insert(map<string,BinaryFactory*>::value_type("m4x4",&M4x4Factory));
	ret.insert(map<string,BinaryFactory*>::value_type("texld",&TexldOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("texldp",&TexldpOp::factory));
	ret.insert(map<string,BinaryFactory*>::value_type("texldb",&TexldbOp::factory));
	return ret;
}
static map<string,TrinaryFactory*> createTrinaryFactory() {
	map<string,TrinaryFactory*> ret;
	ret.insert(map<string,TrinaryFactory*>::value_type("cmp",&ReverseCmpFactory));
	ret.insert(map<string,TrinaryFactory*>::value_type("lrp",&LrpOp::factory));
	ret.insert(map<string,TrinaryFactory*>::value_type("mad",&MadOp::factory));
	ret.insert(map<string,TrinaryFactory*>::value_type("dp2add",&Dp2addOp::factory));
	return ret;
}

static std::map<string,VoidFactory*> void_factory=createVoidFactory();
static std::map<string,UnaryFactory*> unary_factory=createUnaryFactory();
static std::map<string,BinaryFactory*> binary_factory=createBinaryFactory();
static std::map<string,TrinaryFactory*> trinary_factory=createTrinaryFactory();

#ifdef WIN32
#pragma warning( disable : 4102 ) 
#endif

extern int yylex(void);
static void yyerror (char *s) {
  fprintf (stderr, "Error Line %d: %s\n", ps_lineno,s);
}


#line 199 "ps2arb_gram.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "ps2arb_gram.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_PS_NOP = 3,                     /* PS_NOP  */
  YYSYMBOL_PS_NEGATE = 4,                  /* PS_NEGATE  */
  YYSYMBOL_PS_SWIZZLEMASK = 5,             /* PS_SWIZZLEMASK  */
  YYSYMBOL_PS_COLORREG = 6,                /* PS_COLORREG  */
  YYSYMBOL_PS_TEMPREG = 7,                 /* PS_TEMPREG  */
  YYSYMBOL_PS_TEXCOORDREG = 8,             /* PS_TEXCOORDREG  */
  YYSYMBOL_PS_OUTPUTREG = 9,               /* PS_OUTPUTREG  */
  YYSYMBOL_PS_SAMPLEREG = 10,              /* PS_SAMPLEREG  */
  YYSYMBOL_PS_CONSTREG = 11,               /* PS_CONSTREG  */
  YYSYMBOL_PS_TEXKILL = 12,                /* PS_TEXKILL  */
  YYSYMBOL_PS_SINCOS = 13,                 /* PS_SINCOS  */
  YYSYMBOL_PS_UNARY_OP = 14,               /* PS_UNARY_OP  */
  YYSYMBOL_PS_BINARY_OP = 15,              /* PS_BINARY_OP  */
  YYSYMBOL_PS_TRINARY_OP = 16,             /* PS_TRINARY_OP  */
  YYSYMBOL_PS_OP_FLAGS = 17,               /* PS_OP_FLAGS  */
  YYSYMBOL_PS_DCLTEX = 18,                 /* PS_DCLTEX  */
  YYSYMBOL_PS_DCL = 19,                    /* PS_DCL  */
  YYSYMBOL_PS_DEF = 20,                    /* PS_DEF  */
  YYSYMBOL_PS_COMMA = 21,                  /* PS_COMMA  */
  YYSYMBOL_PS_MOV = 22,                    /* PS_MOV  */
  YYSYMBOL_PS_COMMENT = 23,                /* PS_COMMENT  */
  YYSYMBOL_PS_ENDLESS_COMMENT = 24,        /* PS_ENDLESS_COMMENT  */
  YYSYMBOL_PS_FLOAT = 25,                  /* PS_FLOAT  */
  YYSYMBOL_PS_NEWLINE = 26,                /* PS_NEWLINE  */
  YYSYMBOL_PS_PSHEADER = 27,               /* PS_PSHEADER  */
  YYSYMBOL_YYACCEPT = 28,                  /* $accept  */
  YYSYMBOL_program = 29,                   /* program  */
  YYSYMBOL_declarations = 30,              /* declarations  */
  YYSYMBOL_instructions = 31,              /* instructions  */
  YYSYMBOL_declaration = 32,               /* declaration  */
  YYSYMBOL_instruction = 33,               /* instruction  */
  YYSYMBOL_trinary_op = 34,                /* trinary_op  */
  YYSYMBOL_binary_op = 35,                 /* binary_op  */
  YYSYMBOL_unary_op = 36,                  /* unary_op  */
  YYSYMBOL_optional_dualreg = 37,          /* optional_dualreg  */
  YYSYMBOL_sincos = 38,                    /* sincos  */
  YYSYMBOL_texkill = 39,                   /* texkill  */
  YYSYMBOL_mov = 40,                       /* mov  */
  YYSYMBOL_optional_flags = 41,            /* optional_flags  */
  YYSYMBOL_optionalwritemask = 42,         /* optionalwritemask  */
  YYSYMBOL_colorortexreg = 43,             /* colorortexreg  */
  YYSYMBOL_declreg = 44,                   /* declreg  */
  YYSYMBOL_movreg = 45,                    /* movreg  */
  YYSYMBOL_dstreg = 46,                    /* dstreg  */
  YYSYMBOL_optionalnegate = 47,            /* optionalnegate  */
  YYSYMBOL_srcreg = 48,                    /* srcreg  */
  YYSYMBOL_readablereg = 49,               /* readablereg  */
  YYSYMBOL_newlines = 50,                  /* newlines  */
  YYSYMBOL_optendlesscomment = 51,         /* optendlesscomment  */
  YYSYMBOL_optnewlines = 52                /* optnewlines  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;


/* Second part of user prologue.  */
#line 131 "ps2arb_gram.y"


/* so about this function...it's nice cus it's centrally called
** --however there might be some argument that we're redoing the parsing
** phase of PS2 and that we should put this in the individual sections
** that know that ps2registers are being used...  static Register
*/
static Register
createPS2Register (const YYSTYPE::Register &r) {
	if (r.reg[0]!=0) {
		Symbol s;
		unsigned int index = atoi (r.reg+1);
		switch (r.reg[0]) {
		case 't':
			s.Set(Symbol::TEXCOORD,index);
			break;
		case 's':
			s.Set(Symbol::SAMPLE,index);
			break;
		case 'v':
			s.Set(Symbol::COLOR,index);
			break;
		case 'o':
			switch ( r.reg[1]) {
			case 'D':
			case 'd':
				s.Set (Symbol::OUTPUTDEPTH,0);
				break;
			case 'c':
			default:
				index = atoi(r.reg+2);
				s.Set(Symbol::OUTPUTCOLOR,index);
				break;
			}
			//output
			break;
		case 'c':
			s.Set(Symbol::CONST,index);
			break;
		case 'r':
		default:
			s.Set(Symbol::TEMP,index);
			break;			
		}
		iLanguage->SpecifySymbol (r.reg,s);
	}else{
		fprintf (stderr,"register %s not properly specified:",r.reg);
	}
	return Register(r.reg,r.swizzlemask,r.negate);
}

char * incRegister (string s) {
	if (s.length()) {
		char c=*s.begin();
		s = s.substr(1);
		int which=1+strtol(s.c_str(),NULL,10);
		char out [1024];
		sprintf(out,"%c%d",c,which);
		return strdup(out);
	}	
	return strdup("");
}

BinOp * MXxYFactory  (BinaryFactory * ThreeOrFour, const InstructionFlags & iflag, const Register & dst, const Register & src0, const Register & src1, int y) {
	YYSTYPE cinc;
	char * tmp=NULL;
	Register destination(dst);
	cinc.reg.swizzlemask="";
	cinc.reg.negate="";
	cinc.reg.reg = incRegister(src1);
	destination.swizzle="x";
	iLanguage->AddInst((*ThreeOrFour)(iflag,destination,src0,src1));
	destination.swizzle="y";
	BinOp * ret =(*ThreeOrFour)(iflag,destination,src0,createPS2Register(cinc.reg));
	if (y>2) {
		iLanguage->AddInst(ret);
		cinc.reg.reg = incRegister(tmp = cinc.reg.reg);
		free(tmp);	
		destination.swizzle="z";
		ret = (*ThreeOrFour)(iflag,destination,src0,createPS2Register(cinc.reg));
		if (y>3) {
			iLanguage->AddInst(ret);
			cinc.reg.reg = incRegister(tmp = cinc.reg.reg);
			free(tmp);
			destination.swizzle="w";
			ret= (*ThreeOrFour)(iflag,destination,src0,createPS2Register(cinc.reg));
		}
	}
	free(cinc.reg.reg);
	return ret;
}

static string strtoupper (string s) {
	for (string::iterator i =s.begin();i!=s.end();++i) {
		*i= toupper(*i);
	}
	return s;
}

InstructionFlags AdjustInstructionFlags (std::string flags) {
	flags = strtoupper(flags);
	bool pp = flags.find("_PP")!=string::npos;	
	bool sat = flags.find("_SAT")!=string::npos;
	return InstructionFlags(pp?InstructionFlags::PP:InstructionFlags::FULL,sat);
}

#define DEFINERETVAL YYSTYPE ret


#line 395 "ps2arb_gram.tab.c"


#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  8
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   80

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  28
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  25
/* YYNRULES -- Number of rules.  */
#define YYNRULES  50
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  99

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   282


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   255,   255,   257,   258,   260,   261,   262,   265,   269,
     300,   312,   312,   312,   312,   312,   312,   315,   325,   334,
     342,   345,   347,   355,   362,   370,   375,   379,   384,   388,
     389,   392,   401,   407,   416,   425,   430,   434,   443,   443,
     443,   443,   443,   446,   452,   458,   464,   471,   475,   478,
     483
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "PS_NOP", "PS_NEGATE",
  "PS_SWIZZLEMASK", "PS_COLORREG", "PS_TEMPREG", "PS_TEXCOORDREG",
  "PS_OUTPUTREG", "PS_SAMPLEREG", "PS_CONSTREG", "PS_TEXKILL", "PS_SINCOS",
  "PS_UNARY_OP", "PS_BINARY_OP", "PS_TRINARY_OP", "PS_OP_FLAGS",
  "PS_DCLTEX", "PS_DCL", "PS_DEF", "PS_COMMA", "PS_MOV", "PS_COMMENT",
  "PS_ENDLESS_COMMENT", "PS_FLOAT", "PS_NEWLINE", "PS_PSHEADER", "$accept",
  "program", "declarations", "instructions", "declaration", "instruction",
  "trinary_op", "binary_op", "unary_op", "optional_dualreg", "sincos",
  "texkill", "mov", "optional_flags", "optionalwritemask", "colorortexreg",
  "declreg", "movreg", "dstreg", "optionalnegate", "srcreg", "readablereg",
  "newlines", "optendlesscomment", "optnewlines", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-71)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
       0,     0,     0,    16,   -71,    -7,   -71,   -71,   -71,     0,
      27,    11,    42,    21,    -3,     0,   -71,   -71,   -71,    22,
     -71,    19,    32,    32,    32,    32,    32,    32,    28,     0,
     -71,   -71,   -71,   -71,   -71,   -71,    27,   -71,   -71,    29,
     -71,    51,    49,    49,    49,    49,    44,   -71,   -71,    -3,
     -71,    36,   -71,    23,   -71,    22,    37,    38,    39,    40,
     -71,    41,   -71,   -71,    43,   -71,   -71,   -71,   -71,   -71,
      22,   -71,    51,    51,    51,    51,    51,    45,   -71,    46,
     -71,    48,    50,   -71,    47,    51,   -71,    51,    51,    52,
      53,   -71,    54,    55,    51,    51,   -71,   -71,   -71
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
      50,    46,    44,     0,    49,     0,    45,    43,     1,     0,
       4,     0,     0,     0,     7,     0,     9,    29,    30,    28,
       8,     0,    26,    26,    26,    26,    26,    26,    48,     6,
      13,    12,    11,    14,    15,    16,     4,    27,    31,     0,
      25,    36,     0,     0,     0,     0,     0,    47,     2,     7,
       3,     0,    35,     0,    23,    28,     0,     0,     0,     0,
      33,     0,    32,     5,     0,    38,    42,    39,    40,    41,
      28,    34,    36,    36,    36,    36,    36,     0,    37,    21,
      19,     0,     0,    24,     0,    36,    22,    36,    36,     0,
       0,    18,     0,     0,    36,    36,    10,    20,    17
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -71,   -71,    34,    14,   -71,   -71,   -71,   -71,   -71,   -71,
     -71,   -71,   -71,    12,   -48,   -71,   -71,   -71,    -2,   -71,
     -70,   -71,    -1,   -71,   -71
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     3,    14,    28,    15,    29,    30,    31,    32,    86,
      33,    34,    35,    41,    38,    19,    20,    61,    56,    53,
      54,    70,     4,    48,     5
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
       6,     7,    79,    80,    81,    82,    83,    71,    10,    22,
      23,    24,    25,    26,    36,    90,     8,    91,    92,    27,
       9,    16,    78,     1,    97,    98,     2,    37,    49,    65,
      66,    67,    21,    68,    69,    42,    43,    44,    45,    46,
      39,    57,    58,    59,    62,    11,    12,    13,    17,    40,
      18,    55,    47,    60,    51,    52,    55,    64,    72,    73,
      74,    75,    76,    63,     0,     0,    84,    85,    77,    87,
      50,    88,    89,    93,    94,    95,     0,     0,     0,     0,
      96
};

static const yytype_int8 yycheck[] =
{
       1,     2,    72,    73,    74,    75,    76,    55,     9,    12,
      13,    14,    15,    16,    15,    85,     0,    87,    88,    22,
      27,    10,    70,    23,    94,    95,    26,     5,    29,     6,
       7,     8,    11,    10,    11,    23,    24,    25,    26,    27,
      21,    43,    44,    45,    46,    18,    19,    20,     6,    17,
       8,     7,    24,     9,    25,     4,     7,    21,    21,    21,
      21,    21,    21,    49,    -1,    -1,    21,    21,    25,    21,
      36,    21,    25,    21,    21,    21,    -1,    -1,    -1,    -1,
      25
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    23,    26,    29,    50,    52,    50,    50,     0,    27,
      50,    18,    19,    20,    30,    32,    10,     6,     8,    43,
      44,    11,    12,    13,    14,    15,    16,    22,    31,    33,
      34,    35,    36,    38,    39,    40,    50,     5,    42,    21,
      17,    41,    41,    41,    41,    41,    41,    24,    51,    50,
      30,    25,     4,    47,    48,     7,    46,    46,    46,    46,
       9,    45,    46,    31,    21,     6,     7,     8,    10,    11,
      49,    42,    21,    21,    21,    21,    21,    25,    42,    48,
      48,    48,    48,    48,    21,    21,    37,    21,    21,    25,
      48,    48,    48,    21,    21,    21,    25,    48,    48
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    28,    29,    30,    30,    31,    31,    31,    32,    32,
      32,    33,    33,    33,    33,    33,    33,    34,    35,    36,
      37,    37,    38,    39,    40,    41,    41,    42,    42,    43,
      43,    44,    45,    45,    46,    47,    47,    48,    49,    49,
      49,    49,    49,    50,    50,    50,    50,    51,    51,    52,
      52
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     6,     3,     0,     3,     1,     0,     2,     2,
      10,     1,     1,     1,     1,     1,     1,     9,     7,     5,
       4,     0,     6,     3,     5,     1,     0,     1,     0,     1,
       1,     2,     1,     1,     2,     1,     0,     3,     1,     1,
       1,     1,     1,     2,     1,     2,     1,     1,     0,     1,
       0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 8: /* declaration: PS_DCL declreg  */
#line 266 "ps2arb_gram.y"
{	
	iLanguage->AddDecl(new DeclareRegisterUsage(createPS2Register((yyvsp[0].reg))));
}
#line 1412 "ps2arb_gram.tab.c"
    break;

  case 9: /* declaration: PS_DCLTEX PS_SAMPLEREG  */
#line 270 "ps2arb_gram.y"
{
	Symbol::TEXTARGET tt=Symbol::UNSPECIFIED;
	switch ((yyvsp[-1].s)[4]) {
           case '2':
              tt=Symbol::TEX2D;
	   break;
	   case '1':
              tt=Symbol::TEX1D;
	   break;
	   case 'c':
              tt=Symbol::CUBE;
	   break;
	   case '3':
	   case 'v':
	   default:
              tt=Symbol::TEX3D;	
	   break;
        }

	Symbol s;
	s.Set(Symbol::SAMPLE,atoi((yyvsp[0].s)+1),tt);
	iLanguage->SpecifySymbol((yyvsp[0].s),s);
	YYSTYPE tmp;
        tmp.reg.reg=(yyvsp[0].s); 
        tmp.reg.swizzlemask="";
        tmp.reg.negate="";
	iLanguage->AddDecl(
            new DeclareSampleRegister(
               createPS2Register(tmp.reg)));
}
#line 1447 "ps2arb_gram.tab.c"
    break;

  case 10: /* declaration: PS_DEF PS_CONSTREG PS_COMMA PS_FLOAT PS_COMMA PS_FLOAT PS_COMMA PS_FLOAT PS_COMMA PS_FLOAT  */
#line 301 "ps2arb_gram.y"
{       
        YYSTYPE tmp;
        tmp.reg.reg=(yyvsp[-8].s); 
        tmp.reg.swizzlemask="";
        tmp.reg.negate="";
	iLanguage->AddDecl(
           new DefineConstantRegister(
             createPS2Register(tmp.reg),
		(yyvsp[-6].f), (yyvsp[-4].f), (yyvsp[-2].f), (yyvsp[0].f)));
}
#line 1462 "ps2arb_gram.tab.c"
    break;

  case 16: /* instruction: mov  */
#line 313 "ps2arb_gram.y"
{}
#line 1468 "ps2arb_gram.tab.c"
    break;

  case 17: /* trinary_op: PS_TRINARY_OP optional_flags dstreg PS_COMMA srcreg PS_COMMA srcreg PS_COMMA srcreg  */
#line 316 "ps2arb_gram.y"
{
        iLanguage->AddInst((*trinary_factory[(yyvsp[-8].s)])(
	     AdjustInstructionFlags((yyvsp[-7].s)),
             createPS2Register((yyvsp[-6].reg)),
             createPS2Register((yyvsp[-4].reg)),
             createPS2Register((yyvsp[-2].reg)),
             createPS2Register((yyvsp[0].reg))));
}
#line 1481 "ps2arb_gram.tab.c"
    break;

  case 18: /* binary_op: PS_BINARY_OP optional_flags dstreg PS_COMMA srcreg PS_COMMA srcreg  */
#line 326 "ps2arb_gram.y"
{
        iLanguage->AddInst((*binary_factory[(yyvsp[-6].s)])(
             AdjustInstructionFlags((yyvsp[-5].s)),
             createPS2Register((yyvsp[-4].reg)),
             createPS2Register((yyvsp[-2].reg)),
             createPS2Register((yyvsp[0].reg))));
}
#line 1493 "ps2arb_gram.tab.c"
    break;

  case 19: /* unary_op: PS_UNARY_OP optional_flags dstreg PS_COMMA srcreg  */
#line 335 "ps2arb_gram.y"
{
        iLanguage->AddInst((*unary_factory[(yyvsp[-4].s)])(
             AdjustInstructionFlags((yyvsp[-3].s)),
             createPS2Register((yyvsp[-2].reg)),
             createPS2Register((yyvsp[0].reg))));
}
#line 1504 "ps2arb_gram.tab.c"
    break;

  case 20: /* optional_dualreg: PS_COMMA srcreg PS_COMMA srcreg  */
#line 343 "ps2arb_gram.y"
{}
#line 1510 "ps2arb_gram.tab.c"
    break;

  case 21: /* optional_dualreg: %empty  */
#line 345 "ps2arb_gram.y"
{}
#line 1516 "ps2arb_gram.tab.c"
    break;

  case 22: /* sincos: PS_SINCOS optional_flags dstreg PS_COMMA srcreg optional_dualreg  */
#line 348 "ps2arb_gram.y"
{
        iLanguage->AddInst((*unary_factory[(yyvsp[-5].s)])(
             AdjustInstructionFlags((yyvsp[-4].s)),
             createPS2Register((yyvsp[-3].reg)),
             createPS2Register((yyvsp[-1].reg))));
}
#line 1527 "ps2arb_gram.tab.c"
    break;

  case 23: /* texkill: PS_TEXKILL optional_flags srcreg  */
#line 356 "ps2arb_gram.y"
{
        iLanguage->AddInst((*void_factory[(yyvsp[-2].s)])(
             AdjustInstructionFlags((yyvsp[-1].s)),
             createPS2Register((yyvsp[0].reg))));
}
#line 1537 "ps2arb_gram.tab.c"
    break;

  case 24: /* mov: PS_MOV optional_flags movreg PS_COMMA srcreg  */
#line 363 "ps2arb_gram.y"
{
        iLanguage->AddInst((*unary_factory[(yyvsp[-4].s)])(
             AdjustInstructionFlags((yyvsp[-3].s)),
             createPS2Register((yyvsp[-2].reg)),
             createPS2Register((yyvsp[0].reg))));
}
#line 1548 "ps2arb_gram.tab.c"
    break;

  case 25: /* optional_flags: PS_OP_FLAGS  */
#line 371 "ps2arb_gram.y"
{
        (yyval.s)=(yyvsp[0].s);
}
#line 1556 "ps2arb_gram.tab.c"
    break;

  case 26: /* optional_flags: %empty  */
#line 375 "ps2arb_gram.y"
{
        (yyval.s)="";
}
#line 1564 "ps2arb_gram.tab.c"
    break;

  case 27: /* optionalwritemask: PS_SWIZZLEMASK  */
#line 380 "ps2arb_gram.y"
{
        (yyval.s)=(yyvsp[0].s);
}
#line 1572 "ps2arb_gram.tab.c"
    break;

  case 28: /* optionalwritemask: %empty  */
#line 384 "ps2arb_gram.y"
{
        (yyval.s)="";
}
#line 1580 "ps2arb_gram.tab.c"
    break;

  case 31: /* declreg: colorortexreg optionalwritemask  */
#line 393 "ps2arb_gram.y"
{
        DEFINERETVAL; 
        ret.reg.reg=(yyvsp[-1].s);
        ret.reg.swizzlemask=(yyvsp[0].s);
        ret.reg.negate="";
        (yyval.reg)=ret.reg;
}
#line 1592 "ps2arb_gram.tab.c"
    break;

  case 32: /* movreg: dstreg  */
#line 402 "ps2arb_gram.y"
{
        DEFINERETVAL;
        ret.reg = (yyvsp[0].reg);
        (yyval.reg) = ret.reg;
}
#line 1602 "ps2arb_gram.tab.c"
    break;

  case 33: /* movreg: PS_OUTPUTREG  */
#line 408 "ps2arb_gram.y"
{
        DEFINERETVAL;
        ret.reg.reg = (yyvsp[0].s);
        ret.reg.swizzlemask = "";
        ret.reg.negate = "";
        (yyval.reg) = ret.reg;
}
#line 1614 "ps2arb_gram.tab.c"
    break;

  case 34: /* dstreg: PS_TEMPREG optionalwritemask  */
#line 417 "ps2arb_gram.y"
{
        DEFINERETVAL;
        ret.reg.reg=(yyvsp[-1].s);
        ret.reg.swizzlemask = (yyvsp[0].s);
        ret.reg.negate="";
        (yyval.reg)=ret.reg;
}
#line 1626 "ps2arb_gram.tab.c"
    break;

  case 35: /* optionalnegate: PS_NEGATE  */
#line 426 "ps2arb_gram.y"
{
        (yyval.s)=(yyvsp[0].s);
}
#line 1634 "ps2arb_gram.tab.c"
    break;

  case 36: /* optionalnegate: %empty  */
#line 430 "ps2arb_gram.y"
{
        (yyval.s)="";
}
#line 1642 "ps2arb_gram.tab.c"
    break;

  case 37: /* srcreg: optionalnegate readablereg optionalwritemask  */
#line 435 "ps2arb_gram.y"
{
        DEFINERETVAL;
        ret.reg.negate = (yyvsp[-2].s);
        ret.reg.reg = (yyvsp[-1].s);
        ret.reg.swizzlemask = (yyvsp[0].s);
        (yyval.reg)=ret.reg;
}
#line 1654 "ps2arb_gram.tab.c"
    break;

  case 43: /* newlines: PS_NEWLINE newlines  */
#line 447 "ps2arb_gram.y"
{
        iLanguage->AddCommentOrNewline(
           new Newline((int)(yyvsp[-1].f)));
        (yyval.f) = (yyvsp[-1].f) +(yyvsp[0].f);
}
#line 1664 "ps2arb_gram.tab.c"
    break;

  case 44: /* newlines: PS_NEWLINE  */
#line 453 "ps2arb_gram.y"
{
        iLanguage->AddCommentOrNewline(
           new Newline((int)(yyvsp[0].f)));
        (yyval.f) = (yyvsp[0].f);
}
#line 1674 "ps2arb_gram.tab.c"
    break;

  case 45: /* newlines: PS_COMMENT newlines  */
#line 459 "ps2arb_gram.y"
{
        iLanguage->AddCommentOrNewline(
           new Comment((yyvsp[-1].s)));
        (yyval.f) = 1 + (yyvsp[0].f);
}
#line 1684 "ps2arb_gram.tab.c"
    break;

  case 46: /* newlines: PS_COMMENT  */
#line 465 "ps2arb_gram.y"
{
        iLanguage->AddCommentOrNewline(
          new Comment((yyvsp[0].s)));
        (yyval.f) = 1; 
}
#line 1694 "ps2arb_gram.tab.c"
    break;

  case 47: /* optendlesscomment: PS_ENDLESS_COMMENT  */
#line 472 "ps2arb_gram.y"
{
        iLanguage->AddCommentOrNewline(new Comment((yyvsp[0].s)));
}
#line 1702 "ps2arb_gram.tab.c"
    break;

  case 49: /* optnewlines: newlines  */
#line 479 "ps2arb_gram.y"
{
        (yyval.f) = (yyvsp[0].f);
}
#line 1710 "ps2arb_gram.tab.c"
    break;

  case 50: /* optnewlines: %empty  */
#line 483 "ps2arb_gram.y"
{
        (yyval.f) = 0;
}
#line 1718 "ps2arb_gram.tab.c"
    break;


#line 1722 "ps2arb_gram.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 487 "ps2arb_gram.y"





