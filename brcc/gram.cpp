/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "gram.y"

 /*
 ======================================================================

    CTool Library
    Copyright (C) 1995-2001	Shaun Flisakowski

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 1, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 ======================================================================
 */

/* grammar File for C - Shaun Flisakowski and Patrick Baudin */
/* Grammar was constructed with the assistance of:
    "C - A Reference Manual" (Fourth Edition),
    by Samuel P Harbison, and Guy L Steele Jr. */

#ifdef    NO_ALLOCA
#define   alloca  __builtin_alloca
#endif

#ifdef _WIN32
/* Don't complain about switch() statements that only have a 'default' */
#pragma warning( disable : 4065 )
#endif

#include <stdio.h>
#include <errno.h>
#include <setjmp.h>
#include <string.h>

#include "lexer.h"
#include "symbol.h"
#include "token.h"
#include "stemnt.h"
#include "location.h"
#include "project.h"
#include "brtexpress.h"
extern int err_cnt;
int yylex(YYSTYPE *lvalp);

extern int err_top_level;
/* Cause the `yydebug' variable to be defined.  */
#define YYDEBUG 1
void baseTypeFixup(BaseType * bt,Decl * decl) {
  BaseType * b = decl->form->getBase();
  while ((decl=decl->next)) {
    BaseType *nb = decl->form->getBase();
    if (memcmp(nb,b,sizeof(BaseType))!=0) {
      decl->form = decl->form->dup();
      *decl->form->getBase()=*b;
    }
  }

}
/*  int  yydebug = 1;  */

/* ###################################################### */
#line 233 "gram.y"

/* 1 if we explained undeclared var errors.  */
/*  static int undeclared_variable_notice = 0;  */

#line 147 "gram.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "gram.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_IDENT = 3,                      /* IDENT  */
  YYSYMBOL_TAG_NAME = 4,                   /* TAG_NAME  */
  YYSYMBOL_LABEL_NAME = 5,                 /* LABEL_NAME  */
  YYSYMBOL_TYPEDEF_NAME = 6,               /* TYPEDEF_NAME  */
  YYSYMBOL_STRING = 7,                     /* STRING  */
  YYSYMBOL_LSTRING = 8,                    /* LSTRING  */
  YYSYMBOL_CHAR_CONST = 9,                 /* CHAR_CONST  */
  YYSYMBOL_LCHAR_CONST = 10,               /* LCHAR_CONST  */
  YYSYMBOL_INUM = 11,                      /* INUM  */
  YYSYMBOL_RNUM = 12,                      /* RNUM  */
  YYSYMBOL_PP_DIR = 13,                    /* PP_DIR  */
  YYSYMBOL_PP_LINE = 14,                   /* PP_LINE  */
  YYSYMBOL_INVALID = 15,                   /* INVALID  */
  YYSYMBOL_CONST = 16,                     /* CONST  */
  YYSYMBOL_VOLATILE = 17,                  /* VOLATILE  */
  YYSYMBOL_OUT = 18,                       /* OUT  */
  YYSYMBOL_REDUCE = 19,                    /* REDUCE  */
  YYSYMBOL_VOUT = 20,                      /* VOUT  */
  YYSYMBOL_ITER = 21,                      /* ITER  */
  YYSYMBOL_KERNEL = 22,                    /* KERNEL  */
  YYSYMBOL_AUTO = 23,                      /* AUTO  */
  YYSYMBOL_EXTRN = 24,                     /* EXTRN  */
  YYSYMBOL_REGISTR = 25,                   /* REGISTR  */
  YYSYMBOL_STATIC = 26,                    /* STATIC  */
  YYSYMBOL_TYPEDEF = 27,                   /* TYPEDEF  */
  YYSYMBOL_VOID = 28,                      /* VOID  */
  YYSYMBOL_SHORT = 29,                     /* SHORT  */
  YYSYMBOL_INT = 30,                       /* INT  */
  YYSYMBOL_LONG = 31,                      /* LONG  */
  YYSYMBOL_SGNED = 32,                     /* SGNED  */
  YYSYMBOL_UNSGNED = 33,                   /* UNSGNED  */
  YYSYMBOL_FLOAT = 34,                     /* FLOAT  */
  YYSYMBOL_FLOAT2 = 35,                    /* FLOAT2  */
  YYSYMBOL_FLOAT3 = 36,                    /* FLOAT3  */
  YYSYMBOL_FLOAT4 = 37,                    /* FLOAT4  */
  YYSYMBOL_FIXED = 38,                     /* FIXED  */
  YYSYMBOL_FIXED2 = 39,                    /* FIXED2  */
  YYSYMBOL_FIXED3 = 40,                    /* FIXED3  */
  YYSYMBOL_FIXED4 = 41,                    /* FIXED4  */
  YYSYMBOL_SHORTFIXED = 42,                /* SHORTFIXED  */
  YYSYMBOL_SHORTFIXED2 = 43,               /* SHORTFIXED2  */
  YYSYMBOL_SHORTFIXED3 = 44,               /* SHORTFIXED3  */
  YYSYMBOL_SHORTFIXED4 = 45,               /* SHORTFIXED4  */
  YYSYMBOL_DOUBLE = 46,                    /* DOUBLE  */
  YYSYMBOL_DOUBLE2 = 47,                   /* DOUBLE2  */
  YYSYMBOL_CHAR = 48,                      /* CHAR  */
  YYSYMBOL_CHAR2 = 49,                     /* CHAR2  */
  YYSYMBOL_CHAR3 = 50,                     /* CHAR3  */
  YYSYMBOL_CHAR4 = 51,                     /* CHAR4  */
  YYSYMBOL_UCHAR = 52,                     /* UCHAR  */
  YYSYMBOL_UCHAR2 = 53,                    /* UCHAR2  */
  YYSYMBOL_UCHAR3 = 54,                    /* UCHAR3  */
  YYSYMBOL_UCHAR4 = 55,                    /* UCHAR4  */
  YYSYMBOL_ENUM = 56,                      /* ENUM  */
  YYSYMBOL_STRUCT = 57,                    /* STRUCT  */
  YYSYMBOL_UNION = 58,                     /* UNION  */
  YYSYMBOL_BREAK = 59,                     /* BREAK  */
  YYSYMBOL_CASE = 60,                      /* CASE  */
  YYSYMBOL_CONT = 61,                      /* CONT  */
  YYSYMBOL_DEFLT = 62,                     /* DEFLT  */
  YYSYMBOL_DO = 63,                        /* DO  */
  YYSYMBOL_ELSE = 64,                      /* ELSE  */
  YYSYMBOL_IF = 65,                        /* IF  */
  YYSYMBOL_FOR = 66,                       /* FOR  */
  YYSYMBOL_GOTO = 67,                      /* GOTO  */
  YYSYMBOL_RETURN = 68,                    /* RETURN  */
  YYSYMBOL_SWITCH = 69,                    /* SWITCH  */
  YYSYMBOL_WHILE = 70,                     /* WHILE  */
  YYSYMBOL_PLUS_EQ = 71,                   /* PLUS_EQ  */
  YYSYMBOL_MINUS_EQ = 72,                  /* MINUS_EQ  */
  YYSYMBOL_STAR_EQ = 73,                   /* STAR_EQ  */
  YYSYMBOL_DIV_EQ = 74,                    /* DIV_EQ  */
  YYSYMBOL_MOD_EQ = 75,                    /* MOD_EQ  */
  YYSYMBOL_B_AND_EQ = 76,                  /* B_AND_EQ  */
  YYSYMBOL_B_OR_EQ = 77,                   /* B_OR_EQ  */
  YYSYMBOL_B_XOR_EQ = 78,                  /* B_XOR_EQ  */
  YYSYMBOL_L_SHIFT_EQ = 79,                /* L_SHIFT_EQ  */
  YYSYMBOL_R_SHIFT_EQ = 80,                /* R_SHIFT_EQ  */
  YYSYMBOL_EQUAL = 81,                     /* EQUAL  */
  YYSYMBOL_LESS_EQ = 82,                   /* LESS_EQ  */
  YYSYMBOL_GRTR_EQ = 83,                   /* GRTR_EQ  */
  YYSYMBOL_NOT_EQ = 84,                    /* NOT_EQ  */
  YYSYMBOL_RPAREN = 85,                    /* RPAREN  */
  YYSYMBOL_RBRCKT = 86,                    /* RBRCKT  */
  YYSYMBOL_LBRACE = 87,                    /* LBRACE  */
  YYSYMBOL_RBRACE = 88,                    /* RBRACE  */
  YYSYMBOL_SEMICOLON = 89,                 /* SEMICOLON  */
  YYSYMBOL_COMMA = 90,                     /* COMMA  */
  YYSYMBOL_ELLIPSIS = 91,                  /* ELLIPSIS  */
  YYSYMBOL_LB_SIGN = 92,                   /* LB_SIGN  */
  YYSYMBOL_DOUB_LB_SIGN = 93,              /* DOUB_LB_SIGN  */
  YYSYMBOL_BACKQUOTE = 94,                 /* BACKQUOTE  */
  YYSYMBOL_AT = 95,                        /* AT  */
  YYSYMBOL_ATTRIBUTE = 96,                 /* ATTRIBUTE  */
  YYSYMBOL_ALIGNED = 97,                   /* ALIGNED  */
  YYSYMBOL_PACKED = 98,                    /* PACKED  */
  YYSYMBOL_CDECL = 99,                     /* CDECL  */
  YYSYMBOL_MODE = 100,                     /* MODE  */
  YYSYMBOL_FORMAT = 101,                   /* FORMAT  */
  YYSYMBOL_NORETURN = 102,                 /* NORETURN  */
  YYSYMBOL_COMMA_OP = 103,                 /* COMMA_OP  */
  YYSYMBOL_EQ = 104,                       /* EQ  */
  YYSYMBOL_ASSIGN = 105,                   /* ASSIGN  */
  YYSYMBOL_QUESTMARK = 106,                /* QUESTMARK  */
  YYSYMBOL_COLON = 107,                    /* COLON  */
  YYSYMBOL_COMMA_SEP = 108,                /* COMMA_SEP  */
  YYSYMBOL_OR = 109,                       /* OR  */
  YYSYMBOL_AND = 110,                      /* AND  */
  YYSYMBOL_B_OR = 111,                     /* B_OR  */
  YYSYMBOL_B_XOR = 112,                    /* B_XOR  */
  YYSYMBOL_B_AND = 113,                    /* B_AND  */
  YYSYMBOL_COMP_EQ = 114,                  /* COMP_EQ  */
  YYSYMBOL_COMP_ARITH = 115,               /* COMP_ARITH  */
  YYSYMBOL_COMP_LESS = 116,                /* COMP_LESS  */
  YYSYMBOL_COMP_GRTR = 117,                /* COMP_GRTR  */
  YYSYMBOL_LESS = 118,                     /* LESS  */
  YYSYMBOL_GRTR = 119,                     /* GRTR  */
  YYSYMBOL_L_SHIFT = 120,                  /* L_SHIFT  */
  YYSYMBOL_R_SHIFT = 121,                  /* R_SHIFT  */
  YYSYMBOL_PLUS = 122,                     /* PLUS  */
  YYSYMBOL_MINUS = 123,                    /* MINUS  */
  YYSYMBOL_STAR = 124,                     /* STAR  */
  YYSYMBOL_DIV = 125,                      /* DIV  */
  YYSYMBOL_MOD = 126,                      /* MOD  */
  YYSYMBOL_CAST = 127,                     /* CAST  */
  YYSYMBOL_UNARY = 128,                    /* UNARY  */
  YYSYMBOL_NOT = 129,                      /* NOT  */
  YYSYMBOL_B_NOT = 130,                    /* B_NOT  */
  YYSYMBOL_SIZEOF = 131,                   /* SIZEOF  */
  YYSYMBOL_INDEXOF = 132,                  /* INDEXOF  */
  YYSYMBOL_INCR = 133,                     /* INCR  */
  YYSYMBOL_DECR = 134,                     /* DECR  */
  YYSYMBOL_HYPERUNARY = 135,               /* HYPERUNARY  */
  YYSYMBOL_ARROW = 136,                    /* ARROW  */
  YYSYMBOL_DOT = 137,                      /* DOT  */
  YYSYMBOL_LPAREN = 138,                   /* LPAREN  */
  YYSYMBOL_LBRCKT = 139,                   /* LBRCKT  */
  YYSYMBOL_YYACCEPT = 140,                 /* $accept  */
  YYSYMBOL_program = 141,                  /* program  */
  YYSYMBOL_trans_unit = 142,               /* trans_unit  */
  YYSYMBOL_top_level_exit = 143,           /* top_level_exit  */
  YYSYMBOL_top_level_decl = 144,           /* top_level_decl  */
  YYSYMBOL_func_def = 145,                 /* func_def  */
  YYSYMBOL_func_spec = 146,                /* func_spec  */
  YYSYMBOL_cmpnd_stemnt = 147,             /* cmpnd_stemnt  */
  YYSYMBOL_148_1 = 148,                    /* $@1  */
  YYSYMBOL_opt_stemnt_list = 149,          /* opt_stemnt_list  */
  YYSYMBOL_stemnt_list = 150,              /* stemnt_list  */
  YYSYMBOL_stemnt_list2 = 151,             /* stemnt_list2  */
  YYSYMBOL_stemnt = 152,                   /* stemnt  */
  YYSYMBOL_cmpnd_stemnt_reentrance = 153,  /* cmpnd_stemnt_reentrance  */
  YYSYMBOL_opt_stemnt_list_reentrance = 154, /* opt_stemnt_list_reentrance  */
  YYSYMBOL_stemnt_list_reentrance = 155,   /* stemnt_list_reentrance  */
  YYSYMBOL_stemnt_list_reentrance2 = 156,  /* stemnt_list_reentrance2  */
  YYSYMBOL_stemnt_reentrance = 157,        /* stemnt_reentrance  */
  YYSYMBOL_non_constructor_stemnt = 158,   /* non_constructor_stemnt  */
  YYSYMBOL_constructor_stemnt = 159,       /* constructor_stemnt  */
  YYSYMBOL_expr_stemnt = 160,              /* expr_stemnt  */
  YYSYMBOL_labeled_stemnt = 161,           /* labeled_stemnt  */
  YYSYMBOL_cond_stemnt = 162,              /* cond_stemnt  */
  YYSYMBOL_iter_stemnt = 163,              /* iter_stemnt  */
  YYSYMBOL_switch_stemnt = 164,            /* switch_stemnt  */
  YYSYMBOL_break_stemnt = 165,             /* break_stemnt  */
  YYSYMBOL_continue_stemnt = 166,          /* continue_stemnt  */
  YYSYMBOL_return_stemnt = 167,            /* return_stemnt  */
  YYSYMBOL_goto_stemnt = 168,              /* goto_stemnt  */
  YYSYMBOL_null_stemnt = 169,              /* null_stemnt  */
  YYSYMBOL_if_stemnt = 170,                /* if_stemnt  */
  YYSYMBOL_if_else_stemnt = 171,           /* if_else_stemnt  */
  YYSYMBOL_do_stemnt = 172,                /* do_stemnt  */
  YYSYMBOL_while_stemnt = 173,             /* while_stemnt  */
  YYSYMBOL_for_stemnt = 174,               /* for_stemnt  */
  YYSYMBOL_label = 175,                    /* label  */
  YYSYMBOL_named_label = 176,              /* named_label  */
  YYSYMBOL_case_label = 177,               /* case_label  */
  YYSYMBOL_deflt_label = 178,              /* deflt_label  */
  YYSYMBOL_cond_expr = 179,                /* cond_expr  */
  YYSYMBOL_assign_expr = 180,              /* assign_expr  */
  YYSYMBOL_opt_const_expr = 181,           /* opt_const_expr  */
  YYSYMBOL_const_expr = 182,               /* const_expr  */
  YYSYMBOL_opt_expr = 183,                 /* opt_expr  */
  YYSYMBOL_expr = 184,                     /* expr  */
  YYSYMBOL_log_or_expr = 185,              /* log_or_expr  */
  YYSYMBOL_log_and_expr = 186,             /* log_and_expr  */
  YYSYMBOL_log_neg_expr = 187,             /* log_neg_expr  */
  YYSYMBOL_bitwise_or_expr = 188,          /* bitwise_or_expr  */
  YYSYMBOL_bitwise_xor_expr = 189,         /* bitwise_xor_expr  */
  YYSYMBOL_bitwise_and_expr = 190,         /* bitwise_and_expr  */
  YYSYMBOL_bitwise_neg_expr = 191,         /* bitwise_neg_expr  */
  YYSYMBOL_cast_expr = 192,                /* cast_expr  */
  YYSYMBOL_equality_expr = 193,            /* equality_expr  */
  YYSYMBOL_relational_expr = 194,          /* relational_expr  */
  YYSYMBOL_shift_expr = 195,               /* shift_expr  */
  YYSYMBOL_additive_expr = 196,            /* additive_expr  */
  YYSYMBOL_mult_expr = 197,                /* mult_expr  */
  YYSYMBOL_constructor_expr = 198,         /* constructor_expr  */
  YYSYMBOL_iter_constructor_arg = 199,     /* iter_constructor_arg  */
  YYSYMBOL_iter_constructor_expr = 200,    /* iter_constructor_expr  */
  YYSYMBOL_unary_expr = 201,               /* unary_expr  */
  YYSYMBOL_sizeof_expr = 202,              /* sizeof_expr  */
  YYSYMBOL_indexof_expr = 203,             /* indexof_expr  */
  YYSYMBOL_unary_minus_expr = 204,         /* unary_minus_expr  */
  YYSYMBOL_unary_plus_expr = 205,          /* unary_plus_expr  */
  YYSYMBOL_addr_expr = 206,                /* addr_expr  */
  YYSYMBOL_indirection_expr = 207,         /* indirection_expr  */
  YYSYMBOL_preinc_expr = 208,              /* preinc_expr  */
  YYSYMBOL_predec_expr = 209,              /* predec_expr  */
  YYSYMBOL_comma_expr = 210,               /* comma_expr  */
  YYSYMBOL_prim_expr = 211,                /* prim_expr  */
  YYSYMBOL_paren_expr = 212,               /* paren_expr  */
  YYSYMBOL_postfix_expr = 213,             /* postfix_expr  */
  YYSYMBOL_subscript_expr = 214,           /* subscript_expr  */
  YYSYMBOL_comp_select_expr = 215,         /* comp_select_expr  */
  YYSYMBOL_postinc_expr = 216,             /* postinc_expr  */
  YYSYMBOL_postdec_expr = 217,             /* postdec_expr  */
  YYSYMBOL_field_ident = 218,              /* field_ident  */
  YYSYMBOL_direct_comp_select = 219,       /* direct_comp_select  */
  YYSYMBOL_indirect_comp_select = 220,     /* indirect_comp_select  */
  YYSYMBOL_func_call = 221,                /* func_call  */
  YYSYMBOL_opt_expr_list = 222,            /* opt_expr_list  */
  YYSYMBOL_expr_list = 223,                /* expr_list  */
  YYSYMBOL_add_op = 224,                   /* add_op  */
  YYSYMBOL_mult_op = 225,                  /* mult_op  */
  YYSYMBOL_equality_op = 226,              /* equality_op  */
  YYSYMBOL_relation_op = 227,              /* relation_op  */
  YYSYMBOL_shift_op = 228,                 /* shift_op  */
  YYSYMBOL_assign_op = 229,                /* assign_op  */
  YYSYMBOL_constant = 230,                 /* constant  */
  YYSYMBOL_opt_KnR_declaration_list = 231, /* opt_KnR_declaration_list  */
  YYSYMBOL_232_2 = 232,                    /* $@2  */
  YYSYMBOL_233_3 = 233,                    /* $@3  */
  YYSYMBOL_234_4 = 234,                    /* $@4  */
  YYSYMBOL_opt_declaration_list = 235,     /* opt_declaration_list  */
  YYSYMBOL_236_5 = 236,                    /* $@5  */
  YYSYMBOL_237_6 = 237,                    /* $@6  */
  YYSYMBOL_238_7 = 238,                    /* $@7  */
  YYSYMBOL_declaration_list = 239,         /* declaration_list  */
  YYSYMBOL_decl_stemnt = 240,              /* decl_stemnt  */
  YYSYMBOL_old_style_declaration = 241,    /* old_style_declaration  */
  YYSYMBOL_declaration = 242,              /* declaration  */
  YYSYMBOL_no_decl_specs = 243,            /* no_decl_specs  */
  YYSYMBOL_decl_specs = 244,               /* decl_specs  */
  YYSYMBOL_abs_decl = 245,                 /* abs_decl  */
  YYSYMBOL_type_name = 246,                /* type_name  */
  YYSYMBOL_247_8 = 247,                    /* $@8  */
  YYSYMBOL_type_name_bis = 248,            /* type_name_bis  */
  YYSYMBOL_decl_specs_reentrance_bis = 249, /* decl_specs_reentrance_bis  */
  YYSYMBOL_local_or_global_storage_class = 250, /* local_or_global_storage_class  */
  YYSYMBOL_local_storage_class = 251,      /* local_storage_class  */
  YYSYMBOL_storage_class = 252,            /* storage_class  */
  YYSYMBOL_type_spec = 253,                /* type_spec  */
  YYSYMBOL_opt_decl_specs_reentrance = 254, /* opt_decl_specs_reentrance  */
  YYSYMBOL_decl_specs_reentrance = 255,    /* decl_specs_reentrance  */
  YYSYMBOL_256_9 = 256,                    /* $@9  */
  YYSYMBOL_opt_comp_decl_specs = 257,      /* opt_comp_decl_specs  */
  YYSYMBOL_comp_decl_specs_reentrance = 258, /* comp_decl_specs_reentrance  */
  YYSYMBOL_259_10 = 259,                   /* $@10  */
  YYSYMBOL_comp_decl_specs = 260,          /* comp_decl_specs  */
  YYSYMBOL_decl = 261,                     /* decl  */
  YYSYMBOL_262_11 = 262,                   /* $@11  */
  YYSYMBOL_init_decl = 263,                /* init_decl  */
  YYSYMBOL_opt_init_decl_list = 264,       /* opt_init_decl_list  */
  YYSYMBOL_init_decl_list = 265,           /* init_decl_list  */
  YYSYMBOL_init_decl_list_reentrance = 266, /* init_decl_list_reentrance  */
  YYSYMBOL_initializer = 267,              /* initializer  */
  YYSYMBOL_initializer_list = 268,         /* initializer_list  */
  YYSYMBOL_initializer_reentrance = 269,   /* initializer_reentrance  */
  YYSYMBOL_opt_comma = 270,                /* opt_comma  */
  YYSYMBOL_type_qual = 271,                /* type_qual  */
  YYSYMBOL_type_qual_token = 272,          /* type_qual_token  */
  YYSYMBOL_type_qual_list = 273,           /* type_qual_list  */
  YYSYMBOL_opt_type_qual_list = 274,       /* opt_type_qual_list  */
  YYSYMBOL_type_spec_reentrance = 275,     /* type_spec_reentrance  */
  YYSYMBOL_typedef_name = 276,             /* typedef_name  */
  YYSYMBOL_tag_ref = 277,                  /* tag_ref  */
  YYSYMBOL_struct_tag_ref = 278,           /* struct_tag_ref  */
  YYSYMBOL_union_tag_ref = 279,            /* union_tag_ref  */
  YYSYMBOL_enum_tag_ref = 280,             /* enum_tag_ref  */
  YYSYMBOL_struct_tag_def = 281,           /* struct_tag_def  */
  YYSYMBOL_struct_type_define = 282,       /* struct_type_define  */
  YYSYMBOL_union_tag_def = 283,            /* union_tag_def  */
  YYSYMBOL_union_type_define = 284,        /* union_type_define  */
  YYSYMBOL_enum_tag_def = 285,             /* enum_tag_def  */
  YYSYMBOL_enum_type_define = 286,         /* enum_type_define  */
  YYSYMBOL_struct_or_union_definition = 287, /* struct_or_union_definition  */
  YYSYMBOL_enum_definition = 288,          /* enum_definition  */
  YYSYMBOL_opt_trailing_comma = 289,       /* opt_trailing_comma  */
  YYSYMBOL_enum_def_list = 290,            /* enum_def_list  */
  YYSYMBOL_enum_def_list_reentrance = 291, /* enum_def_list_reentrance  */
  YYSYMBOL_enum_const_def = 292,           /* enum_const_def  */
  YYSYMBOL_enum_constant = 293,            /* enum_constant  */
  YYSYMBOL_field_list = 294,               /* field_list  */
  YYSYMBOL_295_12 = 295,                   /* $@12  */
  YYSYMBOL_296_13 = 296,                   /* $@13  */
  YYSYMBOL_field_list_reentrance = 297,    /* field_list_reentrance  */
  YYSYMBOL_comp_decl = 298,                /* comp_decl  */
  YYSYMBOL_comp_decl_list = 299,           /* comp_decl_list  */
  YYSYMBOL_comp_decl_list_reentrance = 300, /* comp_decl_list_reentrance  */
  YYSYMBOL_301_14 = 301,                   /* $@14  */
  YYSYMBOL_302_15 = 302,                   /* $@15  */
  YYSYMBOL_comp_declarator = 303,          /* comp_declarator  */
  YYSYMBOL_simple_comp = 304,              /* simple_comp  */
  YYSYMBOL_bit_field = 305,                /* bit_field  */
  YYSYMBOL_306_16 = 306,                   /* $@16  */
  YYSYMBOL_width = 307,                    /* width  */
  YYSYMBOL_opt_declarator = 308,           /* opt_declarator  */
  YYSYMBOL_declarator = 309,               /* declarator  */
  YYSYMBOL_func_declarator = 310,          /* func_declarator  */
  YYSYMBOL_declarator_reentrance_bis = 311, /* declarator_reentrance_bis  */
  YYSYMBOL_direct_declarator_reentrance_bis = 312, /* direct_declarator_reentrance_bis  */
  YYSYMBOL_direct_declarator_reentrance = 313, /* direct_declarator_reentrance  */
  YYSYMBOL_array_decl = 314,               /* array_decl  */
  YYSYMBOL_stream_decl = 315,              /* stream_decl  */
  YYSYMBOL_dimension_constraint = 316,     /* dimension_constraint  */
  YYSYMBOL_comma_constants = 317,          /* comma_constants  */
  YYSYMBOL_pointer_start = 318,            /* pointer_start  */
  YYSYMBOL_pointer_reentrance = 319,       /* pointer_reentrance  */
  YYSYMBOL_pointer = 320,                  /* pointer  */
  YYSYMBOL_ident_list = 321,               /* ident_list  */
  YYSYMBOL_322_17 = 322,                   /* $@17  */
  YYSYMBOL_ident_list_reentrance = 323,    /* ident_list_reentrance  */
  YYSYMBOL_ident = 324,                    /* ident  */
  YYSYMBOL_typename_as_ident = 325,        /* typename_as_ident  */
  YYSYMBOL_any_ident = 326,                /* any_ident  */
  YYSYMBOL_opt_param_type_list = 327,      /* opt_param_type_list  */
  YYSYMBOL_328_18 = 328,                   /* $@18  */
  YYSYMBOL_param_type_list = 329,          /* param_type_list  */
  YYSYMBOL_330_19 = 330,                   /* $@19  */
  YYSYMBOL_param_type_list_bis = 331,      /* param_type_list_bis  */
  YYSYMBOL_param_list = 332,               /* param_list  */
  YYSYMBOL_param_decl = 333,               /* param_decl  */
  YYSYMBOL_334_20 = 334,                   /* $@20  */
  YYSYMBOL_param_decl_bis = 335,           /* param_decl_bis  */
  YYSYMBOL_abs_decl_reentrance = 336,      /* abs_decl_reentrance  */
  YYSYMBOL_direct_abs_decl_reentrance_bis = 337, /* direct_abs_decl_reentrance_bis  */
  YYSYMBOL_direct_abs_decl_reentrance = 338, /* direct_abs_decl_reentrance  */
  YYSYMBOL_opt_gcc_attrib = 339,           /* opt_gcc_attrib  */
  YYSYMBOL_gcc_attrib = 340,               /* gcc_attrib  */
  YYSYMBOL_gcc_inner = 341                 /* gcc_inner  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  87
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2026

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  140
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  202
/* YYNRULES -- Number of rules.  */
#define YYNRULES  422
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  665

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   394


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   245,   245,   252,   263,   271,   276,   283,   291,   295,
     299,   303,   307,   311,   317,   345,   374,   408,   407,   425,
     432,   435,   438,   457,   460,   469,   485,   488,   498,   505,
     508,   511,   530,   533,   542,   553,   557,   563,   564,   565,
     566,   567,   568,   569,   570,   571,   572,   573,   574,   597,
     604,   611,   625,   626,   629,   630,   631,   634,   643,   651,
     659,   667,   675,   682,   691,   701,   712,   721,   733,   734,
     735,   738,   746,   751,   759,   775,   776,   784,   785,   789,
     796,   799,   802,   806,   809,   812,   815,   816,   823,   824,
     831,   838,   839,   846,   847,   854,   855,   862,   869,   870,
     878,   879,   885,   886,   892,   893,   899,   900,   906,   907,
     913,   921,   931,   942,   946,   950,   954,   962,   972,   983,
     991,  1001,  1012,  1016,  1020,  1024,  1028,  1032,  1038,  1039,
    1042,  1059,  1065,  1066,  1067,  1068,  1069,  1070,  1071,  1072,
    1073,  1074,  1077,  1084,  1091,  1095,  1115,  1121,  1128,  1135,
    1141,  1148,  1155,  1156,  1163,  1167,  1168,  1174,  1180,  1188,
    1189,  1190,  1191,  1192,  1193,  1194,  1198,  1206,  1207,  1210,
    1217,  1224,  1228,  1251,  1274,  1288,  1291,  1294,  1295,  1296,
    1303,  1312,  1313,  1316,  1317,  1318,  1321,  1324,  1325,  1326,
    1329,  1330,  1333,  1334,  1337,  1338,  1339,  1340,  1341,  1342,
    1351,  1351,  1362,  1367,  1362,  1383,  1383,  1393,  1398,  1393,
    1406,  1411,  1424,  1431,  1439,  1457,  1483,  1489,  1498,  1502,
    1502,  1513,  1522,  1545,  1551,  1552,  1553,  1556,  1557,  1560,
    1561,  1571,  1575,  1578,  1581,  1597,  1597,  1626,  1647,  1650,
    1653,  1653,  1663,  1678,  1688,  1687,  1698,  1699,  1707,  1710,
    1713,  1716,  1720,  1736,  1738,  1743,  1751,  1752,  1753,  1754,
    1763,  1766,  1778,  1781,  1782,  1783,  1784,  1785,  1786,  1787,
    1795,  1796,  1805,  1808,  1815,  1816,  1817,  1818,  1819,  1820,
    1821,  1822,  1823,  1824,  1825,  1826,  1827,  1828,  1829,  1830,
    1831,  1832,  1833,  1834,  1835,  1836,  1837,  1838,  1839,  1840,
    1841,  1842,  1843,  1844,  1845,  1846,  1847,  1848,  1849,  1853,
    1860,  1868,  1878,  1888,  1898,  1908,  1915,  1939,  1949,  1957,
    1982,  1992,  1999,  2023,  2026,  2030,  2033,  2039,  2042,  2052,
    2055,  2060,  2068,  2080,  2094,  2101,  2106,  2101,  2120,  2126,
    2134,  2142,  2147,  2158,  2162,  2162,  2170,  2169,  2181,  2199,
    2210,  2214,  2213,  2233,  2237,  2240,  2247,  2253,  2259,  2264,
    2267,  2270,  2275,  2281,  2282,  2283,  2299,  2315,  2339,  2353,
    2364,  2368,  2372,  2376,  2383,  2386,  2390,  2400,  2406,  2407,
    2414,  2421,  2421,  2436,  2441,  2453,  2457,  2469,  2470,  2479,
    2483,  2483,  2492,  2492,  2505,  2506,  2515,  2516,  2524,  2524,
    2534,  2550,  2567,  2580,  2584,  2588,  2595,  2598,  2602,  2606,
    2616,  2620,  2637,  2640,  2643,  2654,  2658,  2662,  2666,  2670,
    2674,  2688,  2697
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "IDENT", "TAG_NAME",
  "LABEL_NAME", "TYPEDEF_NAME", "STRING", "LSTRING", "CHAR_CONST",
  "LCHAR_CONST", "INUM", "RNUM", "PP_DIR", "PP_LINE", "INVALID", "CONST",
  "VOLATILE", "OUT", "REDUCE", "VOUT", "ITER", "KERNEL", "AUTO", "EXTRN",
  "REGISTR", "STATIC", "TYPEDEF", "VOID", "SHORT", "INT", "LONG", "SGNED",
  "UNSGNED", "FLOAT", "FLOAT2", "FLOAT3", "FLOAT4", "FIXED", "FIXED2",
  "FIXED3", "FIXED4", "SHORTFIXED", "SHORTFIXED2", "SHORTFIXED3",
  "SHORTFIXED4", "DOUBLE", "DOUBLE2", "CHAR", "CHAR2", "CHAR3", "CHAR4",
  "UCHAR", "UCHAR2", "UCHAR3", "UCHAR4", "ENUM", "STRUCT", "UNION",
  "BREAK", "CASE", "CONT", "DEFLT", "DO", "ELSE", "IF", "FOR", "GOTO",
  "RETURN", "SWITCH", "WHILE", "PLUS_EQ", "MINUS_EQ", "STAR_EQ", "DIV_EQ",
  "MOD_EQ", "B_AND_EQ", "B_OR_EQ", "B_XOR_EQ", "L_SHIFT_EQ", "R_SHIFT_EQ",
  "EQUAL", "LESS_EQ", "GRTR_EQ", "NOT_EQ", "RPAREN", "RBRCKT", "LBRACE",
  "RBRACE", "SEMICOLON", "COMMA", "ELLIPSIS", "LB_SIGN", "DOUB_LB_SIGN",
  "BACKQUOTE", "AT", "ATTRIBUTE", "ALIGNED", "PACKED", "CDECL", "MODE",
  "FORMAT", "NORETURN", "COMMA_OP", "EQ", "ASSIGN", "QUESTMARK", "COLON",
  "COMMA_SEP", "OR", "AND", "B_OR", "B_XOR", "B_AND", "COMP_EQ",
  "COMP_ARITH", "COMP_LESS", "COMP_GRTR", "LESS", "GRTR", "L_SHIFT",
  "R_SHIFT", "PLUS", "MINUS", "STAR", "DIV", "MOD", "CAST", "UNARY", "NOT",
  "B_NOT", "SIZEOF", "INDEXOF", "INCR", "DECR", "HYPERUNARY", "ARROW",
  "DOT", "LPAREN", "LBRCKT", "$accept", "program", "trans_unit",
  "top_level_exit", "top_level_decl", "func_def", "func_spec",
  "cmpnd_stemnt", "$@1", "opt_stemnt_list", "stemnt_list", "stemnt_list2",
  "stemnt", "cmpnd_stemnt_reentrance", "opt_stemnt_list_reentrance",
  "stemnt_list_reentrance", "stemnt_list_reentrance2", "stemnt_reentrance",
  "non_constructor_stemnt", "constructor_stemnt", "expr_stemnt",
  "labeled_stemnt", "cond_stemnt", "iter_stemnt", "switch_stemnt",
  "break_stemnt", "continue_stemnt", "return_stemnt", "goto_stemnt",
  "null_stemnt", "if_stemnt", "if_else_stemnt", "do_stemnt",
  "while_stemnt", "for_stemnt", "label", "named_label", "case_label",
  "deflt_label", "cond_expr", "assign_expr", "opt_const_expr",
  "const_expr", "opt_expr", "expr", "log_or_expr", "log_and_expr",
  "log_neg_expr", "bitwise_or_expr", "bitwise_xor_expr",
  "bitwise_and_expr", "bitwise_neg_expr", "cast_expr", "equality_expr",
  "relational_expr", "shift_expr", "additive_expr", "mult_expr",
  "constructor_expr", "iter_constructor_arg", "iter_constructor_expr",
  "unary_expr", "sizeof_expr", "indexof_expr", "unary_minus_expr",
  "unary_plus_expr", "addr_expr", "indirection_expr", "preinc_expr",
  "predec_expr", "comma_expr", "prim_expr", "paren_expr", "postfix_expr",
  "subscript_expr", "comp_select_expr", "postinc_expr", "postdec_expr",
  "field_ident", "direct_comp_select", "indirect_comp_select", "func_call",
  "opt_expr_list", "expr_list", "add_op", "mult_op", "equality_op",
  "relation_op", "shift_op", "assign_op", "constant",
  "opt_KnR_declaration_list", "$@2", "$@3", "$@4", "opt_declaration_list",
  "$@5", "$@6", "$@7", "declaration_list", "decl_stemnt",
  "old_style_declaration", "declaration", "no_decl_specs", "decl_specs",
  "abs_decl", "type_name", "$@8", "type_name_bis",
  "decl_specs_reentrance_bis", "local_or_global_storage_class",
  "local_storage_class", "storage_class", "type_spec",
  "opt_decl_specs_reentrance", "decl_specs_reentrance", "$@9",
  "opt_comp_decl_specs", "comp_decl_specs_reentrance", "$@10",
  "comp_decl_specs", "decl", "$@11", "init_decl", "opt_init_decl_list",
  "init_decl_list", "init_decl_list_reentrance", "initializer",
  "initializer_list", "initializer_reentrance", "opt_comma", "type_qual",
  "type_qual_token", "type_qual_list", "opt_type_qual_list",
  "type_spec_reentrance", "typedef_name", "tag_ref", "struct_tag_ref",
  "union_tag_ref", "enum_tag_ref", "struct_tag_def", "struct_type_define",
  "union_tag_def", "union_type_define", "enum_tag_def", "enum_type_define",
  "struct_or_union_definition", "enum_definition", "opt_trailing_comma",
  "enum_def_list", "enum_def_list_reentrance", "enum_const_def",
  "enum_constant", "field_list", "$@12", "$@13", "field_list_reentrance",
  "comp_decl", "comp_decl_list", "comp_decl_list_reentrance", "$@14",
  "$@15", "comp_declarator", "simple_comp", "bit_field", "$@16", "width",
  "opt_declarator", "declarator", "func_declarator",
  "declarator_reentrance_bis", "direct_declarator_reentrance_bis",
  "direct_declarator_reentrance", "array_decl", "stream_decl",
  "dimension_constraint", "comma_constants", "pointer_start",
  "pointer_reentrance", "pointer", "ident_list", "$@17",
  "ident_list_reentrance", "ident", "typename_as_ident", "any_ident",
  "opt_param_type_list", "$@18", "param_type_list", "$@19",
  "param_type_list_bis", "param_list", "param_decl", "$@20",
  "param_decl_bis", "abs_decl_reentrance",
  "direct_abs_decl_reentrance_bis", "direct_abs_decl_reentrance",
  "opt_gcc_attrib", "gcc_attrib", "gcc_inner", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-471)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-390)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     583,    67,  -471,  -471,  -471,  -471,  -471,  -471,  -471,   -64,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,    43,    54,    65,    81,   709,
    -471,  -471,    50,  -471,    -3,     8,   114,   114,  -471,  -471,
    -471,  1915,  -471,  -471,  1915,  -471,  -471,  -471,  -471,  -471,
    -471,    59,  -471,    72,  -471,    80,  -471,  -471,  -471,  1666,
    -471,    92,   112,    32,   117,    32,   125,  -471,    67,  -471,
    -471,    91,  -471,  -471,  -471,  -471,  -471,   253,   114,    87,
    -471,  -471,  -471,   130,   172,  -471,  -471,    31,  -471,  -471,
    -471,   122,    33,  -471,  -471,  -471,    56,   135,  -471,  -471,
    1915,  -471,    32,    32,    92,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  1666,  1666,  1666,  1666,  1666,  1666,  1688,    51,
    1712,  1712,   833,  -471,  -471,   116,  -471,  -471,    71,   127,
    -471,   140,   148,   150,  -471,  -471,   151,   113,   -30,    73,
     196,   104,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
     177,  -471,  -471,   160,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,   189,   222,  -471,  -471,   209,  -471,
    -471,  -471,   227,  -471,  -471,   228,  -471,  -471,  1320,  -471,
     253,  -471,   238,  1631,   114,  -471,  -471,  -471,   229,   233,
      49,  1666,  -471,  -471,  -471,  -471,   236,   239,   240,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,   967,  -471,   323,  -471,
     329,  -471,  -471,   244,   246,   248,   -39,  -471,  1666,  1666,
    1666,  1666,  1666,  1666,  -471,  1666,  -471,  -471,  -471,  1666,
    -471,  -471,  1666,  -471,  -471,  1666,  -471,  -471,  -471,  1666,
    -471,  -471,  1653,  1666,  -471,  -471,    92,    92,  1653,  1666,
    -471,    92,  -471,  1666,  -471,  1968,  -471,  1333,  -471,  -471,
    -471,  -471,   197,   204,   205,   206,   208,   210,   211,   212,
     213,   214,  1631,  -471,  -471,  -471,  -471,  -471,  -471,  1915,
     215,  -471,  -471,   329,   160,   -41,   -40,  -471,   -23,    -6,
    -471,   249,   323,   260,  -471,   261,  -471,  -471,  -471,   270,
    1915,   271,  -471,  -471,  1666,   179,  1666,  -471,   110,  -471,
    -471,   115,   250,   127,   140,   148,   150,   151,   113,   -30,
      73,   196,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,   274,   272,   275,  -471,  -471,  -471,   277,  1968,  -471,
    1841,   278,   169,   279,  1666,   280,  -471,  1237,   232,   234,
     368,  1666,   237,   241,  1320,  -471,   288,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,   273,  -471,  -471,  -471,   289,
     276,  1915,    52,   533,   556,   690,   779,   913,  1403,  1429,
    1486,  1499,   287,  -471,  -471,   292,   114,   247,   297,   233,
    -471,  -471,   294,  -471,  -471,  -471,   296,  -471,  1915,  -471,
    -471,  -471,  -471,  -471,   304,  -471,   305,   308,  -471,   311,
    1666,  1666,  -471,  1653,  -471,  -471,   307,   114,  -471,  -471,
    1968,  -471,   309,  -471,  -471,  -471,  -471,   310,  -471,   330,
    -471,  -471,   313,  1666,  1666,   314,   315,  -471,  1666,  1666,
    1416,  -471,  1057,  1237,  -471,  -471,   320,  -471,  -471,   316,
     322,   318,   324,   321,   325,   331,   327,   333,   328,   335,
     334,   337,   344,   341,   348,   345,   352,   349,  1631,   350,
    1915,   207,     2,  -471,   323,   353,    68,  -471,  -471,  -471,
    -471,  -471,   355,   357,  -471,  -471,  -471,  -471,   229,  -471,
    -471,   338,   339,  -471,  -471,  1666,   303,  -471,   362,   326,
    -471,  -471,   370,   371,   369,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  1653,  -471,  1666,  -471,  1666,  -471,  1666,  -471,
    1666,  -471,  1666,  -471,  1666,  -471,  1666,  -471,  1666,  -471,
    1666,  -471,  -471,  -471,  -471,   332,  -471,  -471,   336,   340,
    -471,   379,  -471,  -471,  -471,    79,  -471,    62,  -471,  -471,
    -471,   114,  -471,  -471,  -471,  1666,  1237,  1666,  1237,  1237,
    -471,  1147,   380,   381,   378,   382,   384,   383,   385,   386,
     389,   391,   388,   323,   323,   397,   229,  1666,   398,   412,
     395,  -471,  -471,  -471,  -471,  -471,  -471,  1666,  1666,  -471,
    1666,  1666,  -471,  1666,  1666,   400,   401,   399,  -471,  -471,
    -471,  -471,   402,  1237,  1666,   403,   405,   407,   408,   416,
     413,  -471,  -471,   476,  -471,  -471,   417,  -471,  1666,  -471,
    1666,  -471,  1666,   414,  1237,   420,   421,   422,   479,  -471,
    -471,  -471,  -471,   423,  -471
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,     4,   309,    10,    11,   263,   264,   265,   266,     0,
     267,   268,   227,   224,   228,   225,   226,   281,   283,   284,
     285,   300,   301,   286,   287,   288,   289,   290,   291,   292,
     293,   294,   295,   296,   297,   298,   299,   282,   302,   303,
     304,   305,   306,   307,   308,     0,     0,     0,     0,     0,
       7,     9,     0,     8,     0,     0,   248,   248,   217,   229,
     230,   232,   235,   223,   232,   262,   231,   280,   278,   279,
     277,     0,   275,     0,   276,     0,   274,     7,    12,    80,
     310,   325,   313,   335,   311,   335,   312,     1,     0,     7,
       5,     0,    17,    14,   212,   213,   385,   272,     0,   246,
     251,   214,   249,   250,   202,   356,   359,   360,   363,   364,
     378,   380,     0,   361,   215,   244,   202,   357,   234,   233,
     232,   237,   335,   335,   325,    13,   198,   199,   196,   197,
     194,   195,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    77,   152,     0,    81,    82,    75,    86,
     136,    88,    91,    93,   137,   108,    95,   100,   102,   104,
     106,    98,   133,   165,   134,   135,   138,   139,   140,   141,
      85,   159,   155,   132,   160,   161,   163,   164,   167,   168,
     162,   156,   154,   386,     0,   327,   329,   330,   332,   387,
     388,   334,     0,   324,   336,     0,     6,    19,   207,   270,
     273,   377,     0,     0,     0,    16,   201,   203,   412,   374,
     392,    80,   379,   358,    15,   236,     0,     0,     0,   148,
      98,   147,   146,   149,    90,    97,     0,   143,     0,   144,
       0,   150,   151,     0,     0,     0,   221,   269,     0,     0,
       0,     0,     0,     0,   186,     0,   187,   188,   189,     0,
     190,   191,     0,   181,   182,     0,   183,   184,   185,     0,
     192,   193,     0,     0,   169,   170,     0,     0,   175,     0,
     321,   328,   326,     0,   315,     0,   318,     0,   206,   208,
     271,   362,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   256,   257,   258,   247,   253,   252,     0,
       0,   245,   413,     0,     0,   162,   156,   375,     0,   154,
     367,     0,     0,     0,   398,     0,   316,   319,   322,     0,
       0,     0,   158,   157,     0,   390,    80,   222,   403,   218,
     404,   406,     0,    87,    89,    92,    94,    96,   101,   103,
     105,   107,   109,    78,    79,   153,   173,   171,   172,   177,
     178,     0,   176,     0,   331,   333,   243,   344,   238,   240,
     337,     0,     0,     0,     0,     0,    74,     0,     0,     0,
       0,    83,     0,     0,   207,    62,     0,    21,    39,    23,
      37,    38,    40,    41,    42,    43,    44,    45,    46,    47,
      52,    53,    54,    55,    56,     0,    68,    69,    70,     0,
     154,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   260,   254,   204,     0,   248,     0,     0,     0,
     369,   366,   382,   383,   365,   393,   394,   396,     0,   368,
     142,   220,   145,    99,     0,   398,     0,     0,   405,   390,
      80,     0,   174,     0,   166,   341,   343,   354,   242,   239,
     238,   339,     0,   338,    28,    48,    58,    72,    59,     0,
      35,    36,     0,     0,    83,     0,     0,    84,     0,     0,
       0,    18,     0,     0,    50,   209,     0,   128,   129,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   261,     0,
     210,   415,   157,   376,     0,   398,   402,   399,   410,   391,
     407,   408,     0,     0,    76,   179,   180,   346,   412,   348,
     349,     0,   350,   241,   340,     0,     0,    49,     0,     0,
      61,    60,     0,     0,     0,    30,    32,    25,    24,    26,
      51,   131,     0,   113,     0,   114,     0,   115,     0,   122,
       0,   123,     0,   124,     0,   125,     0,   126,     0,   127,
       0,   255,   259,   211,   418,     0,   416,   417,     0,     0,
     419,     0,   384,   395,   397,   390,   400,   403,   401,   411,
     409,   354,   345,   351,    73,     0,     0,    83,     0,     0,
      27,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   412,     0,     0,    63,
       0,    57,    66,    34,    33,   130,   110,     0,     0,   116,
       0,     0,   119,     0,     0,     0,     0,     0,   414,   347,
     353,   352,     0,     0,    83,     0,     0,     0,     0,     0,
       0,   420,   421,     0,    65,    64,     0,   111,     0,   117,
       0,   120,     0,     0,     0,     0,     0,     0,     0,    67,
     112,   118,   121,     0,   422
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -471,  -471,  -471,    89,   461,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -428,  -251,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -420,
    -124,  -193,  -337,  -436,   -76,  -471,   283,  -471,   291,   282,
     285,  -471,  -102,   269,   268,   265,   263,   262,  -188,   -26,
    -471,  -127,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -471,  -471,  -471,  -187,  -471,  -471,  -471,  -471,   251,  -471,
    -471,  -186,  -471,  -471,  -471,  -471,  -471,  -471,  -471,  -471,
    -185,   430,  -471,  -471,  -471,   163,  -471,  -471,  -471,  -355,
    -471,  -471,    78,  -471,    93,  -471,  -471,  -471,   218,  -126,
    -471,  -471,  -471,  -471,   -24,     9,  -471,    99,  -319,  -471,
    -471,  -471,  -471,   346,   483,  -471,  -471,  -471,  -471,  -263,
    -471,  -237,   -60,  -471,  -471,  -234,  -471,   245,  -471,  -471,
    -471,  -471,  -471,  -471,  -471,  -471,  -471,   -13,   427,  -471,
    -471,  -471,   281,  -471,  -471,  -471,  -471,  -471,   193,  -471,
    -471,  -471,  -471,   -27,  -471,  -471,  -471,  -471,  -471,   -52,
    -471,   -55,  -100,  -471,  -471,  -471,   136,  -471,   445,  -471,
    -217,  -471,  -471,  -471,   -56,  -471,    34,   121,  -471,  -471,
    -471,   126,  -471,    57,  -471,  -471,  -216,  -311,  -471,  -470,
    -471,  -471
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    48,    49,    90,    50,    51,    52,    93,   198,   376,
     377,   472,   538,   378,   534,   535,   591,   459,   460,   461,
     380,   381,   382,   383,   384,   385,   386,   387,   388,   389,
     390,   391,   392,   393,   394,   395,   396,   397,   398,   143,
     144,   145,   146,   466,   399,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   462,   479,
     295,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   346,   178,
     179,   180,   351,   352,   255,   259,   245,   249,   252,   262,
     181,   205,   206,   207,   299,   277,   278,   279,   401,   414,
      53,    54,   415,    56,   416,   327,   319,   320,   235,    58,
      59,    60,    61,    62,   118,    63,   120,   448,   356,   450,
     357,    99,   208,   100,   114,   102,   103,   296,   412,   297,
     499,    64,    65,   200,   201,    66,    67,    82,    68,    69,
      70,    71,    72,    73,    74,    75,    76,   192,   184,   272,
     185,   186,   187,   188,   193,   194,   275,   360,   361,   445,
     446,   447,   581,   518,   519,   520,   607,   631,   521,   115,
     116,   105,   106,   107,   108,   109,   307,   308,   110,   111,
     112,   311,   312,   422,   182,   190,   191,   434,   435,   313,
     314,   425,   426,   427,   428,   507,   436,   330,   331,   301,
     302,   571
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     113,   113,   117,   147,   104,   220,   220,   220,   220,   220,
     220,   227,   213,   231,   232,   294,   236,   438,   315,   328,
     329,   514,   304,   305,   306,   189,   379,   457,   529,   413,
     219,   221,   222,   223,   224,   225,    96,   199,   358,   449,
     121,   359,   113,   202,   539,   540,   475,    80,   582,  -371,
    -370,    91,  -381,   476,    96,    96,   113,  -200,    80,   126,
     127,   128,   129,   130,   131,    96,   234,   419,   189,    80,
     119,    96,   195,   119,   344,    79,  -371,  -370,    55,   293,
     350,    87,    96,   229,  -373,    97,    94,   283,   284,   285,
     250,   251,  -372,    57,   420,    96,   215,    95,   183,   325,
     326,   286,   287,   288,   294,   289,   290,   291,   328,   216,
     217,  -373,   220,   220,   220,   220,   220,    96,   220,  -372,
    -323,   358,   220,   358,   359,   220,   359,    55,   220,   119,
      81,   449,   220,   437,   310,   147,   629,    92,   343,   345,
     280,    83,    57,  -200,   349,   563,   122,   209,   113,   355,
     234,   610,    85,   309,   234,    77,    78,   342,   609,   123,
     611,   612,   332,   614,  -389,   132,   125,   124,   293,   210,
     211,    98,   321,  -200,   133,   134,   135,   238,   196,   197,
     239,   136,   137,   138,   139,   140,   141,   630,   584,   228,
     142,   203,    97,   353,   236,   253,   254,   220,   646,  -320,
     575,   326,   237,    97,  -314,   645,   575,   326,   260,   261,
     189,   189,  -317,   358,   478,   189,   359,   575,   326,   536,
     204,   400,   433,   564,  -356,  -356,   659,   418,   246,   247,
     248,  -356,   304,   305,   306,   561,    96,   240,    97,  -356,
     126,   127,   128,   129,   130,   131,    97,   513,   325,   326,
     147,   241,    98,   439,   440,   516,   423,   454,   455,  -200,
     242,  -244,  -244,   243,  -389,   244,   438,   263,  -244,     5,
       6,     7,     8,     9,    10,    11,  -244,   270,   477,   481,
     483,   485,   487,   489,   491,   493,   495,   497,   147,   577,
     578,    84,    86,   264,   265,   467,   266,   267,   268,   269,
     347,   347,   506,    97,   565,   566,   567,   568,   569,   570,
     294,   400,   271,   273,   220,   274,   276,   325,   326,   515,
     256,   257,   258,   281,   316,   300,    96,   317,   318,   322,
     233,   323,    96,   324,   421,   402,   126,   127,   128,   129,
     130,   131,   403,   404,   405,   424,   406,   429,   407,   408,
     409,   410,   411,   417,   478,   430,   432,   441,   577,   442,
     113,   444,   443,   309,   147,   139,  -342,   453,   456,   458,
     463,   303,   464,   465,   293,   468,   471,   498,   474,   469,
     473,   500,   502,   -71,   504,   501,   505,   528,   467,   508,
     510,   113,   532,   533,   511,   522,  -389,   517,   524,   625,
     526,   525,   527,   530,   531,   541,   542,   543,   544,   545,
     547,   546,   549,   551,   400,   587,   400,   400,   477,   553,
     593,   548,   594,   550,   595,   552,   596,   554,   597,   555,
     598,   556,   599,   557,   600,   558,   601,   559,   562,   560,
     579,   585,   132,   580,   573,   583,  -355,   586,   572,   147,
     113,   133,   134,   135,   576,   588,   589,   590,   136,   137,
     138,   139,   140,   141,   605,   615,   616,   142,   617,   619,
     602,   622,   618,   620,   603,   621,   633,   213,   604,   623,
     220,   624,   628,   632,   634,   641,   642,   653,   647,   643,
     663,   644,   649,   635,   636,   648,   637,   638,   650,   639,
     640,   651,   654,   652,   658,   660,   661,   662,   664,   608,
      89,   467,   337,   338,   339,   340,   592,   341,   348,   113,
     202,   113,   333,   335,   655,   113,   656,   336,   657,   522,
     400,   334,   400,   400,   480,   400,    96,   470,   431,   101,
     126,   127,   128,   129,   130,   131,   214,   626,   627,   523,
     298,   218,   354,   452,   606,   503,   212,   482,   467,    96,
     512,   509,   574,   126,   127,   128,   129,   130,   131,     0,
       0,     0,     0,     0,     0,     0,     0,   400,     0,     0,
       0,     0,     0,    -2,     1,     0,  -216,     0,     0,     2,
       0,     0,     0,     0,     0,     0,     3,     4,   400,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,     0,     0,     0,     0,   132,     0,     0,     0,
       0,     0,     0,     0,     0,   133,   134,   135,     0,     0,
       0,     0,   136,   137,   138,   139,   140,   141,     0,   132,
       0,   142,  -216,     0,     0,     0,     0,     0,   133,   134,
     135,     0,     0,     0,     0,   136,   137,   138,   139,   140,
     141,   484,     0,    96,   142,     0,     0,   126,   127,   128,
     129,   130,   131,     0,     0,     0,     0,  -216,     0,    -3,
      88,     0,  -216,     0,     0,     2,     0,     0,     0,     0,
       0,  -216,     3,     4,     0,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     486,     0,    96,     0,     0,     0,   126,   127,   128,   129,
     130,   131,     0,     0,     0,     0,     0,     0,  -216,     0,
       0,     0,     0,   132,     0,     0,     0,     0,     0,     0,
       0,     0,   133,   134,   135,     0,     0,     0,     0,   136,
     137,   138,   139,   140,   141,     0,     0,     0,   142,     0,
       0,     0,     0,  -216,   233,     0,    96,     0,     0,     2,
     126,   127,   128,   129,   130,   131,     0,  -216,     0,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,   132,     0,     0,     0,     0,     0,     0,     0,
       0,   133,   134,   135,     0,     0,     0,     0,   136,   137,
     138,   139,   140,   141,   488,     0,    96,   142,     0,     0,
     126,   127,   128,   129,   130,   131,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   132,     0,     0,     0,
       0,     0,     0,     0,     0,   133,   134,   135,     0,     0,
       0,     0,   136,   137,   138,   139,   140,   141,   233,     0,
      96,   142,     0,  -219,   126,   127,   128,   129,   130,   131,
       0,     0,     0,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -219,   132,     0,     0,     0,
       0,     0,     0,     0,     0,   133,   134,   135,     0,     0,
       0,     0,   136,   137,   138,   139,   140,   141,     0,     0,
       0,   142,     0,     0,     0,     0,     0,     0,   362,     0,
      96,     0,     0,     0,   126,   127,   128,   129,   130,   131,
       0,   537,     0,     0,     0,     0,     0,     0,     0,     0,
     132,     0,     0,     0,     0,     0,     0,     0,     0,   133,
     134,   135,   283,   284,   285,     0,   136,   137,   138,   139,
     140,   141,     0,     0,     0,   142,   286,   287,   288,     0,
     289,   290,   291,     0,     0,     0,   363,   364,   365,   366,
     367,     0,   368,   369,   370,   371,   372,   373,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   374,   -22,   375,     0,   362,     0,
      96,     0,     0,     0,   126,   127,   128,   129,   130,   131,
       0,   613,     0,     0,     0,     0,     0,     0,     0,     0,
     132,     0,     0,     0,     0,     0,     0,     0,     0,   133,
     134,   135,   283,   284,   285,     0,   136,   137,   138,   139,
     140,   141,     0,     0,     0,   142,   286,   287,   288,     0,
     289,   290,   291,     0,     0,     0,   363,   364,   365,   366,
     367,     0,   368,   369,   370,   371,   372,   373,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   374,   -31,   375,     0,   362,     0,
      96,     0,     0,     0,   126,   127,   128,   129,   130,   131,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     132,     0,     0,     0,     0,     0,     0,     0,     0,   133,
     134,   135,   283,   284,   285,     0,   136,   137,   138,   139,
     140,   141,     0,     0,     0,   142,   286,   287,   288,     0,
     289,   290,   291,     0,     0,     0,   363,   364,   365,   366,
     367,     0,   368,   369,   370,   371,   372,   373,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  -205,     0,  -205,   374,     0,   375,  -205,  -205,  -205,
    -205,  -205,  -205,     0,   362,     0,    96,     0,     0,     0,
     126,   127,   128,   129,   130,   131,     0,     0,     0,     0,
     132,     0,     0,     0,     0,     0,     0,     0,     0,   133,
     134,   135,     0,     0,     0,     0,   136,   137,   138,   139,
     140,   141,     0,     0,     0,   142,     0,     0,     0,  -205,
    -205,  -205,  -205,  -205,     0,  -205,  -205,  -205,  -205,  -205,
    -205,     0,   363,   364,   365,   366,   367,     0,   368,   369,
     370,   371,   372,   373,   490,     0,    96,  -205,  -205,  -205,
     126,   127,   128,   129,   130,   131,     0,   362,     0,    96,
     374,   -20,   375,   126,   127,   128,   129,   130,   131,     0,
     492,     0,    96,  -205,     0,     0,   126,   127,   128,   129,
     130,   131,  -205,  -205,  -205,     0,   132,     0,     0,  -205,
    -205,  -205,  -205,  -205,  -205,   133,   134,   135,  -205,     0,
       0,     0,   136,   137,   138,   139,   140,   141,     0,     0,
       0,   142,     0,     0,     0,   363,   364,   365,   366,   367,
       0,   368,   369,   370,   371,   372,   373,   494,     0,    96,
       0,     0,     0,   126,   127,   128,   129,   130,   131,     0,
     496,     0,    96,   374,   -29,   375,   126,   127,   128,   129,
     130,   131,     0,     0,     0,     0,   132,     0,     0,     0,
       0,     0,     0,     0,     0,   133,   134,   135,     0,   132,
       0,     0,   136,   137,   138,   139,   140,   141,   133,   134,
     135,   142,   132,     0,     0,   136,   137,   138,   139,   140,
     141,   133,   134,   135,   142,     0,     0,     0,   136,   137,
     138,   139,   140,   141,     0,     0,     0,   142,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   132,
       0,     0,     0,     0,     0,     0,     0,     0,   133,   134,
     135,     0,   132,     0,     0,   136,   137,   138,   139,   140,
     141,   133,   134,   135,   142,     0,     0,     0,   136,   137,
     138,   139,   140,   141,    96,     0,     0,   142,   126,   127,
     128,   129,   130,   131,     0,     0,     0,     0,     0,     0,
       0,     0,   282,     0,     0,     0,    96,     0,     0,     0,
     126,   127,   128,   129,   130,   131,   283,   284,   285,    96,
       0,     0,     0,   126,   127,   128,   129,   130,   131,     0,
     286,   287,   288,     0,   289,   290,   291,     0,   283,   284,
     285,    96,     0,     0,     0,   126,   127,   128,   129,   130,
     131,     0,   286,   287,   288,     0,   289,   290,   291,     0,
       0,     0,     0,     0,     0,    96,     0,     0,   292,   126,
     127,   128,   129,   130,   131,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   132,     0,     0,     0,     0,     0,
       0,     0,     0,   133,   134,   135,     0,     0,     0,     0,
     136,   137,   138,   139,   140,   141,   132,     0,     0,   142,
       0,     0,     0,     0,     0,   133,   134,   135,     0,   132,
       0,     0,   136,   137,   138,   139,   140,   141,   133,   134,
     135,   142,     0,     0,     0,   136,   137,   138,   139,   140,
     141,   132,     0,     0,   142,     0,     0,     0,     0,     0,
     133,   134,   135,     0,     0,     0,     0,   136,   137,   138,
     139,   140,   141,     0,     0,   132,   226,     0,     0,     0,
       0,     0,     0,     0,   133,   134,   135,     0,     0,     0,
       0,   136,   137,   138,   139,   140,   141,     2,     0,     0,
     230,     0,     0,     0,     0,     0,     0,     5,     6,     7,
       8,     9,    10,    11,     0,     0,     0,     0,     0,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     2,     0,     0,     0,     0,     0,     0,     0,     0,
     451,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,     2,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     5,     6,     7,     8,     9,    10,
      11,     0,     0,     0,     0,     0,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47
};

static const yytype_int16 yycheck[] =
{
      56,    57,    57,    79,    56,   132,   133,   134,   135,   136,
     137,   138,   112,   140,   141,   203,   142,   328,   211,   236,
     236,   441,   209,   209,   209,    81,   277,   364,   464,   292,
     132,   133,   134,   135,   136,   137,     3,    97,   275,   358,
      64,   275,    98,    98,   472,   473,   401,     4,   518,    90,
      90,     1,     3,     1,     3,     3,   112,     1,     4,     7,
       8,     9,    10,    11,    12,     3,   142,    90,   124,     4,
      61,     3,    85,    64,   262,   139,   117,   117,     0,   203,
     268,     0,     3,   139,    90,   124,    89,    35,    36,    37,
     120,   121,    90,     0,   117,     3,   120,    89,     6,   138,
     139,    49,    50,    51,   292,    53,    54,    55,   325,   122,
     123,   117,   239,   240,   241,   242,   243,     3,   245,   117,
      88,   358,   249,   360,   358,   252,   360,    49,   255,   120,
      87,   450,   259,   326,    85,   211,   606,    87,   262,   263,
     200,    87,    49,    87,   268,   500,    87,   116,   204,   273,
     226,   587,    87,   209,   230,    88,    89,   259,   586,    87,
     588,   589,   238,   591,    85,   113,    77,    87,   292,   138,
     139,   138,   228,     1,   122,   123,   124,   106,    89,    88,
     109,   129,   130,   131,   132,   133,   134,   607,   525,   138,
     138,   104,   124,   269,   320,   122,   123,   324,   634,    87,
     138,   139,    86,   124,    87,   633,   138,   139,   104,   105,
     266,   267,    87,   450,   402,   271,   450,   138,   139,   470,
      90,   277,   324,    16,    89,    90,   654,   303,   115,   116,
     117,    96,   419,   419,   419,   498,     3,   110,   124,   104,
       7,     8,     9,    10,    11,    12,   124,   440,   138,   139,
     326,   111,   138,   138,   139,   443,   312,    88,    89,    87,
     112,    89,    90,   113,    85,   114,   577,    90,    96,    16,
      17,    18,    19,    20,    21,    22,   104,    88,   402,   403,
     404,   405,   406,   407,   408,   409,   410,   411,   364,   506,
     506,    46,    47,   133,   134,   371,   136,   137,   138,   139,
     266,   267,   428,   124,    97,    98,    99,   100,   101,   102,
     498,   367,    90,   104,   441,    88,    88,   138,   139,   443,
     124,   125,   126,    85,    88,    96,     3,    88,    88,    85,
       1,    85,     3,    85,    85,   138,     7,     8,     9,    10,
      11,    12,   138,   138,   138,    85,   138,    86,   138,   138,
     138,   138,   138,   138,   542,    85,    85,   107,   575,    85,
     416,    86,    90,   419,   440,   132,    89,    89,    89,    89,
     138,   138,   138,     5,   498,   138,    88,    90,    89,   138,
     107,    89,    85,   107,    90,   138,    90,   463,   464,    85,
      85,   447,   468,   469,    86,   447,    85,    90,    89,    11,
      70,    91,    89,    89,    89,    85,    90,    85,    90,    85,
      85,    90,    85,    85,   470,    89,   472,   473,   542,    85,
     544,    90,   546,    90,   548,    90,   550,    90,   552,    85,
     554,    90,   556,    85,   558,    90,   560,    85,    88,    90,
      85,   138,   113,    86,    91,   107,   107,    85,   504,   525,
     506,   122,   123,   124,   506,    85,    85,    88,   129,   130,
     131,   132,   133,   134,    85,    85,    85,   138,    90,    85,
     138,    85,    90,    90,   138,    90,    64,   577,   138,    90,
     607,    90,    85,    85,    89,    85,    85,    11,    85,    90,
      11,    89,    85,   617,   618,    90,   620,   621,    90,   623,
     624,    85,    85,    90,    90,    85,    85,    85,    85,   585,
      49,   587,   243,   245,   249,   252,   542,   255,   267,   575,
     575,   577,   239,   241,   648,   581,   650,   242,   652,   581,
     586,   240,   588,   589,     1,   591,     3,   374,   320,    56,
       7,     8,     9,    10,    11,    12,   116,   603,   604,   450,
     204,   124,   271,   360,   581,   419,   111,     1,   634,     3,
     439,   435,   505,     7,     8,     9,    10,    11,    12,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   633,    -1,    -1,
      -1,    -1,    -1,     0,     1,    -1,     3,    -1,    -1,     6,
      -1,    -1,    -1,    -1,    -1,    -1,    13,    14,   654,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    -1,    -1,    -1,    -1,   113,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   122,   123,   124,    -1,    -1,
      -1,    -1,   129,   130,   131,   132,   133,   134,    -1,   113,
      -1,   138,    89,    -1,    -1,    -1,    -1,    -1,   122,   123,
     124,    -1,    -1,    -1,    -1,   129,   130,   131,   132,   133,
     134,     1,    -1,     3,   138,    -1,    -1,     7,     8,     9,
      10,    11,    12,    -1,    -1,    -1,    -1,   124,    -1,     0,
       1,    -1,     3,    -1,    -1,     6,    -1,    -1,    -1,    -1,
      -1,   138,    13,    14,    -1,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    -1,     3,    -1,    -1,    -1,     7,     8,     9,    10,
      11,    12,    -1,    -1,    -1,    -1,    -1,    -1,    89,    -1,
      -1,    -1,    -1,   113,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   122,   123,   124,    -1,    -1,    -1,    -1,   129,
     130,   131,   132,   133,   134,    -1,    -1,    -1,   138,    -1,
      -1,    -1,    -1,   124,     1,    -1,     3,    -1,    -1,     6,
       7,     8,     9,    10,    11,    12,    -1,   138,    -1,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,   113,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   122,   123,   124,    -1,    -1,    -1,    -1,   129,   130,
     131,   132,   133,   134,     1,    -1,     3,   138,    -1,    -1,
       7,     8,     9,    10,    11,    12,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   113,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   122,   123,   124,    -1,    -1,
      -1,    -1,   129,   130,   131,   132,   133,   134,     1,    -1,
       3,   138,    -1,     6,     7,     8,     9,    10,    11,    12,
      -1,    -1,    -1,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,   113,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   122,   123,   124,    -1,    -1,
      -1,    -1,   129,   130,   131,   132,   133,   134,    -1,    -1,
      -1,   138,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,
       3,    -1,    -1,    -1,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     113,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   122,
     123,   124,    35,    36,    37,    -1,   129,   130,   131,   132,
     133,   134,    -1,    -1,    -1,   138,    49,    50,    51,    -1,
      53,    54,    55,    -1,    -1,    -1,    59,    60,    61,    62,
      63,    -1,    65,    66,    67,    68,    69,    70,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    -1,     1,    -1,
       3,    -1,    -1,    -1,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     113,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   122,
     123,   124,    35,    36,    37,    -1,   129,   130,   131,   132,
     133,   134,    -1,    -1,    -1,   138,    49,    50,    51,    -1,
      53,    54,    55,    -1,    -1,    -1,    59,    60,    61,    62,
      63,    -1,    65,    66,    67,    68,    69,    70,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    -1,     1,    -1,
       3,    -1,    -1,    -1,     7,     8,     9,    10,    11,    12,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     113,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   122,
     123,   124,    35,    36,    37,    -1,   129,   130,   131,   132,
     133,   134,    -1,    -1,    -1,   138,    49,    50,    51,    -1,
      53,    54,    55,    -1,    -1,    -1,    59,    60,    61,    62,
      63,    -1,    65,    66,    67,    68,    69,    70,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,    -1,     3,    87,    -1,    89,     7,     8,     9,
      10,    11,    12,    -1,     1,    -1,     3,    -1,    -1,    -1,
       7,     8,     9,    10,    11,    12,    -1,    -1,    -1,    -1,
     113,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   122,
     123,   124,    -1,    -1,    -1,    -1,   129,   130,   131,   132,
     133,   134,    -1,    -1,    -1,   138,    -1,    -1,    -1,    59,
      60,    61,    62,    63,    -1,    65,    66,    67,    68,    69,
      70,    -1,    59,    60,    61,    62,    63,    -1,    65,    66,
      67,    68,    69,    70,     1,    -1,     3,    87,    88,    89,
       7,     8,     9,    10,    11,    12,    -1,     1,    -1,     3,
      87,    88,    89,     7,     8,     9,    10,    11,    12,    -1,
       1,    -1,     3,   113,    -1,    -1,     7,     8,     9,    10,
      11,    12,   122,   123,   124,    -1,   113,    -1,    -1,   129,
     130,   131,   132,   133,   134,   122,   123,   124,   138,    -1,
      -1,    -1,   129,   130,   131,   132,   133,   134,    -1,    -1,
      -1,   138,    -1,    -1,    -1,    59,    60,    61,    62,    63,
      -1,    65,    66,    67,    68,    69,    70,     1,    -1,     3,
      -1,    -1,    -1,     7,     8,     9,    10,    11,    12,    -1,
       1,    -1,     3,    87,    88,    89,     7,     8,     9,    10,
      11,    12,    -1,    -1,    -1,    -1,   113,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   122,   123,   124,    -1,   113,
      -1,    -1,   129,   130,   131,   132,   133,   134,   122,   123,
     124,   138,   113,    -1,    -1,   129,   130,   131,   132,   133,
     134,   122,   123,   124,   138,    -1,    -1,    -1,   129,   130,
     131,   132,   133,   134,    -1,    -1,    -1,   138,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   113,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   122,   123,
     124,    -1,   113,    -1,    -1,   129,   130,   131,   132,   133,
     134,   122,   123,   124,   138,    -1,    -1,    -1,   129,   130,
     131,   132,   133,   134,     3,    -1,    -1,   138,     7,     8,
       9,    10,    11,    12,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    21,    -1,    -1,    -1,     3,    -1,    -1,    -1,
       7,     8,     9,    10,    11,    12,    35,    36,    37,     3,
      -1,    -1,    -1,     7,     8,     9,    10,    11,    12,    -1,
      49,    50,    51,    -1,    53,    54,    55,    -1,    35,    36,
      37,     3,    -1,    -1,    -1,     7,     8,     9,    10,    11,
      12,    -1,    49,    50,    51,    -1,    53,    54,    55,    -1,
      -1,    -1,    -1,    -1,    -1,     3,    -1,    -1,    87,     7,
       8,     9,    10,    11,    12,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   113,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   122,   123,   124,    -1,    -1,    -1,    -1,
     129,   130,   131,   132,   133,   134,   113,    -1,    -1,   138,
      -1,    -1,    -1,    -1,    -1,   122,   123,   124,    -1,   113,
      -1,    -1,   129,   130,   131,   132,   133,   134,   122,   123,
     124,   138,    -1,    -1,    -1,   129,   130,   131,   132,   133,
     134,   113,    -1,    -1,   138,    -1,    -1,    -1,    -1,    -1,
     122,   123,   124,    -1,    -1,    -1,    -1,   129,   130,   131,
     132,   133,   134,    -1,    -1,   113,   138,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   122,   123,   124,    -1,    -1,    -1,
      -1,   129,   130,   131,   132,   133,   134,     6,    -1,    -1,
     138,    -1,    -1,    -1,    -1,    -1,    -1,    16,    17,    18,
      19,    20,    21,    22,    -1,    -1,    -1,    -1,    -1,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     6,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      89,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,     6,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    16,    17,    18,    19,    20,    21,
      22,    -1,    -1,    -1,    -1,    -1,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,     1,     6,    13,    14,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,   141,   142,
     144,   145,   146,   240,   241,   242,   243,   244,   249,   250,
     251,   252,   253,   255,   271,   272,   275,   276,   278,   279,
     280,   281,   282,   283,   284,   285,   286,    88,    89,   139,
       4,    87,   277,    87,   277,    87,   277,     0,     1,   144,
     143,     1,    87,   147,    89,    89,     3,   124,   138,   261,
     263,   264,   265,   266,   309,   311,   312,   313,   314,   315,
     318,   319,   320,   324,   264,   309,   310,   311,   254,   255,
     256,   254,    87,    87,    87,   143,     7,     8,     9,    10,
      11,    12,   113,   122,   123,   124,   129,   130,   131,   132,
     133,   134,   138,   179,   180,   181,   182,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   201,   202,   203,   204,   205,   206,   207,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   219,   220,
     221,   230,   324,     6,   288,   290,   291,   292,   293,   324,
     325,   326,   287,   294,   295,   287,   143,    88,   148,   272,
     273,   274,   311,   104,    90,   231,   232,   233,   262,   116,
     138,   139,   318,   312,   231,   254,   287,   287,   288,   192,
     201,   192,   192,   192,   192,   192,   138,   201,   138,   324,
     138,   201,   201,     1,   184,   248,   249,    86,   106,   109,
     110,   111,   112,   113,   114,   226,   115,   116,   117,   227,
     120,   121,   228,   122,   123,   224,   124,   125,   126,   225,
     104,   105,   229,    90,   133,   134,   136,   137,   138,   139,
      88,    90,   289,   104,    88,   296,    88,   235,   236,   237,
     272,    85,    21,    35,    36,    37,    49,    50,    51,    53,
      54,    55,    87,   180,   198,   200,   267,   269,   263,   234,
      96,   339,   340,   138,   213,   221,   230,   316,   317,   324,
      85,   321,   322,   329,   330,   181,    88,    88,    88,   246,
     247,   324,    85,    85,    85,   138,   139,   245,   320,   336,
     337,   338,   184,   186,   188,   189,   190,   193,   194,   195,
     196,   197,   192,   180,   198,   180,   218,   326,   218,   180,
     198,   222,   223,   184,   292,   180,   258,   260,   271,   275,
     297,   298,     1,    59,    60,    61,    62,    63,    65,    66,
      67,    68,    69,    70,    87,    89,   149,   150,   153,   158,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   184,
     324,   238,   138,   138,   138,   138,   138,   138,   138,   138,
     138,   138,   268,   269,   239,   242,   244,   138,   184,    90,
     117,    85,   323,   324,    85,   331,   332,   333,   334,    86,
      85,   248,    85,   192,   327,   328,   336,   181,   337,   138,
     139,   107,    85,    90,    86,   299,   300,   301,   257,   258,
     259,    89,   298,    89,    88,    89,    89,   182,    89,   157,
     158,   159,   198,   138,   138,     5,   183,   184,   138,   138,
     235,    88,   151,   107,    89,   239,     1,   180,   198,   199,
       1,   180,     1,   180,     1,   180,     1,   180,     1,   180,
       1,   180,     1,   180,     1,   180,     1,   180,    90,   270,
      89,   138,    85,   316,    90,    90,   249,   335,    85,   331,
      85,    86,   327,   181,   179,   180,   198,    90,   303,   304,
     305,   308,   309,   257,    89,    91,    70,    89,   184,   183,
      89,    89,   184,   184,   154,   155,   158,    14,   152,   157,
     157,    85,    90,    85,    90,    85,    90,    85,    90,    85,
      90,    85,    90,    85,    90,    85,    90,    85,    90,    85,
      90,   269,    88,   239,    16,    97,    98,    99,   100,   101,
     102,   341,   324,    91,   333,   138,   309,   320,   336,    85,
      86,   302,   339,   107,   182,   138,    85,    89,    85,    85,
      88,   156,   199,   180,   180,   180,   180,   180,   180,   180,
     180,   180,   138,   138,   138,    85,   303,   306,   184,   157,
     183,   157,   157,    14,   157,    85,    85,    90,    90,    85,
      90,    90,    85,    90,    90,    11,   324,   324,    85,   339,
     179,   307,    85,    64,    89,   180,   180,   180,   180,   180,
     180,    85,    85,    90,    89,   157,   183,    85,    90,    85,
      90,    85,    90,    11,    85,   180,   180,   180,    90,   157,
      85,    85,    85,    11,    85
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   140,   141,   141,   141,   142,   142,   143,   144,   144,
     144,   144,   144,   144,   145,   146,   146,   148,   147,   147,
     149,   149,   150,   151,   151,   151,   152,   153,   153,   154,
     154,   155,   156,   156,   156,   157,   157,   158,   158,   158,
     158,   158,   158,   158,   158,   158,   158,   158,   158,   159,
     160,   161,   162,   162,   163,   163,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   175,
     175,   176,   177,   177,   178,   179,   179,   180,   180,   180,
     181,   181,   182,   183,   183,   184,   185,   185,   186,   186,
     187,   188,   188,   189,   189,   190,   190,   191,   192,   192,
     193,   193,   194,   194,   195,   195,   196,   196,   197,   197,
     198,   198,   198,   198,   198,   198,   198,   198,   198,   198,
     198,   198,   198,   198,   198,   198,   198,   198,   199,   199,
     200,   200,   201,   201,   201,   201,   201,   201,   201,   201,
     201,   201,   202,   202,   203,   203,   204,   205,   206,   207,
     208,   209,   210,   210,   211,   211,   211,   212,   212,   213,
     213,   213,   213,   213,   213,   213,   214,   215,   215,   216,
     217,   218,   219,   220,   221,   222,   222,   223,   223,   223,
     223,   224,   224,   225,   225,   225,   226,   227,   227,   227,
     228,   228,   229,   229,   230,   230,   230,   230,   230,   230,
     232,   231,   233,   234,   231,   236,   235,   237,   238,   235,
     239,   239,   240,   240,   241,   242,   243,   244,   245,   247,
     246,   248,   248,   249,   250,   250,   250,   251,   251,   252,
     252,   253,   254,   254,   255,   256,   255,   255,   257,   257,
     259,   258,   258,   260,   262,   261,   263,   263,   264,   264,
     265,   266,   266,   267,   268,   268,   269,   269,   269,   269,
     270,   270,   271,   272,   272,   272,   272,   272,   272,   272,
     273,   273,   274,   274,   275,   275,   275,   275,   275,   275,
     275,   275,   275,   275,   275,   275,   275,   275,   275,   275,
     275,   275,   275,   275,   275,   275,   275,   275,   275,   275,
     275,   275,   275,   275,   275,   275,   275,   275,   275,   276,
     277,   278,   279,   280,   281,   282,   282,   283,   284,   284,
     285,   286,   286,   287,   287,   288,   288,   289,   289,   290,
     291,   291,   292,   292,   293,   295,   296,   294,   297,   297,
     297,   298,   298,   299,   301,   300,   302,   300,   303,   303,
     304,   306,   305,   307,   308,   308,   309,   310,   311,   311,
     312,   313,   313,   313,   313,   313,   313,   313,   314,   315,
     316,   316,   316,   316,   317,   317,   317,   318,   319,   319,
     320,   322,   321,   323,   323,   324,   325,   326,   326,   327,
     328,   327,   330,   329,   331,   331,   332,   332,   334,   333,
     335,   335,   335,   336,   336,   336,   337,   338,   338,   338,
     338,   338,   339,   339,   340,   341,   341,   341,   341,   341,
     341,   341,   341
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     1,     1,     2,     3,     0,     1,     1,
       1,     1,     2,     3,     2,     3,     3,     0,     5,     2,
       0,     1,     2,     0,     2,     2,     1,     4,     2,     0,
       1,     2,     0,     2,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     2,
       2,     3,     1,     1,     1,     1,     1,     5,     2,     2,
       3,     3,     1,     5,     7,     7,     5,     9,     1,     1,
       1,     1,     2,     4,     1,     1,     5,     1,     3,     3,
       0,     1,     1,     0,     1,     1,     1,     3,     1,     3,
       2,     1,     3,     1,     3,     1,     3,     2,     1,     4,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       6,     8,    10,     4,     4,     4,     6,     8,    10,     6,
       8,    10,     4,     4,     4,     4,     4,     4,     1,     1,
       6,     4,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     2,     2,     4,     2,     2,     2,     2,
       2,     2,     1,     3,     1,     1,     1,     3,     3,     1,
       1,     1,     1,     1,     1,     1,     4,     1,     1,     2,
       2,     1,     3,     3,     4,     0,     1,     1,     1,     3,
       3,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     1,     0,     0,     3,     0,     1,     0,     0,     3,
       2,     3,     2,     2,     2,     2,     0,     1,     1,     0,
       2,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     1,     2,     0,     3,     2,     0,     1,
       0,     3,     2,     1,     0,     3,     1,     3,     0,     1,
       1,     1,     3,     1,     1,     3,     1,     1,     1,     4,
       0,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       1,     2,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     2,     2,     2,     4,     4,     2,     4,     4,
       2,     4,     4,     0,     1,     0,     2,     0,     1,     1,
       1,     3,     1,     3,     1,     0,     0,     3,     2,     2,
       3,     2,     1,     1,     0,     3,     0,     5,     1,     1,
       1,     0,     4,     1,     0,     1,     1,     1,     2,     1,
       1,     1,     3,     1,     1,     4,     4,     3,     4,     4,
       1,     1,     3,     1,     0,     1,     3,     2,     1,     2,
       1,     0,     2,     1,     3,     1,     1,     1,     1,     0,
       0,     2,     0,     2,     1,     3,     1,     3,     0,     2,
       2,     2,     1,     1,     1,     2,     1,     3,     3,     4,
       3,     4,     0,     1,     6,     0,     1,     1,     1,     1,
       4,     4,     8
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* program: %empty  */
#line 245 "gram.y"
        {
            if (err_cnt == 0)
              *gProject->Parse_TOS->yyerrstream
              << "Warning: ANSI/ISO C forbids an empty source file.\n";
            gProject->Parse_TOS->transUnit = (TransUnit*) NULL;
            (yyval.transunit) = (TransUnit*) NULL;
        }
#line 2327 "gram.tab.c"
    break;

  case 3: /* program: trans_unit  */
#line 253 "gram.y"
        {
            if (err_cnt)
            {
                *gProject->Parse_TOS->yyerrstream
                << err_cnt << " errors found.\n";
                gProject->Parse_TOS->transUnit = (TransUnit*) NULL;
            } else {
                gProject->Parse_TOS->transUnit = (yyval.transunit);
            }
        }
#line 2342 "gram.tab.c"
    break;

  case 4: /* program: error  */
#line 264 "gram.y"
        {
            *gProject->Parse_TOS->yyerrstream << "Errors - Aborting parse.\n";
            gProject->Parse_TOS->transUnit = (TransUnit*) NULL;
            YYACCEPT;
        }
#line 2352 "gram.tab.c"
    break;

  case 5: /* trans_unit: top_level_decl top_level_exit  */
#line 272 "gram.y"
        {
            (yyval.transunit) = gProject->Parse_TOS->transUnit;
            (yyval.transunit)->add((yyvsp[-1].stemnt));
        }
#line 2361 "gram.tab.c"
    break;

  case 6: /* trans_unit: trans_unit top_level_decl top_level_exit  */
#line 277 "gram.y"
        {
            (yyval.transunit)->add((yyvsp[-1].stemnt));
        }
#line 2369 "gram.tab.c"
    break;

  case 7: /* top_level_exit: %empty  */
#line 283 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->ReinitializeCtxt();
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScopes(FILE_SCOPE);
            err_top_level = 0;            
        }
#line 2380 "gram.tab.c"
    break;

  case 8: /* top_level_decl: decl_stemnt  */
#line 292 "gram.y"
        {
            (yyval.stemnt) = (yyvsp[0].declStemnt);
        }
#line 2388 "gram.tab.c"
    break;

  case 9: /* top_level_decl: func_def  */
#line 296 "gram.y"
        {
            (yyval.stemnt) = (yyvsp[0].functionDef);
        }
#line 2396 "gram.tab.c"
    break;

  case 10: /* top_level_decl: PP_DIR  */
#line 300 "gram.y"
        {
            (yyval.stemnt) = (yyvsp[0].stemnt);
        }
#line 2404 "gram.tab.c"
    break;

  case 11: /* top_level_decl: PP_LINE  */
#line 304 "gram.y"
        {
            (yyval.stemnt) = (yyvsp[0].stemnt);
        }
#line 2412 "gram.tab.c"
    break;

  case 12: /* top_level_decl: error SEMICOLON  */
#line 308 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2420 "gram.tab.c"
    break;

  case 13: /* top_level_decl: error RBRACE top_level_exit  */
#line 312 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2428 "gram.tab.c"
    break;

  case 14: /* func_def: func_spec cmpnd_stemnt  */
#line 318 "gram.y"
        {
            if ((yyvsp[0].stemnt) != NULL)
            {
                (yyval.functionDef) = new FunctionDef((yyvsp[0].stemnt)->location);
                Block *blk = (Block*) (yyvsp[0].stemnt);
    
                (yyval.functionDef)->decl = (yyvsp[-1].decl);
                
                if ((yyvsp[-1].decl)->name &&
                    (yyvsp[-1].decl)->name->entry)
                    (yyvsp[-1].decl)->name->entry->u2FunctionDef = (yyval.functionDef);
                
                // Steal internals of the compound statement
                (yyval.functionDef)->head = blk->head;
                (yyval.functionDef)->tail = blk->tail;
    
                blk->head = blk->tail = (Statement*) NULL;
                delete (yyvsp[0].stemnt);    
            }
			else
			{
				delete (yyvsp[-1].decl);
				(yyval.functionDef) = (FunctionDef*) NULL;
			}
        }
#line 2458 "gram.tab.c"
    break;

  case 15: /* func_spec: decl_specs func_declarator opt_KnR_declaration_list  */
#line 346 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
            
            possibleType = true;
            (yyval.decl) = (yyvsp[-1].decl);

            if ((yyval.decl)->form != NULL)
            {
                assert(err_top_level ||
                       (yyval.decl)->form->type == TT_Function );
    
                (yyval.decl)->extend((yyvsp[-2].base));
    
                /* This is adding K&R-style declarations if $3 exists */
                if ((yyvsp[0].decl) != NULL)
                {
                    FunctionType *fnc = (FunctionType*) ((yyval.decl)->form);
                    fnc->KnR_decl = true;
                    Decl *param = (yyvsp[0].decl);
                    while (param != NULL)
                    {
                        Decl *next= param->next;
                            delete param ;
                        param = next;
                    }
                }
            }
        }
#line 2491 "gram.tab.c"
    break;

  case 16: /* func_spec: no_decl_specs declarator opt_KnR_declaration_list  */
#line 375 "gram.y"
        {

            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
            
            (yyval.decl) = (yyvsp[-1].decl);

            if ((yyval.decl)->form != NULL)
            {
                assert(err_top_level ||
                       (yyval.decl)->form->type == TT_Function );
                (yyval.decl)->extend((yyvsp[-2].base));
    
                /* This is adding K&R-style declarations if $3 exists */
                if ((yyvsp[0].decl) != NULL)
                {
                    FunctionType *fnc = (FunctionType*) ((yyval.decl)->form);
                    fnc->KnR_decl = true;
                    Decl *param = (yyvsp[0].decl);
                    while (param != NULL)
                    {
                        Decl *next= param->next;
                            delete param ;
                        param = next;
                    }
                }
            }
        }
#line 2523 "gram.tab.c"
    break;

  case 17: /* $@1: %empty  */
#line 408 "gram.y"
        {  
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ReEnterScope();
        }
#line 2532 "gram.tab.c"
    break;

  case 18: /* cmpnd_stemnt: LBRACE $@1 opt_declaration_list opt_stemnt_list RBRACE  */
#line 413 "gram.y"
        {
            Block*    block = new Block(*(yyvsp[-4].loc));
            (yyval.stemnt) = block;
            block->addDecls((yyvsp[-2].decl));
            block->addStatements(ReverseList((yyvsp[-1].stemnt)));
            if (gProject->Parse_TOS->transUnit)
            {
                yyCheckLabelsDefinition(gProject->Parse_TOS->transUnit->contxt.labels);
                gProject->Parse_TOS->transUnit->contxt.ExitScope();
                gProject->Parse_TOS->transUnit->contxt.ExitScope();
            }
        }
#line 2549 "gram.tab.c"
    break;

  case 19: /* cmpnd_stemnt: error RBRACE  */
#line 426 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2557 "gram.tab.c"
    break;

  case 20: /* opt_stemnt_list: %empty  */
#line 432 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2565 "gram.tab.c"
    break;

  case 22: /* stemnt_list: non_constructor_stemnt stemnt_list2  */
#line 439 "gram.y"
        {
	    /*
	     * All the statements are expected in a reversed list (because
	     * of how we parse stemnt_list2) so we need to take the
	     * non_constructor statement at the end.
	     */
            if ((yyvsp[0].stemnt))
            {
	        Statement *s;

		for (s = (yyvsp[0].stemnt); s->next; s = s->next) /* Traverse to the end */;
		s->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2585 "gram.tab.c"
    break;

  case 23: /* stemnt_list2: %empty  */
#line 457 "gram.y"
        {
	   (yyval.stemnt) = (Statement *) NULL;
	}
#line 2593 "gram.tab.c"
    break;

  case 24: /* stemnt_list2: stemnt_list2 stemnt  */
#line 461 "gram.y"
        {
            /* Hook them up backwards, we'll reverse them later. */
            if ((yyvsp[0].stemnt))
            {
                (yyvsp[0].stemnt)->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2606 "gram.tab.c"
    break;

  case 25: /* stemnt_list2: stemnt_list2 PP_LINE  */
#line 470 "gram.y"
        {    /* preprocessor #line directive */
            /* Hook them up backwards, we'll reverse them later. */
            if ((yyvsp[0].stemnt))
            {
                (yyvsp[0].stemnt)->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2619 "gram.tab.c"
    break;

  case 27: /* cmpnd_stemnt_reentrance: LBRACE opt_declaration_list opt_stemnt_list_reentrance RBRACE  */
#line 489 "gram.y"
        {
            Block*    block = new Block(*(yyvsp[-3].loc));
            (yyval.stemnt) = block;
            block->addDecls((yyvsp[-2].decl));
            block->addStatements(ReverseList((yyvsp[-1].stemnt)));
            
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope();
        }
#line 2633 "gram.tab.c"
    break;

  case 28: /* cmpnd_stemnt_reentrance: error RBRACE  */
#line 499 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2641 "gram.tab.c"
    break;

  case 29: /* opt_stemnt_list_reentrance: %empty  */
#line 505 "gram.y"
        {
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2649 "gram.tab.c"
    break;

  case 31: /* stemnt_list_reentrance: non_constructor_stemnt stemnt_list_reentrance2  */
#line 512 "gram.y"
        {
	    /*
	     * All the statements are expected in a reversed list (because
	     * of how we parse stemnt_list_reentrance2) so we need to take
	     * the non_constructor statement at the end.
	     */
            if ((yyvsp[0].stemnt))
            {
	        Statement *s;

		for (s = (yyvsp[0].stemnt); s->next; s = s->next) /* Traverse to the end */;
		s->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2669 "gram.tab.c"
    break;

  case 32: /* stemnt_list_reentrance2: %empty  */
#line 530 "gram.y"
        {
	   (yyval.stemnt) = (Statement *) NULL;
	}
#line 2677 "gram.tab.c"
    break;

  case 33: /* stemnt_list_reentrance2: stemnt_list_reentrance2 stemnt_reentrance  */
#line 534 "gram.y"
        {
            /* Hook them up backwards, we'll reverse them later. */
            if ((yyvsp[0].stemnt))
            {
                (yyvsp[0].stemnt)->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2690 "gram.tab.c"
    break;

  case 34: /* stemnt_list_reentrance2: stemnt_list_reentrance2 PP_LINE  */
#line 543 "gram.y"
        {    /* preprocessor #line directive */
            /* Hook them up backwards, we'll reverse them later. */
            if ((yyvsp[0].stemnt))
            {
                (yyvsp[0].stemnt)->next = (yyvsp[-1].stemnt);
                (yyval.stemnt) = (yyvsp[0].stemnt);
            }
        }
#line 2703 "gram.tab.c"
    break;

  case 35: /* stemnt_reentrance: non_constructor_stemnt  */
#line 554 "gram.y"
         {
	    (yyval.stemnt) = (yyvsp[0].stemnt);
	 }
#line 2711 "gram.tab.c"
    break;

  case 36: /* stemnt_reentrance: constructor_stemnt  */
#line 558 "gram.y"
         {
	    (yyval.stemnt) = (yyvsp[0].stemnt);
	 }
#line 2719 "gram.tab.c"
    break;

  case 48: /* non_constructor_stemnt: error SEMICOLON  */
#line 575 "gram.y"
        {
            delete (yyvsp[0].loc);
            (yyval.stemnt) = (Statement*) NULL;
        }
#line 2728 "gram.tab.c"
    break;

  case 49: /* constructor_stemnt: constructor_expr SEMICOLON  */
#line 598 "gram.y"
        {
            (yyval.stemnt) = new ExpressionStemnt((yyvsp[-1].value),*(yyvsp[0].loc));
            delete (yyvsp[0].loc);
	}
#line 2737 "gram.tab.c"
    break;

  case 50: /* expr_stemnt: expr SEMICOLON  */
#line 605 "gram.y"
        {
            (yyval.stemnt) = new ExpressionStemnt((yyvsp[-1].value),*(yyvsp[0].loc));
            delete (yyvsp[0].loc);
        }
#line 2746 "gram.tab.c"
    break;

  case 51: /* labeled_stemnt: label COLON stemnt_reentrance  */
#line 612 "gram.y"
        {
            (yyval.stemnt) = (yyvsp[0].stemnt);
            if ((yyval.stemnt) == NULL)
            {
              /* Sorry, we must have a statement here. */
              yyerr("Can't have a label at the end of a block! ");
              (yyval.stemnt) = new Statement(ST_NullStemnt,*(yyvsp[-1].loc));
            }
            (yyval.stemnt)->addHeadLabel((yyvsp[-2].label));
            delete (yyvsp[-1].loc);
        }
#line 2762 "gram.tab.c"
    break;

  case 57: /* switch_stemnt: SWITCH LPAREN expr RPAREN stemnt_reentrance  */
#line 635 "gram.y"
        {
            (yyval.stemnt) = new SwitchStemnt((yyvsp[-2].value),(yyvsp[0].stemnt),*(yyvsp[-4].loc));
            delete (yyvsp[-4].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2773 "gram.tab.c"
    break;

  case 58: /* break_stemnt: BREAK SEMICOLON  */
#line 644 "gram.y"
        {
            (yyval.stemnt) = new Statement(ST_BreakStemnt,*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
            delete (yyvsp[0].loc);
        }
#line 2783 "gram.tab.c"
    break;

  case 59: /* continue_stemnt: CONT SEMICOLON  */
#line 652 "gram.y"
        {
            (yyval.stemnt) = new Statement(ST_ContinueStemnt,*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
            delete (yyvsp[0].loc);
        }
#line 2793 "gram.tab.c"
    break;

  case 60: /* return_stemnt: RETURN opt_expr SEMICOLON  */
#line 660 "gram.y"
        {
            (yyval.stemnt) = new ReturnStemnt((yyvsp[-1].value),*(yyvsp[-2].loc));
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 2803 "gram.tab.c"
    break;

  case 61: /* goto_stemnt: GOTO LABEL_NAME SEMICOLON  */
#line 668 "gram.y"
        {
            (yyval.stemnt) = new GotoStemnt((yyvsp[-1].symbol),*(yyvsp[-2].loc));
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 2813 "gram.tab.c"
    break;

  case 62: /* null_stemnt: SEMICOLON  */
#line 676 "gram.y"
        {
            (yyval.stemnt) = new Statement(ST_NullStemnt,*(yyvsp[0].loc));
            delete (yyvsp[0].loc);
        }
#line 2822 "gram.tab.c"
    break;

  case 63: /* if_stemnt: IF LPAREN expr RPAREN stemnt_reentrance  */
#line 683 "gram.y"
        {
            (yyval.stemnt) = new IfStemnt((yyvsp[-2].value),(yyvsp[0].stemnt),*(yyvsp[-4].loc));
            delete (yyvsp[-4].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2833 "gram.tab.c"
    break;

  case 64: /* if_else_stemnt: IF LPAREN expr RPAREN stemnt_reentrance ELSE stemnt_reentrance  */
#line 692 "gram.y"
        {
            (yyval.stemnt) = new IfStemnt((yyvsp[-4].value),(yyvsp[-2].stemnt),*(yyvsp[-6].loc),(yyvsp[0].stemnt));
            delete (yyvsp[-6].loc);
            delete (yyvsp[-5].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2845 "gram.tab.c"
    break;

  case 65: /* do_stemnt: DO stemnt_reentrance WHILE LPAREN expr RPAREN SEMICOLON  */
#line 702 "gram.y"
        {
            (yyval.stemnt) = new DoWhileStemnt((yyvsp[-2].value),(yyvsp[-5].stemnt),*(yyvsp[-6].loc));
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
            delete (yyvsp[0].loc);
        }
#line 2858 "gram.tab.c"
    break;

  case 66: /* while_stemnt: WHILE LPAREN expr RPAREN stemnt_reentrance  */
#line 713 "gram.y"
        {
            (yyval.stemnt) = new WhileStemnt((yyvsp[-2].value),(yyvsp[0].stemnt),*(yyvsp[-4].loc));
            delete (yyvsp[-4].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2869 "gram.tab.c"
    break;

  case 67: /* for_stemnt: FOR LPAREN opt_expr SEMICOLON opt_expr SEMICOLON opt_expr RPAREN stemnt_reentrance  */
#line 723 "gram.y"
        {
            (yyval.stemnt) = new ForStemnt((yyvsp[-6].value),(yyvsp[-4].value),(yyvsp[-2].value),*(yyvsp[-8].loc),(yyvsp[0].stemnt));
            delete (yyvsp[-8].loc);
            delete (yyvsp[-7].loc);
            delete (yyvsp[-5].loc);
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2882 "gram.tab.c"
    break;

  case 71: /* named_label: ident  */
#line 739 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.label) = gProject->Parse_TOS->parseCtxt->Mk_named_label((yyvsp[0].symbol),
                                gProject->Parse_TOS->transUnit->contxt.labels);
        }
#line 2892 "gram.tab.c"
    break;

  case 72: /* case_label: CASE const_expr  */
#line 747 "gram.y"
        {
            (yyval.label) = new Label((yyvsp[0].value));
            delete (yyvsp[-1].loc);
        }
#line 2901 "gram.tab.c"
    break;

  case 73: /* case_label: CASE const_expr ELLIPSIS const_expr  */
#line 752 "gram.y"
        {
            (yyval.label) = new Label((yyvsp[-2].value),(yyvsp[0].value));
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2911 "gram.tab.c"
    break;

  case 74: /* deflt_label: DEFLT  */
#line 760 "gram.y"
        {
            (yyval.label) = new Label(LT_Default);
            delete (yyvsp[0].loc);
        }
#line 2920 "gram.tab.c"
    break;

  case 76: /* cond_expr: log_or_expr QUESTMARK expr COLON cond_expr  */
#line 777 "gram.y"
        {
            (yyval.value) = new TrinaryExpr((yyvsp[-4].value),(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-3].loc));
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 2930 "gram.tab.c"
    break;

  case 78: /* assign_expr: unary_expr assign_op assign_expr  */
#line 786 "gram.y"
        {
            (yyval.value) = new AssignExpr((yyvsp[-1].assignOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 2938 "gram.tab.c"
    break;

  case 79: /* assign_expr: unary_expr assign_op constructor_expr  */
#line 790 "gram.y"
        {
            (yyval.value) = new AssignExpr((yyvsp[-1].assignOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 2946 "gram.tab.c"
    break;

  case 80: /* opt_const_expr: %empty  */
#line 796 "gram.y"
        {
            (yyval.value) = (Expression*) NULL;
        }
#line 2954 "gram.tab.c"
    break;

  case 83: /* opt_expr: %empty  */
#line 806 "gram.y"
        {
           (yyval.value) = (Expression*) NULL;
        }
#line 2962 "gram.tab.c"
    break;

  case 87: /* log_or_expr: log_or_expr OR log_and_expr  */
#line 817 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_Or,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 2971 "gram.tab.c"
    break;

  case 89: /* log_and_expr: log_and_expr AND bitwise_or_expr  */
#line 825 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_And,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 2980 "gram.tab.c"
    break;

  case 90: /* log_neg_expr: NOT cast_expr  */
#line 832 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_Not,(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 2989 "gram.tab.c"
    break;

  case 92: /* bitwise_or_expr: bitwise_or_expr B_OR bitwise_xor_expr  */
#line 840 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_BitOr,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 2998 "gram.tab.c"
    break;

  case 94: /* bitwise_xor_expr: bitwise_xor_expr B_XOR bitwise_and_expr  */
#line 848 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_BitXor,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3007 "gram.tab.c"
    break;

  case 96: /* bitwise_and_expr: bitwise_and_expr B_AND equality_expr  */
#line 856 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_BitAnd,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3016 "gram.tab.c"
    break;

  case 97: /* bitwise_neg_expr: B_NOT cast_expr  */
#line 863 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_BitNot,(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3025 "gram.tab.c"
    break;

  case 99: /* cast_expr: LPAREN type_name_bis RPAREN cast_expr  */
#line 871 "gram.y"
        {
            (yyval.value) = new CastExpr((yyvsp[-2].type),(yyvsp[0].value),*(yyvsp[-3].loc));
            delete (yyvsp[-3].loc);
            delete (yyvsp[-1].loc);
        }
#line 3035 "gram.tab.c"
    break;

  case 101: /* equality_expr: equality_expr equality_op relational_expr  */
#line 880 "gram.y"
        {
            (yyval.value) = new RelExpr((yyvsp[-1].relOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 3043 "gram.tab.c"
    break;

  case 103: /* relational_expr: relational_expr relation_op shift_expr  */
#line 887 "gram.y"
        {
            (yyval.value) = new RelExpr((yyvsp[-1].relOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 3051 "gram.tab.c"
    break;

  case 105: /* shift_expr: shift_expr shift_op additive_expr  */
#line 894 "gram.y"
        {
            (yyval.value) = new BinaryExpr((yyvsp[-1].binOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 3059 "gram.tab.c"
    break;

  case 107: /* additive_expr: additive_expr add_op mult_expr  */
#line 901 "gram.y"
        {
            (yyval.value) = new BinaryExpr((yyvsp[-1].binOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 3067 "gram.tab.c"
    break;

  case 109: /* mult_expr: mult_expr mult_op cast_expr  */
#line 908 "gram.y"
        {
            (yyval.value) = new BinaryExpr((yyvsp[-1].binOp),(yyvsp[-2].value),(yyvsp[0].value),NoLocation);
        }
#line 3075 "gram.tab.c"
    break;

  case 110: /* constructor_expr: FLOAT2 LPAREN assign_expr COMMA assign_expr RPAREN  */
#line 914 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-5].base), exprs, NoLocation);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3087 "gram.tab.c"
    break;

  case 111: /* constructor_expr: FLOAT3 LPAREN assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 923 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-7].base), exprs, NoLocation);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3100 "gram.tab.c"
    break;

  case 112: /* constructor_expr: FLOAT4 LPAREN assign_expr COMMA assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 933 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-7].value), (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-9].base), exprs, NoLocation);
            delete (yyvsp[-8].loc);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3114 "gram.tab.c"
    break;

  case 113: /* constructor_expr: FLOAT2 LPAREN error RPAREN  */
#line 943 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3122 "gram.tab.c"
    break;

  case 114: /* constructor_expr: FLOAT3 LPAREN error RPAREN  */
#line 947 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3130 "gram.tab.c"
    break;

  case 115: /* constructor_expr: FLOAT4 LPAREN error RPAREN  */
#line 951 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3138 "gram.tab.c"
    break;

  case 116: /* constructor_expr: CHAR2 LPAREN assign_expr COMMA assign_expr RPAREN  */
#line 955 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-5].base), exprs, NoLocation);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3150 "gram.tab.c"
    break;

  case 117: /* constructor_expr: CHAR3 LPAREN assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 964 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-7].base), exprs, NoLocation);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3163 "gram.tab.c"
    break;

  case 118: /* constructor_expr: CHAR4 LPAREN assign_expr COMMA assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 974 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-7].value), (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-9].base), exprs, NoLocation);
            delete (yyvsp[-8].loc);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3177 "gram.tab.c"
    break;

  case 119: /* constructor_expr: UCHAR2 LPAREN assign_expr COMMA assign_expr RPAREN  */
#line 984 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-5].base), exprs, NoLocation);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3189 "gram.tab.c"
    break;

  case 120: /* constructor_expr: UCHAR3 LPAREN assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 993 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-7].base), exprs, NoLocation);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3202 "gram.tab.c"
    break;

  case 121: /* constructor_expr: UCHAR4 LPAREN assign_expr COMMA assign_expr COMMA assign_expr COMMA assign_expr RPAREN  */
#line 1003 "gram.y"
        {
	    Expression *exprs[] = { (yyvsp[-7].value), (yyvsp[-5].value), (yyvsp[-3].value), (yyvsp[-1].value) };
            (yyval.value) = new ConstructorExpr((yyvsp[-9].base), exprs, NoLocation);
            delete (yyvsp[-8].loc);
            delete (yyvsp[-6].loc);
            delete (yyvsp[-4].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3216 "gram.tab.c"
    break;

  case 122: /* constructor_expr: CHAR2 LPAREN error RPAREN  */
#line 1013 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3224 "gram.tab.c"
    break;

  case 123: /* constructor_expr: CHAR3 LPAREN error RPAREN  */
#line 1017 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3232 "gram.tab.c"
    break;

  case 124: /* constructor_expr: CHAR4 LPAREN error RPAREN  */
#line 1021 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3240 "gram.tab.c"
    break;

  case 125: /* constructor_expr: UCHAR2 LPAREN error RPAREN  */
#line 1025 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3248 "gram.tab.c"
    break;

  case 126: /* constructor_expr: UCHAR3 LPAREN error RPAREN  */
#line 1029 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3256 "gram.tab.c"
    break;

  case 127: /* constructor_expr: UCHAR4 LPAREN error RPAREN  */
#line 1033 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3264 "gram.tab.c"
    break;

  case 130: /* iter_constructor_expr: ITER LPAREN iter_constructor_arg COMMA iter_constructor_arg RPAREN  */
#line 1044 "gram.y"
        {
	   Symbol *sym = new Symbol();
	   Variable *var;

	   sym->name = strdup("iter");
	   var = new Variable(sym, *(yyvsp[-4].loc));
	   (yyval.value) = new FunctionCall(var, *(yyvsp[-4].loc));

	   ((FunctionCall *) (yyval.value))->addArg((yyvsp[-3].value));
	   ((FunctionCall *) (yyval.value))->addArg((yyvsp[-1].value));

           delete (yyvsp[-4].loc);
           delete (yyvsp[-2].loc);
           delete (yyvsp[0].loc);
	}
#line 3284 "gram.tab.c"
    break;

  case 131: /* iter_constructor_expr: ITER LPAREN error RPAREN  */
#line 1060 "gram.y"
        {
	   (yyval.value) = (Expression *) NULL;
	}
#line 3292 "gram.tab.c"
    break;

  case 142: /* sizeof_expr: SIZEOF LPAREN type_name RPAREN  */
#line 1078 "gram.y"
        {
            (yyval.value) = new SizeofExpr((yyvsp[-1].type),*(yyvsp[-3].loc));
            delete (yyvsp[-3].loc);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3303 "gram.tab.c"
    break;

  case 143: /* sizeof_expr: SIZEOF unary_expr  */
#line 1085 "gram.y"
        {
            (yyval.value) = new SizeofExpr((yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3312 "gram.tab.c"
    break;

  case 144: /* indexof_expr: INDEXOF ident  */
#line 1092 "gram.y"
        {
	  (yyval.value) = new BrtIndexofExpr(new Variable((yyvsp[0].symbol),*(yyvsp[-1].loc)),*(yyvsp[-1].loc));
	}
#line 3320 "gram.tab.c"
    break;

  case 145: /* indexof_expr: INDEXOF LPAREN ident RPAREN  */
#line 1096 "gram.y"
        {
	  (yyval.value) = new BrtIndexofExpr(new Variable((yyvsp[-1].symbol),*(yyvsp[-3].loc)),*(yyvsp[-3].loc));
	}
#line 3328 "gram.tab.c"
    break;

  case 146: /* unary_minus_expr: MINUS cast_expr  */
#line 1116 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_Minus,(yyvsp[0].value),NoLocation);
        }
#line 3336 "gram.tab.c"
    break;

  case 147: /* unary_plus_expr: PLUS cast_expr  */
#line 1122 "gram.y"
        {
            /* Unary plus is an ISO addition (for symmetry) - ignore it */
            (yyval.value) = (yyvsp[0].value);
        }
#line 3345 "gram.tab.c"
    break;

  case 148: /* addr_expr: B_AND cast_expr  */
#line 1129 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_AddrOf,(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3354 "gram.tab.c"
    break;

  case 149: /* indirection_expr: STAR cast_expr  */
#line 1136 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_Deref,(yyvsp[0].value),NoLocation);
        }
#line 3362 "gram.tab.c"
    break;

  case 150: /* preinc_expr: INCR unary_expr  */
#line 1142 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_PreInc,(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3371 "gram.tab.c"
    break;

  case 151: /* predec_expr: DECR unary_expr  */
#line 1149 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_PreDec,(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3380 "gram.tab.c"
    break;

  case 153: /* comma_expr: comma_expr COMMA assign_expr  */
#line 1157 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_Comma,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 3389 "gram.tab.c"
    break;

  case 154: /* prim_expr: ident  */
#line 1164 "gram.y"
        {
            (yyval.value) = new Variable((yyvsp[0].symbol),NoLocation);
        }
#line 3397 "gram.tab.c"
    break;

  case 156: /* prim_expr: constant  */
#line 1169 "gram.y"
        {
            (yyval.value) = (yyvsp[0].consValue);
        }
#line 3405 "gram.tab.c"
    break;

  case 157: /* paren_expr: LPAREN expr RPAREN  */
#line 1175 "gram.y"
        {
            (yyval.value) = (yyvsp[-1].value);
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3415 "gram.tab.c"
    break;

  case 158: /* paren_expr: LPAREN error RPAREN  */
#line 1181 "gram.y"
        {
            (yyval.value) = (Expression*) NULL;
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3425 "gram.tab.c"
    break;

  case 166: /* subscript_expr: postfix_expr LBRCKT expr RBRCKT  */
#line 1199 "gram.y"
        {
            (yyval.value) = new IndexExpr((yyvsp[-3].value),(yyvsp[-1].value),*(yyvsp[-2].loc));
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 3435 "gram.tab.c"
    break;

  case 169: /* postinc_expr: postfix_expr INCR  */
#line 1211 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_PostInc,(yyvsp[-1].value),*(yyvsp[0].loc));
            delete (yyvsp[0].loc);
        }
#line 3444 "gram.tab.c"
    break;

  case 170: /* postdec_expr: postfix_expr DECR  */
#line 1218 "gram.y"
        {
            (yyval.value) = new UnaryExpr(UO_PostDec,(yyvsp[-1].value),*(yyvsp[0].loc));
            delete (yyvsp[0].loc);
        }
#line 3453 "gram.tab.c"
    break;

  case 172: /* direct_comp_select: postfix_expr DOT field_ident  */
#line 1229 "gram.y"
        {
            Variable *var = new Variable((yyvsp[0].symbol),*(yyvsp[-1].loc));
            BinaryExpr *be = new BinaryExpr(BO_Member,(yyvsp[-2].value),var,*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
            (yyval.value) = be;

            // Lookup the component in its struct
            // if possible.
            if ((yyvsp[-2].value)->etype == ET_Variable)
            {
                Variable  *var = (Variable*) (yyvsp[-2].value);
                Symbol    *varName = var->name;
                SymEntry  *entry = varName->entry;

                if (entry && entry->uVarDecl)
                {
                    entry->uVarDecl->lookup((yyvsp[0].symbol));
                }
            }
        }
#line 3478 "gram.tab.c"
    break;

  case 173: /* indirect_comp_select: postfix_expr ARROW field_ident  */
#line 1252 "gram.y"
        {
            Variable *var = new Variable((yyvsp[0].symbol),*(yyvsp[-1].loc));
            BinaryExpr *be = new BinaryExpr(BO_PtrMember,(yyvsp[-2].value),var,*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
            (yyval.value) = be;

            // Lookup the component in its struct
            // if possible.
            if ((yyvsp[-2].value)->etype == ET_Variable)
            {
                Variable  *var = (Variable*) (yyvsp[-2].value);
                Symbol    *varName = var->name;
                SymEntry  *entry = varName->entry;

                if (entry && entry->uVarDecl)
                {
                    entry->uVarDecl->lookup((yyvsp[0].symbol));
                }
            }
        }
#line 3503 "gram.tab.c"
    break;

  case 174: /* func_call: postfix_expr LPAREN opt_expr_list RPAREN  */
#line 1275 "gram.y"
        {
            FunctionCall* fc = new FunctionCall((yyvsp[-3].value),*(yyvsp[-2].loc));

            /* add function args */
            fc->addArgs(ReverseList((yyvsp[-1].value)));

            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
            (yyval.value) = fc;
        }
#line 3518 "gram.tab.c"
    break;

  case 175: /* opt_expr_list: %empty  */
#line 1288 "gram.y"
        {
            (yyval.value) = (Expression*) NULL;
        }
#line 3526 "gram.tab.c"
    break;

  case 179: /* expr_list: expr_list COMMA assign_expr  */
#line 1297 "gram.y"
        {
            (yyval.value) = (yyvsp[0].value);
            (yyval.value)->next = (yyvsp[-2].value);

            delete (yyvsp[-1].loc);
        }
#line 3537 "gram.tab.c"
    break;

  case 180: /* expr_list: expr_list COMMA constructor_expr  */
#line 1304 "gram.y"
        {
            (yyval.value) = (yyvsp[0].value);
            (yyval.value)->next = (yyvsp[-2].value);

            delete (yyvsp[-1].loc);
        }
#line 3548 "gram.tab.c"
    break;

  case 200: /* $@2: %empty  */
#line 1351 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ReEnterScope();
        }
#line 3557 "gram.tab.c"
    break;

  case 201: /* opt_KnR_declaration_list: $@2  */
#line 1356 "gram.y"
        {
            (yyval.decl) = (Decl*) NULL;
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope();
        }
#line 3567 "gram.tab.c"
    break;

  case 202: /* $@3: %empty  */
#line 1362 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ReEnterScope();
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
        }
#line 3577 "gram.tab.c"
    break;

  case 203: /* $@4: %empty  */
#line 1367 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->SetVarParam(1, !err_top_level, 0); 
            gProject->Parse_TOS->parseCtxt->SetIsKnR(true); 
        }
#line 3585 "gram.tab.c"
    break;

  case 204: /* opt_KnR_declaration_list: $@3 $@4 declaration_list  */
#line 1371 "gram.y"
        {   (yyval.decl) = (yyvsp[0].decl);
            gProject->Parse_TOS->parseCtxt->SetIsKnR(false); 
            gProject->Parse_TOS->parseCtxt->SetVarParam(0, !err_top_level, 1); 
            
            // Exit, but will allow re-enter for a function.
            // Hack, to handle parameters being in the function's scope.
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope(true);
        }
#line 3599 "gram.tab.c"
    break;

  case 205: /* $@5: %empty  */
#line 1383 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.EnterScope();
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
        }
#line 3609 "gram.tab.c"
    break;

  case 206: /* opt_declaration_list: $@5  */
#line 1389 "gram.y"
        {
            (yyval.decl) = (Decl*) NULL;
        }
#line 3617 "gram.tab.c"
    break;

  case 207: /* $@6: %empty  */
#line 1393 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.EnterScope();
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
        }
#line 3627 "gram.tab.c"
    break;

  case 208: /* $@7: %empty  */
#line 1398 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->SetVarParam(0, !err_top_level, 0); 
        }
#line 3634 "gram.tab.c"
    break;

  case 209: /* opt_declaration_list: $@6 $@7 declaration_list  */
#line 1401 "gram.y"
        {   (yyval.decl) = (yyvsp[0].decl);
            gProject->Parse_TOS->parseCtxt->SetVarParam(0, !err_top_level, 0);
        }
#line 3642 "gram.tab.c"
    break;

  case 210: /* declaration_list: declaration SEMICOLON  */
#line 1407 "gram.y"
        {
            (yyval.decl) = (yyvsp[-1].decl);
            delete (yyvsp[0].loc);
        }
#line 3651 "gram.tab.c"
    break;

  case 211: /* declaration_list: declaration SEMICOLON declaration_list  */
#line 1412 "gram.y"
        {
            (yyval.decl) = (yyvsp[-2].decl);

			Decl*	appendDecl = (yyvsp[-2].decl);
			while (appendDecl->next != NULL)
				appendDecl = appendDecl->next;

            appendDecl->next = (yyvsp[0].decl);
            delete (yyvsp[-1].loc);
        }
#line 3666 "gram.tab.c"
    break;

  case 212: /* decl_stemnt: old_style_declaration SEMICOLON  */
#line 1425 "gram.y"
        {
            (yyval.declStemnt) = new DeclStemnt(*(yyvsp[0].loc));
            (yyval.declStemnt)->addDecls(ReverseList((yyvsp[-1].decl)));
            delete (yyvsp[0].loc);
        }
#line 3676 "gram.tab.c"
    break;

  case 213: /* decl_stemnt: declaration SEMICOLON  */
#line 1432 "gram.y"
        {
            (yyval.declStemnt) = new DeclStemnt(*(yyvsp[0].loc));
            (yyval.declStemnt)->addDecls(ReverseList((yyvsp[-1].decl)));
            delete (yyvsp[0].loc);
        }
#line 3686 "gram.tab.c"
    break;

  case 214: /* old_style_declaration: no_decl_specs opt_init_decl_list  */
#line 1440 "gram.y"
        {
            assert (err_top_level ||
                    (yyvsp[-1].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();
            
            yywarn("old-style declaration or incorrect type");

            possibleType = true;
            (yyval.decl) = (yyvsp[0].decl);

            if ((yyval.decl) == NULL)
            {
                (yyval.decl) = new Decl((yyvsp[-1].base));
            }
        }
#line 3706 "gram.tab.c"
    break;

  case 215: /* declaration: decl_specs opt_init_decl_list  */
#line 1458 "gram.y"
        {
            assert (1||err_top_level ||
                    (yyvsp[-1].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            if ((yyvsp[-1].base)!=gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs) {
              if (!err_top_level) {
                baseTypeFixup((yyvsp[-1].base),(yyvsp[0].decl));
              }
            }
            gProject->Parse_TOS->parseCtxt->ResetDeclCtxt();            
            
            possibleType = true;
            (yyval.decl) = (yyvsp[0].decl);
            
            if ((yyval.decl) == NULL)
            {
                (yyval.decl) = new Decl((yyvsp[-1].base));
            }
        }
#line 3729 "gram.tab.c"
    break;

  case 216: /* no_decl_specs: %empty  */
#line 1483 "gram.y"
        {
            (yyval.base) = new BaseType(BT_Int);
            gProject->Parse_TOS->parseCtxt->SetDeclCtxt((yyval.base));
        }
#line 3738 "gram.tab.c"
    break;

  case 219: /* $@8: %empty  */
#line 1502 "gram.y"
        {   
            gProject->Parse_TOS->parseCtxt->PushCtxt();
            gProject->Parse_TOS->parseCtxt->ResetVarParam();
        }
#line 3747 "gram.tab.c"
    break;

  case 220: /* type_name: $@8 type_name_bis  */
#line 1507 "gram.y"
        {
            (yyval.type) = (yyvsp[0].type);
            gProject->Parse_TOS->parseCtxt->PopCtxt(false);
        }
#line 3756 "gram.tab.c"
    break;

  case 221: /* type_name_bis: decl_specs_reentrance_bis  */
#line 1514 "gram.y"
        {
            assert ((yyvsp[0].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            
            possibleType = true;
            (yyval.type) = (yyvsp[0].base);
            if ((yyval.type)->isFunction())
                yyerr ("Function type not allowed as type name");
        }
#line 3769 "gram.tab.c"
    break;

  case 222: /* type_name_bis: decl_specs_reentrance_bis abs_decl  */
#line 1523 "gram.y"
        {
            assert ((yyvsp[-1].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            
            possibleType = true;
            (yyval.type) = (yyvsp[0].type);
            
            Type * extended = (yyval.type)->extend((yyvsp[-1].base));
            if ((yyval.type)->isFunction())
                yyerr ("Function type not allowed as type name");
            else if (extended && 
                (yyvsp[-1].base) && (yyvsp[-1].base)->isFunction() && 
                ! extended->isPointer())
                yyerr ("Wrong type combination") ;
                
        }
#line 3789 "gram.tab.c"
    break;

  case 223: /* decl_specs_reentrance_bis: decl_specs_reentrance  */
#line 1546 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->SetDeclCtxt((yyval.base));
        }
#line 3797 "gram.tab.c"
    break;

  case 230: /* storage_class: local_storage_class  */
#line 1562 "gram.y"
        {
            if (! gProject->Parse_TOS->transUnit ||
                gProject->Parse_TOS->transUnit->contxt.syms->current->level >= FUNCTION_SCOPE)
                 (yyval.storage) = (yyvsp[0].storage) ;             
             else
                 (yyval.storage) = ST_None ;              
        }
#line 3809 "gram.tab.c"
    break;

  case 232: /* opt_decl_specs_reentrance: %empty  */
#line 1575 "gram.y"
        {
            (yyval.base) = (BaseType*) NULL;
        }
#line 3817 "gram.tab.c"
    break;

  case 234: /* decl_specs_reentrance: storage_class opt_decl_specs_reentrance  */
#line 1582 "gram.y"
        {
            (yyval.base) = (yyvsp[0].base);

            if (!(yyval.base))
            {
                (yyval.base) = new BaseType();
            }

            if ((yyvsp[-1].storage) == ST_None)
                 yyerr("Invalid use of local storage type");
            else if ((yyval.base)->storage != ST_None)             
                 yywarn("Overloading previous storage type specification");
            else
                 (yyval.base)->storage = (yyvsp[-1].storage);
        }
#line 3837 "gram.tab.c"
    break;

  case 235: /* $@9: %empty  */
#line 1597 "gram.y"
                       { possibleType = false; }
#line 3843 "gram.tab.c"
    break;

  case 236: /* decl_specs_reentrance: type_spec $@9 opt_decl_specs_reentrance  */
#line 1598 "gram.y"
        {
            (yyval.base) = (yyvsp[-2].base);

            if ((yyvsp[0].base))
            {
                if (((yyvsp[0].base)->typemask & BT_Long)
                    && ((yyval.base)->typemask & BT_Long))
                {
                   // long long : A likely C9X addition 
                   yyerr("long long support has been removed");
                }
                else
                    (yyval.base)->typemask |= (yyvsp[0].base)->typemask;

                if ((yyvsp[0].base)->storage != ST_None)
                    (yyval.base)->storage = (yyvsp[0].base)->storage;

                // delete $3;
            }

            /*
            std::cout << "In decl_spec: ";
            $$->printBase(std::cout,0);
            if ($$->storage == ST_Typedef)
                std::cout << "(is a typedef)";
            std::cout << std::endl;
            */
        }
#line 3876 "gram.tab.c"
    break;

  case 237: /* decl_specs_reentrance: type_qual opt_decl_specs_reentrance  */
#line 1627 "gram.y"
        {
            (yyval.base) = (yyvsp[0].base);

            if (!(yyval.base))
            {
                (yyval.base) = new BaseType();
            }

            if (TQ_None != ((yyval.base)->qualifier & (yyvsp[-1].typeQual)))
                yywarn("qualifier already specified");  
                              
            (yyval.base)->qualifier |= (yyvsp[-1].typeQual);

        }
#line 3895 "gram.tab.c"
    break;

  case 238: /* opt_comp_decl_specs: %empty  */
#line 1647 "gram.y"
        {
           (yyval.base) = (BaseType*) NULL;
        }
#line 3903 "gram.tab.c"
    break;

  case 240: /* $@10: %empty  */
#line 1653 "gram.y"
                                                  { possibleType = false; }
#line 3909 "gram.tab.c"
    break;

  case 241: /* comp_decl_specs_reentrance: type_spec_reentrance $@10 opt_comp_decl_specs  */
#line 1654 "gram.y"
        {
            (yyval.base) = (yyvsp[-2].base);

            if ((yyvsp[0].base))
            {
                (yyval.base)->typemask |= (yyvsp[0].base)->typemask;
                // delete $3;
            }
        }
#line 3923 "gram.tab.c"
    break;

  case 242: /* comp_decl_specs_reentrance: type_qual opt_comp_decl_specs  */
#line 1664 "gram.y"
        {
            (yyval.base) = (yyvsp[0].base);

            if (!(yyval.base))
            {
                (yyval.base) = new BaseType();
            }

            if (TQ_None != ((yyval.base)->qualifier & (yyvsp[-1].typeQual)))
                yywarn("qualifier already specified");
            (yyval.base)->qualifier |= (yyvsp[-1].typeQual);
        }
#line 3940 "gram.tab.c"
    break;

  case 243: /* comp_decl_specs: comp_decl_specs_reentrance  */
#line 1679 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->SetDeclCtxt((yyval.base));
        }
#line 3948 "gram.tab.c"
    break;

  case 244: /* $@11: %empty  */
#line 1688 "gram.y"
        {
           (yyvsp[0].decl)->extend(gProject->Parse_TOS->parseCtxt->UseDeclCtxt());
        }
#line 3956 "gram.tab.c"
    break;

  case 245: /* decl: declarator $@11 opt_gcc_attrib  */
#line 1692 "gram.y"
        {
           (yyvsp[-2].decl)->attrib = (yyvsp[0].gccAttrib);
           (yyval.decl) = (yyvsp[-2].decl);
        }
#line 3965 "gram.tab.c"
    break;

  case 247: /* init_decl: decl EQ initializer  */
#line 1700 "gram.y"
        {
           (yyvsp[-2].decl)->initializer = (yyvsp[0].value);
           (yyval.decl) = (yyvsp[-2].decl);
        }
#line 3974 "gram.tab.c"
    break;

  case 248: /* opt_init_decl_list: %empty  */
#line 1707 "gram.y"
        {
          (yyval.decl) = (Decl*) NULL;
        }
#line 3982 "gram.tab.c"
    break;

  case 251: /* init_decl_list_reentrance: init_decl  */
#line 1717 "gram.y"
        {
            (yyval.decl) = (yyvsp[0].decl);
        }
#line 3990 "gram.tab.c"
    break;

  case 252: /* init_decl_list_reentrance: init_decl_list_reentrance COMMA init_decl  */
#line 1721 "gram.y"
        {
            (yyval.decl) = (yyvsp[-2].decl);

			Decl*	appendDecl = (yyvsp[-2].decl);
			while (appendDecl->next != NULL)
				appendDecl = appendDecl->next;

            appendDecl->next = (yyvsp[0].decl);
            delete (yyvsp[-1].loc);
        }
#line 4005 "gram.tab.c"
    break;

  case 254: /* initializer_list: initializer_reentrance  */
#line 1739 "gram.y"
        {
            (yyval.arrayConst) = new ArrayConstant(NoLocation);
            (yyval.arrayConst)->addElement((yyvsp[0].value));
        }
#line 4014 "gram.tab.c"
    break;

  case 255: /* initializer_list: initializer_list COMMA initializer_reentrance  */
#line 1744 "gram.y"
        {
            (yyval.arrayConst) = (yyvsp[-2].arrayConst);
            (yyval.arrayConst)->addElement((yyvsp[0].value));
            delete (yyvsp[-1].loc);
        }
#line 4024 "gram.tab.c"
    break;

  case 259: /* initializer_reentrance: LBRACE initializer_list opt_comma RBRACE  */
#line 1755 "gram.y"
        {
            (yyval.value) = (yyvsp[-2].arrayConst);
            delete (yyvsp[-3].loc);
            delete (yyvsp[0].loc);
        }
#line 4034 "gram.tab.c"
    break;

  case 260: /* opt_comma: %empty  */
#line 1763 "gram.y"
        {
            (yyval.loc) = (Location*) NULL;
        }
#line 4042 "gram.tab.c"
    break;

  case 261: /* opt_comma: COMMA  */
#line 1767 "gram.y"
        {
            delete (yyvsp[0].loc);
            (yyval.loc) = (Location*) NULL;
        }
#line 4051 "gram.tab.c"
    break;

  case 269: /* type_qual_token: VOUT LBRCKT opt_const_expr RBRCKT  */
#line 1788 "gram.y"
        {
           TypeQual r((yyvsp[-3].typeQual));
           r.vout=(yyvsp[-1].value);
           (yyval.typeQual) = r;
        }
#line 4061 "gram.tab.c"
    break;

  case 271: /* type_qual_list: type_qual_list type_qual_token  */
#line 1797 "gram.y"
        {
            (yyval.typeQual) = (yyvsp[-1].typeQual) | (yyvsp[0].typeQual);
            if (TQ_None != ((yyvsp[0].typeQual) & (yyvsp[-1].typeQual)))
                yywarn("qualifier already specified");                               
        }
#line 4071 "gram.tab.c"
    break;

  case 272: /* opt_type_qual_list: %empty  */
#line 1805 "gram.y"
        {
            (yyval.typeQual) = TQ_None;
        }
#line 4079 "gram.tab.c"
    break;

  case 309: /* typedef_name: TYPEDEF_NAME  */
#line 1854 "gram.y"
        {
            (yyval.base) = new BaseType(BT_UserType);
            (yyval.base)->typeName = (yyvsp[0].symbol);
        }
#line 4088 "gram.tab.c"
    break;

  case 310: /* tag_ref: TAG_NAME  */
#line 1861 "gram.y"
        {
            assert ((! (yyval.symbol)->entry) || 
                    (yyval.symbol)->entry->IsTagDecl()) ;
        }
#line 4097 "gram.tab.c"
    break;

  case 311: /* struct_tag_ref: STRUCT tag_ref  */
#line 1869 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_ref((yyvsp[-1].typeSpec), (yyvsp[0].symbol),
                                                                gProject->Parse_TOS->transUnit->contxt.tags);
            else
                (yyval.base) = NULL;                                         
        }
#line 4109 "gram.tab.c"
    break;

  case 312: /* union_tag_ref: UNION tag_ref  */
#line 1879 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_ref((yyvsp[-1].typeSpec), (yyvsp[0].symbol),
                                                                gProject->Parse_TOS->transUnit->contxt.tags);
            else
                (yyval.base) = NULL;                                         
        }
#line 4121 "gram.tab.c"
    break;

  case 313: /* enum_tag_ref: ENUM tag_ref  */
#line 1889 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_ref((yyvsp[-1].typeSpec), (yyvsp[0].symbol),
                                                                gProject->Parse_TOS->transUnit->contxt.tags);
            else
                (yyval.base) = NULL;                                         
        }
#line 4133 "gram.tab.c"
    break;

  case 314: /* struct_tag_def: STRUCT tag_ref  */
#line 1899 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_def((yyvsp[-1].typeSpec), (yyvsp[0].symbol),
                                                            gProject->Parse_TOS->transUnit->contxt.tags);
            else
                (yyval.base) = NULL;                                         
        }
#line 4145 "gram.tab.c"
    break;

  case 315: /* struct_type_define: STRUCT LBRACE struct_or_union_definition RBRACE  */
#line 1909 "gram.y"
        {
            (yyval.base) = new BaseType((yyvsp[-1].strDef));
            (yyvsp[-1].strDef)->_isUnion = false;
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 4156 "gram.tab.c"
    break;

  case 316: /* struct_type_define: struct_tag_def LBRACE struct_or_union_definition RBRACE  */
#line 1916 "gram.y"
        {
            (yyval.base) = (yyvsp[-3].base);
            assert (! (yyval.base)->stDefn);
            (yyval.base)->stDefn = (yyvsp[-1].strDef);
            (yyvsp[-1].strDef)->tag = (yyvsp[-3].base)->tag->dup();
            (yyvsp[-1].strDef)->_isUnion = false;

            // Overload the incomplete definition
            (yyval.base)->tag->entry->uStructDef = (yyval.base) ;
            
//             std::cout << "struct/union/enum_type_define:"
//                          "The definition of:"
//                       << "(uStructDef:" << $1->tag->entry->uStructDef << ")"
//                       << "(uStructDef->stDefn:" << $1->tag->entry->uStructDef->stDefn << ")"
//                       << "(" << $1->tag->entry << ")" << $1->tag->name  << "$" ;
//             $1->tag->entry->scope->ShowScopeId(std::cout);
//             std::cout << " has been completed" << endl; 
            
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 4182 "gram.tab.c"
    break;

  case 317: /* union_tag_def: UNION tag_ref  */
#line 1940 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_def((yyvsp[-1].typeSpec), (yyvsp[0].symbol),
                                                            gProject->Parse_TOS->transUnit->contxt.tags);
            else
              (yyval.base) = NULL ;
        }
#line 4194 "gram.tab.c"
    break;

  case 318: /* union_type_define: UNION LBRACE struct_or_union_definition RBRACE  */
#line 1950 "gram.y"
        {
            (yyval.base) = new BaseType((yyvsp[-1].strDef));
            (yyvsp[-1].strDef)->_isUnion = true;

            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 4206 "gram.tab.c"
    break;

  case 319: /* union_type_define: union_tag_def LBRACE struct_or_union_definition RBRACE  */
#line 1958 "gram.y"
        {
            (yyval.base) = (yyvsp[-3].base);
            assert (! (yyval.base)->stDefn);
            (yyval.base)->stDefn = (yyvsp[-1].strDef);
            (yyvsp[-1].strDef)->tag = (yyvsp[-3].base)->tag->dup();
            (yyvsp[-1].strDef)->_isUnion = true;

            // Overload the incomplete definition
            (yyval.base)->tag->entry->uStructDef = (yyval.base) ;
            
//             std::cout << "struct/union/enum_type_define:"
//                          "The definition of:"
//                       << "(uStructDef:" << $1->tag->entry->uStructDef << ")"
//                       << "(uStructDef->stDefn:" << $1->tag->entry->uStructDef->stDefn << ")"
//                       << "(" << $1->tag->entry << ")" << $1->tag->name  << "$" ;
//             $1->tag->entry->scope->ShowScopeId(std::cout);
//             std::cout << " has been completed" << endl; 
            
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
 
        }
#line 4233 "gram.tab.c"
    break;

  case 320: /* enum_tag_def: ENUM tag_ref  */
#line 1983 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                (yyval.base) = gProject->Parse_TOS->parseCtxt->Mk_tag_def((yyvsp[-1].typeSpec),(yyvsp[0].symbol),
                                                            gProject->Parse_TOS->transUnit->contxt.tags);
            else
              (yyval.base) = NULL;
        }
#line 4245 "gram.tab.c"
    break;

  case 321: /* enum_type_define: ENUM LBRACE enum_definition RBRACE  */
#line 1993 "gram.y"
        {
            (yyval.base) = new BaseType((yyvsp[-1].enDef));

            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 4256 "gram.tab.c"
    break;

  case 322: /* enum_type_define: enum_tag_def LBRACE enum_definition RBRACE  */
#line 2000 "gram.y"
        {
            (yyval.base) = (yyvsp[-3].base);
            assert (! (yyval.base)->enDefn);
            (yyval.base)->enDefn = (yyvsp[-1].enDef);
            (yyvsp[-1].enDef)->tag = (yyvsp[-3].base)->tag->dup();

            // Overload the incomplete definition
            (yyval.base)->tag->entry->uStructDef = (yyval.base) ;
            
//             std::cout << "struct/union/enum_type_define:"
//                          "The definition of:"
//                       << "(uStructDef:" << $1->tag->entry->uStructDef << ")"
//                       << "(uStructDef->stDefn:" << $1->tag->entry->uStructDef->stDefn << ")"
//                       << "(" << $1->tag->entry << ")" << $1->tag->name  << "$" ;
//             $1->tag->entry->scope->ShowScopeId(std::cout);
//             std::cout << " has been completed" << endl; 
            
            delete (yyvsp[-2].loc);
            delete (yyvsp[0].loc);
        }
#line 4281 "gram.tab.c"
    break;

  case 323: /* struct_or_union_definition: %empty  */
#line 2023 "gram.y"
        {  (yyval.strDef) = new StructDef();
           yywarn("ANSI/ISO C prohibits empty struct/union");
        }
#line 4289 "gram.tab.c"
    break;

  case 325: /* enum_definition: %empty  */
#line 2030 "gram.y"
        {  (yyval.enDef) = new EnumDef();
           yywarn("ANSI/ISO C prohibits empty enum");
        }
#line 4297 "gram.tab.c"
    break;

  case 326: /* enum_definition: enum_def_list opt_trailing_comma  */
#line 2034 "gram.y"
        {  (yyval.enDef) = (yyvsp[-1].enDef);
        }
#line 4304 "gram.tab.c"
    break;

  case 327: /* opt_trailing_comma: %empty  */
#line 2039 "gram.y"
        {
            (yyval.loc) = NULL;
        }
#line 4312 "gram.tab.c"
    break;

  case 328: /* opt_trailing_comma: COMMA  */
#line 2043 "gram.y"
        {
          yywarn("Trailing comma in enum type definition");
        }
#line 4320 "gram.tab.c"
    break;

  case 330: /* enum_def_list_reentrance: enum_const_def  */
#line 2056 "gram.y"
        {
            (yyval.enDef) = new EnumDef();
            (yyval.enDef)->addElement((yyvsp[0].enConst));
        }
#line 4329 "gram.tab.c"
    break;

  case 331: /* enum_def_list_reentrance: enum_def_list COMMA enum_const_def  */
#line 2061 "gram.y"
        {
            (yyval.enDef) = (yyvsp[-2].enDef);
            (yyval.enDef)->addElement((yyvsp[0].enConst));
            delete (yyvsp[-1].loc);
        }
#line 4339 "gram.tab.c"
    break;

  case 332: /* enum_const_def: enum_constant  */
#line 2069 "gram.y"
        {
            (yyval.enConst) = new EnumConstant((yyvsp[0].symbol),NULL,NoLocation);
            if (gProject->Parse_TOS->transUnit)
            {
              if (gProject->Parse_TOS->transUnit->contxt.syms->IsDefined((yyvsp[0].symbol)->name))
                 yyerr("Duplicate enumeration constant");
                 
              (yyvsp[0].symbol)->entry = gProject->Parse_TOS->transUnit->contxt.syms->Insert(
                                  mk_enum_const((yyvsp[0].symbol)->name, (yyval.enConst)));
            }
        }
#line 4355 "gram.tab.c"
    break;

  case 333: /* enum_const_def: enum_constant EQ assign_expr  */
#line 2081 "gram.y"
        {
            (yyval.enConst) = new EnumConstant((yyvsp[-2].symbol),(yyvsp[0].value),NoLocation);
            if (gProject->Parse_TOS->transUnit)
            {
              if (gProject->Parse_TOS->transUnit->contxt.syms->IsDefined((yyvsp[-2].symbol)->name))
                 yyerr("Duplicate enumeration constant");
                 
              (yyvsp[-2].symbol)->entry = gProject->Parse_TOS->transUnit->contxt.syms->Insert(
                                  mk_enum_const((yyvsp[-2].symbol)->name, (yyval.enConst)));
            }
        }
#line 4371 "gram.tab.c"
    break;

  case 335: /* $@12: %empty  */
#line 2101 "gram.y"
        {
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.EnterScope();
            gProject->Parse_TOS->parseCtxt->PushCtxt();
        }
#line 4381 "gram.tab.c"
    break;

  case 336: /* $@13: %empty  */
#line 2106 "gram.y"
        {
            assert (!err_top_level || possibleType);
             /* Safety precaution! */
             possibleType=true;
        }
#line 4391 "gram.tab.c"
    break;

  case 337: /* field_list: $@12 $@13 field_list_reentrance  */
#line 2112 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->PopCtxt(false);
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope();
            (yyval.strDef) = (yyvsp[0].strDef) ;
        }
#line 4402 "gram.tab.c"
    break;

  case 338: /* field_list_reentrance: comp_decl SEMICOLON  */
#line 2121 "gram.y"
        {
            (yyval.strDef) = new StructDef();
            (yyval.strDef)->addComponent(ReverseList((yyvsp[-1].decl)));
            delete (yyvsp[0].loc);
        }
#line 4412 "gram.tab.c"
    break;

  case 339: /* field_list_reentrance: field_list_reentrance SEMICOLON  */
#line 2127 "gram.y"
        {
            // A useful gcc extension:
            //   naked semicolons in struct/union definitions. 
            (yyval.strDef) = (yyvsp[-1].strDef);
            yywarn ("Empty declaration");
            delete (yyvsp[0].loc);
        }
#line 4424 "gram.tab.c"
    break;

  case 340: /* field_list_reentrance: field_list_reentrance comp_decl SEMICOLON  */
#line 2135 "gram.y"
        {
            (yyval.strDef) = (yyvsp[-2].strDef);
            (yyval.strDef)->addComponent(ReverseList((yyvsp[-1].decl)));
            delete (yyvsp[0].loc);
        }
#line 4434 "gram.tab.c"
    break;

  case 341: /* comp_decl: comp_decl_specs comp_decl_list  */
#line 2143 "gram.y"
        {
            possibleType = true;
            (yyval.decl) = (yyvsp[0].decl);
        }
#line 4443 "gram.tab.c"
    break;

  case 342: /* comp_decl: comp_decl_specs  */
#line 2148 "gram.y"
        {
            possibleType = true;
            (yyval.decl) = new Decl ((yyvsp[0].base));
            yywarn ("No field declarator");
        }
#line 4453 "gram.tab.c"
    break;

  case 344: /* $@14: %empty  */
#line 2162 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->SetIsFieldId(true); 
        }
#line 4460 "gram.tab.c"
    break;

  case 345: /* comp_decl_list_reentrance: $@14 comp_declarator opt_gcc_attrib  */
#line 2165 "gram.y"
        {
            (yyval.decl) = (yyvsp[-1].decl);
            (yyval.decl)->attrib = (yyvsp[0].gccAttrib);
        }
#line 4469 "gram.tab.c"
    break;

  case 346: /* $@15: %empty  */
#line 2170 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->SetIsFieldId(true); 
        }
#line 4476 "gram.tab.c"
    break;

  case 347: /* comp_decl_list_reentrance: comp_decl_list_reentrance COMMA $@15 comp_declarator opt_gcc_attrib  */
#line 2173 "gram.y"
        {
            (yyval.decl) = (yyvsp[-1].decl);
            (yyval.decl)->attrib = (yyvsp[0].gccAttrib);
            (yyval.decl)->next = (yyvsp[-4].decl);
            delete (yyvsp[-3].loc);
        }
#line 4487 "gram.tab.c"
    break;

  case 348: /* comp_declarator: simple_comp  */
#line 2182 "gram.y"
        {
           gProject->Parse_TOS->parseCtxt->SetIsFieldId(false); 
           Type * decl = gProject->Parse_TOS->parseCtxt->UseDeclCtxt();
           Type * extended = (yyval.decl)->extend(decl);
           if ((yyval.decl)->form &&
               (yyval.decl)->form->isFunction())
               yyerr ("Function type not allowed as field");
           else if ((yyval.decl)->form &&
                    (yyval.decl)->form->isArray() &&
                    ! ((ArrayType *) (yyval.decl)->form)->size)
               yyerr ("Unsized array not allowed as field");
           else if (extended && 
               decl && decl->isFunction() && 
               ! extended->isPointer())
               yyerr ("Wrong type combination") ;
                
        }
#line 4509 "gram.tab.c"
    break;

  case 349: /* comp_declarator: bit_field  */
#line 2200 "gram.y"
        {
           Type * decl = gProject->Parse_TOS->parseCtxt->UseDeclCtxt();
           (yyval.decl)->extend(decl);
           if (! decl)
               yyerr ("No type specifier for bit field") ;
           else if (!(yyval.decl)->form)
               yyerr ("Wrong type combination") ;
        }
#line 4522 "gram.tab.c"
    break;

  case 351: /* $@16: %empty  */
#line 2214 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->SetIsFieldId(false); 
        }
#line 4529 "gram.tab.c"
    break;

  case 352: /* bit_field: opt_declarator COLON $@16 width  */
#line 2217 "gram.y"
        {
            BitFieldType  *bf = new BitFieldType((yyvsp[0].value));
            (yyval.decl) = (yyvsp[-3].decl);

            if ((yyval.decl) == NULL)
            {
                (yyval.decl) = new Decl(bf);
            }
            else
            {
                bf->subType = (yyval.decl)->form;
                (yyval.decl)->form = bf;
            }
        }
#line 4548 "gram.tab.c"
    break;

  case 354: /* opt_declarator: %empty  */
#line 2237 "gram.y"
        {
           (yyval.decl) = (Decl*) NULL;
        }
#line 4556 "gram.tab.c"
    break;

  case 356: /* declarator: declarator_reentrance_bis  */
#line 2248 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->Mk_declarator ((yyval.decl));
        }
#line 4564 "gram.tab.c"
    break;

  case 357: /* func_declarator: declarator_reentrance_bis  */
#line 2254 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->Mk_func_declarator ((yyval.decl));
        }
#line 4572 "gram.tab.c"
    break;

  case 358: /* declarator_reentrance_bis: pointer direct_declarator_reentrance_bis  */
#line 2260 "gram.y"
        {
            (yyval.decl) = (yyvsp[0].decl);
            (yyval.decl)->extend((yyvsp[-1].ptr));
        }
#line 4581 "gram.tab.c"
    break;

  case 361: /* direct_declarator_reentrance: ident  */
#line 2271 "gram.y"
        {  if (gProject->Parse_TOS->transUnit)
                (yyval.decl) = gProject->Parse_TOS->parseCtxt->Mk_direct_declarator_reentrance ((yyvsp[0].symbol),
                gProject->Parse_TOS->transUnit->contxt.syms);
        }
#line 4590 "gram.tab.c"
    break;

  case 362: /* direct_declarator_reentrance: LPAREN declarator_reentrance_bis RPAREN  */
#line 2276 "gram.y"
        {
            (yyval.decl) = (yyvsp[-1].decl);
            delete (yyvsp[-2].loc) ;
            delete (yyvsp[0].loc) ;
        }
#line 4600 "gram.tab.c"
    break;

  case 365: /* direct_declarator_reentrance: direct_declarator_reentrance LPAREN param_type_list RPAREN  */
#line 2284 "gram.y"
        {
            (yyval.decl) = (yyvsp[-3].decl);
            FunctionType * ft = new FunctionType(ReverseList((yyvsp[-1].decl)));
            Type * extended = (yyval.decl)->extend(ft);
            if (extended && ! extended->isPointer())
                yyerr ("Wrong type combination") ;
                
            delete (yyvsp[-2].loc) ;
            delete (yyvsp[0].loc) ;
            // Exit, but will allow re-enter for a function.
            // Hack, to handle parameters being in the function's scope.
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope(true);

        }
#line 4620 "gram.tab.c"
    break;

  case 366: /* direct_declarator_reentrance: direct_declarator_reentrance LPAREN ident_list RPAREN  */
#line 2300 "gram.y"
        {
            (yyval.decl) = (yyvsp[-3].decl);
            FunctionType * ft = new FunctionType(ReverseList((yyvsp[-1].decl)));
            Type * extended = (yyval.decl)->extend(ft);
            if (extended && ! extended->isPointer())
                yyerr ("Wrong type combination") ;

            delete (yyvsp[-2].loc) ;
            delete (yyvsp[0].loc) ;
            // Exit, but will allow re-enter for a function.
            // Hack, to handle parameters being in the function's scope.
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.ExitScope(true);

        }
#line 4640 "gram.tab.c"
    break;

  case 367: /* direct_declarator_reentrance: direct_declarator_reentrance LPAREN RPAREN  */
#line 2316 "gram.y"
        {
            (yyval.decl) = (yyvsp[-2].decl);

			if ((yyval.decl) != NULL)
			{
				FunctionType* ft = new FunctionType();
				Type* extended = (yyval.decl)->extend(ft);
				if (extended && ! extended->isPointer())
           	 	    yyerr ("Wrong type combination") ;
			}
            
            delete (yyvsp[-1].loc) ;
            delete (yyvsp[0].loc) ;
            if (gProject->Parse_TOS->transUnit)
            {
                gProject->Parse_TOS->transUnit->contxt.EnterScope();
                // Exit, but will allow re-enter for a function.
                // Hack, to handle parameters being in the function's scope.
                gProject->Parse_TOS->transUnit->contxt.ExitScope(true);
            }
        }
#line 4666 "gram.tab.c"
    break;

  case 368: /* array_decl: direct_declarator_reentrance LBRCKT opt_const_expr RBRCKT  */
#line 2340 "gram.y"
        {
            (yyval.decl) = (yyvsp[-3].decl);
            ArrayType * at = new ArrayType(TT_Array, (yyvsp[-1].value));
            Type * extended = (yyval.decl)->extend(at);
            if (extended && 
                extended->isFunction())
                yyerr ("Wrong type combination") ;
              
            delete (yyvsp[-2].loc) ;
            delete (yyvsp[0].loc) ;
        }
#line 4682 "gram.tab.c"
    break;

  case 369: /* stream_decl: direct_declarator_reentrance COMP_LESS comma_constants COMP_GRTR  */
#line 2354 "gram.y"
        {
            (yyval.decl) = (yyvsp[-3].decl);
            ArrayType * at = new ArrayType(TT_Stream, (yyvsp[-1].value));
            Type * extended = (yyval.decl)->extend(at);

            if (extended &&
                extended->isFunction())
                yyerr ("Wrong type combination") ;
        }
#line 4696 "gram.tab.c"
    break;

  case 370: /* dimension_constraint: constant  */
#line 2365 "gram.y"
        {
            (yyval.value) = (yyvsp[0].consValue);
        }
#line 4704 "gram.tab.c"
    break;

  case 371: /* dimension_constraint: func_call  */
#line 2369 "gram.y"
        {
            (yyval.value) = (yyvsp[0].value);
        }
#line 4712 "gram.tab.c"
    break;

  case 372: /* dimension_constraint: LPAREN expr RPAREN  */
#line 2373 "gram.y"
        {
           (yyval.value) = (yyvsp[-1].value);
        }
#line 4720 "gram.tab.c"
    break;

  case 373: /* dimension_constraint: ident  */
#line 2377 "gram.y"
        { 
            (yyval.value) = new Variable ((yyvsp[0].symbol),NoLocation);
        }
#line 4728 "gram.tab.c"
    break;

  case 374: /* comma_constants: %empty  */
#line 2383 "gram.y"
        {
	   (yyval.value) = NULL;
	}
#line 4736 "gram.tab.c"
    break;

  case 375: /* comma_constants: dimension_constraint  */
#line 2387 "gram.y"
        {
            (yyval.value) = (yyvsp[0].value);
        }
#line 4744 "gram.tab.c"
    break;

  case 376: /* comma_constants: comma_constants COMMA dimension_constraint  */
#line 2391 "gram.y"
        {
            (yyval.value) = new BinaryExpr(BO_Comma,(yyvsp[-2].value),(yyvsp[0].value),*(yyvsp[-1].loc));
            delete (yyvsp[-1].loc);
        }
#line 4753 "gram.tab.c"
    break;

  case 377: /* pointer_start: STAR opt_type_qual_list  */
#line 2401 "gram.y"
        {
            (yyval.ptr) = new PtrType((yyvsp[0].typeQual));    
        }
#line 4761 "gram.tab.c"
    break;

  case 379: /* pointer_reentrance: pointer_reentrance pointer_start  */
#line 2408 "gram.y"
        {
            (yyval.ptr) = (yyvsp[0].ptr);
            (yyval.ptr)->subType = (yyvsp[-1].ptr);
        }
#line 4770 "gram.tab.c"
    break;

  case 381: /* $@17: %empty  */
#line 2421 "gram.y"
       {  gProject->Parse_TOS->parseCtxt->IncrVarParam(1);
          if (gProject->Parse_TOS->transUnit)
              gProject->Parse_TOS->transUnit->contxt.EnterScope();
          gProject->Parse_TOS->parseCtxt->PushCtxt();
        }
#line 4780 "gram.tab.c"
    break;

  case 382: /* ident_list: $@17 ident_list_reentrance  */
#line 2427 "gram.y"
       {
          // Exit, but will allow re-enter for a function.
          // Hack, to handle parameters being in the function's scope.
          gProject->Parse_TOS->parseCtxt->PopCtxt(true);
          gProject->Parse_TOS->parseCtxt->IncrVarParam(-1);
          (yyval.decl) = (yyvsp[0].decl);
       }
#line 4792 "gram.tab.c"
    break;

  case 383: /* ident_list_reentrance: ident  */
#line 2437 "gram.y"
        {  if (gProject->Parse_TOS->transUnit)
               (yyval.decl) = gProject->Parse_TOS->parseCtxt->Mk_direct_declarator_reentrance ((yyvsp[0].symbol),
                gProject->Parse_TOS->transUnit->contxt.syms);
        }
#line 4801 "gram.tab.c"
    break;

  case 384: /* ident_list_reentrance: ident_list_reentrance COMMA ident  */
#line 2442 "gram.y"
        {  (yyval.decl) = (yyvsp[-2].decl);
           if (gProject->Parse_TOS->transUnit)
           {
              (yyval.decl) = gProject->Parse_TOS->parseCtxt->Mk_direct_declarator_reentrance ((yyvsp[0].symbol),
                gProject->Parse_TOS->transUnit->contxt.syms);
              (yyval.decl)->next = (yyvsp[-2].decl);
           }
        }
#line 4814 "gram.tab.c"
    break;

  case 386: /* typename_as_ident: TYPEDEF_NAME  */
#line 2458 "gram.y"
        {
            /* Convert a TYPEDEF_NAME back into a normal IDENT */
            (yyval.symbol) = (yyvsp[0].symbol);
            (yyval.symbol)->entry = (SymEntry*) NULL;
        }
#line 4824 "gram.tab.c"
    break;

  case 389: /* opt_param_type_list: %empty  */
#line 2479 "gram.y"
        {
           (yyval.decl) = (Decl*) NULL;
        }
#line 4832 "gram.tab.c"
    break;

  case 390: /* $@18: %empty  */
#line 2483 "gram.y"
        { gProject->Parse_TOS->parseCtxt->IncrVarParam(1); 
        }
#line 4839 "gram.tab.c"
    break;

  case 391: /* opt_param_type_list: $@18 param_type_list_bis  */
#line 2486 "gram.y"
        { gProject->Parse_TOS->parseCtxt->IncrVarParam(-1); 
           (yyval.decl) = (yyvsp[0].decl);
        }
#line 4847 "gram.tab.c"
    break;

  case 392: /* $@19: %empty  */
#line 2492 "gram.y"
        {   gProject->Parse_TOS->parseCtxt->IncrVarParam(1);
            if (gProject->Parse_TOS->transUnit)
                gProject->Parse_TOS->transUnit->contxt.EnterScope();
            gProject->Parse_TOS->parseCtxt->PushCtxt();
        }
#line 4857 "gram.tab.c"
    break;

  case 393: /* param_type_list: $@19 param_type_list_bis  */
#line 2498 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->PopCtxt(true);
            gProject->Parse_TOS->parseCtxt->IncrVarParam(-1);
            (yyval.decl) = (yyvsp[0].decl) ;
        }
#line 4867 "gram.tab.c"
    break;

  case 395: /* param_type_list_bis: param_list COMMA ELLIPSIS  */
#line 2507 "gram.y"
        {
            BaseType *bt = new BaseType(BT_Ellipsis);

            (yyval.decl) = new Decl(bt);
            (yyval.decl)->next = (yyvsp[-2].decl);
        }
#line 4878 "gram.tab.c"
    break;

  case 397: /* param_list: param_list COMMA param_decl  */
#line 2517 "gram.y"
        {
            (yyval.decl) = (yyvsp[0].decl);
            (yyval.decl)->next = (yyvsp[-2].decl);
        }
#line 4887 "gram.tab.c"
    break;

  case 398: /* $@20: %empty  */
#line 2524 "gram.y"
        {   
            gProject->Parse_TOS->parseCtxt->PushCtxt();
        }
#line 4895 "gram.tab.c"
    break;

  case 399: /* param_decl: $@20 param_decl_bis  */
#line 2528 "gram.y"
        {
            gProject->Parse_TOS->parseCtxt->PopCtxt(true);
            (yyval.decl) = (yyvsp[0].decl);
        }
#line 4904 "gram.tab.c"
    break;

  case 400: /* param_decl_bis: decl_specs_reentrance_bis declarator  */
#line 2535 "gram.y"
        {
            assert (err_top_level ||
                    (yyvsp[-1].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            possibleType = true;
            (yyval.decl) = (yyvsp[0].decl);
            Type * decl = gProject->Parse_TOS->parseCtxt->UseDeclCtxt();
            Type * extended = (yyval.decl)->extend(decl);             
            if ((yyval.decl)->form &&
                (yyval.decl)->form->isFunction())
                yyerr ("Function type not allowed");
            else if (extended && 
                decl && decl->isFunction() && 
                ! extended->isPointer())
                yyerr ("Wrong type combination") ;
        }
#line 4924 "gram.tab.c"
    break;

  case 401: /* param_decl_bis: decl_specs_reentrance_bis abs_decl_reentrance  */
#line 2551 "gram.y"
        {
            assert (err_top_level ||
                    (yyvsp[-1].base) == gProject->Parse_TOS->parseCtxt->curCtxt->decl_specs);
            possibleType = true;
            (yyval.decl) = new Decl((yyvsp[0].type));
            
            Type * decl = gProject->Parse_TOS->parseCtxt->UseDeclCtxt();
            Type * extended = (yyval.decl)->extend(decl);
            if ((yyval.decl)->form &&
                (yyval.decl)->form->isFunction())
                yyerr ("Function type not allowed for parameter");
            else if (extended && 
                decl && decl->isFunction() && 
                ! extended->isPointer())
                yyerr ("Wrong type combination") ;
        }
#line 4945 "gram.tab.c"
    break;

  case 402: /* param_decl_bis: decl_specs_reentrance_bis  */
#line 2568 "gram.y"
        {
            possibleType = true;
            (yyval.decl) = new Decl((yyvsp[0].base));
            if ((yyval.decl)->form &&
                (yyval.decl)->form->isFunction())
                yyerr ("Function type not allowed for parameter");
        }
#line 4957 "gram.tab.c"
    break;

  case 403: /* abs_decl_reentrance: pointer  */
#line 2581 "gram.y"
        {
            (yyval.type) = (yyvsp[0].ptr);
        }
#line 4965 "gram.tab.c"
    break;

  case 404: /* abs_decl_reentrance: direct_abs_decl_reentrance_bis  */
#line 2585 "gram.y"
        {
            (yyval.type) = (yyvsp[0].type);
        }
#line 4973 "gram.tab.c"
    break;

  case 405: /* abs_decl_reentrance: pointer direct_abs_decl_reentrance_bis  */
#line 2589 "gram.y"
        {
            (yyval.type) = (yyvsp[0].type);
            (yyval.type)->extend((yyvsp[-1].ptr));
        }
#line 4982 "gram.tab.c"
    break;

  case 407: /* direct_abs_decl_reentrance: LPAREN abs_decl_reentrance RPAREN  */
#line 2599 "gram.y"
        {
            (yyval.type) = (yyvsp[-1].type);
        }
#line 4990 "gram.tab.c"
    break;

  case 408: /* direct_abs_decl_reentrance: LBRCKT opt_const_expr RBRCKT  */
#line 2603 "gram.y"
        {
            (yyval.type) = new ArrayType(TT_Array, (yyvsp[-1].value));
        }
#line 4998 "gram.tab.c"
    break;

  case 409: /* direct_abs_decl_reentrance: direct_abs_decl_reentrance LBRCKT opt_const_expr RBRCKT  */
#line 2607 "gram.y"
        {
            ArrayType *at = new ArrayType(TT_Array, (yyvsp[-1].value));
            (yyval.type) = (yyvsp[-3].type);
            (yyval.type)->extend(at);
            Type * extended = (yyval.type)->extend(at) ;
            if (extended && 
                extended->isFunction())
                yyerr ("Wrong type combination") ;
        }
#line 5012 "gram.tab.c"
    break;

  case 410: /* direct_abs_decl_reentrance: LPAREN opt_param_type_list RPAREN  */
#line 2617 "gram.y"
        {
            (yyval.type) = new FunctionType(ReverseList((yyvsp[-1].decl)));
        }
#line 5020 "gram.tab.c"
    break;

  case 411: /* direct_abs_decl_reentrance: direct_abs_decl_reentrance LPAREN opt_param_type_list RPAREN  */
#line 2621 "gram.y"
        {
            FunctionType * ft = new FunctionType(ReverseList((yyvsp[-1].decl)));
            (yyval.type) = (yyvsp[-3].type);
            Type * extended = (yyval.type)->extend(ft) ;
            if (extended && 
                ! extended->isPointer())
                yyerr ("Wrong type combination") ;
                
        }
#line 5034 "gram.tab.c"
    break;

  case 412: /* opt_gcc_attrib: %empty  */
#line 2637 "gram.y"
        {
            (yyval.gccAttrib) = (GccAttrib*) NULL;
        }
#line 5042 "gram.tab.c"
    break;

  case 414: /* gcc_attrib: ATTRIBUTE LPAREN LPAREN gcc_inner RPAREN RPAREN  */
#line 2644 "gram.y"
            {
                (yyval.gccAttrib) = (yyvsp[-2].gccAttrib);
                delete (yyvsp[-4].loc);
                delete (yyvsp[-3].loc);
                delete (yyvsp[-1].loc);
                delete (yyvsp[0].loc);
            }
#line 5054 "gram.tab.c"
    break;

  case 415: /* gcc_inner: %empty  */
#line 2654 "gram.y"
            {
                /* The lexer ate some unsupported option. */
                (yyval.gccAttrib) = new GccAttrib( GCC_Unsupported);
            }
#line 5063 "gram.tab.c"
    break;

  case 416: /* gcc_inner: PACKED  */
#line 2659 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_Packed );
            }
#line 5071 "gram.tab.c"
    break;

  case 417: /* gcc_inner: CDECL  */
#line 2663 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_CDecl );
            }
#line 5079 "gram.tab.c"
    break;

  case 418: /* gcc_inner: CONST  */
#line 2667 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_Const );
            }
#line 5087 "gram.tab.c"
    break;

  case 419: /* gcc_inner: NORETURN  */
#line 2671 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_NoReturn );
            }
#line 5095 "gram.tab.c"
    break;

  case 420: /* gcc_inner: ALIGNED LPAREN INUM RPAREN  */
#line 2675 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_Aligned );

                if ((yyvsp[-1].consValue)->ctype == CT_Int)
                {
                    IntConstant    *iCons = (IntConstant*) (yyvsp[-1].consValue);

                    (yyval.gccAttrib)->value = iCons->lng;
                }

                delete (yyvsp[-2].loc);
                delete (yyvsp[0].loc);
            }
#line 5113 "gram.tab.c"
    break;

  case 421: /* gcc_inner: MODE LPAREN ident RPAREN  */
#line 2689 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_Mode );

                (yyval.gccAttrib)->mode = (yyvsp[-1].symbol);

                delete (yyvsp[-2].loc);
                delete (yyvsp[0].loc);
            }
#line 5126 "gram.tab.c"
    break;

  case 422: /* gcc_inner: FORMAT LPAREN ident COMMA INUM COMMA INUM RPAREN  */
#line 2698 "gram.y"
            {
                (yyval.gccAttrib) = new GccAttrib( GCC_Format );
    
                (yyval.gccAttrib)->mode = (yyvsp[-5].symbol);

                if ((yyvsp[-3].consValue)->ctype == CT_Int)
                {
                    IntConstant    *iCons = (IntConstant*) (yyvsp[-3].consValue);

                    (yyval.gccAttrib)->strIdx = iCons->lng;
                }

                if ((yyvsp[-1].consValue)->ctype == CT_Int)
                {
                    IntConstant    *iCons = (IntConstant*) (yyvsp[-1].consValue);

                    (yyval.gccAttrib)->first = iCons->lng;
                }

                delete (yyvsp[-6].loc);
                delete (yyvsp[0].loc);
            }
#line 5153 "gram.tab.c"
    break;


#line 5157 "gram.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 2722 "gram.y"


/*******************************************************/
