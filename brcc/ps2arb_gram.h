/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_PS2ARB_PS2ARB_GRAM_TAB_H_INCLUDED
# define YY_PS2ARB_PS2ARB_GRAM_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int ps2arb_debug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    PS_NOP = 258,                  /* PS_NOP  */
    PS_NEGATE = 259,               /* PS_NEGATE  */
    PS_SWIZZLEMASK = 260,          /* PS_SWIZZLEMASK  */
    PS_COLORREG = 261,             /* PS_COLORREG  */
    PS_TEMPREG = 262,              /* PS_TEMPREG  */
    PS_TEXCOORDREG = 263,          /* PS_TEXCOORDREG  */
    PS_OUTPUTREG = 264,            /* PS_OUTPUTREG  */
    PS_SAMPLEREG = 265,            /* PS_SAMPLEREG  */
    PS_CONSTREG = 266,             /* PS_CONSTREG  */
    PS_TEXKILL = 267,              /* PS_TEXKILL  */
    PS_SINCOS = 268,               /* PS_SINCOS  */
    PS_UNARY_OP = 269,             /* PS_UNARY_OP  */
    PS_BINARY_OP = 270,            /* PS_BINARY_OP  */
    PS_TRINARY_OP = 271,           /* PS_TRINARY_OP  */
    PS_OP_FLAGS = 272,             /* PS_OP_FLAGS  */
    PS_DCLTEX = 273,               /* PS_DCLTEX  */
    PS_DCL = 274,                  /* PS_DCL  */
    PS_DEF = 275,                  /* PS_DEF  */
    PS_COMMA = 276,                /* PS_COMMA  */
    PS_MOV = 277,                  /* PS_MOV  */
    PS_COMMENT = 278,              /* PS_COMMENT  */
    PS_ENDLESS_COMMENT = 279,      /* PS_ENDLESS_COMMENT  */
    PS_FLOAT = 280,                /* PS_FLOAT  */
    PS_NEWLINE = 281,              /* PS_NEWLINE  */
    PS_PSHEADER = 282              /* PS_PSHEADER  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 122 "ps2arb_gram.y"

  char *s;
  float f;
  struct Register{
    char * swizzlemask;
    char * negate;
    char * reg;
  } reg;

#line 101 "ps2arb_gram.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE ps2arb_lval;


int ps2arb_parse (void);


#endif /* !YY_PS2ARB_PS2ARB_GRAM_TAB_H_INCLUDED  */
