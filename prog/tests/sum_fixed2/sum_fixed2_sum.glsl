#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp vec2 _61 = texture2D(_tex_a, param).xy;
    highp vec2 a = _61;
    highp vec2 param_1 = _tex_b_pos;
    highp vec2 _67 = texture2D(_tex_b, param_1).xy;
    highp vec2 b = _67;
    highp vec2 param_2 = a;
    highp vec2 param_3 = b;
    highp vec2 param_4 = param_2 + param_3;
    highp vec2 c = param_4;
    _output_0 = vec4(c, 0.0, 0.0);
}

