
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Feb  6 2024 10:50:01
////////////////////////////////////////////

#include <brook/brook.hpp>
#include <stdio.h>

static const char *__sum_ps20= NULL;
static const char *__sum_ps2b= NULL;
static const char *__sum_ps2a= NULL;
static const char *__sum_ps30= NULL;
static const char *__sum_ctm= NULL;
static const char *__sum_fp30= NULL;
static const char *__sum_fp40= NULL;
static const char *__sum_arb= NULL;
static const char *__sum_glsl= NULL;

static const void* __sum_gles = 0;

static void  __sum_cpu_inner(const __BrtInt1  &a,
                            const __BrtInt1  &b,
                            __BrtInt1  &c)
{
  c = a + b;
}
void  __sum_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_a = (::brook::StreamInterface *) args[0];
  ::brook::StreamInterface *arg_b = (::brook::StreamInterface *) args[1];
  ::brook::StreamInterface *arg_c = (::brook::StreamInterface *) args[2];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtInt1  > __out_arg_c((__BrtInt1 *) __k->FetchElem(arg_c, __brt_idx));
    __sum_cpu_inner (Addressable <__BrtInt1 >((__BrtInt1 *) __k->FetchElem(arg_a, __brt_idx)),
                     Addressable <__BrtInt1 >((__BrtInt1 *) __k->FetchElem(arg_b, __brt_idx)),
                     __out_arg_c);
    *reinterpret_cast<__BrtInt1 *>(__out_arg_c.address) = __out_arg_c.castToArg(*reinterpret_cast<__BrtInt1 *>(__out_arg_c.address));
  }
}

extern void  sum (::brook::stream a,
		::brook::stream b,
		::brook::stream c) {
  static const void *__sum_fp[] = {
     "fp30", __sum_fp30,
     "fp40", __sum_fp40,
     "arb", __sum_arb,
     "glsl", __sum_glsl,
     "gles", __sum_gles,
     "ps20", __sum_ps20,
     "ps2b", __sum_ps2b,
     "ps2a", __sum_ps2a,
     "ps30", __sum_ps30,
     "ctm", __sum_ctm,
     "cpu", (void *) __sum_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__sum_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushStream(a);
  __k->PushStream(b);
  __k->PushOutput(c);
  __k->Map();

}


int  main()
{
  int  i;
  int  j;
  ::brook::stream a1(::brook::getStreamType(( unsigned int  *)0), 100,-1);
  ::brook::stream b1(::brook::getStreamType(( unsigned int  *)0), 100,-1);
  ::brook::stream c1(::brook::getStreamType(( unsigned int  *)0), 100,-1);
  ::brook::stream a2(::brook::getStreamType(( unsigned int  *)0), 10 , 10,-1);
  ::brook::stream b2(::brook::getStreamType(( unsigned int  *)0), 10 , 10,-1);
  ::brook::stream c2(::brook::getStreamType(( unsigned int  *)0), 10 , 10,-1);
  BRTALIGNED unsigned int  input_a1[100];
  BRTALIGNED unsigned int  input_b1[100];
  BRTALIGNED unsigned int  output_c1[100];
  BRTALIGNED unsigned int  input_a2[10][10];
  BRTALIGNED unsigned int  input_b2[10][10];
  BRTALIGNED unsigned int  output_c2[10][10];

  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 10; j++)
    {
      input_a1[10 * i + j] = (unsigned int ) (1) << 23;
      input_b1[10 * i + j] = (unsigned int ) (10) * i + j;
      input_a2[i][j] = (unsigned int ) (1) << 23;
      input_b2[i][j] = (unsigned int ) (10) * i + j;
    }

  }

  streamRead(a1,input_a1);
  streamRead(b1,input_b1);
  streamRead(a2,input_a1);
  streamRead(b2,input_b2);
  sum(a1,b1,c1);
  sum(a2,b2,c2);
  streamWrite(c1,output_c1);
  streamWrite(c2,output_c2);
  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 10; j++)
      printf("%d ",output_c1[10 * i + j]);
    printf("\n");
  }

  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 10; j++)
      printf("%d ",output_c2[i][j]);
    printf("\n");
  }

  return 0;
}


