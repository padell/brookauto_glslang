#version 100
precision mediump float;
precision highp int;

struct u_block_indexx
{
    highp float indexx;
};

uniform u_block_indexx u_bl_indexx;

struct u_block_indexy
{
    highp float indexy;
};

uniform u_block_indexy u_bl_indexy;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

uniform highp sampler2D a[1];

highp vec4 _output_0;

void main()
{
    highp float param = u_bl_indexx.indexx;
    highp float param_1 = u_bl_indexy.indexy;
    highp vec4 param_2 = u_bl_gatherconst_a._gatherconst_a;
    highp vec2 _100 = vec2(param, param_1);
    highp float _101 = _100.x;
    highp vec4 _102 = param_2;
    highp float _113 = (_101 * _102.x) + _102.z;
    highp float _103 = _113;
    highp float _122 = texture2D(a[0], vec2(_103, 0.0)).x;
    highp float param_3 = _122;
    highp float b = param_3;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

