#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp vec4 _62 = texture2D(_tex_a, param);
    highp vec4 a = _62;
    highp vec2 param_1 = _tex_b_pos;
    highp vec4 _67 = texture2D(_tex_b, param_1);
    highp vec4 b = _67;
    highp vec4 param_2 = a;
    highp vec4 param_3 = b;
    highp vec4 param_4 = param_2 + param_3;
    highp vec4 c = param_4;
    _output_0 = vec4(c);
}

