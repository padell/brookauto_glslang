#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_blockIteratorBias
{
    highp vec4 IteratorBias;
};

uniform u_blockIteratorBias _33;

highp float i;
highp vec4 _output_0;

void main()
{
    highp float param = i;
    highp float param_1 = param;
    highp float outvar = param_1;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

