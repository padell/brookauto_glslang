#version 100
precision mediump float;
precision highp int;

struct u_blocka
{
    highp vec4 _gatherconst_a;
};

uniform u_blocka _83;

uniform highp sampler2D _tex_index;
uniform highp sampler2D a[1];

highp vec2 _tex_index_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_index_pos;
    highp vec2 _99 = texture2D(_tex_index, param).xy;
    highp vec2 index = _99;
    highp vec2 param_1 = index;
    highp vec4 param_2 = _83._gatherconst_a;
    highp vec2 _105 = param_1;
    highp vec4 _106 = param_2;
    highp vec2 _113 = (_105 * _106.xy) + _106.zw;
    highp vec2 _107 = _113;
    highp float _122 = texture2D(a[0], _107).x;
    highp float param_3 = _122;
    highp float b = param_3;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

