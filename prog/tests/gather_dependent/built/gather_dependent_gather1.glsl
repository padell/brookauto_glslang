#version 100
precision mediump float;
precision highp int;

struct u_blocka
{
    highp vec4 _gatherconst_a;
};

uniform u_blocka _86;

uniform highp sampler2D _tex_index;
uniform highp sampler2D a[1];

highp vec2 _tex_index_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_index_pos;
    highp float _101 = texture2D(_tex_index, param).x;
    highp float index = _101;
    highp float param_1 = index;
    highp vec4 param_2 = _86._gatherconst_a;
    highp float _107 = param_1;
    highp vec4 _108 = param_2;
    highp float _115 = (_107 * _108.x) + _108.z;
    highp float _109 = _115;
    highp float _124 = texture2D(a[0], vec2(_109, 0.0)).x;
    highp float param_3 = _124;
    highp float b = param_3;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

