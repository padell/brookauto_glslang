
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Jan 18 2024 12:27:37
////////////////////////////////////////////

#include <brook/brook.hpp>
#include <stdio.h>

static const char *__gather1_ps20= NULL;
static const char *__gather1_ps2b= NULL;
static const char *__gather1_ps2a= NULL;
static const char *__gather1_ps30= NULL;
static const char *__gather1_ctm= NULL;
static const char *__gather1_fp30= NULL;
static const char *__gather1_fp40= NULL;
static const char *__gather1_arb= NULL;
static const char *__gather1_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __gather1_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"//var sampler2D _tex_index : TEXUNIT0 : _tex_index 0 : 0 : 1\n"
				"//var sampler2D a[0] : TEXUNIT1 : a[0] 1 : 2 : 1\n"
				"//var float4 __gatherconst_a : C0 : _gatherconst_a : 3 : 1\n"
				"//var float2 _tex_index_pos : $vin.TEXCOORD0 : TEXCOORD0 : 1 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 4 : 1\n"
				"precision highp float;\n"
				"precision highp int;\n"
				"\n"
				"uniform vec4 _gatherconst_a;\n"
				"\n"
				"\n"
				"uniform sampler2D _tex_index;\n"
				"uniform sampler2D a[1];\n"
				"\n"
				"varying vec4 TEX0;\n"
				"vec4 _output_0;\n"
				"\n"
				"void main()\n"
				"{\n"
				"     vec2 param = TEX0.xy;\n"
				"     float _101;\n"
				"reconstruct_float(_101, _tex_index, param.xy);\n"
				"     float index = _101;\n"
				"     float param_1 = index;\n"
				"     vec4 param_2 = _gatherconst_a;\n"
				"     float _107 = param_1;\n"
				"     vec4 _108 = param_2;\n"
				"     float _115 = (_107 * _108.x) + _108.z;\n"
				"     float _109 = _115;\n"
				"     float _124;\n"
				"reconstruct_float(_124, a[0], vec2(_109, 0.0.xy);\n"
				"     float param_3 = _124;\n"
				"     float b = param_3;\n"
				"encode_output_float(b);\n"
				"}\n"
				"\n"
				" \n"
				"//!!BRCC\n"
				"//narg:3\n"
				"//s:1:index\n"
				"//c:1:a\n"
				"//o:1:b\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(2, kGatherConstant_Shape)
				.constant(2, kGatherConstant_Shape)
				.sampler(1, 0)
				.sampler(2, 0)
				.sampler(1, 0)
				.sampler(2, 0)
				.interpolant(1, kStreamInterpolant_Position)
				.interpolant(1, kStreamInterpolant_Position)
				.output(3, 0)
				.output(3, 0)
			)
		);
	static const void* __gather1_gles = &__gather1_gles_desc;
}

static void  __gather1_cpu_inner(const __BrtFloat1  &index,
                                const __BrtArray<__BrtFloat1  > &a,
                                __BrtFloat1  &b)
{
  b = a[index.swizzle1(maskX)];
}
void  __gather1_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_index = (::brook::StreamInterface *) args[0];
  __BrtArray<__BrtFloat1  > *arg_a = (__BrtArray<__BrtFloat1  > *) args[1];
  ::brook::StreamInterface *arg_b = (::brook::StreamInterface *) args[2];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_b((__BrtFloat1 *) __k->FetchElem(arg_b, __brt_idx));
    __gather1_cpu_inner (Addressable <__BrtFloat1 >((__BrtFloat1 *) __k->FetchElem(arg_index, __brt_idx)),
                         *arg_a,
                         __out_arg_b);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_b.address) = __out_arg_b.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_b.address));
  }
}

extern void  gather1 (::brook::stream index,
		::brook::stream a,
		::brook::stream b) {
  static const void *__gather1_fp[] = {
     "fp30", __gather1_fp30,
     "fp40", __gather1_fp40,
     "arb", __gather1_arb,
     "glsl", __gather1_glsl,
     "gles", __gather1_gles,
     "ps20", __gather1_ps20,
     "ps2b", __gather1_ps2b,
     "ps2a", __gather1_ps2a,
     "ps30", __gather1_ps30,
     "ctm", __gather1_ctm,
     "cpu", (void *) __gather1_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__gather1_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushStream(index);
  __k->PushGatherStream(a);
  __k->PushOutput(b);
  __k->Map();

}


