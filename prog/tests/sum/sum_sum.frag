#version 450 core

#define _stype1 sampler2D
#define _stype2 sampler2D
#define _stype3 sampler3D
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#define _FRAGMENTKILL discard
#ifdef USERECT
#define SKIPSCALEBIAS
#endif

#ifdef SKIPSCALEBIAS
float _gatherindex1( float index, vec4 scalebias ) { return (index+scalebias.z); }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return (index+scalebias.zw); }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) vec4(a, 0.0, 0.0, 0.0)
#else
float _gatherindex1( float index, vec4 scalebias ) { return index*scalebias.x+scalebias.z; }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return index*scalebias.xy+scalebias.zw; }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) (b)
#endif

double _fetch_double( _stype1 s, float i ) { return texture(s, vec2(i, 0.0)).x;}
double _fetch_double( _stype2 s, vec2 i ) { return texture(s, i).x;}
double _fetch_double( _stype3 s, vec3 i ) { return texture(s, i).x;}
dvec2 _fetch_double2( _stype1 s, float i ) { dvec2 r; r[0] = texture(s, vec2(i, 0.0)).x; r[1] = texture(s, vec2(i, 0.0)).y; return r;}
dvec2 _fetch_double2( _stype2 s, vec2 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
dvec2 _fetch_double2( _stype3 s, vec3 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
float _fetch_float( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_float( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_float( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_fixed( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_fixed( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_fixed( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_float2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_float2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_float2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec2 _fetch_fixed2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_fixed2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_fixed2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_float3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_float3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_float3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec3 _fetch_fixed3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_fixed3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_fixed3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_float4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_float4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_float4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
vec4 _fetch_fixed4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_fixed4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_fixed4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_unsigned_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_unsigned_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_unsigned_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_unsigned_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_unsigned_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_unsigned_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_unsigned_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_unsigned_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_unsigned_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }


float _gather_float( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_float( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_float( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_float2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_float2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_float2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_float3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_float3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_float3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_float4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_float4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_float4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_shortfloat( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_shortfloat( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_shortfloat( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_shortvec2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_shortvec2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_shortvec2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_shortvec3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_shortvec3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_shortvec3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_shortvec4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_shortvec4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_shortvec4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_fixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_fixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_fixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_fixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_fixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_fixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_fixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_fixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_fixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_fixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_fixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_fixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_char( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_char( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_char( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_char2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_char2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_char2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_char3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_char3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_char3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_char4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_char4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_char4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_ufloat( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_ufloat( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_ufloat( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_uvec2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_uvec2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_uvec2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_uvec3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_uvec3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_uvec3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_uvec4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_uvec4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_uvec4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }


void sum (float  a,
            float  b,
            out float  c)
{
  c = a + b;
}
layout(binding = 0) uniform _stype2 _tex_a; // register (s0) //GL_ES_in float 
vec2 _tex_a_pos; // TEXCOORD0
layout(binding = 1) uniform _stype2 _tex_b; // register (s1) //GL_ES_in float 
vec2 _tex_b_pos; // TEXCOORD1
float  _outvar_0; // COLOR0//GL_ES_out float
layout(binding = 0) uniform u_block_workspace {
		 vec4 workspace;
} u_bl_workspace; // register (c0)


void main () {
	float  a;
	float  b;
	float  c;
a = _fetch_float(_tex_a, _tex_a_pos );//float
b = _fetch_float(_tex_b, _tex_b_pos );//float

	sum(
		a,
		b,
		c );

	_outvar_0 = c;
}
