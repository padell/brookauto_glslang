#version 100
precision mediump float;
precision highp int;

struct u_block_workspace
{
    highp vec4 workspace;
};

uniform u_block_workspace u_bl_workspace;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp float _outvar_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float _63 = texture2D(_tex_a, param).x;
    highp float a = _63;
    highp vec2 param_1 = _tex_b_pos;
    highp float _69 = texture2D(_tex_b, param_1).x;
    highp float b = _69;
    highp float param_2 = a;
    highp float param_3 = b;
    highp float param_4 = param_2 + param_3;
    highp float c = param_4;
    _outvar_0 = c;
}

