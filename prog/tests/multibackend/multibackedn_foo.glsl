#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_b;

highp vec2 _tex_b_pos;
highp vec4 _output_0;

highp float _fetch_float(highp sampler2D s, highp vec2 i)
{
    return texture2D(s, i).x;
}

void foo(inout highp float a, highp float b)
{
    a = b;
    for (int n = 0; n < 50; n++)
    {
        a += b;
    }
}

void main()
{
    highp vec2 param = _tex_b_pos;
    highp float b = _fetch_float(_tex_b, param);
    highp float param_2 = b;
    highp float param_1;
    foo(param_1, param_2);
    highp float a = param_1;
    _output_0 = vec4(a, 0.0, 0.0, 0.0);
}

