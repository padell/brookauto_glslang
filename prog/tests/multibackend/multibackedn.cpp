
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Aug 28 2023 16:36:22
////////////////////////////////////////////

#include <brook/brook.hpp>
static const char *__foo_ps20= NULL;
static const char *__foo_ps2b= NULL;
static const char *__foo_ps2a= NULL;
static const char *__foo_ps30= NULL;
static const char *__foo_ctm= NULL;
static const char *__foo_fp30= NULL;
static const char *__foo_fp40= NULL;
static const char *__foo_arb= NULL;
static const char *__foo_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __foo_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"precision highp float;\n"
				"precision highp int;\n"
				"\n"
				"uniform sampler2D _tex_b;\n"
				"\n"
				"varying vec4 TEX0;\n"
				"vec4 _output_0;\n"
				"\n"
				"\n"
				"void foo(inout float a, float b)\n"
				"{\n"
				"    a = b;\n"
				"    for (int n = 0; n < 50; n++)\n"
				"    {\n"
				"        a += b;\n"
				"    }\n"
				"}\n"
				"\n"
				"void main()\n"
				"{\n"
				"     vec2 param = TEX0.xy;\n"
				"     float b;\n"
				"reconstruct_float(b, _tex_b, param.xy);\n"
				"     float param_2 = b;\n"
				"     float param_1;\n"
				"    foo(param_1, param_2);\n"
				"     float a = param_1;\n"
				"encode_output_float(a);\n"
				"}\n"
				"\n"
				" \n"
				"//!!BRCC\n"
				"//narg:2\n"
				"//o:1:a\n"
				"//s:1:b\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.sampler(2, 0)
				.sampler(2, 0)
				.interpolant(2, kStreamInterpolant_Position)
				.interpolant(2, kStreamInterpolant_Position)
				.output(1, 0)
				.output(1, 0)
			)
		);
	static const void* __foo_gles = &__foo_gles_desc;
}

static void  __foo_cpu_inner(__BrtFloat1  &a,
                            const __BrtFloat1  &b)
{
  __BrtInt1  n;

  a = b;
  for (n = __BrtFloat1((float)0); n < __BrtFloat1((float)50); ++n)
    a += b;
}
void  __foo_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_a = (::brook::StreamInterface *) args[0];
  ::brook::StreamInterface *arg_b = (::brook::StreamInterface *) args[1];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_a((__BrtFloat1 *) __k->FetchElem(arg_a, __brt_idx));
    __foo_cpu_inner (__out_arg_a,
                     Addressable <__BrtFloat1 >((__BrtFloat1 *) __k->FetchElem(arg_b, __brt_idx)));
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_a.address) = __out_arg_a.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_a.address));
  }
}

extern void  foo (::brook::stream a,
		::brook::stream b) {
  static const void *__foo_fp[] = {
     "fp30", __foo_fp30,
     "fp40", __foo_fp40,
     "arb", __foo_arb,
     "glsl", __foo_glsl,
     "gles", __foo_gles,
     "ps20", __foo_ps20,
     "ps2b", __foo_ps2b,
     "ps2a", __foo_ps2a,
     "ps30", __foo_ps30,
     "ctm", __foo_ctm,
     "cpu", (void *) __foo_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__foo_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushOutput(a);
  __k->PushStream(b);
  __k->Map();

}



