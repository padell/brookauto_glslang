#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_b;

highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_b_pos;
    highp float _67 = texture2D(_tex_b, param).x;
    highp float b = _67;
    highp float param_2 = b;
    highp float param_1 = param_2;
    for (highp float _73 = 0.0; _73 < 50.0; _73 += 1.0)
    {
        param_1 += param_2;
    }
    highp float a = param_1;
    _output_0 = vec4(a, 0.0, 0.0, 0.0);
}

