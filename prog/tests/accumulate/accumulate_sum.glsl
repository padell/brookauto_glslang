#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float _62 = texture2D(_tex_a, param).x;
    highp float a = _62;
    highp vec2 param_1 = _tex_b_pos;
    highp float _68 = texture2D(_tex_b, param_1).x;
    highp float b = _68;
    highp float param_2 = a;
    highp float param_3 = b;
    highp float param_4 = param_2 + param_3;
    highp float c = param_4;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

