#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

uniform highp sampler2D _tex_indexx;
uniform highp sampler2D _tex_indexy;
uniform highp sampler2D a[1];

highp vec2 _tex_indexx_pos;
highp vec2 _tex_indexy_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_indexx_pos;
    highp float _112 = texture2D(_tex_indexx, param).x;
    highp float indexx = _112;
    highp vec2 param_1 = _tex_indexy_pos;
    highp float _118 = texture2D(_tex_indexy, param_1).x;
    highp float indexy = _118;
    highp float param_2 = indexx;
    highp float param_3 = indexy;
    highp vec4 param_4 = u_bl_gatherconst_a._gatherconst_a;
    highp vec2 _124 = vec2(param_2, param_3);
    highp vec2 _125 = _124;
    highp vec4 _126 = param_4;
    highp vec2 _136 = (_125 * _126.xy) + _126.zw;
    highp vec2 _127 = _136;
    highp float _145 = texture2D(a[0], _127).x;
    highp float param_5 = _145;
    highp float b = param_5;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

