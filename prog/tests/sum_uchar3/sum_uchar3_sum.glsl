#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp vec3 _64 = texture2D(_tex_a, param).xyz;
    highp vec3 a = _64;
    highp vec2 param_1 = _tex_b_pos;
    highp vec3 _70 = texture2D(_tex_b, param_1).xyz;
    highp vec3 b = _70;
    highp vec3 param_2 = a;
    highp vec3 param_3 = b;
    highp vec3 param_4 = param_2 + param_3;
    highp vec3 c = param_4;
    _output_0 = vec4(c, 0.0);
}

