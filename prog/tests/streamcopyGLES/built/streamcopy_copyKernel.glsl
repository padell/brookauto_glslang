#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_src;

highp vec2 _tex_src_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_src_pos;
    highp float _51 = texture2D(_tex_src, param).x;
    highp float src = _51;
    highp float param_1 = src;
    highp float param_2 = param_1;
    highp float dst = param_2;
    _output_0 = vec4(dst, 0.0, 0.0, 0.0);
}

