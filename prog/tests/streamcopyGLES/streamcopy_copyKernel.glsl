#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_src;

highp vec2 _tex_src_pos;
highp vec4 _output_0;

highp float _fetch_float(highp sampler2D s, highp vec2 i)
{
    return texture2D(s, i).x;
}

void copyKernel(highp float src, out highp float dst)
{
    dst = src;
}

void main()
{
    highp vec2 param = _tex_src_pos;
    highp float src = _fetch_float(_tex_src, param);
    highp float param_1 = src;
    highp float param_2;
    copyKernel(param_1, param_2);
    highp float dst = param_2;
    _output_0 = vec4(dst, 0.0, 0.0, 0.0);
}

