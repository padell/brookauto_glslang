#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_inpvar;

highp vec2 _tex_inpvar_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_inpvar_pos;
    highp float _51 = texture2D(_tex_inpvar, param).x;
    highp float inpvar = _51;
    highp float param_1 = inpvar;
    highp float param_2 = param_1;
    highp float outvar = param_2;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

