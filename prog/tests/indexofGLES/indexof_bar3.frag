#version 450 core

#if defined(DXPIXELSHADER)
#define fixed float
#define fixed2 vec2
#define fixed3 vec3
#define fixed4 vec4
#endif
#define shortfixed float
#define shortfixed2 vec2
#define shortfixed3 vec3
#define shortfixed4 vec4
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define double2 dvec2
#if defined(DXPIXELSHADER) || !defined(USERECT)
#define _stype1 sampler2D
#define _stype2 sampler2D
#define _stype3 sampler3D
#if defined(SHADERMODEL3)
#define _sample1(s,i) textureLod((s),i,0.0)
#define _sample2(s,i) textureLod((s),i,0.0)
#define _sample3(s,i) textureLod((s),i,0.0)
#else
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif
#else
#define _stype1 sampler2DRect
#define _stype2 sampler2DRect
#define _stype3 sampler3DRect
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif

#define _FRAGMENTKILL discard
#ifdef USERECT
#define SKIPSCALEBIAS
#endif

#ifdef SKIPSCALEBIAS
float _gatherindex1( float index, vec4 scalebias ) { return (index+scalebias.z); }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return (index+scalebias.zw); }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) vec4(a, 0.0, 0.0, 0.0)
#else
float _gatherindex1( float index, vec4 scalebias ) { return index*scalebias.x+scalebias.z; }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return index*scalebias.xy+scalebias.zw; }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) (b)
#endif

double _fetch_double( _stype1 s, float i ) { return texture(s, vec2(i, 0.0)).x;}
double _fetch_double( _stype2 s, vec2 i ) { return texture(s, i).x;}
double _fetch_double( _stype3 s, vec3 i ) { return texture(s, i).x;}
dvec2 _fetch_double2( _stype1 s, float i ) { dvec2 r; r[0] = texture(s, vec2(i, 0.0)).x; r[1] = texture(s, vec2(i, 0.0)).y; return r;}
dvec2 _fetch_double2( _stype2 s, vec2 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
dvec2 _fetch_double2( _stype3 s, vec3 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
float _fetch_float( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_float( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_float( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_fixed( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_fixed( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_fixed( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_float2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_float2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_float2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec2 _fetch_fixed2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_fixed2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_fixed2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_float3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_float3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_float3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec3 _fetch_fixed3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_fixed3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_fixed3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_float4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_float4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_float4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
vec4 _fetch_fixed4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_fixed4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_fixed4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_unsigned_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_unsigned_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_unsigned_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_unsigned_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_unsigned_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_unsigned_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_unsigned_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_unsigned_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_unsigned_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }


float _gather_float( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_float( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_float( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_float2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_float2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_float2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_float3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_float3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_float3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_float4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_float4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_float4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_shortfixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_shortfixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_shortfixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_shortfixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_shortfixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_shortfixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_shortfixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_shortfixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_shortfixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_shortfixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_shortfixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_shortfixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_fixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_fixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_fixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_fixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_fixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_fixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_fixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_fixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_fixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_fixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_fixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_fixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_char( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_char( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_char( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_char2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_char2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_char2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_char3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_char3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_char3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_char4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_char4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_char4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_uchar( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_uchar( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_uchar( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_uchar2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_uchar2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_uchar2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_uchar3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_uchar3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_uchar3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_uchar4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_uchar4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_uchar4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
#define uchar float;
#define uchar2 vec2;
#define uchar3 vec3;
#define uchar4 vec4;


void bar1 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             vec4  indexofe,
             float  x,
             float  y,
             float4 _indexof_d_prime)
{
  e = _gather_float (d,_gatherindex2((_indexof_d_prime).xy, _const_d_scalebias));
}
void bar2 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             vec4  indexofe,
             float x,
             float y,
             float4 _indexof_d_prime)
{
  e = _gather_float (d,_gatherindex2((_indexof_d_prime).xy, _const_d_scalebias));
}
void bar3 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             vec4  indexofe,
             vec2  dim,
             float4 _indexof_d_prime)
{
  e = _gather_float (d,_gatherindex2(((_indexof_d_prime).xy).xy, _const_d_scalebias));
}
void bar4 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             vec4  indexofe)
{
  e = _gather_float (d,_gatherindex2((indexofe.xy).xy, _const_d_scalebias));
}
void foo1 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             float  x,
             float  y,
             float4 _indexof_d_prime,
             float4 _indexof_e)
{
  bar1(d_prime,d,_const_d_scalebias,e,_indexof_e,x,y,_indexof_d_prime);
}
void foo2 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             float x,
             float y,
             float4 _indexof_d_prime,
             float4 _indexof_e)
{
  bar2(d_prime,d,_const_d_scalebias,e,_indexof_e,x,y,_indexof_d_prime);
}
void foo3 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             vec2  dim,
             float4 _indexof_d_prime,
             float4 _indexof_e)
{
  bar3(d_prime,d,_const_d_scalebias,e,_indexof_e,dim,_indexof_d_prime);
}
void foo4 (float  d_prime,
             _stype2 d[1],
             float4 _const_d_scalebias,
             out float  e,
             float4 _indexof_e)
{
  bar4(d_prime,d,_const_d_scalebias,e,_indexof_e);
}
void test (float  a,
             out float  b,
             float4 _indexof_a)
{
  b = (_indexof_a).x;
}
layout(binding = 0) uniform _stype2 _tex_d_prime;//GL_ES_in float 
layout(binding = 0) uniform u_block_const_d_prime_invscalebias {
	 uniform vec4 _const_d_prime_invscalebias;
} u_bl_const_d_prime_invscalebias;
layout(binding = 1) uniform u_block_StreamDim {
	 uniform vec4 StreamDim;
} u_bl_StreamDim;
vec2 _tex_d_prime_pos;
layout(binding = 1) uniform _stype2 d[1];//GL_ES_in float 
layout(binding = 2) uniform u_block_gatherconst_d {
	 uniform vec4 _gatherconst_d;
} u_bl_gatherconst_d;
vec4 _output_0;//GL_ES_out float
layout(binding = 2) uniform u_block_indexofe {
	vec4  indexofe;
} u_bl_indexofe;
layout(binding = 3) uniform u_block_dim {
	vec2  dim;
} u_bl_dim;


void main () {
	float  d_prime;
	float  e;
d_prime = _fetch_float(_tex_d_prime, _tex_d_prime_pos );//float
vec4 _indexof_d_prime = floor(u_bl_StreamDim.StreamDim*_computeindexof( _tex_d_prime_pos, (vec4( _tex_d_prime_pos*u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.xy + u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.zw,0,0))));

	bar3(
		d_prime,
		d, u_bl_gatherconst_d._gatherconst_d,
		e,
		u_bl_indexofe.indexofe,
		u_bl_dim.dim,
		_indexof_d_prime );

	_output_0 = vec4( e, 0, 0, 0);
}
