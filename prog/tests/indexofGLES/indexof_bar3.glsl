#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_d_prime_invscalebias
{
    highp vec4 _const_d_prime_invscalebias;
};

uniform u_block_const_d_prime_invscalebias u_bl_const_d_prime_invscalebias;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

struct u_block_indexofe
{
    highp vec4 indexofe;
};

uniform u_block_indexofe u_bl_indexofe;

struct u_block_dim
{
    highp vec2 dim;
};

uniform u_block_dim u_bl_dim;

uniform highp sampler2D _tex_d_prime;
uniform highp sampler2D d[1];

highp vec2 _tex_d_prime_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_d_prime_pos;
    highp float _141 = texture2D(_tex_d_prime, param).x;
    highp float d_prime = _141;
    highp vec4 _indexof_d_prime = floor(u_bl_StreamDim.StreamDim * vec4((_tex_d_prime_pos * u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.xy) + u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.zw, 0.0, 0.0));
    highp float param_1 = d_prime;
    highp vec4 param_2 = u_bl_gatherconst_d._gatherconst_d;
    highp vec4 param_4 = u_bl_indexofe.indexofe;
    highp vec2 param_5 = u_bl_dim.dim;
    highp vec4 param_6 = _indexof_d_prime;
    highp vec2 _147 = param_6.xy;
    highp vec4 _148 = param_2;
    highp vec2 _156 = (_147 * _148.xy) + _148.zw;
    highp vec2 _149 = _156;
    highp float _165 = texture2D(d[0], _149).x;
    highp float param_3 = _165;
    highp float e = param_3;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

