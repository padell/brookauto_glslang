#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_a_invscalebias
{
    highp vec4 _const_a_invscalebias;
};

uniform u_block_const_a_invscalebias u_bl_const_a_invscalebias;

uniform highp sampler2D _tex_a;

highp vec2 _tex_a_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float _82 = texture2D(_tex_a, param).x;
    highp float a = _82;
    highp vec4 _indexof_a = floor(u_bl_StreamDim.StreamDim * vec4((_tex_a_pos * u_bl_const_a_invscalebias._const_a_invscalebias.xy) + u_bl_const_a_invscalebias._const_a_invscalebias.zw, 0.0, 0.0));
    highp float param_1 = a;
    highp vec4 param_3 = _indexof_a;
    highp float param_2 = param_3.x;
    highp float b = param_2;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

