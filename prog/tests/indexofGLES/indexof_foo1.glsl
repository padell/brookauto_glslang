#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_d_prime_invscalebias
{
    highp vec4 _const_d_prime_invscalebias;
};

uniform u_block_const_d_prime_invscalebias u_bl_const_d_prime_invscalebias;

struct u_block_const_e
{
    highp vec4 _const_e_invscalebias;
};

uniform u_block_const_e u_bl_const_e_invscalebias;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

struct u_block_x
{
    highp float x;
};

uniform u_block_x u_bl_x;

struct u_block_y
{
    highp float y;
};

uniform u_block_y u_bl_y;

uniform highp sampler2D _tex_d_prime;
uniform highp sampler2D d[1];

highp vec2 _tex_d_prime_pos;
highp vec2 _tex_e_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_d_prime_pos;
    highp float _191 = texture2D(_tex_d_prime, param).x;
    highp float d_prime = _191;
    highp vec4 _indexof_d_prime = floor(u_bl_StreamDim.StreamDim * vec4((_tex_d_prime_pos * u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.xy) + u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.zw, 0.0, 0.0));
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_e_pos * u_bl_const_e_invscalebias._const_e_invscalebias.xy) + u_bl_const_e_invscalebias._const_e_invscalebias.zw, 0.0, 0.0));
    highp float param_1 = d_prime;
    highp vec4 param_2 = u_bl_gatherconst_d._gatherconst_d;
    highp float param_4 = u_bl_x.x;
    highp float param_5 = u_bl_y.y;
    highp vec4 param_6 = _indexof_d_prime;
    highp vec4 param_7 = _indexofoutvar;
    highp float _197 = param_1;
    highp vec4 _198 = param_2;
    highp vec4 _200 = param_7;
    highp float _201 = param_4;
    highp float _202 = param_5;
    highp vec4 _203 = param_6;
    highp vec2 _213 = _203.xy;
    highp vec4 _214 = _198;
    highp vec2 _222 = (_213 * _214.xy) + _214.zw;
    highp vec2 _215 = _222;
    highp float _231 = texture2D(d[0], _215).x;
    highp float _199 = _231;
    highp float param_3 = _199;
    highp float e = param_3;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

