#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_e
{
    highp vec4 _const_e_invscalebias;
};

uniform u_block_const_e u_bl_const_e_invscalebias;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

uniform highp sampler2D _tex_d_prime;
uniform highp sampler2D d[1];

highp vec2 _tex_d_prime_pos;
highp vec2 _tex_e_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_d_prime_pos;
    highp float _143 = texture2D(_tex_d_prime, param).x;
    highp float d_prime = _143;
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_e_pos * u_bl_const_e_invscalebias._const_e_invscalebias.xy) + u_bl_const_e_invscalebias._const_e_invscalebias.zw, 0.0, 0.0));
    highp float param_1 = d_prime;
    highp vec4 param_2 = u_bl_gatherconst_d._gatherconst_d;
    highp vec4 param_4 = _indexofoutvar;
    highp float _149 = param_1;
    highp vec4 _150 = param_2;
    highp vec4 _152 = param_4;
    highp vec2 _159 = _152.xy;
    highp vec4 _160 = _150;
    highp vec2 _168 = (_159 * _160.xy) + _160.zw;
    highp vec2 _161 = _168;
    highp float _177 = texture2D(d[0], _161).x;
    highp float _151 = _177;
    highp float param_3 = _151;
    highp float e = param_3;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

