#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

struct u_block_indexofe
{
    highp vec4 indexofe;
};

uniform u_block_indexofe u_bl_indexofe;

uniform highp sampler2D _tex_d_prime;
uniform highp sampler2D d[1];

highp vec2 _tex_d_prime_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_d_prime_pos;
    highp float _107 = texture2D(_tex_d_prime, param).x;
    highp float d_prime = _107;
    highp float param_1 = d_prime;
    highp vec4 param_2 = u_bl_gatherconst_d._gatherconst_d;
    highp vec4 param_4 = u_bl_indexofe.indexofe;
    highp vec2 _113 = param_4.xy;
    highp vec4 _114 = param_2;
    highp vec2 _122 = (_113 * _114.xy) + _114.zw;
    highp vec2 _115 = _122;
    highp float _131 = texture2D(d[0], _115).x;
    highp float param_3 = _131;
    highp float e = param_3;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

