#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_d_prime_invscalebias
{
    highp vec4 _const_d_prime_invscalebias;
};

uniform u_block_const_d_prime_invscalebias u_bl_const_d_prime_invscalebias;

struct u_block_const_e
{
    highp vec4 _const_e_invscalebias;
};

uniform u_block_const_e u_bl_const_e_invscalebias;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

struct u_block_dim
{
    highp vec2 dim;
};

uniform u_block_dim u_bl_dim;

uniform highp sampler2D _tex_d_prime;
uniform highp sampler2D d[1];

highp vec2 _tex_d_prime_pos;
highp vec2 _tex_e_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_d_prime_pos;
    highp float _181 = texture2D(_tex_d_prime, param).x;
    highp float d_prime = _181;
    highp vec4 _indexof_d_prime = floor(u_bl_StreamDim.StreamDim * vec4((_tex_d_prime_pos * u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.xy) + u_bl_const_d_prime_invscalebias._const_d_prime_invscalebias.zw, 0.0, 0.0));
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_e_pos * u_bl_const_e_invscalebias._const_e_invscalebias.xy) + u_bl_const_e_invscalebias._const_e_invscalebias.zw, 0.0, 0.0));
    highp float param_1 = d_prime;
    highp vec4 param_2 = u_bl_gatherconst_d._gatherconst_d;
    highp vec2 param_4 = u_bl_dim.dim;
    highp vec4 param_5 = _indexof_d_prime;
    highp vec4 param_6 = _indexofoutvar;
    highp float _187 = param_1;
    highp vec4 _188 = param_2;
    highp vec4 _190 = param_6;
    highp vec2 _191 = param_4;
    highp vec4 _192 = param_5;
    highp vec2 _201 = _192.xy;
    highp vec4 _202 = _188;
    highp vec2 _210 = (_201 * _202.xy) + _202.zw;
    highp vec2 _203 = _210;
    highp float _219 = texture2D(d[0], _203).x;
    highp float _189 = _219;
    highp float param_3 = _189;
    highp float e = param_3;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

