#version 100
precision mediump float;
precision highp int;

struct u_block_a
{
    highp float a;
};

uniform u_block_a u_bl_a;

highp vec4 _output_0;

void main()
{
    highp float param = u_bl_a.a;
    highp float param_1 = param;
    highp float b = param_1;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

