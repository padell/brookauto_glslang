
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Feb  6 2024 10:50:01
////////////////////////////////////////////

#include <brook/brook.hpp>
#include <stdio.h>

typedef struct Pair_t {
  float  field1;
} Pair;
typedef struct __cpustruct_Pair_t {
  template <typename T> T castToArg(const T& dummy)const{
    T ret;
    ret.field1 = this->field1.castToArg(ret.field1);
    return ret;
  }

__BrtFloat1 field1;
}
__cpustruct_Pair;

typedef struct __castablestruct_Pair_t {
  template <typename T> T castToArg(const T& dummy)const{
    T ret;
    ret.field1 = this->field1.castToArg(ret.field1);
    return ret;
  }

__BrtFloat1 field1;
}
__castablestruct_Pair;

namespace brook {
	template<> inline const StreamType* getStreamType(Pair*) {
		static const StreamType result[] = {__BRTFLOAT, __BRTNONE};
		return result;
	}
}

#define SIZE_X1 333

#define START_X1 127

#define END_X1 305

#define DOMAIN_X1 (END_X1 - START_X1)

#define SIZE_X2 427

#define SIZE_Y2 987

#define START_X2 0

#define END_X2 199

#define DOMAIN_X2 (END_X2 - START_X2)

#define START_Y2 576

#define END_Y2 987

#define DOMAIN_Y2 (END_Y2 - START_Y2)

static const char *__clear2_ps20= NULL;
static const char *__clear2_ps2b= NULL;
static const char *__clear2_ps2a= NULL;
static const char *__clear2_ps30= NULL;
static const char *__clear2_ctm= NULL;
static const char *__clear2_fp30= NULL;
static const char *__clear2_fp40= NULL;
static const char *__clear2_arb= NULL;
static const char *__clear2_glsl= NULL;

static const void* __clear2_gles = 0;

static void  __clear2_cpu_inner(__BrtFloat1  &result)
{
  result = -__BrtFloat1((float)1);
}
void  __clear2_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_result = (::brook::StreamInterface *) args[0];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_result((__BrtFloat1 *) __k->FetchElem(arg_result, __brt_idx));
    __clear2_cpu_inner (__out_arg_result);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address) = __out_arg_result.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address));
  }
}

extern void  clear2 (::brook::stream result) {
  static const void *__clear2_fp[] = {
     "fp30", __clear2_fp30,
     "fp40", __clear2_fp40,
     "arb", __clear2_arb,
     "glsl", __clear2_glsl,
     "gles", __clear2_gles,
     "ps20", __clear2_ps20,
     "ps2b", __clear2_ps2b,
     "ps2a", __clear2_ps2a,
     "ps30", __clear2_ps30,
     "ctm", __clear2_ctm,
     "cpu", (void *) __clear2_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__clear2_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushOutput(result);
  __k->Map();

}


