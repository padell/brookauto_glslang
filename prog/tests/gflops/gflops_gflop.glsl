#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;

highp vec2 _tex_a_pos;
highp vec4 _output_0;

highp float _fetch_float(highp sampler2D s, highp vec2 i)
{
    return texture2D(s, i).x;
}

void gflop(highp float a, inout highp float c)
{
    c = a;
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
    c += (a * c);
}

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float a = _fetch_float(_tex_a, param);
    highp float param_1 = a;
    highp float param_2;
    gflop(param_1, param_2);
    highp float c = param_2;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

