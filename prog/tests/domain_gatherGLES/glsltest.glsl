#version 100
precision mediump float;
precision highp int;

struct u_blockg
{
    highp vec4 _gatherconst_g;
};

uniform u_blockg _94;

uniform highp sampler2D _tex_indexx;
uniform highp sampler2D _tex_indexy;
uniform highp sampler2D g[1];

highp vec2 _tex_indexx_pos;
highp vec2 _tex_indexy_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_indexx_pos;
    highp float _112 = texture2D(_tex_indexx, param).x;
    highp float indexx = _112;
    highp vec2 param_1 = _tex_indexy_pos;
    highp float _118 = texture2D(_tex_indexy, param_1).x;
    highp float indexy = _118;
    highp float param_2 = indexx;
    highp float param_3 = indexy;
    highp vec4 param_4 = _94._gatherconst_g;
    highp vec2 _124 = vec2(param_2, param_3);
    highp vec2 _125 = _124;
    highp vec4 _126 = param_4;
    highp vec2 _136 = (_125 * _126.xy) + _126.zw;
    highp vec2 _127 = _136;
    highp float _145 = texture2D(g[0], _127).x;
    highp float param_5 = _145;
    highp float result = param_5;
    _output_0 = vec4(result, 0.0, 0.0, 0.0);
}

