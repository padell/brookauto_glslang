#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_inStream;

highp vec2 _tex_inStream_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_inStream_pos;
    highp float _51 = texture2D(_tex_inStream, param).x;
    highp float inStream = _51;
    highp float param_1 = inStream;
    highp float param_2 = param_1;
    highp float o = param_2;
    _output_0 = vec4(o, 0.0, 0.0, 0.0);
}

