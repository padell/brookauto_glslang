#version 100
precision mediump float;
precision highp int;

struct u_block_t
{
    highp float t;
};

uniform u_block_t u_bl_t;

uniform highp sampler2D _tex_inStream;

highp vec2 _tex_inStream_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_inStream_pos;
    highp float _68 = texture2D(_tex_inStream, param).x;
    highp float inStream = _68;
    highp float param_1 = inStream;
    highp float param_2 = u_bl_t.t;
    if (param_1 >= param_2)
    {
        discard;
    }
    highp float param_3 = param_1;
    highp float o = param_3;
    _output_0 = vec4(o, 0.0, 0.0, 0.0);
}

