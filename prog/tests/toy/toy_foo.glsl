#version 100
precision mediump float;
precision highp int;

struct u_block_c
{
    highp vec4 c;
};

uniform u_block_c u_bl_c;

struct u_block_gatherconst_d
{
    highp vec4 _gatherconst_d;
};

uniform u_block_gatherconst_d u_bl_gatherconst_d;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;
uniform highp sampler2D d[1];

highp vec2 _tex_a_pos;
highp vec2 _tex_b_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float _121 = texture2D(_tex_a, param).x;
    highp float a = _121;
    highp vec2 param_1 = _tex_b_pos;
    highp float _127 = texture2D(_tex_b, param_1).x;
    highp float b = _127;
    highp float param_2 = a;
    highp float param_3 = b;
    highp vec4 param_4 = u_bl_c.c;
    highp vec4 param_5 = u_bl_gatherconst_d._gatherconst_d;
    highp vec2 _133 = param_4.xy;
    highp vec4 _134 = param_5;
    highp vec2 _146 = (_133 * _134.xy) + _134.zw;
    highp vec2 _135 = _146;
    highp float _155 = texture2D(d[0], _135).x;
    highp float param_6 = (param_2 + param_3) + _155;
    highp float e = param_6;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

