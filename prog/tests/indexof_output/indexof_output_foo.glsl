#version 100
precision mediump float;
precision highp int;

struct u_block_const_e
{
    highp vec4 _const_e_invscalebias;
};

uniform u_block_const_e u_bl_const_e_invscalebias;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

highp vec2 _tex_e_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(vec4((_tex_e_pos * u_bl_const_e_invscalebias._const_e_invscalebias.xy) + u_bl_const_e_invscalebias._const_e_invscalebias.zw, 0.0, 0.0));
    highp vec4 param_1 = _indexofoutvar;
    highp float param = param_1.x + (param_1.y * 10.0);
    highp float e = param;
    _output_0 = vec4(e, 0.0, 0.0, 0.0);
}

