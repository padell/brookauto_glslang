#version 100
precision mediump float;
precision highp int;

struct u_block_index
{
    highp vec2 index;
};

uniform u_block_index u_bl_index;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

uniform highp sampler2D a[1];

highp vec4 _output_0;

void main()
{
    highp vec2 param = u_bl_index.index;
    highp vec4 param_1 = u_bl_gatherconst_a._gatherconst_a;
    highp vec2 _86 = param;
    highp vec4 _87 = param_1;
    highp vec2 _94 = (_86 * _87.xy) + _87.zw;
    highp vec2 _88 = _94;
    highp float _103 = texture2D(a[0], _88).x;
    highp float param_2 = _103;
    highp float b = param_2;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

