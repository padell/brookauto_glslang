#version 100
precision mediump float;
precision highp int;

uniform highp sampler2D _tex_a;

highp vec2 _tex_a_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_a_pos;
    highp float _53 = texture2D(_tex_a, param).x;
    highp float a = _53;
    highp float param_1 = a;
    highp float param_2 = param_1 + 1.0;
    highp float c = param_2;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

