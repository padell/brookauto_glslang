#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_value
{
    highp vec4 _gatherconst_value;
};

uniform u_block_gatherconst_value u_bl_gatherconst_value;

struct u_block_index
{
    highp vec2 index;
};

uniform u_block_index u_bl_index;

struct u_block_maxvalue
{
    highp vec2 maxvalue;
};

uniform u_block_maxvalue u_bl_maxvalue;

struct u_block_nothing
{
    highp float nothing;
};

uniform u_block_nothing u_bl_nothing;

uniform highp sampler2D value[1];

highp vec4 _output_0;

void main()
{
    highp vec4 param = u_bl_gatherconst_value._gatherconst_value;
    highp vec2 param_1 = u_bl_index.index;
    highp vec2 param_3 = u_bl_maxvalue.maxvalue;
    highp float param_4 = u_bl_nothing.nothing;
    bool _127 = param_1.y >= param_3.y;
    bool _134;
    if (!_127)
    {
        _134 = param_1.y < (-0.125);
    }
    else
    {
        _134 = _127;
    }
    highp float param_2;
    if (_134)
    {
        param_2 = param_4;
    }
    else
    {
        highp vec2 _119 = param_1;
        highp vec4 _120 = param;
        highp vec2 _143 = (_119 * _120.xy) + _120.zw;
        highp vec2 _121 = _143;
        highp float _152 = texture2D(value[0], _121).x;
        param_2 = _152;
    }
    highp float outvar = param_2;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

