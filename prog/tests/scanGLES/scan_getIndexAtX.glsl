#version 100
precision mediump float;
precision highp int;

struct u_block_shiftRight
{
    highp float shiftRight;
};

uniform u_block_shiftRight u_bl_shiftRight;

struct u_block_maxvalue
{
    highp vec2 maxvalue;
};

uniform u_block_maxvalue u_bl_maxvalue;

uniform highp sampler2D _tex_inpvarindexx;
uniform highp sampler2D _tex_inpvarindexy;

highp vec2 _tex_inpvarindexx_pos;
highp vec2 _tex_inpvarindexy_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_inpvarindexx_pos;
    highp float _106 = texture2D(_tex_inpvarindexx, param).x;
    highp float inpvarindexx = _106;
    highp vec2 param_1 = _tex_inpvarindexy_pos;
    highp float _112 = texture2D(_tex_inpvarindexy, param_1).x;
    highp float inpvarindexy = _112;
    highp float param_2 = inpvarindexx;
    highp float param_3 = inpvarindexy;
    highp float param_4 = u_bl_shiftRight.shiftRight;
    highp vec2 param_5 = u_bl_maxvalue.maxvalue;
    highp float _118 = param_2 + param_4;
    _118 = floor(mod(floor(_118 + float(0.5)), param_5.x) + float(0.5));
    if (_118 <= (-0.5))
    {
        _118 += param_5.x;
    }
    if ((_118 + 0.25) >= param_5.x)
    {
        _118 = 0.0;
    }
    highp float param_6 = _118;
    highp float outvarindexx = param_6;
    _output_0 = vec4(outvarindexx, 0.0, 0.0, 0.0);
}

