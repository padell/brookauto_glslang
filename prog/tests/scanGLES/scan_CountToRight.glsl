#version 100
precision mediump float;
precision highp int;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_value
{
    highp vec4 _gatherconst_value;
};

uniform u_block_gatherconst_value u_bl_gatherconst_value;

struct u_block_twotoi
{
    highp float twotoi;
};

uniform u_block_twotoi u_bl_twotoi;

struct u_block_maxvalue
{
    highp vec2 maxvalue;
};

uniform u_block_maxvalue u_bl_maxvalue;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

uniform highp sampler2D value[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_value._gatherconst_value;
    highp float param_2 = u_bl_twotoi.twotoi;
    highp vec2 param_3 = u_bl_maxvalue.maxvalue;
    highp vec4 param_4 = _indexofoutvar;
    highp float _259 = param_4.x;
    highp float _260 = param_4.y;
    highp float _261 = param_2;
    highp vec2 _262 = param_3;
    highp float _310 = _259 + _261;
    _310 = floor(mod(floor(_310 + float(0.5)), _262.x) + float(0.5));
    if (_310 <= (-0.5))
    {
        _310 += _262.x;
    }
    if ((_310 + 0.25) >= _262.x)
    {
        _310 = 0.0;
    }
    highp float _263 = _310;
    highp float _258 = _263;
    highp float _265 = param_4.x;
    highp float _266 = param_4.y;
    highp float _267 = param_2;
    highp vec2 _268 = param_3;
    highp vec2 _337;
    _337.x = _265 + _267;
    _337.y = _266 + floor((0.5 + _337.x) / _268.x);
    highp float _269 = _337.y;
    highp float _264 = _269;
    highp vec4 _271 = param;
    highp vec2 _272 = vec2(_258, _264);
    highp vec2 _274 = param_3;
    highp float _275 = 0.0;
    bool _363 = _272.y >= _274.y;
    bool _370;
    if (!_363)
    {
        _370 = _272.y < (-0.125);
    }
    else
    {
        _370 = _363;
    }
    highp float _273;
    if (_370)
    {
        _273 = _275;
    }
    else
    {
        highp vec2 _355 = _272;
        highp vec4 _356 = _271;
        highp vec2 _379 = (_355 * _356.xy) + _356.zw;
        highp vec2 _357 = _379;
        highp float _388 = texture2D(value[0], _357).x;
        _273 = _388;
    }
    highp float _270 = _273;
    highp vec2 _276 = param_4.xy;
    highp vec4 _277 = param;
    highp vec2 _395 = (_276 * _277.xy) + _277.zw;
    highp vec2 _278 = _395;
    highp float _404 = texture2D(value[0], _278).x;
    highp float param_1 = _404 + _270;
    highp float outvar = param_1;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

