#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_g
{
    highp vec4 _gatherconst_g;
};

uniform u_block_gatherconst_g u_bl_gatherconst_g;

uniform highp sampler2D _tex_count;
uniform highp sampler2D _tex_inpvar;
uniform highp sampler2D g[1];

highp vec2 _tex_count_pos;
highp vec2 _tex_inpvar_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_count_pos;
    highp float _126 = texture2D(_tex_count, param).x;
    highp float count = _126;
    highp vec2 param_1 = _tex_inpvar_pos;
    highp float _132 = texture2D(_tex_inpvar, param_1).x;
    highp float inpvar = _132;
    highp float param_2 = count;
    highp float param_3 = inpvar;
    highp vec4 param_4 = u_bl_gatherconst_g._gatherconst_g;
    highp float _138 = param_2;
    highp float _139 = param_3;
    while (_138 > 0.0)
    {
        highp float _140 = _139;
        highp vec4 _141 = param_4;
        highp float _160 = (_140 * _141.x) + _141.z;
        highp float _142 = _160;
        highp float _169 = texture2D(g[0], vec2(_142, 0.0)).x;
        _139 = _169;
        _138 -= 1.0;
    }
    highp float param_5 = _139;
    highp float result = param_5;
    _output_0 = vec4(result, 0.0, 0.0, 0.0);
}

