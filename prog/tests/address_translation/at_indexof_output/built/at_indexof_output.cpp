
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Feb  6 2024 10:50:01
////////////////////////////////////////////

#define BROOK_ENABLE_ADDRESS_TRANSLATION 1
#include <brook/brook.hpp>
#include <stdio.h>

#define X_1D 10000

#define X_2D 5000

#define Y_2D 4

#define X_3D 40

#define Y_3D 10

#define Z_3D 7

#define X_4D 3

#define Y_4D 6

#define Z_4D 8

#define W_4D 5

static const char *__getIndexStream1_ps20= NULL;
static const char *__getIndexStream1_ps2b= NULL;
static const char *__getIndexStream1_ps2a= NULL;
static const char *__getIndexStream1_ps30= NULL;
static const char *__getIndexStream1_ctm= NULL;
static const char *__getIndexStream1_fp30= NULL;
static const char *__getIndexStream1_fp40= NULL;
static const char *__getIndexStream1_arb= NULL;
static const char *__getIndexStream1_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __getIndexStream1_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.output_address_translation()
			.pass( gpu_pass_desc(
				"H��0H���� \n"
				"//!!BRCC\n"
				"//narg:1\n"
				"//oi:1:result\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(0, kGlobalConstant_ATOutputLinearize)
				.constant(0, kGlobalConstant_ATOutputStride)
				.constant(0, kGlobalConstant_ATOutputInvStride)
				.constant(0, kGlobalConstant_ATOutputInvExtent)
				.constant(0, kGlobalConstant_ATOutputDomainMin)
				.constant(0, kGlobalConstant_ATOutputDomainSize)
				.constant(0, kGlobalConstant_ATOutputInvShape)
				.constant(0, kGlobalConstant_ATHackConstant)
				.interpolant(0, kGlobalInterpolant_ATOutputTex)
				.interpolant(0, kGlobalInterpolatn_ATOutputAddress)
				.output(1, 0)
				.output(1, 0)
			)
		)
		.technique( gpu_technique_desc()
			.output_address_translation()
			.input_address_translation()
			.pass( gpu_pass_desc(
				"H��0H���� \n"
				"//!!BRCC\n"
				"//narg:1\n"
				"//oi:1:result\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_1:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(0, kGlobalConstant_ATOutputLinearize)
				.constant(0, kGlobalConstant_ATOutputStride)
				.constant(0, kGlobalConstant_ATOutputInvStride)
				.constant(0, kGlobalConstant_ATOutputInvExtent)
				.constant(0, kGlobalConstant_ATOutputDomainMin)
				.constant(0, kGlobalConstant_ATOutputDomainSize)
				.constant(0, kGlobalConstant_ATOutputInvShape)
				.constant(0, kGlobalConstant_ATHackConstant)
				.interpolant(0, kGlobalInterpolant_ATOutputTex)
				.interpolant(0, kGlobalInterpolatn_ATOutputAddress)
				.output(1, 0)
				.output(1, 0)
			)
		);
	static const void* __getIndexStream1_gles = &__getIndexStream1_gles_desc;
}

static void  __getIndexStream1_cpu_inner(__BrtFloat1  &result)
{
  result = (indexof(result)).swizzle1(maskX);
}
void  __getIndexStream1_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_result = (::brook::StreamInterface *) args[0];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_result((__BrtFloat1 *) __k->FetchElem(arg_result, __brt_idx));
    __getIndexStream1_cpu_inner (__out_arg_result);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address) = __out_arg_result.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address));
  }
}

extern void  getIndexStream1 (::brook::stream result) {
  static const void *__getIndexStream1_fp[] = {
     "fp30", __getIndexStream1_fp30,
     "fp40", __getIndexStream1_fp40,
     "arb", __getIndexStream1_arb,
     "glsl", __getIndexStream1_glsl,
     "gles", __getIndexStream1_gles,
     "ps20", __getIndexStream1_ps20,
     "ps2b", __getIndexStream1_ps2b,
     "ps2a", __getIndexStream1_ps2a,
     "ps30", __getIndexStream1_ps30,
     "ctm", __getIndexStream1_ctm,
     "cpu", (void *) __getIndexStream1_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__getIndexStream1_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushOutput(result);
  __k->Map();

}


