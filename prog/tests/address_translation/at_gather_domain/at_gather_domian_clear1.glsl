#version 100
precision mediump float;
precision highp int;

highp vec4 _output_0;

void main()
{
    highp float param = -1.0;
    highp float result = param;
    _output_0 = vec4(result, 0.0, 0.0, 0.0);
}

