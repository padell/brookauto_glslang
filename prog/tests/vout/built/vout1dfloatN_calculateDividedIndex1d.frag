#version 450 core

#if defined(DXPIXELSHADER)
#define fixed float
#define fixed2 vec2
#define fixed3 vec3
#define fixed4 vec4
#endif
#define shortfixed float
#define shortfixed2 vec2
#define shortfixed3 vec3
#define shortfixed4 vec4
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define double2 dvec2
#if defined(DXPIXELSHADER) || !defined(USERECT)
#define _stype1 sampler2D
#define _stype2 sampler2D
#define _stype3 sampler3D
#if defined(SHADERMODEL3)
#define _sample1(s,i) textureLod((s),i,0.0)
#define _sample2(s,i) textureLod((s),i,0.0)
#define _sample3(s,i) textureLod((s),i,0.0)
#else
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif
#else
#define _stype1 sampler2DRect
#define _stype2 sampler2DRect
#define _stype3 sampler3DRect
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif

#define _FRAGMENTKILL discard
#ifdef USERECT
#define SKIPSCALEBIAS
#endif

#ifdef SKIPSCALEBIAS
float _gatherindex1( float index, vec4 scalebias ) { return (index+scalebias.z); }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return (index+scalebias.zw); }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) vec4(a, 0.0, 0.0, 0.0)
#else
float _gatherindex1( float index, vec4 scalebias ) { return index*scalebias.x+scalebias.z; }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return index*scalebias.xy+scalebias.zw; }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) (b)
#endif

double _fetch_double( _stype1 s, float i ) { return texture(s, vec2(i, 0.0)).x;}
double _fetch_double( _stype2 s, vec2 i ) { return texture(s, i).x;}
double _fetch_double( _stype3 s, vec3 i ) { return texture(s, i).x;}
dvec2 _fetch_double2( _stype1 s, float i ) { dvec2 r; r[0] = texture(s, vec2(i, 0.0)).x; r[1] = texture(s, vec2(i, 0.0)).y; return r;}
dvec2 _fetch_double2( _stype2 s, vec2 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
dvec2 _fetch_double2( _stype3 s, vec3 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
float _fetch_float( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_float( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_float( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_float2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_float2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_float2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_float3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_float3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_float3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_float4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_float4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_float4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_unsigned_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_unsigned_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_unsigned_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_unsigned_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_unsigned_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_unsigned_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_unsigned_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_unsigned_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_unsigned_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }


float _gather_float( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_float( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_float( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_float2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_float2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_float2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_float3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_float3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_float3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_float4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_float4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_float4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_shortfixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_shortfixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_shortfixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_shortfixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_shortfixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_shortfixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_shortfixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_shortfixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_shortfixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_shortfixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_shortfixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_shortfixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_fixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_fixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_fixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_fixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_fixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_fixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_fixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_fixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_fixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_fixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_fixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_fixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_char( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_char( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_char( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_char2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_char2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_char2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_char3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_char3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_char3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_char4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_char4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_char4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_uchar( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_uchar( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_uchar( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_uchar2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_uchar2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_uchar2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_uchar3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_uchar3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_uchar3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_uchar4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_uchar4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_uchar4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
#define uchar1 uint;
#define uchar2 uvec2;
#define uchar3 uvec3;
#define uchar4 uvec4;


void kernelReadItem1d (_stype1 items[1],
                       float4 _const_items_scalebias,
                       float  index,
                       out float  item)
{
  item = _gather_float (items,_gatherindex1((index).x, _const_items_scalebias));
}
void calculateDividedIndex1d (float4  index,
                              float  modulus,
                              out float  newindex)
{
  float  epsilon = 1.000000f / 32.000000f;

  newindex = floor(index.x / modulus + epsilon);
}
void calculateIndexModulus1d (float4  index,
                              float  modulus,
                              float  offset,
                              out float  which)
{
  which = floor(fmod(index.x,modulus) - offset);
}
void valueProducedfloat1d (float  input,
                           inout float  output)
{
  output = (isinf(input.x)) ? (output) : (input);
}
void isFiniteKernelfloat1d (float  inp,
                            out float  outp)
{
  outp = !isinf(inp.x);
}
void valueAtfloat1d (_stype1 value[1],
                     float4 _const_value_scalebias,
                     float  index,
                     out float  output,
                     float  maxvalue,
                     float  nothing)
{
  if (index >= maxvalue || index < -0.100000f)
    output = nothing;
  else
    output = _gather_float (value,_gatherindex1((index).x, _const_value_scalebias));
}
void NanToBoolRightfloat1d (_stype1 value[1],
                            float4 _const_value_scalebias,
                            out float  output,
                            float  sign,
                            float  maxvalue,
                            float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + sign;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = ((isinf(_gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)).x)) ? (1) : (0)) + ((isinf(neighbor.x)) ? (1) : (0));
}
void NanToRightfloat1d (_stype1 value[1],
                        float4 _const_value_scalebias,
                        out float  output,
                        float  twotoi,
                        float  maxvalue,
                        float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = round(_gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor);
}
void CountToRightfloat1d (_stype1 value[1],
                          float4 _const_value_scalebias,
                          out float  output,
                          float  twotoi,
                          float  maxvalue,
                          float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = _gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor;
}
void GatherGuessfloat1d (_stype1 scatterindex[1],
                         float4 _const_scatterindex_scalebias,
                         out float  output,
                         _stype1 value[1],
                         float4 _const_value_scalebias,
                         float  twotologkminusi,
                         float  maxvalue,
                         float  halfk,
                         float  sign,
                         float4 _indexof_output)
{
  float  neighbor;
  float  nextPlaceToLook = (_indexof_output).x - sign * halfk;

  valueAtfloat1d(scatterindex,_const_scatterindex_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  if (neighbor > halfk)
  {
    output = halfk + twotologkminusi;
  }

  else
  {
    float  actualValue;

    valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,actualValue,maxvalue,0);
    if (neighbor == halfk && !isinf(actualValue.x))
    {
      output = halfk;
    }

    else
    {
      output = halfk - twotologkminusi;
    }

  }

}
void EstablishGuessfloat1d (_stype1 scatterindex[1],
                            float4 _const_scatterindex_scalebias,
                            out float  output,
                            _stype1 value[1],
                            float4 _const_value_scalebias,
                            float  twotologkminusi,
                            float  maxvalue,
                            float  halfk,
                            float  sign,
                            float4 _indexof_output)
{
  if (_gather_float (scatterindex,_gatherindex1((_indexof_output).x, _const_scatterindex_scalebias)) == 0)
  {
    output = 0;
  }

  else
  {
    GatherGuessfloat1d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,halfk,sign,_indexof_output);
  }

}
void UpdateGuessfloat1d (_stype1 scatterindex[1],
                         float4 _const_scatterindex_scalebias,
                         out float  output,
                         _stype1 value[1],
                         float4 _const_value_scalebias,
                         float  twotologkminusi,
                         float  maxvalue,
                         float  lastguess,
                         float  sign,
                         float4 _indexof_output)
{
  GatherGuessfloat1d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,lastguess,sign,_indexof_output);
}
void RelativeGatherfloat1d (out float  output,
                            _stype1 gatherindex[1],
                            float4 _const_gatherindex_scalebias,
                            _stype1 value[1],
                            float4 _const_value_scalebias,
                            float2  sign,
                            float  maxvalue,
                            float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x - sign.x * _gather_float (gatherindex,_gatherindex1((_indexof_output).x, _const_gatherindex_scalebias));

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,output,maxvalue,sign.y);
}
void float1dstreamCombine1 (_stype1 input0[1],
                            float4 _const_input0_scalebias,
                            float  modulus,
                            float  offset,
                            float  oldoutput,
                            out float  output,
                            float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = oldoutput;
  }

}
void float1dstreamCombine2f (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             float  modulus,
                             out float  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
  }

}
void float1dstreamCombine2 (_stype1 input0[1],
                            float4 _const_input0_scalebias,
                            _stype1 input1[1],
                            float4 _const_input1_scalebias,
                            float  modulus,
                            float  offset,
                            float  oldoutput,
                            out float  output,
                            float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = oldoutput;
    }

}
void float1dstreamCombine3f (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             float  modulus,
                             out float  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = _gather_float (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
    }

}
void float1dstreamCombine3 (_stype1 input0[1],
                            float4 _const_input0_scalebias,
                            _stype1 input1[1],
                            float4 _const_input1_scalebias,
                            _stype1 input2[1],
                            float4 _const_input2_scalebias,
                            float  modulus,
                            float  offset,
                            float  oldoutput,
                            out float  output,
                            float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = oldoutput;
      }

}
void float1dstreamCombine4f (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             _stype1 input3[1],
                             float4 _const_input3_scalebias,
                             float  modulus,
                             out float  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = _gather_float (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
      }

}
void float1dstreamCombine4 (_stype1 input0[1],
                            float4 _const_input0_scalebias,
                            _stype1 input1[1],
                            float4 _const_input1_scalebias,
                            _stype1 input2[1],
                            float4 _const_input2_scalebias,
                            _stype1 input3[1],
                            float4 _const_input3_scalebias,
                            float  modulus,
                            float  offset,
                            float  oldoutput,
                            out float  output,
                            float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = oldoutput;
        }

}
void float1dstreamCombine5f (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             _stype1 input3[1],
                             float4 _const_input3_scalebias,
                             _stype1 input4[1],
                             float4 _const_input4_scalebias,
                             float  modulus,
                             out float  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = _gather_float (input4,_gatherindex1((newindex).x, _const_input4_scalebias));
        }

}
void combinefloat1d (_stype1 input1[1],
                     float4 _const_input1_scalebias,
                     _stype1 input2[1],
                     float4 _const_input2_scalebias,
                     out float  output,
                     float  startsecond,
                     float  endsecond,
                     float  maxinput2value,
                     float  inf,
                     float4 _indexof_output)
{
  if ((_indexof_output).x >= startsecond.x)
  {
    float  secondindex = (_indexof_output).x - startsecond;

    if (secondindex.x >= endsecond.x)
    {
      output = inf;
    }

    else
    {
      output = _gather_float (input2,_gatherindex1((secondindex).x, _const_input2_scalebias));
    }

  }

  else
  {
    output = _gather_float (input1,_gatherindex1(((_indexof_output).x).x, _const_input1_scalebias));
  }

}
void valueProducedfloat21d (float2  input,
                            inout float2  output)
{
  output = (isinf(input.x)) ? (output) : (input);
}
void isFiniteKernelfloat21d (float2  inp,
                             out float  outp)
{
  outp = !isinf(inp.x);
}
void valueAtfloat21d (_stype1 value[1],
                      float4 _const_value_scalebias,
                      float  index,
                      out float2  output,
                      float  maxvalue,
                      float  nothing)
{
  if (index >= maxvalue || index < -0.100000f)
    output = nothing;
  else
    output = _gather_float2 (value,_gatherindex1((index).x, _const_value_scalebias));
}
void NanToBoolRightfloat21d (_stype1 value[1],
                             float4 _const_value_scalebias,
                             out float  output,
                             float  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + sign;
  float2  neighbor;

  valueAtfloat21d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = ((isinf(_gather_float2 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)).x)) ? (1) : (0)) + ((isinf(neighbor.x)) ? (1) : (0));
}
void NanToRightfloat21d (_stype1 value[1],
                         float4 _const_value_scalebias,
                         out float  output,
                         float  twotoi,
                         float  maxvalue,
                         float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = round(_gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor);
}
void CountToRightfloat21d (_stype1 value[1],
                           float4 _const_value_scalebias,
                           out float2  output,
                           float  twotoi,
                           float  maxvalue,
                           float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float2  neighbor;

  valueAtfloat21d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = _gather_float2 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor;
}
void GatherGuessfloat21d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  halfk,
                          float  sign,
                          float4 _indexof_output)
{
  float  neighbor;
  float  nextPlaceToLook = (_indexof_output).x - sign * halfk;

  valueAtfloat1d(scatterindex,_const_scatterindex_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  if (neighbor > halfk)
  {
    output = halfk + twotologkminusi;
  }

  else
  {
    float2  actualValue;

    valueAtfloat21d(value,_const_value_scalebias,nextPlaceToLook,actualValue,maxvalue,0);
    if (neighbor == halfk && !isinf(actualValue.x))
    {
      output = halfk;
    }

    else
    {
      output = halfk - twotologkminusi;
    }

  }

}
void EstablishGuessfloat21d (_stype1 scatterindex[1],
                             float4 _const_scatterindex_scalebias,
                             out float  output,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float  twotologkminusi,
                             float  maxvalue,
                             float  halfk,
                             float  sign,
                             float4 _indexof_output)
{
  if (_gather_float (scatterindex,_gatherindex1((_indexof_output).x, _const_scatterindex_scalebias)) == 0)
  {
    output = 0;
  }

  else
  {
    GatherGuessfloat21d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,halfk,sign,_indexof_output);
  }

}
void UpdateGuessfloat21d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  lastguess,
                          float  sign,
                          float4 _indexof_output)
{
  GatherGuessfloat21d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,lastguess,sign,_indexof_output);
}
void RelativeGatherfloat21d (out float2  output,
                             _stype1 gatherindex[1],
                             float4 _const_gatherindex_scalebias,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float2  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x - sign.x * _gather_float (gatherindex,_gatherindex1((_indexof_output).x, _const_gatherindex_scalebias));

  valueAtfloat21d(value,_const_value_scalebias,nextPlaceToLook,output,maxvalue,sign.y);
}
void float21dstreamCombine1 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             float  modulus,
                             float  offset,
                             float2  oldoutput,
                             out float2  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = oldoutput;
  }

}
void float21dstreamCombine2f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              float  modulus,
                              out float2  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
  }

}
void float21dstreamCombine2 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             float  modulus,
                             float  offset,
                             float2  oldoutput,
                             out float2  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = oldoutput;
    }

}
void float21dstreamCombine3f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              float  modulus,
                              out float2  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = _gather_float2 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
    }

}
void float21dstreamCombine3 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             float  modulus,
                             float  offset,
                             float2  oldoutput,
                             out float2  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float2 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = oldoutput;
      }

}
void float21dstreamCombine4f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              float  modulus,
                              out float2  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float2 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = _gather_float2 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
      }

}
void float21dstreamCombine4 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             _stype1 input3[1],
                             float4 _const_input3_scalebias,
                             float  modulus,
                             float  offset,
                             float2  oldoutput,
                             out float2  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float2 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float2 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = oldoutput;
        }

}
void float21dstreamCombine5f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              _stype1 input4[1],
                              float4 _const_input4_scalebias,
                              float  modulus,
                              out float2  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float2 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float2 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float2 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float2 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = _gather_float2 (input4,_gatherindex1((newindex).x, _const_input4_scalebias));
        }

}
void combinefloat21d (_stype1 input1[1],
                      float4 _const_input1_scalebias,
                      _stype1 input2[1],
                      float4 _const_input2_scalebias,
                      out float2  output,
                      float  startsecond,
                      float  endsecond,
                      float  maxinput2value,
                      float  inf,
                      float4 _indexof_output)
{
  if ((_indexof_output).x >= startsecond.x)
  {
    float  secondindex = (_indexof_output).x - startsecond;

    if (secondindex.x >= endsecond.x)
    {
      output = inf;
    }

    else
    {
      output = _gather_float2 (input2,_gatherindex1((secondindex).x, _const_input2_scalebias));
    }

  }

  else
  {
    output = _gather_float2 (input1,_gatherindex1(((_indexof_output).x).x, _const_input1_scalebias));
  }

}
void valueProducedfloat31d (float3  input,
                            inout float3  output)
{
  output = (isinf(input.x)) ? (output) : (input);
}
void isFiniteKernelfloat31d (float3  inp,
                             out float  outp)
{
  outp = !isinf(inp.x);
}
void valueAtfloat31d (_stype1 value[1],
                      float4 _const_value_scalebias,
                      float  index,
                      out float3  output,
                      float  maxvalue,
                      float  nothing)
{
  if (index >= maxvalue || index < -0.100000f)
    output = nothing;
  else
    output = _gather_float3 (value,_gatherindex1((index).x, _const_value_scalebias));
}
void NanToBoolRightfloat31d (_stype1 value[1],
                             float4 _const_value_scalebias,
                             out float  output,
                             float  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + sign;
  float3  neighbor;

  valueAtfloat31d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = ((isinf(_gather_float3 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)).x)) ? (1) : (0)) + ((isinf(neighbor.x)) ? (1) : (0));
}
void NanToRightfloat31d (_stype1 value[1],
                         float4 _const_value_scalebias,
                         out float  output,
                         float  twotoi,
                         float  maxvalue,
                         float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = round(_gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor);
}
void CountToRightfloat31d (_stype1 value[1],
                           float4 _const_value_scalebias,
                           out float3  output,
                           float  twotoi,
                           float  maxvalue,
                           float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float3  neighbor;

  valueAtfloat31d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = _gather_float3 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor;
}
void GatherGuessfloat31d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  halfk,
                          float  sign,
                          float4 _indexof_output)
{
  float  neighbor;
  float  nextPlaceToLook = (_indexof_output).x - sign * halfk;

  valueAtfloat1d(scatterindex,_const_scatterindex_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  if (neighbor > halfk)
  {
    output = halfk + twotologkminusi;
  }

  else
  {
    float3  actualValue;

    valueAtfloat31d(value,_const_value_scalebias,nextPlaceToLook,actualValue,maxvalue,0);
    if (neighbor == halfk && !isinf(actualValue.x))
    {
      output = halfk;
    }

    else
    {
      output = halfk - twotologkminusi;
    }

  }

}
void EstablishGuessfloat31d (_stype1 scatterindex[1],
                             float4 _const_scatterindex_scalebias,
                             out float  output,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float  twotologkminusi,
                             float  maxvalue,
                             float  halfk,
                             float  sign,
                             float4 _indexof_output)
{
  if (_gather_float (scatterindex,_gatherindex1((_indexof_output).x, _const_scatterindex_scalebias)) == 0)
  {
    output = 0;
  }

  else
  {
    GatherGuessfloat31d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,halfk,sign,_indexof_output);
  }

}
void UpdateGuessfloat31d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  lastguess,
                          float  sign,
                          float4 _indexof_output)
{
  GatherGuessfloat31d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,lastguess,sign,_indexof_output);
}
void RelativeGatherfloat31d (out float3  output,
                             _stype1 gatherindex[1],
                             float4 _const_gatherindex_scalebias,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float2  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x - sign.x * _gather_float (gatherindex,_gatherindex1((_indexof_output).x, _const_gatherindex_scalebias));

  valueAtfloat31d(value,_const_value_scalebias,nextPlaceToLook,output,maxvalue,sign.y);
}
void float31dstreamCombine1 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             float  modulus,
                             float  offset,
                             float3  oldoutput,
                             out float3  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = oldoutput;
  }

}
void float31dstreamCombine2f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              float  modulus,
                              out float3  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
  }

}
void float31dstreamCombine2 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             float  modulus,
                             float  offset,
                             float3  oldoutput,
                             out float3  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = oldoutput;
    }

}
void float31dstreamCombine3f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              float  modulus,
                              out float3  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = _gather_float3 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
    }

}
void float31dstreamCombine3 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             float  modulus,
                             float  offset,
                             float3  oldoutput,
                             out float3  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float3 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = oldoutput;
      }

}
void float31dstreamCombine4f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              float  modulus,
                              out float3  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float3 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = _gather_float3 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
      }

}
void float31dstreamCombine4 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             _stype1 input3[1],
                             float4 _const_input3_scalebias,
                             float  modulus,
                             float  offset,
                             float3  oldoutput,
                             out float3  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float3 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float3 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = oldoutput;
        }

}
void float31dstreamCombine5f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              _stype1 input4[1],
                              float4 _const_input4_scalebias,
                              float  modulus,
                              out float3  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float3 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float3 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float3 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float3 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = _gather_float3 (input4,_gatherindex1((newindex).x, _const_input4_scalebias));
        }

}
void combinefloat31d (_stype1 input1[1],
                      float4 _const_input1_scalebias,
                      _stype1 input2[1],
                      float4 _const_input2_scalebias,
                      out float3  output,
                      float  startsecond,
                      float  endsecond,
                      float  maxinput2value,
                      float  inf,
                      float4 _indexof_output)
{
  if ((_indexof_output).x >= startsecond.x)
  {
    float  secondindex = (_indexof_output).x - startsecond;

    if (secondindex.x >= endsecond.x)
    {
      output = inf;
    }

    else
    {
      output = _gather_float3 (input2,_gatherindex1((secondindex).x, _const_input2_scalebias));
    }

  }

  else
  {
    output = _gather_float3 (input1,_gatherindex1(((_indexof_output).x).x, _const_input1_scalebias));
  }

}
void valueProducedfloat41d (float4  input,
                            inout float4  output)
{
  output = (isinf(input.x)) ? (output) : (input);
}
void isFiniteKernelfloat41d (float4  inp,
                             out float  outp)
{
  outp = !isinf(inp.x);
}
void valueAtfloat41d (_stype1 value[1],
                      float4 _const_value_scalebias,
                      float  index,
                      out float4  output,
                      float  maxvalue,
                      float  nothing)
{
  if (index >= maxvalue || index < -0.100000f)
    output = nothing;
  else
    output = _gather_float4 (value,_gatherindex1((index).x, _const_value_scalebias));
}
void NanToBoolRightfloat41d (_stype1 value[1],
                             float4 _const_value_scalebias,
                             out float  output,
                             float  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + sign;
  float4  neighbor;

  valueAtfloat41d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = ((isinf(_gather_float4 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)).x)) ? (1) : (0)) + ((isinf(neighbor.x)) ? (1) : (0));
}
void NanToRightfloat41d (_stype1 value[1],
                         float4 _const_value_scalebias,
                         out float  output,
                         float  twotoi,
                         float  maxvalue,
                         float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float  neighbor;

  valueAtfloat1d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = round(_gather_float (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor);
}
void CountToRightfloat41d (_stype1 value[1],
                           float4 _const_value_scalebias,
                           out float4  output,
                           float  twotoi,
                           float  maxvalue,
                           float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x + twotoi;
  float4  neighbor;

  valueAtfloat41d(value,_const_value_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  output = _gather_float4 (value,_gatherindex1((_indexof_output).x, _const_value_scalebias)) + neighbor;
}
void GatherGuessfloat41d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  halfk,
                          float  sign,
                          float4 _indexof_output)
{
  float  neighbor;
  float  nextPlaceToLook = (_indexof_output).x - sign * halfk;

  valueAtfloat1d(scatterindex,_const_scatterindex_scalebias,nextPlaceToLook,neighbor,maxvalue,0);
  if (neighbor > halfk)
  {
    output = halfk + twotologkminusi;
  }

  else
  {
    float4  actualValue;

    valueAtfloat41d(value,_const_value_scalebias,nextPlaceToLook,actualValue,maxvalue,0);
    if (neighbor == halfk && !isinf(actualValue.x))
    {
      output = halfk;
    }

    else
    {
      output = halfk - twotologkminusi;
    }

  }

}
void EstablishGuessfloat41d (_stype1 scatterindex[1],
                             float4 _const_scatterindex_scalebias,
                             out float  output,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float  twotologkminusi,
                             float  maxvalue,
                             float  halfk,
                             float  sign,
                             float4 _indexof_output)
{
  if (_gather_float (scatterindex,_gatherindex1((_indexof_output).x, _const_scatterindex_scalebias)) == 0)
  {
    output = 0;
  }

  else
  {
    GatherGuessfloat41d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,halfk,sign,_indexof_output);
  }

}
void UpdateGuessfloat41d (_stype1 scatterindex[1],
                          float4 _const_scatterindex_scalebias,
                          out float  output,
                          _stype1 value[1],
                          float4 _const_value_scalebias,
                          float  twotologkminusi,
                          float  maxvalue,
                          float  lastguess,
                          float  sign,
                          float4 _indexof_output)
{
  GatherGuessfloat41d(scatterindex,_const_scatterindex_scalebias,output,value,_const_value_scalebias,twotologkminusi,maxvalue,lastguess,sign,_indexof_output);
}
void RelativeGatherfloat41d (out float4  output,
                             _stype1 gatherindex[1],
                             float4 _const_gatherindex_scalebias,
                             _stype1 value[1],
                             float4 _const_value_scalebias,
                             float2  sign,
                             float  maxvalue,
                             float4 _indexof_output)
{
  float  nextPlaceToLook = (_indexof_output).x - sign.x * _gather_float (gatherindex,_gatherindex1((_indexof_output).x, _const_gatherindex_scalebias));

  valueAtfloat41d(value,_const_value_scalebias,nextPlaceToLook,output,maxvalue,sign.y);
}
void float41dstreamCombine1 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             float  modulus,
                             float  offset,
                             float4  oldoutput,
                             out float4  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = oldoutput;
  }

}
void float41dstreamCombine2f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              float  modulus,
                              out float4  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
  {
    output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
  }

}
void float41dstreamCombine2 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             float  modulus,
                             float  offset,
                             float4  oldoutput,
                             out float4  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = oldoutput;
    }

}
void float41dstreamCombine3f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              float  modulus,
                              out float4  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
    {
      output = _gather_float4 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
    }

}
void float41dstreamCombine3 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             float  modulus,
                             float  offset,
                             float4  oldoutput,
                             out float4  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float4 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = oldoutput;
      }

}
void float41dstreamCombine4f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              float  modulus,
                              out float4  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float4 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
      {
        output = _gather_float4 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
      }

}
void float41dstreamCombine4 (_stype1 input0[1],
                             float4 _const_input0_scalebias,
                             _stype1 input1[1],
                             float4 _const_input1_scalebias,
                             _stype1 input2[1],
                             float4 _const_input2_scalebias,
                             _stype1 input3[1],
                             float4 _const_input3_scalebias,
                             float  modulus,
                             float  offset,
                             float4  oldoutput,
                             out float4  output,
                             float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,offset,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float4 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float4 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = oldoutput;
        }

}
void float41dstreamCombine5f (_stype1 input0[1],
                              float4 _const_input0_scalebias,
                              _stype1 input1[1],
                              float4 _const_input1_scalebias,
                              _stype1 input2[1],
                              float4 _const_input2_scalebias,
                              _stype1 input3[1],
                              float4 _const_input3_scalebias,
                              _stype1 input4[1],
                              float4 _const_input4_scalebias,
                              float  modulus,
                              out float4  output,
                              float4 _indexof_output)
{
  float  newindex;
  float  whichmod;

  calculateDividedIndex1d(_indexof_output,modulus,newindex);
  calculateIndexModulus1d(_indexof_output,modulus,-0.500000f,whichmod);
  if (whichmod == 0 || whichmod == modulus)
  {
    output = _gather_float4 (input0,_gatherindex1((newindex).x, _const_input0_scalebias));
  }

  else
    if (whichmod == 1)
    {
      output = _gather_float4 (input1,_gatherindex1((newindex).x, _const_input1_scalebias));
    }

    else
      if (whichmod == 2)
      {
        output = _gather_float4 (input2,_gatherindex1((newindex).x, _const_input2_scalebias));
      }

      else
        if (whichmod == 3)
        {
          output = _gather_float4 (input3,_gatherindex1((newindex).x, _const_input3_scalebias));
        }

        else
        {
          output = _gather_float4 (input4,_gatherindex1((newindex).x, _const_input4_scalebias));
        }

}
void combinefloat41d (_stype1 input1[1],
                      float4 _const_input1_scalebias,
                      _stype1 input2[1],
                      float4 _const_input2_scalebias,
                      out float4  output,
                      float  startsecond,
                      float  endsecond,
                      float  maxinput2value,
                      float  inf,
                      float4 _indexof_output)
{
  if ((_indexof_output).x >= startsecond.x)
  {
    float  secondindex = (_indexof_output).x - startsecond;

    if (secondindex.x >= endsecond.x)
    {
      output = inf;
    }

    else
    {
      output = _gather_float4 (input2,_gatherindex1((secondindex).x, _const_input2_scalebias));
    }

  }

  else
  {
    output = _gather_float4 (input1,_gatherindex1(((_indexof_output).x).x, _const_input1_scalebias));
  }

}
layout(binding = 0) uniform u_block_index {
	float4  index;
} u_bl_index;
layout(binding = 1) uniform u_block_modulus {
	float  modulus;
} u_bl_modulus;
vec4 _output_0;//GL_ES_out float


void main () {
	float  newindex;

	calculateDividedIndex1d(
		u_bl_index,
		u_bl_modulus,
		newindex );

	_output_0 = vec4( newindex, 0, 0, 0);
}
