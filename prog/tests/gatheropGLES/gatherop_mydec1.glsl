#version 100
precision mediump float;
precision highp int;

struct u_blockReductionStep
{
    highp vec4 ReductionStep;
};

uniform u_blockReductionStep _37;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_outStreamDim
{
    highp vec4 outStreamDim;
};

uniform u_block_outStreamDim u_bl_outStreamDim;

uniform highp sampler2D _tex_a;
uniform highp sampler2D _tex_b;

highp vec4 _output_0;

void main()
{
    highp vec2 CoordScaling;
    if (_37.ReductionStep.x == 0.0)
    {
        CoordScaling.x = 1.0;
    }
    else
    {
        CoordScaling.x = 2.0;
    }
    if (_37.ReductionStep.y == 0.0)
    {
        CoordScaling.y = 1.0;
    }
    else
    {
        CoordScaling.y = 2.0;
    }
    highp vec4 _gl_FragCoord;
    highp vec2 coordinates = (CoordScaling * _gl_FragCoord.xy) + vec2(0.5);
    highp vec2 param = coordinates / u_bl_StreamDim.StreamDim.xy;
    highp float _115 = texture2D(_tex_a, param).x;
    highp float a = _115;
    coordinates += _37.ReductionStep.xy;
    highp vec2 param_1 = coordinates / u_bl_StreamDim.StreamDim.xy;
    highp float _121 = texture2D(_tex_b, param_1).x;
    highp float b = _121;
    highp float _saved = b;
    b = _saved;
    highp float param_2 = a;
    highp float param_3 = b;
    param_3 = param_2 - 1.0;
    b = param_3;
    _output_0 = vec4(b, 0.0, 0.0, 0.0);
}

