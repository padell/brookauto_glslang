
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Feb  6 2024 10:50:01
////////////////////////////////////////////

#include <brook/brook.hpp>
#include "common.h"

#include "Timer.h"

#include <stdio.h>

static int  retval = 0;
static const char *__cholesky_diagonal_ps20= NULL;
static const char *__cholesky_diagonal_ps2b= NULL;
static const char *__cholesky_diagonal_ps2a= NULL;
static const char *__cholesky_diagonal_ps30= NULL;
static const char *__cholesky_diagonal_ctm= NULL;
static const char *__cholesky_diagonal_fp30= NULL;
static const char *__cholesky_diagonal_fp40= NULL;
static const char *__cholesky_diagonal_arb= NULL;
static const char *__cholesky_diagonal_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __cholesky_diagonal_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"//var sampler2D A[0] : TEXUNIT0 : A[0] 0 : 0 : 1\n"
				"//var float4 __gatherconst_A : C0 : _gatherconst_A : 1 : 1\n"
				"//var sampler2D L[0] : TEXUNIT1 : L[0] 1 : 2 : 1\n"
				"//var float4 __gatherconst_L : C1 : _gatherconst_L : 3 : 1\n"
				"//var float4 _const_output_invscalebias : C2 : _const_output_invscalebias : 5 : 1\n"
				"//var float4 StreamDim : C3 : StreamDim : 7 : 1\n"
				"//var float column : C4 : column : 8 : 1\n"
				"//var float tile : C5 : tile : 9 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 4 : 1\n"
				"//var float2 _tex_output_pos : $vin.TEXCOORD0 : TEXCOORD0 : 6 : 1\n"
				"precision highp float;\n"
				"precision highp int;\n"
				"\n"
				"uniform vec4 StreamDim;\n"
				"\n"
				"\n"
				"uniform vec4 _const_output_invscalebias;\n"
				"\n"
				"\n"
				"uniform vec4 _gatherconst_A;\n"
				"\n"
				"\n"
				"uniform vec4 _gatherconst_L;\n"
				"\n"
				"\n"
				"uniform float column;\n"
				"\n"
				"\n"
				"uniform float tile;\n"
				"\n"
				"\n"
				"uniform sampler2D A[1];\n"
				"uniform sampler2D L[1];\n"
				"\n"
				"varying vec4 TEX0;\n"
				"vec4 _output_0;\n"
				"\n"
				"void main()\n"
				"{\n"
				"     vec4 _indexofoutvar = floor(StreamDim * vec4((TEX0.xy * _const_output_invscalebias.xy) + _const_output_invscalebias.zw, 0.0, 0.0));\n"
				"     vec4 param = _gatherconst_A;\n"
				"     vec4 param_1 = _gatherconst_L;\n"
				"     float param_3 = column;\n"
				"     float param_4 = tile;\n"
				"     vec4 param_5 = _indexofoutvar;\n"
				"     vec2 _250 = param_5.xy;\n"
				"     float _251 = 7.0;\n"
				"     float param_2;\n"
				"    if (param_3 == _250.x)\n"
				"    {\n"
				"        if (_250.x > _250.y)\n"
				"        {\n"
				"            param_2 = 0.0;\n"
				"        }\n"
				"        else\n"
				"        {\n"
				"             float _252 = 0.0;\n"
				"            for ( float _253 = 0.0; _253 < 7.0; _253 += 1.0)\n"
				"            {\n"
				"                _251 = _253 + (7.0 * param_4);\n"
				"                if (_251 > (_250.x - 1.0))\n"
				"                {\n"
				"                    break;\n"
				"                }\n"
				"                 vec2 _255 = vec2(_251, _250.x);\n"
				"                 vec4 _256 = param_1;\n"
				"                 vec2 _374 = (_255 * _256.xy) + _256.zw;\n"
				"                 vec2 _257 = _374;\n"
				"                 float _383;\n"
				"     reconstruct_float(_383, L[0], _257);\n"
				"                 float _254 = _383;\n"
				"                _252 += (_254 * _254);\n"
				"            }\n"
				"             vec2 _258 = vec2(_250.x, _250.x);\n"
				"             vec4 _259 = param_1;\n"
				"             vec2 _390 = (_258 * _259.xy) + _259.zw;\n"
				"             vec2 _260 = _390;\n"
				"             float _399;\n"
				"     reconstruct_float(_399, L[0], _260);\n"
				"            _252 += _399;\n"
				"             float _265;\n"
				"            if (_251 > (_250.x - 1.0))\n"
				"            {\n"
				"                 vec2 _262 = vec2(_250.x, _250.x);\n"
				"                 vec4 _263 = param;\n"
				"                 vec2 _406 = (_262 * _263.xy) + _263.zw;\n"
				"                 vec2 _264 = _406;\n"
				"                 float _415;\n"
				"     reconstruct_float(_415, A[0], _264);\n"
				"                 float _261 = _415;\n"
				"                _265 = sqrt(_261 - _252);\n"
				"            }\n"
				"            else\n"
				"            {\n"
				"                _265 = _252;\n"
				"            }\n"
				"            if (_250.x == _250.y)\n"
				"            {\n"
				"                param_2 = _265;\n"
				"            }\n"
				"        }\n"
				"    }\n"
				"    else\n"
				"    {\n"
				"        if (_250.x > _250.y)\n"
				"        {\n"
				"            param_2 = 0.0;\n"
				"        }\n"
				"        else\n"
				"        {\n"
				"             vec2 _266 = vec2(_250.x, _250.y);\n"
				"             vec4 _267 = param_1;\n"
				"             vec2 _422 = (_266 * _267.xy) + _267.zw;\n"
				"             vec2 _268 = _422;\n"
				"             float _431;\n"
				"     reconstruct_float(_431, L[0], _268);\n"
				"            param_2 = _431;\n"
				"        }\n"
				"    }\n"
				"     float outvar = param_2;\n"
				"     encode_output_float(outvar);\n"
				"}\n"
				"\n"
				" \n"
				"//!!BRCC\n"
				"//narg:5\n"
				"//c:1:A\n"
				"//c:1:L\n"
				"//oi:1:output\n"
				"//c:1:column\n"
				"//c:1:tile\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(1, kGatherConstant_Shape)
				.constant(2, kGatherConstant_Shape)
				.constant(3, kOutputConstant_Indexof)
				.constant(3, StreamDim)
				.constant(4, 0)
				.constant(5, 0)
				.constant(1, kGatherConstant_Shape)
				.constant(2, kGatherConstant_Shape)
				.constant(3, kOutputConstant_Indexof)
				.constant(3, StreamDim)
				.constant(4, 0)
				.constant(5, 0)
				.sampler(1, 0)
				.sampler(2, 0)
				.sampler(1, 0)
				.sampler(2, 0)
				.interpolant(3, kOutputInterpolant_Position)
				.interpolant(3, kOutputInterpolant_Position)
				.output(3, 0)
				.output(3, 0)
			)
		);
	static const void* __cholesky_diagonal_gles = &__cholesky_diagonal_gles_desc;
}

static void  __cholesky_diagonal_cpu_inner(const __BrtArray<__BrtFloat1  > &A,
                                          const __BrtArray<__BrtFloat1  > &L,
                                          __BrtFloat1  &output,
                                          const __BrtFloat1  &column,
                                          const __BrtFloat1  &tile)
{
  __BrtFloat2  index = (indexof(output)).swizzle2(maskX, maskY);
  const __BrtFloat1  maxIter = __BrtFloat1(7.000000f);
  __BrtFloat1  i;
  __BrtFloat1  tiled_i = maxIter;
  __BrtFloat1  sum;
  __BrtFloat1  temp;
  __BrtFloat1  temp2;
  __BrtFloat1  Akk;
  __BrtFloat1  Lkk;

  if (column == index.swizzle1(maskX))
  {
    if (index.swizzle1(maskX) > index.swizzle1(maskY))
      output = __BrtFloat1(0.000000f);
    else
    {
      sum = __BrtFloat1(0.000000f);
      for (i = __BrtFloat1(0.000000f); i < maxIter; i += __BrtFloat1(1.000000f))
      {
        tiled_i = i + maxIter * tile;
        if (tiled_i > index.swizzle1(maskX) - __BrtFloat1(1.000000f))
          break;
        temp = L[index.swizzle1(maskX)][tiled_i];
        sum += temp * temp;
      }

      sum += L[index.swizzle1(maskX)][index.swizzle1(maskX)];
      if (tiled_i > index.swizzle1(maskX) - __BrtFloat1(1.000000f))
      {
        Akk = A[index.swizzle1(maskX)][index.swizzle1(maskX)];
        Lkk = __sqrt_cpu_inner(Akk - sum);
      }

      else
      {
        Lkk = sum;
      }

      if (index.swizzle1(maskX) == index.swizzle1(maskY))
        output = Lkk;
    }

  }

  else
  {
    if (index.swizzle1(maskX) > index.swizzle1(maskY))
      output = __BrtFloat1(0.000000f);
    else
      output = L[index.swizzle1(maskY)][index.swizzle1(maskX)];
  }

}
void  __cholesky_diagonal_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  __BrtArray<__BrtFloat1  > *arg_A = (__BrtArray<__BrtFloat1  > *) args[0];
  __BrtArray<__BrtFloat1  > *arg_L = (__BrtArray<__BrtFloat1  > *) args[1];
  ::brook::StreamInterface *arg_output = (::brook::StreamInterface *) args[2];
  __BrtFloat1 *arg_column = (__BrtFloat1 *) args[3];
  __BrtFloat1 *arg_tile = (__BrtFloat1 *) args[4];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_output((__BrtFloat1 *) __k->FetchElem(arg_output, __brt_idx));
    __cholesky_diagonal_cpu_inner (*arg_A,
                                   *arg_L,
                                   __out_arg_output,
                                   *arg_column,
                                   *arg_tile);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address) = __out_arg_output.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address));
  }
}

extern void  cholesky_diagonal (::brook::stream A,
		::brook::stream L,
		::brook::stream output,
		const float  & __input_column,
		const float  & __input_tile) {
  float  column(__input_column);
  float  tile(__input_tile);
  static const void *__cholesky_diagonal_fp[] = {
     "fp30", __cholesky_diagonal_fp30,
     "fp40", __cholesky_diagonal_fp40,
     "arb", __cholesky_diagonal_arb,
     "glsl", __cholesky_diagonal_glsl,
     "gles", __cholesky_diagonal_gles,
     "ps20", __cholesky_diagonal_ps20,
     "ps2b", __cholesky_diagonal_ps2b,
     "ps2a", __cholesky_diagonal_ps2a,
     "ps30", __cholesky_diagonal_ps30,
     "ctm", __cholesky_diagonal_ctm,
     "cpu", (void *) __cholesky_diagonal_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__cholesky_diagonal_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushGatherStream(A);
  __k->PushGatherStream(L);
  __k->PushOutput(output);
  __k->PushConstant(column);
  __k->PushConstant(tile);
  __k->Map();

}


static const char *__cholesky_line_ps20= NULL;
static const char *__cholesky_line_ps2b= NULL;
static const char *__cholesky_line_ps2a= NULL;
static const char *__cholesky_line_ps30= NULL;
static const char *__cholesky_line_ctm= NULL;
static const char *__cholesky_line_fp30= NULL;
static const char *__cholesky_line_fp40= NULL;
static const char *__cholesky_line_arb= NULL;
static const char *__cholesky_line_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __cholesky_line_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"//var sampler2D A[0] : TEXUNIT0 : A[0] 0 : 0 : 1\n"
				"//var float4 __gatherconst_A : C0 : _gatherconst_A : 1 : 1\n"
				"//var sampler2D L[0] : TEXUNIT1 : L[0] 1 : 2 : 1\n"
				"//var float4 __gatherconst_L : C1 : _gatherconst_L : 3 : 1\n"
				"//var float4 _const_output_invscalebias : C2 : _const_output_invscalebias : 5 : 1\n"
				"//var float4 StreamDim : C3 : StreamDim : 7 : 1\n"
				"//var float column : C4 : column : 8 : 1\n"
				"//var float tile : C5 : tile : 9 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 4 : 1\n"
				"//var float2 _tex_output_pos : $vin.TEXCOORD0 : TEXCOORD0 : 6 : 1\n"
				"precision highp float;\n"
				"precision highp int;\n"
				"\n"
				"uniform vec4 StreamDim;\n"
				"\n"
				"\n"
				"uniform vec4 _const_output_invscalebias;\n"
				"\n"
				"\n"
				"uniform vec4 _gatherconst_A;\n"
				"\n"
				"\n"
				"uniform vec4 _gatherconst_L;\n"
				"\n"
				"\n"
				"uniform float column;\n"
				"\n"
				"\n"
				"uniform float tile;\n"
				"\n"
				"\n"
				"uniform sampler2D A[1];\n"
				"uniform sampler2D L[1];\n"
				"\n"
				"varying vec4 TEX0;\n"
				"vec4 _output_0;\n"
				"\n"
				"void main()\n"
				"{\n"
				"     vec4 _indexofoutvar = floor(StreamDim * vec4((TEX0.xy * _const_output_invscalebias.xy) + _const_output_invscalebias.zw, 0.0, 0.0));\n"
				"     vec4 param = _gatherconst_A;\n"
				"     vec4 param_1 = _gatherconst_L;\n"
				"     float param_3 = column;\n"
				"     float param_4 = tile;\n"
				"     vec4 param_5 = _indexofoutvar;\n"
				"     vec2 _275 = param_5.xy;\n"
				"     float _276 = 7.0;\n"
				"     float param_2;\n"
				"    if (param_3 == _275.x)\n"
				"    {\n"
				"        if (_275.x > _275.y)\n"
				"        {\n"
				"            param_2 = 0.0;\n"
				"        }\n"
				"        else\n"
				"        {\n"
				"             vec2 _278 = vec2(_275.x, _275.x);\n"
				"             vec4 _279 = param_1;\n"
				"             vec2 _424 = (_278 * _279.xy) + _279.zw;\n"
				"             vec2 _280 = _424;\n"
				"             float _433;\n"
				"     reconstruct_float(_433, L[0], _280);\n"
				"             float _277 = _433;\n"
				"            if (_275.x == _275.y)\n"
				"            {\n"
				"                param_2 = _277;\n"
				"            }\n"
				"            else\n"
				"            {\n"
				"                 float _281 = 0.0;\n"
				"                for ( float _282 = 0.0; _282 < 7.0; _282 += 1.0)\n"
				"                {\n"
				"                    _276 = _282 + (7.0 * param_4);\n"
				"                    if (_276 > (_275.x - 1.0))\n"
				"                    {\n"
				"                        break;\n"
				"                    }\n"
				"                     vec2 _284 = vec2(_276, _275.y);\n"
				"                     vec4 _285 = param_1;\n"
				"                     vec2 _440 = (_284 * _285.xy) + _285.zw;\n"
				"                     vec2 _286 = _440;\n"
				"                     float _449;\n"
				"     reconstruct_float(_449, L[0], _286);\n"
				"                     float _283 = _449;\n"
				"                     vec2 _288 = vec2(_276, _275.y);\n"
				"                     vec4 _289 = param_1;\n"
				"                     vec2 _456 = (_288 * _289.xy) + _289.zw;\n"
				"                     vec2 _290 = _456;\n"
				"                     float _465;\n"
				"     reconstruct_float(_465, L[0], _290);\n"
				"                     float _287 = _465;\n"
				"                    _281 += (_283 * _287);\n"
				"                }\n"
				"                 vec2 _291 = vec2(_275.x, _275.y);\n"
				"                 vec4 _292 = param_1;\n"
				"                 vec2 _472 = (_291 * _292.xy) + _292.zw;\n"
				"                 vec2 _293 = _472;\n"
				"                 float _481;\n"
				"     reconstruct_float(_481, L[0], _293);\n"
				"                _281 += _481;\n"
				"                if (_276 > (_275.x - 1.0))\n"
				"                {\n"
				"                     vec2 _295 = vec2(_275.x, _275.x);\n"
				"                     vec4 _296 = param;\n"
				"                     vec2 _488 = (_295 * _296.xy) + _296.zw;\n"
				"                     vec2 _297 = _488;\n"
				"                     float _497;\n"
				"     reconstruct_float(_497, A[0], _297);\n"
				"                     float _294 = _497;\n"
				"                    param_2 = (1.0 / _277) * (_294 - _281);\n"
				"                }\n"
				"                else\n"
				"                {\n"
				"                    param_2 = _281;\n"
				"                }\n"
				"            }\n"
				"        }\n"
				"    }\n"
				"    else\n"
				"    {\n"
				"        if (_275.x > _275.y)\n"
				"        {\n"
				"            param_2 = 0.0;\n"
				"        }\n"
				"        else\n"
				"        {\n"
				"             vec2 _298 = vec2(_275.x, _275.y);\n"
				"             vec4 _299 = param_1;\n"
				"             vec2 _504 = (_298 * _299.xy) + _299.zw;\n"
				"             vec2 _300 = _504;\n"
				"             float _513;\n"
				"     reconstruct_float(_513, L[0], _300);\n"
				"            param_2 = _513;\n"
				"        }\n"
				"    }\n"
				"     float outvar = param_2;\n"
				"     encode_output_float(outvar);\n"
				"}\n"
				"\n"
				" \n"
				"//!!BRCC\n"
				"//narg:5\n"
				"//c:1:A\n"
				"//c:1:L\n"
				"//oi:1:output\n"
				"//c:1:column\n"
				"//c:1:tile\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(1, kGatherConstant_Shape)
				.constant(2, kGatherConstant_Shape)
				.constant(3, kOutputConstant_Indexof)
				.constant(3, StreamDim)
				.constant(4, 0)
				.constant(5, 0)
				.constant(1, kGatherConstant_Shape)
				.constant(2, kGatherConstant_Shape)
				.constant(3, kOutputConstant_Indexof)
				.constant(3, StreamDim)
				.constant(4, 0)
				.constant(5, 0)
				.sampler(1, 0)
				.sampler(2, 0)
				.sampler(1, 0)
				.sampler(2, 0)
				.interpolant(3, kOutputInterpolant_Position)
				.interpolant(3, kOutputInterpolant_Position)
				.output(3, 0)
				.output(3, 0)
			)
		);
	static const void* __cholesky_line_gles = &__cholesky_line_gles_desc;
}

static void  __cholesky_line_cpu_inner(const __BrtArray<__BrtFloat1  > &A,
                                      const __BrtArray<__BrtFloat1  > &L,
                                      __BrtFloat1  &output,
                                      const __BrtFloat1  &column,
                                      const __BrtFloat1  &tile)
{
  __BrtFloat2  index = (indexof(output)).swizzle2(maskX, maskY);
  const __BrtFloat1  maxIter = __BrtFloat1(7.000000f);
  __BrtFloat1  i;
  __BrtFloat1  tiled_i = maxIter;
  __BrtFloat1  sum;
  __BrtFloat1  temp;
  __BrtFloat1  temp2;
  __BrtFloat1  Akk;
  __BrtFloat1  Lkk;

  if (column == index.swizzle1(maskX))
  {
    if (index.swizzle1(maskX) > index.swizzle1(maskY))
      output = __BrtFloat1(0.000000f);
    else
    {
      Lkk = L[index.swizzle1(maskX)][index.swizzle1(maskX)];
      if (index.swizzle1(maskX) == index.swizzle1(maskY))
        output = Lkk;
      else
      {
        sum = __BrtFloat1(0.000000f);
        for (i = __BrtFloat1(0.000000f); i < maxIter; i += __BrtFloat1(1.000000f))
        {
          tiled_i = i + maxIter * tile;
          if (tiled_i > index.swizzle1(maskX) - __BrtFloat1(1.000000f))
            break;
          temp = L[index.swizzle1(maskY)][tiled_i];
          temp2 = L[index.swizzle1(maskY)][tiled_i];
          sum += temp * temp2;
        }

        sum += L[index.swizzle1(maskY)][index.swizzle1(maskX)];
        if (tiled_i > index.swizzle1(maskX) - __BrtFloat1(1.000000f))
        {
          Akk = A[index.swizzle1(maskX)][index.swizzle1(maskX)];
          output = __BrtFloat1(1.000000f) / Lkk * (Akk - sum);
        }

        else
          output = sum;
      }

    }

  }

  else
  {
    if (index.swizzle1(maskX) > index.swizzle1(maskY))
      output = __BrtFloat1(0.000000f);
    else
      output = L[index.swizzle1(maskY)][index.swizzle1(maskX)];
  }

}
void  __cholesky_line_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  __BrtArray<__BrtFloat1  > *arg_A = (__BrtArray<__BrtFloat1  > *) args[0];
  __BrtArray<__BrtFloat1  > *arg_L = (__BrtArray<__BrtFloat1  > *) args[1];
  ::brook::StreamInterface *arg_output = (::brook::StreamInterface *) args[2];
  __BrtFloat1 *arg_column = (__BrtFloat1 *) args[3];
  __BrtFloat1 *arg_tile = (__BrtFloat1 *) args[4];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_output((__BrtFloat1 *) __k->FetchElem(arg_output, __brt_idx));
    __cholesky_line_cpu_inner (*arg_A,
                               *arg_L,
                               __out_arg_output,
                               *arg_column,
                               *arg_tile);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address) = __out_arg_output.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address));
  }
}

extern void  cholesky_line (::brook::stream A,
		::brook::stream L,
		::brook::stream output,
		const float  & __input_column,
		const float  & __input_tile) {
  float  column(__input_column);
  float  tile(__input_tile);
  static const void *__cholesky_line_fp[] = {
     "fp30", __cholesky_line_fp30,
     "fp40", __cholesky_line_fp40,
     "arb", __cholesky_line_arb,
     "glsl", __cholesky_line_glsl,
     "gles", __cholesky_line_gles,
     "ps20", __cholesky_line_ps20,
     "ps2b", __cholesky_line_ps2b,
     "ps2a", __cholesky_line_ps2a,
     "ps30", __cholesky_line_ps30,
     "ctm", __cholesky_line_ctm,
     "cpu", (void *) __cholesky_line_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__cholesky_line_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushGatherStream(A);
  __k->PushGatherStream(L);
  __k->PushOutput(output);
  __k->PushConstant(column);
  __k->PushConstant(tile);
  __k->Map();

}


int  GenerateInputMatrix(unsigned int  w, unsigned int  h, float  *I, float  *M)
{
  unsigned int  index = 0;
  unsigned int  srcindex = 0;
  unsigned int  dstindex = 0;
  unsigned int  k = 0;
  unsigned int  j = 0;
  unsigned int  i = 0;
  float  value = 0.000000f;

  if (!I || !M)
  {
    printf("Null pointer!\n");
    return 0;
  }

  for (i = 0; i < h; i++)
  {
    for (j = 0; j < w; j++)
    {
      if (j <= i)
      {
        value = 0.000000f;
        for (k = 0; k <= j; k++)
        {
          value += (k + 1) * (k + 1);
        }

      }

      index = i * w + j;
      I[index] = value;
    }

  }

  for (i = 0; i < h; i++)
  {
    for (j = 0; j < w; j++)
    {
      srcindex = i * w + j;
      M[srcindex] = I[srcindex];
    }

  }

  return 1;
}

unsigned int  AdjustSampleSize(unsigned int  w, unsigned int  h)
{
  unsigned int  final_dimension = w;

  if (w != h)
  {
    final_dimension = (w > h) ? (w) : (h);
    printf("Only square matrices allowed. Setting dimension = %dx%d\n",final_dimension,final_dimension);
  }

  printf("Adjusted Matrix Dimensions: %dx%d\n",final_dimension,final_dimension);
  return final_dimension;
}

void  CholeskyCPU(unsigned int  width, unsigned int  height, float  *L)
{
  float  dot_product;
  float  sqrt_Akk;
  unsigned int  k = 0;
  unsigned int  i = 0;
  unsigned int  j = 0;
  unsigned int  p = 0;

  for (k = 0; k < width - 1; k++)
  {
    sqrt_Akk = sqrt(L[k * width + k]);
    L[k * width + k] /= sqrt_Akk;
    for (i = k + 1; i < width; i++)
    {
      L[i * width + k] /= sqrt_Akk;
    }

    for (j = k + 1; j < width; j++)
    {
      float  temp = L[j * width + k + 1];

      dot_product = 0.000000f;
      for (p = 0; p <= k; p++)
      {
        dot_product += L[j * width + p] * L[(k + 1) * width + p];
      }

      temp -= dot_product;
      L[j * width + k + 1] = temp;
    }

  }

  L[(height - 1) * width + height - 1] /= sqrt(L[(height - 1) * width + height - 1]);
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width; j++)
    {
      if (j > i)
      {
        L[i * width + j] = 0.000000f;
      }

    }

  }

}

int  main(int  argc, char  **argv)
{
  float  *L = NULL;
  float  *input_matrix = NULL;
  unsigned int  itr = 0;
  unsigned int  j = 0;
  unsigned int  Height;
  unsigned int  Width;
  unsigned int  dim;
  int  i = 0;
  struct infoRec  cmd;
  BRTALIGNED char  buffer[40];

  Setup(0);
  Setup(1);
  Setup(2);
  ParseCommandLine(argc,argv,&cmd);
  srand(cmd.Seed);
  dim = AdjustSampleSize(cmd.Width,cmd.Height);
  Height = dim;
  Width = dim;
  L = allocate_mat_f(Height,Width);
  input_matrix = allocate_mat_f(Height,Width);
  if (!L)
  {
    retval = -1;
    goto cleanup;
  }

  GenerateInputMatrix(Width,Height,input_matrix,L);
  if (cmd.Verbose)
  {
    sprintf(buffer,"Input Matrix M:\n");
    print_mat_f(buffer,"%3.2lf ",(float *) (input_matrix),Height,Width);
  }

  else
    if (!cmd.Quiet)
    {
      printf("Printing the first row, use -v for more.\n");
      sprintf(buffer,"Input Matrix M:\n");
      print_mat_f(buffer,"%3.2lf ",(float *) (L),1,Width);
    }

  {
    ::brook::stream Astream(::brook::getStreamType(( float  *)0), Height , Width,-1);
    ::brook::stream Lstream(::brook::getStreamType(( float  *)0), Height , Width,-1);
    ::brook::stream Lstream2(::brook::getStreamType(( float  *)0), Height , Width,-1);
    float  *empty = (float *) (calloc(Height * Width,sizeof(float ) ));
    int  j;
    unsigned char  flip = 0;

    Start(0);
    for (itr = 0; itr < cmd.Iterations; itr++)
    {
      streamRead(Astream,L);
      streamRead(Lstream,empty);
      streamRead(Lstream2,empty);
      for (i = 0; i < Width; i++)
      {
        const int  maxIter = 7;

        for (j = 0; j <= (i - 1) / maxIter; j++)
        {
          cholesky_diagonal(Astream,(flip) ? (Lstream2) : (Lstream),(flip) ? (Lstream) : (Lstream2),(float ) (i),(float ) (j));
          flip ^= 1;
        }

        for (j = 0; j <= (i - 1) / maxIter; j++)
        {
          cholesky_line(Astream,(flip) ? (Lstream2) : (Lstream),(flip) ? (Lstream) : (Lstream2),(float ) (i),(float ) (j));
          flip ^= 1;
        }

      }

      streamWrite((!flip) ? (Lstream2) : (Lstream),L);
    }

    Stop(0);
  }

  if (cmd.Verbose)
  {
    print_mat_f("Output Matrix L = cholesky(M):\n","%3.2lf ",(float *) (L),Height,Width);
  }

  else
    if (!cmd.Quiet)
    {
      printf("Printing out first row of output L = cholesky(M), use -v for more info.\n");
      print_mat_f("","%3.2lf ",(float *) (L),1,Width);
    }

  if (cmd.Timing)
  {
    double  time = GetElapsedTime(0);
    double  gflop = 0.000000f;

    printf("%-8s%-8s%-16s%-16s\n","Width","Height","Iterations","GPU Total Time");
    printf("%-8d%-8d%-16d%-16lf",dim,dim,cmd.Iterations,time);
    printf("\n\n");
  }

  if (cmd.Verify)
  {
    int  res = 0;

    printf("-e Verify correct output.\n");
    printf("Performing Cholesky Factorization on CPU ... ");
    CholeskyCPU(Width,Height,input_matrix);
    printf("Done\n");
    if (cmd.Verbose)
      print_mat_f("CPU Output Matrix L = cholesky(M):\n","%3.2lf ",(float *) (input_matrix),Height,Width);
    res = compare_mat_f(input_matrix,(float *) (L),Height,Width);
    if (res > 0)
    {
      printf("%s: Failed!\n\n",argv[0]);
      retval = -1;
    }

    else
    {
      printf("Passed! %d wrong\n\n",res);
      retval = 0;
    }

  }

  if (cmd.Performance)
  {
    double  cpu_time = 0.000000f;
    double  gpu_time = 0.000000f;

    printf("-p Compare performance with CPU.\n");
    Start(1);
    for (i = 0; i < cmd.Iterations; ++i)
    {
      CholeskyCPU(Width,Height,input_matrix);
    }

    Stop(1);
    cpu_time = GetElapsedTime(1);
    gpu_time = GetElapsedTime(0);
    printf("%-8s%-8s%-16s%-16s%-16s%-16s\n","Width","Height","Iterations","CPU Total Time","GPU Total Time","Speedup");
    printf("%-8d%-8d%-16d%-16lf%-16lf%-16lf",cmd.Width,cmd.Height,cmd.Iterations,cpu_time,gpu_time,cpu_time / gpu_time);
    printf("\n\n");
  }

cleanup:
  if (L)
  {
    free(L);
  }

  if (input_matrix)
  {
    free(input_matrix);
  }

  if (!cmd.Verify)
  {
    printf("\nPress enter to exit...\n");
    getchar();
  }

  return retval;
}


