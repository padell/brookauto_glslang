#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_A
{
    highp vec4 _gatherconst_A;
};

uniform u_block_gatherconst_A u_bl_gatherconst_A;

struct u_block_gatherconst_L
{
    highp vec4 _gatherconst_L;
};

uniform u_block_gatherconst_L u_bl_gatherconst_L;

struct u_block_column
{
    highp float column;
};

uniform u_block_column u_bl_column;

struct u_block_tile
{
    highp float tile;
};

uniform u_block_tile u_bl_tile;

uniform highp sampler2D A[1];
uniform highp sampler2D L[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_A._gatherconst_A;
    highp vec4 param_1 = u_bl_gatherconst_L._gatherconst_L;
    highp float param_3 = u_bl_column.column;
    highp float param_4 = u_bl_tile.tile;
    highp vec4 param_5 = _indexofoutvar;
    highp vec2 _275 = param_5.xy;
    highp float _276 = 7.0;
    highp float param_2;
    if (param_3 == _275.x)
    {
        if (_275.x > _275.y)
        {
            param_2 = 0.0;
        }
        else
        {
            highp vec2 _278 = vec2(_275.x, _275.x);
            highp vec4 _279 = param_1;
            highp vec2 _424 = (_278 * _279.xy) + _279.zw;
            highp vec2 _280 = _424;
            highp float _433 = texture2D(L[0], _280).x;
            highp float _277 = _433;
            if (_275.x == _275.y)
            {
                param_2 = _277;
            }
            else
            {
                highp float _281 = 0.0;
                for (highp float _282 = 0.0; _282 < 7.0; _282 += 1.0)
                {
                    _276 = _282 + (7.0 * param_4);
                    if (_276 > (_275.x - 1.0))
                    {
                        break;
                    }
                    highp vec2 _284 = vec2(_276, _275.y);
                    highp vec4 _285 = param_1;
                    highp vec2 _440 = (_284 * _285.xy) + _285.zw;
                    highp vec2 _286 = _440;
                    highp float _449 = texture2D(L[0], _286).x;
                    highp float _283 = _449;
                    highp vec2 _288 = vec2(_276, _275.y);
                    highp vec4 _289 = param_1;
                    highp vec2 _456 = (_288 * _289.xy) + _289.zw;
                    highp vec2 _290 = _456;
                    highp float _465 = texture2D(L[0], _290).x;
                    highp float _287 = _465;
                    _281 += (_283 * _287);
                }
                highp vec2 _291 = vec2(_275.x, _275.y);
                highp vec4 _292 = param_1;
                highp vec2 _472 = (_291 * _292.xy) + _292.zw;
                highp vec2 _293 = _472;
                highp float _481 = texture2D(L[0], _293).x;
                _281 += _481;
                if (_276 > (_275.x - 1.0))
                {
                    highp vec2 _295 = vec2(_275.x, _275.x);
                    highp vec4 _296 = param;
                    highp vec2 _488 = (_295 * _296.xy) + _296.zw;
                    highp vec2 _297 = _488;
                    highp float _497 = texture2D(A[0], _297).x;
                    highp float _294 = _497;
                    param_2 = (1.0 / _277) * (_294 - _281);
                }
                else
                {
                    param_2 = _281;
                }
            }
        }
    }
    else
    {
        if (_275.x > _275.y)
        {
            param_2 = 0.0;
        }
        else
        {
            highp vec2 _298 = vec2(_275.x, _275.y);
            highp vec4 _299 = param_1;
            highp vec2 _504 = (_298 * _299.xy) + _299.zw;
            highp vec2 _300 = _504;
            highp float _513 = texture2D(L[0], _300).x;
            param_2 = _513;
        }
    }
    highp float outvar = param_2;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

