#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_A
{
    highp vec4 _gatherconst_A;
};

uniform u_block_gatherconst_A u_bl_gatherconst_A;

struct u_block_gatherconst_L
{
    highp vec4 _gatherconst_L;
};

uniform u_block_gatherconst_L u_bl_gatherconst_L;

struct u_block_column
{
    highp float column;
};

uniform u_block_column u_bl_column;

struct u_block_tile
{
    highp float tile;
};

uniform u_block_tile u_bl_tile;

uniform highp sampler2D A[1];
uniform highp sampler2D L[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_A._gatherconst_A;
    highp vec4 param_1 = u_bl_gatherconst_L._gatherconst_L;
    highp float param_3 = u_bl_column.column;
    highp float param_4 = u_bl_tile.tile;
    highp vec4 param_5 = _indexofoutvar;
    highp vec2 _250 = param_5.xy;
    highp float _251 = 7.0;
    highp float param_2;
    if (param_3 == _250.x)
    {
        if (_250.x > _250.y)
        {
            param_2 = 0.0;
        }
        else
        {
            highp float _252 = 0.0;
            for (highp float _253 = 0.0; _253 < 7.0; _253 += 1.0)
            {
                _251 = _253 + (7.0 * param_4);
                if (_251 > (_250.x - 1.0))
                {
                    break;
                }
                highp vec2 _255 = vec2(_251, _250.x);
                highp vec4 _256 = param_1;
                highp vec2 _374 = (_255 * _256.xy) + _256.zw;
                highp vec2 _257 = _374;
                highp float _383 = texture2D(L[0], _257).x;
                highp float _254 = _383;
                _252 += (_254 * _254);
            }
            highp vec2 _258 = vec2(_250.x, _250.x);
            highp vec4 _259 = param_1;
            highp vec2 _390 = (_258 * _259.xy) + _259.zw;
            highp vec2 _260 = _390;
            highp float _399 = texture2D(L[0], _260).x;
            _252 += _399;
            highp float _265;
            if (_251 > (_250.x - 1.0))
            {
                highp vec2 _262 = vec2(_250.x, _250.x);
                highp vec4 _263 = param;
                highp vec2 _406 = (_262 * _263.xy) + _263.zw;
                highp vec2 _264 = _406;
                highp float _415 = texture2D(A[0], _264).x;
                highp float _261 = _415;
                _265 = sqrt(_261 - _252);
            }
            else
            {
                _265 = _252;
            }
            if (_250.x == _250.y)
            {
                param_2 = _265;
            }
        }
    }
    else
    {
        if (_250.x > _250.y)
        {
            param_2 = 0.0;
        }
        else
        {
            highp vec2 _266 = vec2(_250.x, _250.y);
            highp vec4 _267 = param_1;
            highp vec2 _422 = (_266 * _267.xy) + _267.zw;
            highp vec2 _268 = _422;
            highp float _431 = texture2D(L[0], _268).x;
            param_2 = _431;
        }
    }
    highp float outvar = param_2;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

