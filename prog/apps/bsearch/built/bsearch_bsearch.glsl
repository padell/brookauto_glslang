#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_array
{
    highp vec4 _gatherconst_array;
};

uniform u_block_gatherconst_array u_bl_gatherconst_array;

struct u_block_arraySize
{
    highp float arraySize;
};

uniform u_block_arraySize u_bl_arraySize;

uniform highp sampler2D _tex_searchValue;
uniform highp sampler2D array[1];

highp vec2 _tex_searchValue_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_searchValue_pos;
    highp float _185 = texture2D(_tex_searchValue, param).x;
    highp float searchValue = _185;
    highp float param_1 = searchValue;
    highp vec4 param_2 = u_bl_gatherconst_array._gatherconst_array;
    highp float param_4 = u_bl_arraySize.arraySize;
    highp float _191 = 5.0;
    highp float _212 = floor(param_4 * 0.5);
    highp float _193 = _212;
    highp float _192 = _212;
    highp float param_3 = 0.0;
    highp float _195;
    for (highp float _194 = 0.0; _194 < (_191 - 1.0); _194 += 1.0)
    {
        _193 = floor(_193 * 0.5);
        highp float _196 = _192;
        highp vec4 _197 = param_2;
        highp float _270 = (_196 * _197.x) + _197.z;
        highp float _198 = _270;
        highp float _279 = texture2D(array[0], vec2(_198, 0.0)).x;
        _195 = _279;
        highp float _199 = (param_1 <= _195) ? (-1.0) : 1.0;
        _192 += (_199 * _193);
    }
    highp float _200 = _192;
    highp vec4 _201 = param_2;
    highp float _287 = (_200 * _201.x) + _201.z;
    highp float _202 = _287;
    highp float _296 = texture2D(array[0], vec2(_202, 0.0)).x;
    _195 = _296;
    _192 += ((param_1 <= _195) ? (-1.0) : 1.0);
    highp float _203 = _192;
    highp vec4 _204 = param_2;
    highp float _304 = (_203 * _204.x) + _204.z;
    highp float _205 = _304;
    highp float _313 = texture2D(array[0], vec2(_205, 0.0)).x;
    _195 = _313;
    _192 += ((param_1 <= _195) ? 0.0 : 1.0);
    highp float _206 = _192;
    highp vec4 _207 = param_2;
    highp float _321 = (_206 * _207.x) + _207.z;
    highp float _208 = _321;
    highp float _330 = texture2D(array[0], vec2(_208, 0.0)).x;
    _195 = _330;
    _192 = (param_1 == _195) ? _192 : (-1.0);
    param_3 = _192;
    highp float index = param_3;
    _output_0 = vec4(index, 0.0, 0.0, 0.0);
}

