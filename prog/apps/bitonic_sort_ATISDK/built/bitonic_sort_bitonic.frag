#version 450 core

#if defined(DXPIXELSHADER)
#define fixed float
#define fixed2 vec2
#define fixed3 vec3
#define fixed4 vec4
#endif
#define shortfixed float
#define shortfixed2 vec2
#define shortfixed3 vec3
#define shortfixed4 vec4
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define double2 dvec2
#if defined(DXPIXELSHADER) || !defined(USERECT)
#define _stype1 sampler2D
#define _stype2 sampler2D
#define _stype3 sampler3D
#if defined(SHADERMODEL3)
#define _sample1(s,i) textureLod((s),i,0.0)
#define _sample2(s,i) textureLod((s),i,0.0)
#define _sample3(s,i) textureLod((s),i,0.0)
#else
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif
#else
#define _stype1 sampler2DRect
#define _stype2 sampler2DRect
#define _stype3 sampler3DRect
#define _sample1(s,i) texture(s,i)
#define _sample2(s,i) texture(s,i)
#define _sample3(s,i) texture(s,i)
#endif

#define _FRAGMENTKILL discard
#ifdef USERECT
#define SKIPSCALEBIAS
#endif

#ifdef SKIPSCALEBIAS
float _gatherindex1( float index, vec4 scalebias ) { return (index+scalebias.z); }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return (index+scalebias.zw); }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) vec4(a, 0.0, 0.0, 0.0)
#else
float _gatherindex1( float index, vec4 scalebias ) { return index*scalebias.x+scalebias.z; }
vec2 _gatherindex2( vec2 index, vec4 scalebias ) { return index*scalebias.xy+scalebias.zw; }
vec3 _gatherindex3( vec3 index, vec4 scalebias ) { return index; }
#define _computeindexof(a,b) (b)
#endif

double _fetch_double( _stype1 s, float i ) { return texture(s, vec2(i, 0.0)).x;}
double _fetch_double( _stype2 s, vec2 i ) { return texture(s, i).x;}
double _fetch_double( _stype3 s, vec3 i ) { return texture(s, i).x;}
dvec2 _fetch_double2( _stype1 s, float i ) { dvec2 r; r[0] = texture(s, vec2(i, 0.0)).x; r[1] = texture(s, vec2(i, 0.0)).y; return r;}
dvec2 _fetch_double2( _stype2 s, vec2 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
dvec2 _fetch_double2( _stype3 s, vec3 i ) { dvec2 r; r[0] = texture(s, i).x; r[1] = texture(s, i).y; return r;}
float _fetch_float( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_float( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_float( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_fixed( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_fixed( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_fixed( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_float2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_float2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_float2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec2 _fetch_fixed2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_fixed2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_fixed2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_float3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_float3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_float3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec3 _fetch_fixed3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_fixed3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_fixed3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_float4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_float4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_float4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
vec4 _fetch_fixed4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_fixed4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_fixed4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_int( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_int( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_int( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
float _fetch_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }
float _fetch_unsigned_char( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).x; }
float _fetch_unsigned_char( _stype2 s, vec2 i ) { return _sample2(s,i).x; }
float _fetch_unsigned_char( _stype3 s, vec3 i ) { return _sample3(s,i).x; }
vec2 _fetch_unsigned_char2( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xy; }
vec2 _fetch_unsigned_char2( _stype2 s, vec2 i ) { return _sample2(s,i).xy; }
vec2 _fetch_unsigned_char2( _stype3 s, vec3 i ) { return _sample3(s,i).xy; }
vec3 _fetch_unsigned_char3( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyz; }
vec3 _fetch_unsigned_char3( _stype2 s, vec2 i ) { return _sample2(s,i).xyz; }
vec3 _fetch_unsigned_char3( _stype3 s, vec3 i ) { return _sample3(s,i).xyz; }
vec4 _fetch_unsigned_char4( _stype1 s, float i ) { return _sample1(s,vec2(i, 0.0)).xyzw; }
vec4 _fetch_unsigned_char4( _stype2 s, vec2 i ) { return _sample2(s,i).xyzw; }
vec4 _fetch_unsigned_char4( _stype3 s, vec3 i ) { return _sample3(s,i).xyzw; }


float _gather_float( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_float( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_float( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_float2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_float2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_float2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_float3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_float3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_float3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_float4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_float4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_float4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_shortfixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_shortfixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_shortfixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_shortfixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_shortfixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_shortfixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_shortfixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_shortfixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_shortfixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_shortfixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_shortfixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_shortfixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_fixed( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_fixed( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_fixed( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_fixed2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_fixed2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_fixed2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_fixed3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_fixed3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_fixed3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_fixed4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_fixed4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_fixed4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_char( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_char( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_char( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_char2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_char2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_char2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_char3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_char3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_char3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_char4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_char4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_char4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
float _gather_uchar( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).x; }
float _gather_uchar( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).x; }
float _gather_uchar( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).x; }
vec2 _gather_uchar2( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xy; }
vec2 _gather_uchar2( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xy; }
vec2 _gather_uchar2( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xy; }
vec3 _gather_uchar3( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyz; }
vec3 _gather_uchar3( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyz; }
vec3 _gather_uchar3( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyz; }
vec4 _gather_uchar4( _stype1 s[1], float i ) { return _sample1(s[0],vec2(i, 0.0)).xyzw; }
vec4 _gather_uchar4( _stype2 s[1], vec2 i ) { return _sample2(s[0],i).xyzw; }
vec4 _gather_uchar4( _stype3 s[1], vec3 i ) { return _sample3(s[0],i).xyzw; }
#define uchar float;
#define uchar2 vec2;
#define uchar3 vec3;
#define uchar4 vec4;


float round_ (float  a)
{
  float  diff1;
  float  diff2;
  float  floor_;
  float  ceil_;
  float  ret;

  floor_ = floor(a);
  ceil_ = ceil(a);
  diff1 = ceil_ - a;
  diff2 = a - floor_;
  if (diff1 < diff2)
    ret = ceil_;
  else
    ret = floor_;
  return ret;
}
void bitonic (_stype1 inpvar[1],
                float4 _const_inpvar_scalebias,
                out float  outvar,
                float  stageWidth,
                float  offset,
                float  twoOffset,
                float4 _indexof_output)
{
  float  idx2;
  float  tmp;
  float  tmp2;
  float  sign;
  float  dir;
  float  min;
  float  max;
  float  idx1 = (_indexof_output).x + 0.100000f;

  tmp = round_(mod(idx1,twoOffset));
  tmp2 = (tmp == twoOffset) ? (0.000000f) : (tmp);
  sign = (tmp2 < offset) ? (1.000000f) : (-1.000000f);
  tmp = round_(mod(floor(idx1 / stageWidth),2.000000f));
  tmp2 = (tmp == 2.000000f) ? (0.000000f) : (tmp);
  ;
  dir = (abs(tmp2 - 0.000000f) < 0.000001f) ? (1.000000f) : (-1.000000f);
  idx2 = idx1 + sign * offset;
  min = (_gather_float (inpvar,_gatherindex1((idx1).x, _const_inpvar_scalebias)) < _gather_float (inpvar,_gatherindex1((idx2).x, _const_inpvar_scalebias))) ? (_gather_float (inpvar,_gatherindex1((idx1).x, _const_inpvar_scalebias))) : (_gather_float (inpvar,_gatherindex1((idx2).x, _const_inpvar_scalebias)));
  max = (_gather_float (inpvar,_gatherindex1((idx1).x, _const_inpvar_scalebias)) > _gather_float (inpvar,_gatherindex1((idx2).x, _const_inpvar_scalebias))) ? (_gather_float (inpvar,_gatherindex1((idx1).x, _const_inpvar_scalebias))) : (_gather_float (inpvar,_gatherindex1((idx2).x, _const_inpvar_scalebias)));
  outvar = (abs(sign - dir) < 0.000001f) ? (min) : (max);
}
void bitonic_2D (_stype2 inpvar[1],
                   float4 _const_inpvar_scalebias,
                   out float  outvar,
                   float  stageWidth,
                   float  offset,
                   float  twoOffset,
                   vec2  maxvalue,
                   float4 _indexof_output)
{
  vec2  idx1 = (_indexof_output).xy;
  vec2  idx2;
  float  idx;
  float  tmp;
  float  tmp2;
  float  sign;
  float  dir;
  float  min;
  float  max;
  float  in_idx1;
  float  in_idx2;

  idx = idx1.x + maxvalue.x * idx1.y;
  tmp = round_(mod(idx,twoOffset));
  tmp2 = (tmp == twoOffset) ? (0.000000f) : (tmp);
  sign = (tmp2 < offset) ? (1.000000f) : (-1.000000f);
  tmp = round_(mod(floor((idx + 0.100000f) / stageWidth),2.000000f));
  tmp2 = (tmp == 2.000000f) ? (0.000000f) : (tmp);
  ;
  dir = (abs(tmp2 - 0.000000f) < 0.000001f) ? (1.000000f) : (-1.000000f);
  idx2.x = idx1.x + sign * offset;
  idx2.y = idx1.y + floor((idx2.x + 0.100000f) / maxvalue.x);
  tmp = mod(idx2.x,maxvalue.x);
  idx2.x = (abs(tmp - maxvalue.x) < 1.000000f) ? (0.000000f) : (tmp);
  ;
  if (idx2.x < 0.000000f)
  {
    idx2.x += maxvalue.x;
  }

  in_idx1 = _gather_float (inpvar,_gatherindex2((float2(idx1.x,idx1.y)).xy, _const_inpvar_scalebias));
  in_idx2 = _gather_float (inpvar,_gatherindex2((float2(idx2.x,idx2.y)).xy, _const_inpvar_scalebias));
  min = (in_idx1 < in_idx2) ? (in_idx1) : (in_idx2);
  max = (in_idx1 > in_idx2) ? (in_idx1) : (in_idx2);
  outvar = (abs(sign - dir) < 0.000001f) ? (min) : (max);
}
layout(binding = 0) uniform _stype1 inpvar[1];//GL_ES_in float 
layout(binding = 1) uniform u_block_gatherconst_inpvar {
	 uniform vec4 _gatherconst_inpvar;
} u_bl_gatherconst_inpvar;
vec4 _output_0;//GL_ES_out float
layout(binding = 0) uniform u_block_const_output {
	 uniform float4 _const_output_invscalebias;
} u_bl_const_output_invscalebias;
vec2 _tex_output_pos;
layout(binding = 1) uniform u_block_StreamDim {
	 uniform float4 StreamDim;
} u_bl_StreamDim;
layout(binding = 2) uniform u_block_stageWidth {
float  stageWidth;
} u_bl_stageWidth;
layout(binding = 3) uniform u_block_offset {
float  offset;
} u_bl_offset;
layout(binding = 4) uniform u_block_twoOffset {
float  twoOffset;
} u_bl_twoOffset;


void main () {
	float  outvar;
	vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim*_computeindexof( _tex_output_pos, (vec4( _tex_output_pos*u_bl_const_output_invscalebias._const_output_invscalebias.xy + u_bl_const_output_invscalebias._const_output_invscalebias.zw,0,0))));

	bitonic(
		inpvar, u_bl_gatherconst_inpvar._gatherconst_inpvar,
		outvar,
		u_bl_stageWidth.stageWidth,
		u_bl_offset.offset,
		u_bl_twoOffset.twoOffset,

	_indexofoutvar );

	_output_0 = vec4( outvar, 0, 0, 0);
}
