#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_inpvar
{
    highp vec4 _gatherconst_inpvar;
};

uniform u_block_gatherconst_inpvar u_bl_gatherconst_inpvar;

struct u_block_stageWidth
{
    highp float stageWidth;
};

uniform u_block_stageWidth u_bl_stageWidth;

struct u_block_offset
{
    highp float offset;
};

uniform u_block_offset u_bl_offset;

struct u_block_twoOffset
{
    highp float twoOffset;
};

uniform u_block_twoOffset u_bl_twoOffset;

uniform highp sampler2D inpvar[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_inpvar._gatherconst_inpvar;
    highp float param_2 = u_bl_stageWidth.stageWidth;
    highp float param_3 = u_bl_offset.offset;
    highp float param_4 = u_bl_twoOffset.twoOffset;
    highp vec4 param_5 = _indexofoutvar;
    highp float _280 = param_5.x + 0.100000001490116119384765625;
    highp float _282 = mod(_280, param_4);
    highp float _403 = floor(_282);
    highp float _404 = ceil(_282);
    highp float _405 = _404 - _282;
    highp float _406 = _282 - _403;
    highp float _407;
    if (_405 < _406)
    {
        _407 = _404;
    }
    else
    {
        _407 = _403;
    }
    highp float _408 = _407;
    highp float _281 = _408;
    highp float _283 = (_281 == param_4) ? 0.0 : _281;
    highp float _284 = (_283 < param_3) ? 1.0 : (-1.0);
    highp float _285 = mod(floor(_280 / param_2), 2.0);
    highp float _429 = floor(_285);
    highp float _430 = ceil(_285);
    highp float _431 = _430 - _285;
    highp float _432 = _285 - _429;
    highp float _433;
    if (_431 < _432)
    {
        _433 = _430;
    }
    else
    {
        _433 = _429;
    }
    highp float _434 = _433;
    _281 = _434;
    _283 = (_281 == 2.0) ? 0.0 : _281;
    highp float _286 = (abs(_283 - 0.0) < 9.9999999747524270787835121154785e-07) ? 1.0 : (-1.0);
    highp float _287 = _280 + (_284 * param_3);
    highp float _289 = _280;
    highp vec4 _290 = param;
    highp float _455 = (_289 * _290.x) + _290.z;
    highp float _291 = _455;
    highp float _464 = texture2D(inpvar[0], vec2(_291, 0.0)).x;
    highp float _292 = _287;
    highp vec4 _293 = param;
    highp float _472 = (_292 * _293.x) + _293.z;
    highp float _294 = _472;
    highp float _481 = texture2D(inpvar[0], vec2(_294, 0.0)).x;
    highp float _295;
    if (_464 < _481)
    {
        highp float _296 = _280;
        highp vec4 _297 = param;
        highp float _489 = (_296 * _297.x) + _297.z;
        highp float _298 = _489;
        highp float _498 = texture2D(inpvar[0], vec2(_298, 0.0)).x;
        _295 = _498;
    }
    else
    {
        highp float _299 = _287;
        highp vec4 _300 = param;
        highp float _506 = (_299 * _300.x) + _300.z;
        highp float _301 = _506;
        highp float _515 = texture2D(inpvar[0], vec2(_301, 0.0)).x;
        _295 = _515;
    }
    highp float _288 = _295;
    highp float _303 = _280;
    highp vec4 _304 = param;
    highp float _523 = (_303 * _304.x) + _304.z;
    highp float _305 = _523;
    highp float _532 = texture2D(inpvar[0], vec2(_305, 0.0)).x;
    highp float _306 = _287;
    highp vec4 _307 = param;
    highp float _540 = (_306 * _307.x) + _307.z;
    highp float _308 = _540;
    highp float _549 = texture2D(inpvar[0], vec2(_308, 0.0)).x;
    highp float _309;
    if (_532 > _549)
    {
        highp float _310 = _280;
        highp vec4 _311 = param;
        highp float _557 = (_310 * _311.x) + _311.z;
        highp float _312 = _557;
        highp float _566 = texture2D(inpvar[0], vec2(_312, 0.0)).x;
        _309 = _566;
    }
    else
    {
        highp float _313 = _287;
        highp vec4 _314 = param;
        highp float _574 = (_313 * _314.x) + _314.z;
        highp float _315 = _574;
        highp float _583 = texture2D(inpvar[0], vec2(_315, 0.0)).x;
        _309 = _583;
    }
    highp float _302 = _309;
    highp float param_1 = (abs(_284 - _286) < 9.9999999747524270787835121154785e-07) ? _288 : _302;
    highp float outvar = param_1;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

