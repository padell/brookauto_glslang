#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_inpvar
{
    highp vec4 _gatherconst_inpvar;
};

uniform u_block_gatherconst_inpvar u_bl_gatherconst_inpvar;

struct u_block_stageWidth
{
    highp float stageWidth;
};

uniform u_block_stageWidth u_bl_stageWidth;

struct u_block_offset
{
    highp float offset;
};

uniform u_block_offset u_bl_offset;

struct u_block_twoOffset
{
    highp float twoOffset;
};

uniform u_block_twoOffset u_bl_twoOffset;

struct u_block_maxvalue
{
    highp vec2 maxvalue;
};

uniform u_block_maxvalue u_bl_maxvalue;

uniform highp sampler2D inpvar[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_inpvar._gatherconst_inpvar;
    highp float param_2 = u_bl_stageWidth.stageWidth;
    highp float param_3 = u_bl_offset.offset;
    highp float param_4 = u_bl_twoOffset.twoOffset;
    highp vec2 param_5 = u_bl_maxvalue.maxvalue;
    highp vec4 param_6 = _indexofoutvar;
    highp vec2 _302 = param_6.xy;
    highp float _303 = _302.x + (param_5.x * _302.y);
    highp float _305 = mod(_303, param_4);
    highp float _440 = floor(_305);
    highp float _441 = ceil(_305);
    highp float _442 = _441 - _305;
    highp float _443 = _305 - _440;
    highp float _444;
    if (_442 < _443)
    {
        _444 = _441;
    }
    else
    {
        _444 = _440;
    }
    highp float _445 = _444;
    highp float _304 = _445;
    highp float _306 = (_304 == param_4) ? 0.0 : _304;
    highp float _307 = (_306 < param_3) ? 1.0 : (-1.0);
    highp float _308 = mod(floor((_303 + 0.100000001490116119384765625) / param_2), 2.0);
    highp float _466 = floor(_308);
    highp float _467 = ceil(_308);
    highp float _468 = _467 - _308;
    highp float _469 = _308 - _466;
    highp float _470;
    if (_468 < _469)
    {
        _470 = _467;
    }
    else
    {
        _470 = _466;
    }
    highp float _471 = _470;
    _304 = _471;
    _306 = (_304 == 2.0) ? 0.0 : _304;
    highp float _309 = (abs(_306 - 0.0) < 9.9999999747524270787835121154785e-07) ? 1.0 : (-1.0);
    highp vec2 _310;
    _310.x = _302.x + (_307 * param_3);
    _310.y = _302.y + floor((_310.x + 0.100000001490116119384765625) / param_5.x);
    _304 = mod(_310.x, param_5.x);
    _310.x = (abs(_304 - param_5.x) < 1.0) ? 0.0 : _304;
    if (_310.x < 0.0)
    {
        _310.x += param_5.x;
    }
    highp vec2 _312 = vec2(_302.x, _302.y);
    highp vec4 _313 = param;
    highp vec2 _492 = (_312 * _313.xy) + _313.zw;
    highp vec2 _314 = _492;
    highp float _501 = texture2D(inpvar[0], _314).x;
    highp float _311 = _501;
    highp vec2 _316 = vec2(_310.x, _310.y);
    highp vec4 _317 = param;
    highp vec2 _508 = (_316 * _317.xy) + _317.zw;
    highp vec2 _318 = _508;
    highp float _517 = texture2D(inpvar[0], _318).x;
    highp float _315 = _517;
    highp float _319 = (_311 < _315) ? _311 : _315;
    highp float _320 = (_311 > _315) ? _311 : _315;
    highp float param_1 = (abs(_307 - _309) < 9.9999999747524270787835121154785e-07) ? _319 : _320;
    highp float outvar = param_1;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

