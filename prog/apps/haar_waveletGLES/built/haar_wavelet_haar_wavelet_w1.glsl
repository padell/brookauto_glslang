#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_o_wave_1
{
    highp vec4 _const_o_wave_1_invscalebias;
};

uniform u_block_const_o_wave_1 u_bl_const_o_wave_1_invscalebias;

struct u_block_gatherconst_vec
{
    highp vec4 _gatherconst_vec;
};

uniform u_block_gatherconst_vec u_bl_gatherconst_vec;

struct u_block_isqrt2
{
    highp float isqrt2;
};

uniform u_block_isqrt2 u_bl_isqrt2;

uniform highp sampler2D vec[1];

highp vec2 _tex_o_wave_1_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_o_wave_1_pos * u_bl_const_o_wave_1_invscalebias._const_o_wave_1_invscalebias.xy) + u_bl_const_o_wave_1_invscalebias._const_o_wave_1_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_vec._gatherconst_vec;
    highp float param_2 = u_bl_isqrt2.isqrt2;
    highp vec4 param_3 = _indexofoutvar;
    highp vec2 _147 = param_3.xy;
    highp vec2 _148 = _147;
    _147.x = 2.0 * _147.x;
    _148.x = _147.x + 1.0;
    highp vec2 _149 = vec2(_147.x, _147.y);
    highp vec4 _150 = param;
    highp vec2 _186 = (_149 * _150.xy) + _150.zw;
    highp vec2 _151 = _186;
    highp float _195 = texture2D(vec[0], _151).x;
    highp vec2 _152 = vec2(_148.x, _148.y);
    highp vec4 _153 = param;
    highp vec2 _202 = (_152 * _153.xy) + _153.zw;
    highp vec2 _154 = _202;
    highp float _211 = texture2D(vec[0], _154).x;
    highp float param_1 = (_195 - _211) * param_2;
    highp float o_wave_1 = param_1;
    _output_0 = vec4(o_wave_1, 0.0, 0.0, 0.0);
}

