#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_output
{
    highp vec4 _const_output_invscalebias;
};

uniform u_block_const_output u_bl_const_output_invscalebias;

struct u_block_gatherconst_inpvar
{
    highp vec4 _gatherconst_inpvar;
};

uniform u_block_gatherconst_inpvar u_bl_gatherconst_inpvar;

struct u_block_twotoi
{
    highp float twotoi;
};

uniform u_block_twotoi u_bl_twotoi;

struct u_block_max
{
    highp vec2 _max;
};

uniform u_block_max u_bl_max;

uniform highp sampler2D inpvar[1];

highp vec2 _tex_output_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_output_pos * u_bl_const_output_invscalebias._const_output_invscalebias.xy) + u_bl_const_output_invscalebias._const_output_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_inpvar._gatherconst_inpvar;
    highp float param_2 = u_bl_twotoi.twotoi;
    highp vec2 param_3 = u_bl_max._max;
    highp vec4 param_4 = _indexofoutvar;
    highp vec2 _213 = param_4.xy;
    highp vec2 _214;
    _214.x = _213.x + param_2;
    _214.y = _213.y + floor((0.5 + _214.x) / param_3.x);
    _214.x = floor(mod(floor(_214.x + float(0.5)), param_3.x) + float(0.5));
    if (_214.x <= 0.0)
    {
        _214.x += param_3.x;
    }
    if (_214.x >= param_3.x)
    {
        _214.x = 0.0;
    }
    bool _274 = _214.y >= param_3.y;
    bool _281;
    if (!_274)
    {
        _281 = _214.y < 0.0;
    }
    else
    {
        _281 = _274;
    }
    highp float param_1;
    if (_281)
    {
        highp vec2 _215 = vec2(_213.x, _213.y);
        highp vec4 _216 = param;
        highp vec2 _310 = (_215 * _216.xy) + _216.zw;
        highp vec2 _217 = _310;
        highp float _319 = texture2D(inpvar[0], _217).x;
        param_1 = _319;
    }
    else
    {
        highp vec2 _218 = vec2(_213.x, _213.y);
        highp vec4 _219 = param;
        highp vec2 _326 = (_218 * _219.xy) + _219.zw;
        highp vec2 _220 = _326;
        highp float _335 = texture2D(inpvar[0], _220).x;
        highp vec2 _221 = vec2(_214.x, _214.y);
        highp vec4 _222 = param;
        highp vec2 _342 = (_221 * _222.xy) + _222.zw;
        highp vec2 _223 = _342;
        highp float _351 = texture2D(inpvar[0], _223).x;
        param_1 = _335 + _351;
    }
    highp float outvar = param_1;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

