
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Feb  6 2024 10:50:01
////////////////////////////////////////////

#include <brook/brook.hpp>
#include "common.h"

#include "Timer.h"

#include <stdio.h>

static int  retval = 0;
static const char *__scan_ps20= NULL;
static const char *__scan_ps2b= NULL;
static const char *__scan_ps2a= NULL;
static const char *__scan_ps30= NULL;
static const char *__scan_ctm= NULL;
static const char *__scan_fp30= NULL;
static const char *__scan_fp40= NULL;
static const char *__scan_arb= NULL;
static const char *__scan_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __scan_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"//var sampler2D input[0] : TEXUNIT0 : _input2[0] 0 : 0 : 1\n"
				"//var float4 __gatherconst_input : C0 : _gatherconst_input1 : 1 : 1\n"
				"//var float4 _const_output_invscalebias : C1 : _const_output_invscalebias : 3 : 1\n"
				"//var float4 StreamDim : C2 : StreamDim : 5 : 1\n"
				"//var float twotoi : C3 : twotoi : 6 : 1\n"
				"//var float2 max : C4 : max : 7 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 2 : 1\n"
				"//var float2 _tex_output_pos : $vin.TEXCOORD0 : TEXCOORD0 : 4 : 1\n"
				"precision highp float;\n"
				"precision highp int;\n"
				"\n"
				"uniform vec4 StreamDim;\n"
				"\n"
				"\n"
				"uniform vec4 _const_output_invscalebias;\n"
				"\n"
				"\n"
				"uniform vec4 _gatherconst_inpvar;\n"
				"\n"
				"\n"
				"uniform float twotoi;\n"
				"\n"
				"\n"
				"uniform vec2 _max;\n"
				"\n"
				"\n"
				"uniform sampler2D inpvar[1];\n"
				"\n"
				"varying vec4 TEX0;\n"
				"vec4 _output_0;\n"
				"\n"
				"void main()\n"
				"{\n"
				"     vec4 _indexofoutvar = floor(StreamDim * vec4((TEX0.xy * _const_output_invscalebias.xy) + _const_output_invscalebias.zw, 0.0, 0.0));\n"
				"     vec4 param = _gatherconst_inpvar;\n"
				"     float param_2 = twotoi;\n"
				"     vec2 param_3 = _max;\n"
				"     vec4 param_4 = _indexofoutvar;\n"
				"     vec2 _213 = param_4.xy;\n"
				"     vec2 _214;\n"
				"    _214.x = _213.x + param_2;\n"
				"    _214.y = _213.y + floor((0.5 + _214.x) / param_3.x);\n"
				"    _214.x = floor(mod(floor(_214.x + float(0.5)), param_3.x) + float(0.5));\n"
				"    if (_214.x <= 0.0)\n"
				"    {\n"
				"        _214.x += param_3.x;\n"
				"    }\n"
				"    if (_214.x >= param_3.x)\n"
				"    {\n"
				"        _214.x = 0.0;\n"
				"    }\n"
				"    bool _274 = _214.y >= param_3.y;\n"
				"    bool _281;\n"
				"    if (!_274)\n"
				"    {\n"
				"        _281 = _214.y < 0.0;\n"
				"    }\n"
				"    else\n"
				"    {\n"
				"        _281 = _274;\n"
				"    }\n"
				"     float param_1;\n"
				"    if (_281)\n"
				"    {\n"
				"         vec2 _215 = vec2(_213.x, _213.y);\n"
				"         vec4 _216 = param;\n"
				"         vec2 _310 = (_215 * _216.xy) + _216.zw;\n"
				"         vec2 _217 = _310;\n"
				"         float _319;\n"
				"     reconstruct_float(_319, inpvar[0], _217);\n"
				"        param_1 = _319;\n"
				"    }\n"
				"    else\n"
				"    {\n"
				"         vec2 _218 = vec2(_213.x, _213.y);\n"
				"         vec4 _219 = param;\n"
				"         vec2 _326 = (_218 * _219.xy) + _219.zw;\n"
				"         vec2 _220 = _326;\n"
				"         float _335;\n"
				"     reconstruct_float(_335, inpvar[0], _220);\n"
				"         vec2 _221 = vec2(_214.x, _214.y);\n"
				"         vec4 _222 = param;\n"
				"         vec2 _342 = (_221 * _222.xy) + _222.zw;\n"
				"         vec2 _223 = _342;\n"
				"         float _351;\n"
				"     reconstruct_float(_351, inpvar[0], _223);\n"
				"        param_1 = _335 + _351;\n"
				"    }\n"
				"     float outvar = param_1;\n"
				"     encode_output_float(outvar);\n"
				"}\n"
				"\n"
				" \n"
				"//!!BRCC\n"
				"//narg:4\n"
				"//c:1:input\n"
				"//oi:1:output\n"
				"//c:1:twotoi\n"
				"//c:2:max\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:gatherconst_0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(1, kGatherConstant_Shape)
				.constant(2, kOutputConstant_Indexof)
				.constant(2, StreamDim)
				.constant(3, 0)
				.constant(4, 0)
				.constant(1, kGatherConstant_Shape)
				.constant(2, kOutputConstant_Indexof)
				.constant(2, StreamDim)
				.constant(3, 0)
				.constant(4, 0)
				.sampler(1, 0)
				.sampler(1, 0)
				.interpolant(2, kOutputInterpolant_Position)
				.interpolant(2, kOutputInterpolant_Position)
				.output(2, 0)
				.output(2, 0)
			)
		);
	static const void* __scan_gles = &__scan_gles_desc;
}

static void  __scan_cpu_inner(const __BrtArray<__BrtFloat1  > &input,
                             __BrtFloat1  &output,
                             const __BrtFloat1  &twotoi,
                             const __BrtFloat2  &max)
{
  __BrtFloat2  i = (indexof(output)).swizzle2(maskX, maskY);
  __BrtFloat2  index;

  index.mask1(i.swizzle1(maskX) + twotoi,maskX);
  index.mask1(i.swizzle1(maskY) + __floor_cpu_inner((__BrtFloat1(0.500000f) + index.swizzle1(maskX)) / max.swizzle1(maskX)),maskY);
  index.mask1(__round_cpu_inner(__fmod_cpu_inner(__round_cpu_inner(index.swizzle1(maskX)),max.swizzle1(maskX))),maskX);
  if (index.swizzle1(maskX) <= __BrtFloat1(0.000000f))
  {
    index.mask1(index.swizzle1(maskX) + max.swizzle1(maskX),maskX);
  }

  if (index.swizzle1(maskX) >= max.swizzle1(maskX))
  {
    index.mask1(__BrtFloat1(0.000000f),maskX);
  }

  if (index.swizzle1(maskY) >= max.swizzle1(maskY) || index.swizzle1(maskY) < __BrtFloat1(0.000000f))
  {
    output = input[i.swizzle1(maskY)][i.swizzle1(maskX)];
  }

  else
  {
    output = input[i.swizzle1(maskY)][i.swizzle1(maskX)] + input[index.swizzle1(maskY)][index.swizzle1(maskX)];
  }

}
void  __scan_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  __BrtArray<__BrtFloat1  > *arg_input = (__BrtArray<__BrtFloat1  > *) args[0];
  ::brook::StreamInterface *arg_output = (::brook::StreamInterface *) args[1];
  __BrtFloat1 *arg_twotoi = (__BrtFloat1 *) args[2];
  __BrtFloat2 *arg_max = (__BrtFloat2 *) args[3];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_output((__BrtFloat1 *) __k->FetchElem(arg_output, __brt_idx));
    __scan_cpu_inner (*arg_input,
                      __out_arg_output,
                      *arg_twotoi,
                      *arg_max);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address) = __out_arg_output.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_output.address));
  }
}

extern void  scan (::brook::stream input,
		::brook::stream output,
		const float  & __input_twotoi,
		const float2  & __input_max) {
  float  twotoi(__input_twotoi);
  float2  max(__input_max);
  static const void *__scan_fp[] = {
     "fp30", __scan_fp30,
     "fp40", __scan_fp40,
     "arb", __scan_arb,
     "glsl", __scan_glsl,
     "gles", __scan_gles,
     "ps20", __scan_ps20,
     "ps2b", __scan_ps2b,
     "ps2a", __scan_ps2a,
     "ps30", __scan_ps30,
     "ctm", __scan_ctm,
     "cpu", (void *) __scan_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__scan_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushGatherStream(input);
  __k->PushOutput(output);
  __k->PushConstant(twotoi);
  __k->PushConstant(max);
  __k->Map();

}


void  prefix_sum_cpu(float  *input, float  *output, int  len)
{
  float  temp = 0.000000f;
  int  y;

  for (y = 0; y < len; ++y)
  {
    temp += input[y];
    output[y] = temp;
  }

}

int  main(int  argc, char  **argv)
{
  float  *input = NULL;
  float  *output = NULL;
  unsigned int  Length;
  unsigned int  i;
  unsigned int  Height;
  unsigned int  Width;
  struct infoRec  cmd;

  Setup(0);
  Setup(1);
  ParseCommandLine(argc,argv,&cmd);
  srand(cmd.Seed);
  Width = cmd.Width;
  Height = cmd.Height;
  Length = Width * Height;
  input = allocate_mat_f(Height,Width);
  output = allocate_mat_f(Height,Width);
  if (!input || !output)
  {
    fprintf(stderr,"Error: Memory Allocation.\n");
    retval = -1;
    goto cleanup;
  }

  fill_mat_f((float *) (input),Height,Width,1,RANDOM);
  if (cmd.Verbose)
  {
    print_mat_f("Input Matrix: ","%lf ",(float *) (input),Height,Width);
  }

  else
    if (!cmd.Quiet)
    {
      printf("Printing first row of data, for more use -v\n");
      print_mat_f("Input Matrix: ","%3.2lf ",(float *) (input),Height,Width);
    }

  {
    ::brook::stream si2(::brook::getStreamType(( float  *)0), Height , Width,-1);
    ::brook::stream istream(::brook::getStreamType(( float  *)0), Height , Width,-1);
    ::brook::stream so2(::brook::getStreamType(( float  *)0), Height , Width,-1);
    float2  maxvalue = float2 (0.000000f,0.000000f);
    int  sign = -1;
    unsigned int  logN = 0;
    unsigned int  j = 0;

    Start(0);
    for (i = 0; i < cmd.Iterations; ++i)
    {
      streamRead(istream,input);
      j = 0;
      logN = (unsigned int ) (ceil(log((float ) (Height) * Width) / log(2.000000f)));
      maxvalue.x = (float ) (Width);
      maxvalue.y = (float ) (Height);
      scan(istream,so2,(float ) (sign),maxvalue);
      for (j = 1; j < logN; ++j)
      {
        if (j & 1)
        {
          scan(so2,si2,(float ) (sign) * (1 << j),maxvalue);
        }

        else
        {
          scan(si2,so2,(float ) (sign) * (1 << j),maxvalue);
        }

      }

      if (!(logN % 2) && Height * Width != 1)
      {
        ::brook::stream temp(::brook::getStreamType(( float  *)0), Height , Width,-1);

        temp = si2;
        si2 = so2;
        so2 = temp;
      }

      streamWrite(so2,output);
    }

    Stop(0);
  }

  if (cmd.Verbose)
  {
    print_mat_f("Output Matrix: ","%lf ",(float *) (output),Height,Width);
  }

  else
    if (!cmd.Quiet)
    {
      printf("Printing first row of data, for more use -v\n");
      print_mat_f("Output Matrix: ","%lf ",(float *) (output),1,Width);
    }

  if (cmd.Timing)
  {
    double  time = GetElapsedTime(0);

    printf("%-8s%-8s%-16s%-16s\n","Width","Height","Iterations","GPU Total Time");
    printf("%-8d%-8d%-16d%-16lf",cmd.Width,cmd.Height,cmd.Iterations,time);
    printf("\n\n");
  }

  if (cmd.Verify)
  {
    int  res = 0;
    float  *outputCPU;

    if (outputCPU = allocate_mat_f(Height,Width))
    {
      printf("-e Verify correct output.\n");
      printf("Performing Prefix Sum on CPU ... ");
      prefix_sum_cpu((float *) (input),(float *) (outputCPU),Height * Width);
      printf("Done\n");
      res += compare_mat_f((float *) (output),(float *) (outputCPU),Height,Width);
      if (res)
      {
        printf("%s: Failed!\n\n",argv[0]);
        retval = -1;
      }

      else
      {
        printf("%s: Passed!\n\n",argv[0]);
        retval = 0;
      }

      free(outputCPU);
    }

    else
    {
      retval = -1;
      fprintf(stderr,"Error: Memory Allocation.\n");
    }

  }

  if (cmd.Performance)
  {
    unsigned int  i = 0;
    double  cpu_time = 0.000000f;
    double  gpu_time = 0.000000f;
    float  *outputCPU;

    if (outputCPU = allocate_mat_f(Height,Width))
    {
      printf("-p Compare performance with CPU.\n");
      Start(1);
      for (i = 0; i < cmd.Iterations; i++)
      {
        prefix_sum_cpu((float *) (input),(float *) (outputCPU),Height * Width);
      }

      Stop(1);
      cpu_time = GetElapsedTime(1);
      gpu_time = GetElapsedTime(0);
      printf("%-8s%-8s%-16s%-16s%-16s%-16s\n","Width","Height","Iterations","CPU Total Time","GPU Total Time","Speedup");
      printf("%-8d%-8d%-16d%-16lf%-16lf%-16lf",cmd.Width,cmd.Height,cmd.Iterations,cpu_time,gpu_time,cpu_time / gpu_time);
      printf("\n\n");
      free(outputCPU);
    }

    else
    {
      retval = -1;
      fprintf(stderr,"Error: Memory Allocation.\n");
    }

  }

cleanup:
  if (input)
  {
    free(input);
  }

  if (output)
  {
    free(output);
  }

  if (!cmd.Verify)
  {
    printf("\nPress enter to exit...\n");
    getchar();
  }

  return retval;
}


