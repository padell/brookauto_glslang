#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_img
{
    highp vec4 _gatherconst_img;
};

uniform u_block_gatherconst_img u_bl_gatherconst_img;

struct u_block_gatherconst_mask
{
    highp vec4 _gatherconst_mask;
};

uniform u_block_gatherconst_mask u_bl_gatherconst_mask;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_blockIteratorBias
{
    highp vec4 IteratorBias;
};

uniform u_blockIteratorBias _297;

uniform highp sampler2D img[1];
uniform highp sampler2D mask[1];

highp vec2 it;
highp vec4 _output_0;

void main()
{
    highp vec4 param = u_bl_gatherconst_img._gatherconst_img;
    highp vec4 param_1 = u_bl_gatherconst_mask._gatherconst_mask;
    highp vec2 param_2 = it;
    highp vec2 _298 = vec2(-1.0);
    highp vec2 _299 = vec2(-1.0, 0.0);
    highp vec2 _300 = vec2(-1.0, 1.0);
    highp vec2 _301 = vec2(0.0, -1.0);
    highp vec2 _302 = vec2(0.0);
    highp vec2 _303 = vec2(0.0, 1.0);
    highp vec2 _304 = vec2(1.0, -1.0);
    highp vec2 _305 = vec2(1.0, 0.0);
    highp vec2 _306 = vec2(1.0);
    highp vec2 _307 = param_2 + _298;
    highp vec4 _308 = param;
    highp vec2 _495 = (_307 * _308.xy) + _308.zw;
    highp vec2 _309 = _495;
    highp float _504 = texture2D(img[0], _309).x;
    highp vec2 _310 = _298 + vec2(1.0);
    highp vec4 _311 = param_1;
    highp vec2 _511 = (_310 * _311.xy) + _311.zw;
    highp vec2 _312 = _511;
    highp float _520 = texture2D(mask[0], _312).x;
    highp float param_3 = _504 * _520;
    highp vec2 _313 = param_2 + _299;
    highp vec4 _314 = param;
    highp vec2 _527 = (_313 * _314.xy) + _314.zw;
    highp vec2 _315 = _527;
    highp float _536 = texture2D(img[0], _315).x;
    highp vec2 _316 = _299 + vec2(1.0);
    highp vec4 _317 = param_1;
    highp vec2 _543 = (_316 * _317.xy) + _317.zw;
    highp vec2 _318 = _543;
    highp float _552 = texture2D(mask[0], _318).x;
    param_3 += (_536 * _552);
    highp vec2 _319 = param_2 + _300;
    highp vec4 _320 = param;
    highp vec2 _559 = (_319 * _320.xy) + _320.zw;
    highp vec2 _321 = _559;
    highp float _568 = texture2D(img[0], _321).x;
    highp vec2 _322 = _300 + vec2(1.0);
    highp vec4 _323 = param_1;
    highp vec2 _575 = (_322 * _323.xy) + _323.zw;
    highp vec2 _324 = _575;
    highp float _584 = texture2D(mask[0], _324).x;
    param_3 += (_568 * _584);
    highp vec2 _325 = param_2 + _301;
    highp vec4 _326 = param;
    highp vec2 _591 = (_325 * _326.xy) + _326.zw;
    highp vec2 _327 = _591;
    highp float _600 = texture2D(img[0], _327).x;
    highp vec2 _328 = _301 + vec2(1.0);
    highp vec4 _329 = param_1;
    highp vec2 _607 = (_328 * _329.xy) + _329.zw;
    highp vec2 _330 = _607;
    highp float _616 = texture2D(mask[0], _330).x;
    param_3 += (_600 * _616);
    highp vec2 _331 = param_2 + _302;
    highp vec4 _332 = param;
    highp vec2 _623 = (_331 * _332.xy) + _332.zw;
    highp vec2 _333 = _623;
    highp float _632 = texture2D(img[0], _333).x;
    highp vec2 _334 = _302 + vec2(1.0);
    highp vec4 _335 = param_1;
    highp vec2 _639 = (_334 * _335.xy) + _335.zw;
    highp vec2 _336 = _639;
    highp float _648 = texture2D(mask[0], _336).x;
    param_3 += (_632 * _648);
    highp vec2 _337 = param_2 + _303;
    highp vec4 _338 = param;
    highp vec2 _655 = (_337 * _338.xy) + _338.zw;
    highp vec2 _339 = _655;
    highp float _664 = texture2D(img[0], _339).x;
    highp vec2 _340 = _303 + vec2(1.0);
    highp vec4 _341 = param_1;
    highp vec2 _671 = (_340 * _341.xy) + _341.zw;
    highp vec2 _342 = _671;
    highp float _680 = texture2D(mask[0], _342).x;
    param_3 += (_664 * _680);
    highp vec2 _343 = param_2 + _304;
    highp vec4 _344 = param;
    highp vec2 _687 = (_343 * _344.xy) + _344.zw;
    highp vec2 _345 = _687;
    highp float _696 = texture2D(img[0], _345).x;
    highp vec2 _346 = _304 + vec2(1.0);
    highp vec4 _347 = param_1;
    highp vec2 _703 = (_346 * _347.xy) + _347.zw;
    highp vec2 _348 = _703;
    highp float _712 = texture2D(mask[0], _348).x;
    param_3 += (_696 * _712);
    highp vec2 _349 = param_2 + _305;
    highp vec4 _350 = param;
    highp vec2 _719 = (_349 * _350.xy) + _350.zw;
    highp vec2 _351 = _719;
    highp float _728 = texture2D(img[0], _351).x;
    highp vec2 _352 = _305 + vec2(1.0);
    highp vec4 _353 = param_1;
    highp vec2 _735 = (_352 * _353.xy) + _353.zw;
    highp vec2 _354 = _735;
    highp float _744 = texture2D(mask[0], _354).x;
    param_3 += (_728 * _744);
    highp vec2 _355 = param_2 + _306;
    highp vec4 _356 = param;
    highp vec2 _751 = (_355 * _356.xy) + _356.zw;
    highp vec2 _357 = _751;
    highp float _760 = texture2D(img[0], _357).x;
    highp vec2 _358 = _306 + vec2(1.0);
    highp vec4 _359 = param_1;
    highp vec2 _767 = (_358 * _359.xy) + _359.zw;
    highp vec2 _360 = _767;
    highp float _776 = texture2D(mask[0], _360).x;
    param_3 += (_760 * _776);
    highp float o_img = param_3;
    _output_0 = vec4(o_img, 0.0, 0.0, 0.0);
}

