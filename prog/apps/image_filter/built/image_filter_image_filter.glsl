#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_o_img
{
    highp vec4 _const_o_img_invscalebias;
};

uniform u_block_const_o_img u_bl_const_o_img_invscalebias;

struct u_block_gatherconst_img
{
    highp vec4 _gatherconst_img;
};

uniform u_block_gatherconst_img u_bl_gatherconst_img;

struct u_block_gatherconst_mask
{
    highp vec4 _gatherconst_mask;
};

uniform u_block_gatherconst_mask u_bl_gatherconst_mask;

uniform highp sampler2D img[1];
uniform highp sampler2D mask[1];

highp vec2 _tex_o_img_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_o_img_pos * u_bl_const_o_img_invscalebias._const_o_img_invscalebias.xy) + u_bl_const_o_img_invscalebias._const_o_img_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_img._gatherconst_img;
    highp vec4 param_1 = u_bl_gatherconst_mask._gatherconst_mask;
    highp vec4 param_3 = _indexofoutvar;
    highp vec2 _300 = param_3.xy;
    highp float _301 = _300.x;
    highp float _302 = _300.y;
    highp vec2 _303 = vec2(_301, _302);
    highp vec4 _304 = param;
    highp vec2 _482 = (_303 * _304.xy) + _304.zw;
    highp vec2 _305 = _482;
    highp float _491 = texture2D(img[0], _305).x;
    highp vec2 _306 = vec2(0.0);
    highp vec4 _307 = param_1;
    highp vec2 _498 = (_306 * _307.xy) + _307.zw;
    highp vec2 _308 = _498;
    highp float _507 = texture2D(mask[0], _308).x;
    highp float param_2 = _491 * _507;
    highp vec2 _309 = vec2(_301 + 1.0, _302);
    highp vec4 _310 = param;
    highp vec2 _514 = (_309 * _310.xy) + _310.zw;
    highp vec2 _311 = _514;
    highp float _523 = texture2D(img[0], _311).x;
    highp vec2 _312 = vec2(1.0, 0.0);
    highp vec4 _313 = param_1;
    highp vec2 _530 = (_312 * _313.xy) + _313.zw;
    highp vec2 _314 = _530;
    highp float _539 = texture2D(mask[0], _314).x;
    param_2 += (_523 * _539);
    highp vec2 _315 = vec2(_301 + 2.0, _302);
    highp vec4 _316 = param;
    highp vec2 _546 = (_315 * _316.xy) + _316.zw;
    highp vec2 _317 = _546;
    highp float _555 = texture2D(img[0], _317).x;
    highp vec2 _318 = vec2(2.0, 0.0);
    highp vec4 _319 = param_1;
    highp vec2 _562 = (_318 * _319.xy) + _319.zw;
    highp vec2 _320 = _562;
    highp float _571 = texture2D(mask[0], _320).x;
    param_2 += (_555 * _571);
    highp vec2 _321 = vec2(_301, _302 + 1.0);
    highp vec4 _322 = param;
    highp vec2 _578 = (_321 * _322.xy) + _322.zw;
    highp vec2 _323 = _578;
    highp float _587 = texture2D(img[0], _323).x;
    highp vec2 _324 = vec2(0.0, 1.0);
    highp vec4 _325 = param_1;
    highp vec2 _594 = (_324 * _325.xy) + _325.zw;
    highp vec2 _326 = _594;
    highp float _603 = texture2D(mask[0], _326).x;
    param_2 += (_587 * _603);
    highp vec2 _327 = vec2(_301 + 1.0, _302 + 1.0);
    highp vec4 _328 = param;
    highp vec2 _610 = (_327 * _328.xy) + _328.zw;
    highp vec2 _329 = _610;
    highp float _619 = texture2D(img[0], _329).x;
    highp vec2 _330 = vec2(1.0);
    highp vec4 _331 = param_1;
    highp vec2 _626 = (_330 * _331.xy) + _331.zw;
    highp vec2 _332 = _626;
    highp float _635 = texture2D(mask[0], _332).x;
    param_2 += (_619 * _635);
    highp vec2 _333 = vec2(_301 + 2.0, _302 + 1.0);
    highp vec4 _334 = param;
    highp vec2 _642 = (_333 * _334.xy) + _334.zw;
    highp vec2 _335 = _642;
    highp float _651 = texture2D(img[0], _335).x;
    highp vec2 _336 = vec2(2.0, 1.0);
    highp vec4 _337 = param_1;
    highp vec2 _658 = (_336 * _337.xy) + _337.zw;
    highp vec2 _338 = _658;
    highp float _667 = texture2D(mask[0], _338).x;
    param_2 += (_651 * _667);
    highp vec2 _339 = vec2(_301, _302 + 2.0);
    highp vec4 _340 = param;
    highp vec2 _674 = (_339 * _340.xy) + _340.zw;
    highp vec2 _341 = _674;
    highp float _683 = texture2D(img[0], _341).x;
    highp vec2 _342 = vec2(0.0, 2.0);
    highp vec4 _343 = param_1;
    highp vec2 _690 = (_342 * _343.xy) + _343.zw;
    highp vec2 _344 = _690;
    highp float _699 = texture2D(mask[0], _344).x;
    param_2 += (_683 * _699);
    highp vec2 _345 = vec2(_301 + 1.0, _302 + 2.0);
    highp vec4 _346 = param;
    highp vec2 _706 = (_345 * _346.xy) + _346.zw;
    highp vec2 _347 = _706;
    highp float _715 = texture2D(img[0], _347).x;
    highp vec2 _348 = vec2(1.0, 2.0);
    highp vec4 _349 = param_1;
    highp vec2 _722 = (_348 * _349.xy) + _349.zw;
    highp vec2 _350 = _722;
    highp float _731 = texture2D(mask[0], _350).x;
    param_2 += (_715 * _731);
    highp vec2 _351 = vec2(_301 + 2.0, _302 + 2.0);
    highp vec4 _352 = param;
    highp vec2 _738 = (_351 * _352.xy) + _352.zw;
    highp vec2 _353 = _738;
    highp float _747 = texture2D(img[0], _353).x;
    highp vec2 _354 = vec2(2.0);
    highp vec4 _355 = param_1;
    highp vec2 _754 = (_354 * _355.xy) + _355.zw;
    highp vec2 _356 = _754;
    highp float _763 = texture2D(mask[0], _356).x;
    param_2 += (_747 * _763);
    highp float o_img = param_2;
    _output_0 = vec4(o_img, 0.0, 0.0, 0.0);
}

