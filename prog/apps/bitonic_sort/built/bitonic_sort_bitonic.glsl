#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_inpvar
{
    highp vec4 _gatherconst_inpvar;
};

uniform u_block_gatherconst_inpvar u_bl_gatherconst_inpvar;

struct u_block_stageWidth
{
    highp float stageWidth;
};

uniform u_block_stageWidth u_bl_stageWidth;

struct u_block_offset
{
    highp float offset;
};

uniform u_block_offset u_bl_offset;

struct u_block_twoOffset
{
    highp float twoOffset;
};

uniform u_block_twoOffset u_bl_twoOffset;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_blockIteratorBias
{
    highp vec4 IteratorBias;
};

uniform u_blockIteratorBias _205;

uniform highp sampler2D inpvar[1];

highp float idx1;
highp vec4 _output_0;

void main()
{
    highp float param = idx1;
    highp vec4 param_1 = u_bl_gatherconst_inpvar._gatherconst_inpvar;
    highp float param_3 = u_bl_stageWidth.stageWidth;
    highp float param_4 = u_bl_offset.offset;
    highp float param_5 = u_bl_twoOffset.twoOffset;
    highp float _206 = (mod(param, param_5) < param_4) ? 1.0 : (-1.0);
    highp float _207 = (mod(floor(param / param_3), 2.0) == 0.0) ? 1.0 : (-1.0);
    highp float _208 = param + (_206 * param_4);
    highp float _210 = param;
    highp vec4 _211 = param_1;
    highp float _304 = (_210 * _211.x) + _211.z;
    highp float _212 = _304;
    highp float _313 = texture2D(inpvar[0], vec2(_212, 0.0)).x;
    highp float _213 = _208;
    highp vec4 _214 = param_1;
    highp float _321 = (_213 * _214.x) + _214.z;
    highp float _215 = _321;
    highp float _330 = texture2D(inpvar[0], vec2(_215, 0.0)).x;
    highp float _216;
    if (_313 < _330)
    {
        highp float _217 = param;
        highp vec4 _218 = param_1;
        highp float _338 = (_217 * _218.x) + _218.z;
        highp float _219 = _338;
        highp float _347 = texture2D(inpvar[0], vec2(_219, 0.0)).x;
        _216 = _347;
    }
    else
    {
        highp float _220 = _208;
        highp vec4 _221 = param_1;
        highp float _355 = (_220 * _221.x) + _221.z;
        highp float _222 = _355;
        highp float _364 = texture2D(inpvar[0], vec2(_222, 0.0)).x;
        _216 = _364;
    }
    highp float _209 = _216;
    highp float _224 = param;
    highp vec4 _225 = param_1;
    highp float _372 = (_224 * _225.x) + _225.z;
    highp float _226 = _372;
    highp float _381 = texture2D(inpvar[0], vec2(_226, 0.0)).x;
    highp float _227 = _208;
    highp vec4 _228 = param_1;
    highp float _389 = (_227 * _228.x) + _228.z;
    highp float _229 = _389;
    highp float _398 = texture2D(inpvar[0], vec2(_229, 0.0)).x;
    highp float _230;
    if (_381 > _398)
    {
        highp float _231 = param;
        highp vec4 _232 = param_1;
        highp float _406 = (_231 * _232.x) + _232.z;
        highp float _233 = _406;
        highp float _415 = texture2D(inpvar[0], vec2(_233, 0.0)).x;
        _230 = _415;
    }
    else
    {
        highp float _234 = _208;
        highp vec4 _235 = param_1;
        highp float _423 = (_234 * _235.x) + _235.z;
        highp float _236 = _423;
        highp float _432 = texture2D(inpvar[0], vec2(_236, 0.0)).x;
        _230 = _432;
    }
    highp float _223 = _230;
    highp float param_2 = (_206 == _207) ? _209 : _223;
    highp float outvar = param_2;
    _output_0 = vec4(outvar, 0.0, 0.0, 0.0);
}

