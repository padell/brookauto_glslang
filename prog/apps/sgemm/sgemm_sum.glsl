#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

struct u_block_gatherconst_b
{
    highp vec4 _gatherconst_b;
};

uniform u_block_gatherconst_b u_bl_gatherconst_b;

uniform highp sampler2D _tex_indexx;
uniform highp sampler2D _tex_indexy;
uniform highp sampler2D a[1];
uniform highp sampler2D b[1];

highp vec2 _tex_indexx_pos;
highp vec2 _tex_indexy_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_indexx_pos;
    highp float _150 = texture2D(_tex_indexx, param).x;
    highp float indexx = _150;
    highp vec2 param_1 = _tex_indexy_pos;
    highp float _156 = texture2D(_tex_indexy, param_1).x;
    highp float indexy = _156;
    highp float param_2 = indexx;
    highp float param_3 = indexy;
    highp vec4 param_4 = u_bl_gatherconst_a._gatherconst_a;
    highp vec4 param_5 = u_bl_gatherconst_b._gatherconst_b;
    highp float _162 = 0.0;
    for (highp float _163 = 0.0; _163 < 8.0; _163 += 1.0)
    {
        highp vec2 _164 = vec2(_163, param_3);
        highp vec2 _165 = vec2(param_2, _163);
        highp vec2 _166 = _164;
        highp vec4 _167 = param_4;
        highp vec2 _200 = (_166 * _167.xy) + _167.zw;
        highp vec2 _168 = _200;
        highp float _209 = texture2D(a[0], _168).x;
        highp vec2 _169 = _165;
        highp vec4 _170 = param_5;
        highp vec2 _216 = (_169 * _170.xy) + _170.zw;
        highp vec2 _171 = _216;
        highp float _225 = texture2D(b[0], _171).x;
        _162 += (_209 * _225);
    }
    highp float param_6 = _162;
    highp float c = param_6;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

