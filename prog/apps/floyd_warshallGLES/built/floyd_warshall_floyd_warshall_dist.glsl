#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_outDist
{
    highp vec4 _const_outDist_invscalebias;
};

uniform u_block_const_outDist u_bl_const_outDist_invscalebias;

struct u_block_gatherconst_inpvarDist
{
    highp vec4 _gatherconst_inpvarDist;
};

uniform u_block_gatherconst_inpvarDist u_bl_gatherconst_inpvarDist;

struct u_block_gatherconst_inpvarPos
{
    highp vec4 _gatherconst_inpvarPos;
};

uniform u_block_gatherconst_inpvarPos u_bl_gatherconst_inpvarPos;

struct u_block_step
{
    highp float _step;
};

uniform u_block_step u_bl_step;

uniform highp sampler2D inpvarDist[1];
uniform highp sampler2D inpvarPos[1];

highp vec2 _tex_outDist_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_outDist_pos * u_bl_const_outDist_invscalebias._const_outDist_invscalebias.xy) + u_bl_const_outDist_invscalebias._const_outDist_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_inpvarDist._gatherconst_inpvarDist;
    highp vec4 param_1 = u_bl_gatherconst_inpvarPos._gatherconst_inpvarPos;
    highp float param_3 = u_bl_step._step;
    highp vec4 param_4 = _indexofoutvar;
    highp vec2 _172 = param_4.xy;
    highp vec2 _173 = vec2(_172.x, param_3);
    highp vec2 _174 = vec2(param_3, _172.y);
    highp vec2 _176 = vec2(_172.x, _172.y);
    highp vec4 _177 = param;
    highp vec2 _228 = (_176 * _177.xy) + _177.zw;
    highp vec2 _178 = _228;
    highp float _237 = texture2D(inpvarDist[0], _178).x;
    highp float _175 = _237;
    highp vec2 _180 = vec2(_173.x, _173.y);
    highp vec4 _181 = param;
    highp vec2 _244 = (_180 * _181.xy) + _181.zw;
    highp vec2 _182 = _244;
    highp float _253 = texture2D(inpvarDist[0], _182).x;
    highp vec2 _183 = vec2(_174.x, _174.y);
    highp vec4 _184 = param;
    highp vec2 _260 = (_183 * _184.xy) + _184.zw;
    highp vec2 _185 = _260;
    highp float _269 = texture2D(inpvarDist[0], _185).x;
    highp float _179 = _253 + _269;
    highp float param_2 = (_175 < _179) ? _175 : _179;
    highp float outDist = param_2;
    _output_0 = vec4(outDist, 0.0, 0.0, 0.0);
}

