#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_outPos
{
    highp vec4 _const_outPos_invscalebias;
};

uniform u_block_const_outPos u_bl_const_outPos_invscalebias;

struct u_block_gatherconst_inpvarDist
{
    highp vec4 _gatherconst_inpvarDist;
};

uniform u_block_gatherconst_inpvarDist u_bl_gatherconst_inpvarDist;

struct u_block_gatherconst_inpvarPos
{
    highp vec4 _gatherconst_inpvarPos;
};

uniform u_block_gatherconst_inpvarPos u_bl_gatherconst_inpvarPos;

struct u_block_step
{
    highp float _step;
};

uniform u_block_step u_bl_step;

uniform highp sampler2D inpvarDist[1];
uniform highp sampler2D inpvarPos[1];

highp vec2 _tex_outPos_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_outPos_pos * u_bl_const_outPos_invscalebias._const_outPos_invscalebias.xy) + u_bl_const_outPos_invscalebias._const_outPos_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_inpvarDist._gatherconst_inpvarDist;
    highp vec4 param_1 = u_bl_gatherconst_inpvarPos._gatherconst_inpvarPos;
    highp float param_3 = u_bl_step._step;
    highp vec4 param_4 = _indexofoutvar;
    highp vec2 _196 = param_4.xy;
    highp vec2 _197 = vec2(_196.x, param_3);
    highp vec2 _198 = vec2(param_3, _196.y);
    highp vec2 _200 = vec2(_196.x, _196.y);
    highp vec4 _201 = param;
    highp vec2 _276 = (_200 * _201.xy) + _201.zw;
    highp vec2 _202 = _276;
    highp float _285 = texture2D(inpvarDist[0], _202).x;
    highp float _199 = _285;
    highp vec2 _204 = vec2(_197.x, _197.y);
    highp vec4 _205 = param;
    highp vec2 _292 = (_204 * _205.xy) + _205.zw;
    highp vec2 _206 = _292;
    highp float _301 = texture2D(inpvarDist[0], _206).x;
    highp vec2 _207 = vec2(_198.x, _198.y);
    highp vec4 _208 = param;
    highp vec2 _308 = (_207 * _208.xy) + _208.zw;
    highp vec2 _209 = _308;
    highp float _317 = texture2D(inpvarDist[0], _209).x;
    highp float _203 = _301 + _317;
    highp float _210;
    if (_199 < _203)
    {
        highp vec2 _211 = vec2(_196.x, _196.y);
        highp vec4 _212 = param_1;
        highp vec2 _324 = (_211 * _212.xy) + _212.zw;
        highp vec2 _213 = _324;
        highp float _333 = texture2D(inpvarPos[0], _213).x;
        _210 = _333;
    }
    else
    {
        highp vec2 _214 = vec2(_197.x, _197.y);
        highp vec4 _215 = param_1;
        highp vec2 _340 = (_214 * _215.xy) + _215.zw;
        highp vec2 _216 = _340;
        highp float _349 = texture2D(inpvarPos[0], _216).x;
        _210 = _349;
    }
    highp float param_2 = _210;
    highp float outPos = param_2;
    _output_0 = vec4(outPos, 0.0, 0.0, 0.0);
}

