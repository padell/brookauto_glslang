#version 100
precision mediump float;
precision highp int;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

struct u_block_gatherconst_b
{
    highp vec4 _gatherconst_b;
};

uniform u_block_gatherconst_b u_bl_gatherconst_b;

struct u_block_gatherconst_interm
{
    highp vec4 _gatherconst_interm;
};

uniform u_block_gatherconst_interm u_bl_gatherconst_interm;

struct u_block_tile
{
    highp float tile;
};

uniform u_block_tile u_bl_tile;

uniform highp sampler2D _tex_indexx;
uniform highp sampler2D _tex_indexy;
uniform highp sampler2D a[1];
uniform highp sampler2D b[1];
uniform highp sampler2D interm[1];

highp vec2 _tex_indexx_pos;
highp vec2 _tex_indexy_pos;
highp vec4 _output_0;

void main()
{
    highp vec2 param = _tex_indexx_pos;
    highp float _185 = texture2D(_tex_indexx, param).x;
    highp float indexx = _185;
    highp vec2 param_1 = _tex_indexy_pos;
    highp float _191 = texture2D(_tex_indexy, param_1).x;
    highp float indexy = _191;
    highp float param_2 = indexx;
    highp float param_3 = indexy;
    highp vec4 param_4 = u_bl_gatherconst_a._gatherconst_a;
    highp vec4 param_5 = u_bl_gatherconst_b._gatherconst_b;
    highp vec4 param_6 = u_bl_gatherconst_interm._gatherconst_interm;
    highp float param_7 = u_bl_tile.tile;
    highp float _197 = 0.0;
    highp vec2 _198 = vec2(param_2, param_3);
    for (highp float _199 = 0.0; _199 < 512.0; _199 += 1.0)
    {
        highp vec2 _200 = vec2(_199 + (512.0 * param_7), param_3);
        highp vec2 _201 = vec2(param_2, _199 + (512.0 * param_7));
        highp vec2 _202 = _200;
        highp vec4 _203 = param_4;
        highp vec2 _253 = (_202 * _203.xy) + _203.zw;
        highp vec2 _204 = _253;
        highp float _262 = texture2D(a[0], _204).x;
        highp vec2 _205 = _201;
        highp vec4 _206 = param_5;
        highp vec2 _269 = (_205 * _206.xy) + _206.zw;
        highp vec2 _207 = _269;
        highp float _278 = texture2D(b[0], _207).x;
        _197 += (_262 * _278);
    }
    highp vec2 _208 = _198;
    highp vec4 _209 = param_6;
    highp vec2 _285 = (_208 * _209.xy) + _209.zw;
    highp vec2 _210 = _285;
    highp float _294 = texture2D(interm[0], _210).x;
    highp float param_8 = _197 + _294;
    highp float c = param_8;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

