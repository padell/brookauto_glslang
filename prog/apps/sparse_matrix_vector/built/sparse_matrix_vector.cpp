
////////////////////////////////////////////
// Generated by BRCC v0.1
// BRCC Compiled on: Jul 27 2023 11:15:58
////////////////////////////////////////////

#include <brook/brook.hpp>
#include "common.h"

#include "Timer.h"

static int  retval = 0;
static const char *__gather_ps20= NULL;
static const char *__gather_ps2b= NULL;
static const char *__gather_ps2a= NULL;
static const char *__gather_ps30= NULL;
static const char *__gather_ctm= NULL;
static const char *__gather_fp30= NULL;
static const char *__gather_fp40= NULL;
static const char *__gather_arb= NULL;
static const char *__gather_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __gather_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"// glesf output by Cg compiler\n"
				"// cgc version 3.1.0013, build date Apr 24 2012\n"
				"// command line args: -quiet -DCGC=1 -profile glesf -DGL_ES\n"
				"// source file: /tmp/brookShrC33\n"
				"//vendor NVIDIA Corporation\n"
				"//version 3.1.0.13\n"
				"//profile glesf\n"
				"//program main\n"
				"//semantic main._tex_index : TEXUNIT0\n"
				"//semantic main.x : TEXUNIT1\n"
				"//semantic main.__gatherconst_x : C0\n"
				"//semantic main.__workspace : C1\n"
				"//var sampler2D _tex_index : TEXUNIT0 : _tex_index1 0 : 0 : 1\n"
				"//var sampler2D x[0] : TEXUNIT1 : _x2[0] 1 : 2 : 1\n"
				"//var float4 __gatherconst_x : C0 : _gatherconst_x1 : 3 : 1\n"
				"//var float2 _tex_index_pos : $vin.TEXCOORD0 : TEXCOORD0 : 1 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 4 : 1\n"
				"\n"
				"precision highp float;\n"
				"\n"
				"struct double_struct {\n"
				"    vec2 _x3;\n"
				"};\n"
				"\n"
				"struct double2_struct {\n"
				"    vec4 _xy;\n"
				"};\n"
				"\n"
				"float _output_01;\n"
				"float _TMP1;\n"
				"vec4 _TMP2;\n"
				"vec4 _TMP0;\n"
				"uniform sampler2D _tex_index1;\n"
				"uniform sampler2D _x2[1];\n"
				"uniform vec4 _gatherconst_x1;\n"
				"vec2 _c0017;\n"
				"varying vec4 TEX0;\n"
				"\n"
				" // main procedure, the original name was main\n"
				"void main()\n"
				"{\n"
				"\n"
				"\n"
				"    reconstruct_float(_TMP0.x, _tex_index1, TEX0.xy);\n"
				"    _TMP1 = _TMP0.x*_gatherconst_x1.x + _gatherconst_x1.z;\n"
				"    _c0017 = vec2(_TMP1, 0.00000000E+00);\n"
				"    reconstruct_float(_TMP2.x, _x2[0], _c0017);\n"
				"    _output_01 = _TMP2.x;\n"
				"    encode_output_float( _TMP2.x);\n"
				"} //  \n"
				"//!!BRCC\n"
				"//narg:3\n"
				"//s:1:index\n"
				"//c:1:x\n"
				"//o:1:result\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.constant(2, kGatherConstant_Shape)
				.sampler(1, 0)
				.sampler(2, 0)
				.interpolant(1, kStreamInterpolant_Position)
				.output(3, 0)
			)
		);
	static const void* __gather_gles = &__gather_gles_desc;
}

static void  __gather_cpu_inner(const __BrtFloat1  &index,
                               const __BrtArray<__BrtFloat1  > &x,
                               __BrtFloat1  &result)
{
  result = x[index];
}
void  __gather_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_index = (::brook::StreamInterface *) args[0];
  __BrtArray<__BrtFloat1  > *arg_x = (__BrtArray<__BrtFloat1  > *) args[1];
  ::brook::StreamInterface *arg_result = (::brook::StreamInterface *) args[2];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_result((__BrtFloat1 *) __k->FetchElem(arg_result, __brt_idx));
    __gather_cpu_inner (Addressable <__BrtFloat1 >((__BrtFloat1 *) __k->FetchElem(arg_index, __brt_idx)),
                        *arg_x,
                        __out_arg_result);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address) = __out_arg_result.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_result.address));
  }
}

extern void  gather (::brook::stream index,
		::brook::stream x,
		::brook::stream result) {
  static const void *__gather_fp[] = {
     "fp30", __gather_fp30,
     "fp40", __gather_fp40,
     "arb", __gather_arb,
     "glsl", __gather_glsl,
     "gles", __gather_gles,
     "ps20", __gather_ps20,
     "ps2b", __gather_ps2b,
     "ps2a", __gather_ps2a,
     "ps30", __gather_ps30,
     "ctm", __gather_ctm,
     "cpu", (void *) __gather_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__gather_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushStream(index);
  __k->PushGatherStream(x);
  __k->PushOutput(result);
  __k->Map();

}


static const char *__mult_ps20= NULL;
static const char *__mult_ps2b= NULL;
static const char *__mult_ps2a= NULL;
static const char *__mult_ps30= NULL;
static const char *__mult_ctm= NULL;
static const char *__mult_fp30= NULL;
static const char *__mult_fp40= NULL;
static const char *__mult_arb= NULL;
static const char *__mult_glsl= NULL;

namespace {
	using namespace ::brook::desc;
	static const gpu_kernel_desc __mult_gles_desc = gpu_kernel_desc()
		.technique( gpu_technique_desc()
			.pass( gpu_pass_desc(
				"// glesf output by Cg compiler\n"
				"// cgc version 3.1.0013, build date Apr 24 2012\n"
				"// command line args: -quiet -DCGC=1 -profile glesf -DGL_ES\n"
				"// source file: /tmp/brook5HamT1\n"
				"//vendor NVIDIA Corporation\n"
				"//version 3.1.0.13\n"
				"//profile glesf\n"
				"//program main\n"
				"//semantic main._tex_a : TEXUNIT0\n"
				"//semantic main._tex_b : TEXUNIT1\n"
				"//semantic main.__workspace : C0\n"
				"//var sampler2D _tex_a : TEXUNIT0 : _tex_a1 0 : 0 : 1\n"
				"//var sampler2D _tex_b : TEXUNIT1 : _tex_b1 1 : 2 : 1\n"
				"//var float2 _tex_a_pos : $vin.TEXCOORD0 : TEXCOORD0 : 1 : 1\n"
				"//var float2 _tex_b_pos : $vin.TEXCOORD1 : TEXCOORD1 : 3 : 1\n"
				"//var float __output_0 : $vout.COLOR0 : COL : 4 : 1\n"
				"\n"
				"precision highp float;\n"
				"\n"
				"struct double_struct {\n"
				"    vec2 _x;\n"
				"};\n"
				"\n"
				"struct double2_struct {\n"
				"    vec4 _xy;\n"
				"};\n"
				"\n"
				"float _output_01;\n"
				"vec4 _TMP0;\n"
				"uniform sampler2D _tex_a1;\n"
				"uniform sampler2D _tex_b1;\n"
				"float _c0012;\n"
				"varying vec4 TEX0;\n"
				"varying vec4 TEX1;\n"
				"\n"
				" // main procedure, the original name was main\n"
				"void main()\n"
				"{\n"
				"\n"
				"    float _a1;\n"
				"\n"
				"    reconstruct_float(_TMP0.x, _tex_a1, TEX0.xy);\n"
				"    _a1 = _TMP0.x;\n"
				"    reconstruct_float(_TMP0.x, _tex_b1, TEX1.xy);\n"
				"    _c0012 = _a1*_TMP0.x;\n"
				"    _output_01 = _c0012;\n"
				"    encode_output_float( _c0012);\n"
				"} //  \n"
				"//!!BRCC\n"
				"//narg:3\n"
				"//s:1:a\n"
				"//s:1:b\n"
				"//o:1:c\n"
				"//workspace:1024\n"
				"//!!multipleOutputInfo:0:1:\n"
				"//!!fullAddressTrans:0:\n"
				"//!!reductionFactor:0:\n"
				"")
				.sampler(1, 0)
				.sampler(2, 0)
				.interpolant(1, kStreamInterpolant_Position)
				.interpolant(2, kStreamInterpolant_Position)
				.output(3, 0)
			)
		);
	static const void* __mult_gles = &__mult_gles_desc;
}

static void  __mult_cpu_inner(const __BrtFloat1  &a,
                             const __BrtFloat1  &b,
                             __BrtFloat1  &c)
{
  c = a * b;
}
void  __mult_cpu(::brook::Kernel *__k, const std::vector<void *>&args, int __brt_idxstart, int __brt_idxend, bool __brt_isreduce)
{
  ::brook::StreamInterface *arg_a = (::brook::StreamInterface *) args[0];
  ::brook::StreamInterface *arg_b = (::brook::StreamInterface *) args[1];
  ::brook::StreamInterface *arg_c = (::brook::StreamInterface *) args[2];
  
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic, 16) if(!__brt_isreduce)
#endif
  for(int __brt_idx=__brt_idxstart; __brt_idx<__brt_idxend; __brt_idx++) {
    Addressable <__BrtFloat1  > __out_arg_c((__BrtFloat1 *) __k->FetchElem(arg_c, __brt_idx));
    __mult_cpu_inner (Addressable <__BrtFloat1 >((__BrtFloat1 *) __k->FetchElem(arg_a, __brt_idx)),
                      Addressable <__BrtFloat1 >((__BrtFloat1 *) __k->FetchElem(arg_b, __brt_idx)),
                      __out_arg_c);
    *reinterpret_cast<__BrtFloat1 *>(__out_arg_c.address) = __out_arg_c.castToArg(*reinterpret_cast<__BrtFloat1 *>(__out_arg_c.address));
  }
}

extern void  mult (::brook::stream a,
		::brook::stream b,
		::brook::stream c) {
  static const void *__mult_fp[] = {
     "fp30", __mult_fp30,
     "fp40", __mult_fp40,
     "arb", __mult_arb,
     "glsl", __mult_glsl,
     "gles", __mult_gles,
     "ps20", __mult_ps20,
     "ps2b", __mult_ps2b,
     "ps2a", __mult_ps2a,
     "ps30", __mult_ps30,
     "ctm", __mult_ctm,
     "cpu", (void *) __mult_cpu,
     NULL, NULL };
  static BRTTLS ::brook::kernel* __pk;
  if(!__pk) __pk = new ::brook::kernel;
  __pk->initialize(__mult_fp);
  ::brook::kernel& __k = *__pk;

  __k->PushStream(a);
  __k->PushStream(b);
  __k->PushOutput(c);
  __k->Map();

}


