#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_c
{
    highp vec4 _const_c_invscalebias;
};

uniform u_block_const_c u_bl_const_c_invscalebias;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

struct u_block_gatherconst_b
{
    highp vec4 _gatherconst_b;
};

uniform u_block_gatherconst_b u_bl_gatherconst_b;

struct u_block_gatherconst_interm
{
    highp vec4 _gatherconst_interm;
};

uniform u_block_gatherconst_interm u_bl_gatherconst_interm;

struct u_block_tile
{
    highp float tile;
};

uniform u_block_tile u_bl_tile;

struct u_block_tile_size
{
    highp float tile_size;
};

uniform u_block_tile_size u_bl_tile_size;

uniform highp sampler2D a[1];
uniform highp sampler2D b[1];
uniform highp sampler2D interm[1];

highp vec2 _tex_c_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_c_pos * u_bl_const_c_invscalebias._const_c_invscalebias.xy) + u_bl_const_c_invscalebias._const_c_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_a._gatherconst_a;
    highp vec4 param_1 = u_bl_gatherconst_b._gatherconst_b;
    highp vec4 param_2 = u_bl_gatherconst_interm._gatherconst_interm;
    highp float param_3 = u_bl_tile.tile;
    highp float param_4 = u_bl_tile_size.tile_size;
    highp vec4 param_6 = _indexofoutvar;
    highp float _190 = 0.0;
    highp vec2 _191 = param_6.xy;
    for (highp float _192 = 0.0; _192 < 16.0; _192 += 1.0)
    {
        if (_192 >= param_4)
        {
            break;
        }
        highp vec2 _193 = vec2(_192 + (param_4 * param_3), _191.y);
        highp vec2 _194 = _193;
        highp vec4 _195 = param;
        highp vec2 _245 = (_194 * _195.xy) + _195.zw;
        highp vec2 _196 = _245;
        highp float _254 = texture2D(a[0], _196).x;
        highp vec2 _197 = _193;
        highp vec4 _198 = param_1;
        highp vec2 _261 = (_197 * _198.xy) + _198.zw;
        highp vec2 _199 = _261;
        highp float _270 = texture2D(b[0], _199).x;
        _190 += (_254 * _270);
    }
    highp vec2 _200 = _191;
    highp vec4 _201 = param_2;
    highp vec2 _277 = (_200 * _201.xy) + _201.zw;
    highp vec2 _202 = _277;
    highp float _286 = texture2D(interm[0], _202).x;
    highp float param_5 = _190 + _286;
    highp float c = param_5;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

