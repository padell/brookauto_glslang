#version 100
precision mediump float;
precision highp int;

struct u_block_StreamDim
{
    highp vec4 StreamDim;
};

uniform u_block_StreamDim u_bl_StreamDim;

struct u_block_const_c
{
    highp vec4 _const_c_invscalebias;
};

uniform u_block_const_c u_bl_const_c_invscalebias;

struct u_block_gatherconst_a
{
    highp vec4 _gatherconst_a;
};

uniform u_block_gatherconst_a u_bl_gatherconst_a;

struct u_block_gatherconst_b
{
    highp vec4 _gatherconst_b;
};

uniform u_block_gatherconst_b u_bl_gatherconst_b;

struct u_block_gatherconst_interm
{
    highp vec4 _gatherconst_interm;
};

uniform u_block_gatherconst_interm u_bl_gatherconst_interm;

struct u_block_tile
{
    highp float tile;
};

uniform u_block_tile u_bl_tile;

struct u_block_tile_size
{
    highp float tile_size;
};

uniform u_block_tile_size u_bl_tile_size;

uniform highp sampler2D a[1];
uniform highp sampler2D b[1];
uniform highp sampler2D interm[1];

highp vec2 _tex_c_pos;
highp vec4 _output_0;

void main()
{
    highp vec4 _indexofoutvar = floor(u_bl_StreamDim.StreamDim * vec4((_tex_c_pos * u_bl_const_c_invscalebias._const_c_invscalebias.xy) + u_bl_const_c_invscalebias._const_c_invscalebias.zw, 0.0, 0.0));
    highp vec4 param = u_bl_gatherconst_a._gatherconst_a;
    highp vec4 param_1 = u_bl_gatherconst_b._gatherconst_b;
    highp vec4 param_2 = u_bl_gatherconst_interm._gatherconst_interm;
    highp float param_3 = u_bl_tile.tile;
    highp float param_4 = u_bl_tile_size.tile_size;
    highp vec4 param_6 = _indexofoutvar;
    highp float _199 = 0.0;
    highp vec2 _200 = param_6.xy;
    for (highp float _201 = 0.0; _201 < 16.0; _201 += 1.0)
    {
        if (_201 >= param_4)
        {
            break;
        }
        highp vec2 _202 = vec2(_201 + (param_4 * param_3), _200.y);
        highp vec2 _203 = vec2(_200.x, _201 + (param_4 * param_3));
        highp vec2 _204 = _202;
        highp vec4 _205 = param;
        highp vec2 _263 = (_204 * _205.xy) + _205.zw;
        highp vec2 _206 = _263;
        highp float _272 = texture2D(a[0], _206).x;
        highp vec2 _207 = _203;
        highp vec4 _208 = param_1;
        highp vec2 _279 = (_207 * _208.xy) + _208.zw;
        highp vec2 _209 = _279;
        highp float _288 = texture2D(b[0], _209).x;
        _199 += (_272 * _288);
    }
    highp vec2 _210 = _200;
    highp vec4 _211 = param_2;
    highp vec2 _295 = (_210 * _211.xy) + _211.zw;
    highp vec2 _212 = _295;
    highp float _304 = texture2D(interm[0], _212).x;
    highp float param_5 = _199 + _304;
    highp float c = param_5;
    _output_0 = vec4(c, 0.0, 0.0, 0.0);
}

